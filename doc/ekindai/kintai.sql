-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2015 年 9 朁E12 日 17:51
-- サーバのバージョン： 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kintai`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `m0001_symst`
--

CREATE TABLE IF NOT EXISTS `m0001_symst` (
  `SHAINCD` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '社員ｺｰﾄﾞ',
  `SHAINNM` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '社員氏名',
  `SHAINKN` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '社員カナ',
  `SHAINKBN` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '社員区分',
  `BUMONCD` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '部門ｺｰﾄﾞ',
  `PWD` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ﾊﾟｽﾜｰﾄﾞ',
  `KENGEN` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '権限',
  `TEKIYOUKBN` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '適用区分',
  `SHOUNINSHACD` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認者',
  `SHOUNINSHANM` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認者',
  `SHOUNINSHACD1` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '代理承認者',
  `SHOUNINSHANM1` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '代理承認者',
  `BIKOU` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '備考'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='社員マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m0002_bmmst`
--

CREATE TABLE IF NOT EXISTS `m0002_bmmst` (
  `BUMONCD` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `BUMONNM` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='部門マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m0003_projectmst`
--

CREATE TABLE IF NOT EXISTS `m0003_projectmst` (
  `PJNO` varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PJNM` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ﾌﾟﾛｼﾞｪｸﾄマスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `mousikomi`
--

CREATE TABLE IF NOT EXISTS `mousikomi` (
  `NO` int(11) NOT NULL COMMENT '申込み番号',
  `MOUSIKOMI_CD` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '申し込みコード',
  `MOUSIKOMI_NM` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '申し込み内容',
  `COMPANY_NM` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '会社名',
  `URL` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ホームページ',
  `TANTOUSHA_NM` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '担当者名',
  `EMAIL` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Eメール',
  `TEL` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '電話',
  `ZIP_CD` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '住所コード',
  `ADDRESS` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '住所',
  `BIKOU` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '備考（問い合わせ内容）',
  `TIME_STAMP` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '作成時刻'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='申込み';

-- --------------------------------------------------------

--
-- テーブルの構造 `wsm0001`
--

CREATE TABLE IF NOT EXISTS `wsm0001` (
  `TEKIYOUKBN` tinyint(4) NOT NULL COMMENT '適用区分',
  `NENDO` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '年度',
  `NENGETU` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '年月',
  `NENGAPI` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '年月日',
  `YOUBI` tinyint(4) NOT NULL COMMENT '曜日',
  `KYUUJITUFLG` tinyint(4) NOT NULL COMMENT '休日フラグ',
  `CMT` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'コメント'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='カレンダー';

-- --------------------------------------------------------

--
-- テーブルの構造 `wsm0002`
--

CREATE TABLE IF NOT EXISTS `wsm0002` (
  `TEKIYOUKBN` tinyint(4) NOT NULL COMMENT '適用区分',
  `TIMECD` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '時間コード',
  `TEKIYOUNM` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '適用名称',
  `TIMENM` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '時間名称',
  `STIME` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '開始時間',
  `ETIME` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '終了時間',
  `KIKAN` float DEFAULT '0' COMMENT '期間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='勤務条件';

-- --------------------------------------------------------

--
-- テーブルの構造 `wst0006`
--

CREATE TABLE IF NOT EXISTS `wst0006` (
  `SHAINCD` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '社員コード',
  `SINSEIDATE` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '申請日',
  `SINSEICD` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '申請種別　1：定期券・交通費　2：費用精算　3:仮払い',
  `SEQ` bigint(20) NOT NULL COMMENT '行',
  `NENGETU` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '年月',
  `ROOT` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他',
  `EKIFROM` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '出発駅',
  `EKITO` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '到着駅',
  `HASSEIDATE` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '費用発生日',
  `KOUTUUHI` float DEFAULT '0' COMMENT '交通費（円）',
  `SHUKUHAKUHI` float DEFAULT '0' COMMENT '宿泊費（円）',
  `KOUSAIHI` float DEFAULT '0' COMMENT '交際費（円）',
  `SONOTAHI` float DEFAULT '0' COMMENT 'その他の費用（円）',
  `IKISAKI` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '行先・プロジェクト名',
  `UTIWAKE` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '使用目的・内訳・事由',
  `BIKOU` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '備考',
  `UPDATEDATE` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新日',
  `SHOUNINSTATUS` tinyint(4) DEFAULT '0' COMMENT '承認状態 2:申請　3:承認　4:却下',
  `SHOUNINSHACD` varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認者コード',
  `SHOUNINSHANM` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認者氏名',
  `SHOUNINDATE` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='各種申請交通費・費用精算・仮払い';

-- --------------------------------------------------------

--
-- テーブルの構造 `wst0007`
--

CREATE TABLE IF NOT EXISTS `wst0007` (
  `NENDO` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '年度',
  `NENGETU` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '年月',
  `NENGAPI` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '年月日',
  `SHAINCD` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '社員コード',
  `YOUBI` tinyint(4) NOT NULL COMMENT '曜日',
  `CMT` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'コメント',
  `MAILTO` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'メール送信先',
  `BIKOU` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '備考',
  `UPDATEDATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新日',
  `SHOUNINSTATUS` tinyint(4) DEFAULT '0' COMMENT '承認状態 2:申請　3:承認　4:却下',
  `SHOUNINSHACD` varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認者コード',
  `SHOUNINSHANM` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認者氏名',
  `SHOUNINDATE` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認日',
  `SHOUNINCMT` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '承認者コメント'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='週報';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m0001_symst`
--
ALTER TABLE `m0001_symst`
 ADD PRIMARY KEY (`SHAINCD`);

--
-- Indexes for table `m0002_bmmst`
--
ALTER TABLE `m0002_bmmst`
 ADD PRIMARY KEY (`BUMONCD`);

--
-- Indexes for table `m0003_projectmst`
--
ALTER TABLE `m0003_projectmst`
 ADD PRIMARY KEY (`PJNO`);

--
-- Indexes for table `mousikomi`
--
ALTER TABLE `mousikomi`
 ADD PRIMARY KEY (`NO`);

--
-- Indexes for table `wsm0001`
--
ALTER TABLE `wsm0001`
 ADD PRIMARY KEY (`TEKIYOUKBN`,`NENGAPI`);

--
-- Indexes for table `wsm0002`
--
ALTER TABLE `wsm0002`
 ADD PRIMARY KEY (`TEKIYOUKBN`,`TIMECD`);

--
-- Indexes for table `wst0006`
--
ALTER TABLE `wst0006`
 ADD PRIMARY KEY (`SHAINCD`,`SINSEIDATE`,`SINSEICD`,`SEQ`);

--
-- Indexes for table `wst0007`
--
ALTER TABLE `wst0007`
 ADD PRIMARY KEY (`NENGAPI`,`SHAINCD`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
