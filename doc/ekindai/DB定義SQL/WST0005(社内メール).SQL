--社内メールテーブル
DROP TABLE symmetrix.WST0005;
CREATE TABLE symmetrix.WST0005(
SHAINCD							VARCHAR(40) 		NOT NULL,	--送信者コード
SEQ									BIGINT			NOT NULL,
SHAINNM							VARCHAR(60),					--送信者氏名
BUMONCD							VARCHAR(5),						--送信者部門コード
SENDTIME						VARCHAR(20),					--送信時間
READTIME						VARCHAR(20),					--開封時間
TOSHAINCD						VARCHAR(40) 			NOT NULL,	--宛先社員コード
TOSHAINNM						VARCHAR(60) 		NOT NULL,	--宛先表示名
TOBUMONCD						VARCHAR(5), 						--宛先部門コード
TITLE							VARCHAR(60),				--タイトル
DETAIL							VARCHAR(600),					--内容
WARNINGFLG					TINYINT DEFAULT 0,					--注目フラグ
READFLG							TINYINT DEFAULT 0,				--未読フラグ
DELETEFLGS					TINYINT DEFAULT 0,					--送信削除フラグ
DELETEFLGR					TINYINT DEFAULT 0,					--受信削除フラグ
PRIMARY KEY(SHAINCD,SEQ)
);
