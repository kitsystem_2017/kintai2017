-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2015 年 9 朁E12 日 15:14
-- サーバのバージョン： 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kintai`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `m0003_projectmst`
--

CREATE TABLE IF NOT EXISTS `m0003_projectmst` (
  `PJNO` varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PJNM` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ﾌﾟﾛｼﾞｪｸﾄマスタ';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m0003_projectmst`
--
ALTER TABLE `m0003_projectmst`
 ADD PRIMARY KEY (`PJNO`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
