<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WS0002
'機能：勤怠の入力
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<link href="css/common.css" rel="stylesheet" type="text/css">
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim strNengapi							'年月日
	Dim strNengetu							'年月
	Dim strNendo								'年度
	Dim strSysDate							'システム年月日
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strSTime								'開始時間
	Dim strETime								'終了時間
	Dim fltKikan								'期間
	Dim strFormat								'書式
	Dim strColor								'カラー
	Dim chkFlag									'ﾁｪｯｸフラグ
	Dim strDateFrom       			'週間の開始日
	Dim strDateTo								'週間の終了日
	Dim intDateCNT							'週間の日数
	Dim fltYuukyuuCNT						'有休回数
	Dim fltHankyuuCNT						'半休回数
	Dim fltDaikyuuCNT						'代休回数
	Dim fltKekkinCNT						'欠勤回数
	Dim fltSonotaCNT						'その他回数
	Dim fltYuukyuuZan						'有休残数
	Dim fltDaikyuukenTime				'代休権タイム
	Dim fltStandardDay					'標準勤務日数
	Dim fltStandardTime					'標準勤務時間
	Dim strStandardSTime				'標準開始時間

	'画面項目の格納変数定義
	Dim lblNengapi(7)						'年月日
	Dim lblYoubi(7)							'曜日
	Dim lblKyuujituflg(7)				'休日フラグ
	Dim cmbKinmukbn(7)					'勤務区分
	Dim txtSTime(7)							'出勤時間
	Dim txtETime(7)							'退勤時間
	Dim lblKinmuTime(7)					'合計勤務時間
	Dim lblZangyouTimeF(7)			'普通残業
	Dim lblZangyouTimeS(7)			'深夜残業
	Dim lblZangyouTimeK(7)			'休日残業
	Dim lblShortageTime(7)			'不足時間
	Dim lblTikokuTime(7)				'遅刻時間
	Dim lblDaikyuDate(7)				'代休日
	Dim txtCMT(7)								'備考
	Dim lblPJNO(10)							'プロジェクトNo
	Dim lblPJNM(10)							'プロジェクト名称
	Dim txtPJTime(7,10)					'プロジェクト時間
	Dim lblMKinmuDay						'月間合計日数
	Dim lblMKinmuTime						'月間合計勤務時間
	Dim lblMZangyouTimeF				'月間普通残業
	Dim lblMZangyouTimeS				'月間深夜残業
	Dim lblMZangyouTimeK				'月間休日残業
	Dim lblMShortageTime				'月間不足時間
	Dim lblMTikokuTime					'月間遅刻時間
	Dim lblMPJTime(10)					'月間プロジェクト時間
	Dim hdnBackURL							'戻るパス

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが更新の場合
	elseIf strMode = "update" Then
		'スケジュール更新処理を行う
		Call updateProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()
	Dim strTemp
	Dim intYoubi

	'****パラメータ取得******
	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = Empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if

	'実行モードを取得
	strMode = request.querystring("mode")

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	If strNengetu = Empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'年月を取得できない場合はシステム日付を付与する
	If strNengetu = Empty then
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
	end if

	'アドレスバーから週間の開始日、終了日を取得
	strDateFrom = trim(request.querystring("DateFrom"))
	strDateTo = trim(request.querystring("DateTo"))
	'アドレスバーから週間の開始日、終了日を取得出来ない場合はフォームから取得
	If strDateFrom = Empty Then
		strDateFrom = trim(request.form("txtDateFrom"))
		strDateTo = trim(request.form("txtDateTo"))
	End If

	strSysDate = Mid(Date,1,4) & Mid(Date,6,2) & Mid(Date,9,2)

	'アドレスバーからもフォームからも週間の開始日、終了日取得できない場合
	If strDateFrom = Empty Then
		'システム日付の所在週の開始日、終了日を計算
		intYoubi = getWeekDay(Date())

		strDateFrom = strSysDate - intYoubi + 1
		strDateTo = strSysDate - intYoubi + 7
		If Left(strDateFrom,6) < Left(strSysDate,6) Or Right(strDateFrom,2) = "00" Then
			strDateFrom = Left(strSysDate,6) & "01"
		End If
	End If
	'月末の終了日を判定
	strTemp = GetMaxMonthDate(strDateTo)
	If (strDateTo - strTemp) > 0 Then
		strDateTo = strTemp
	End If

	'週間の日数計算
	intDateCNT = strDateTo - strDateFrom + 1

	'年度計算
	strNendo = Left(strNengetu,4)
	If Mid(strNengetu,5,2) < 4 Then
		strNendo = strNendo - 1
	End If

	'戻るパス
	hdnBackURL = "WST0001.asp?shaincd=" & strShainCD & "&nengetu=" & strNengetu & "&datefrom=" & strDateFrom & "&dateto=" & strDateTo

	'セッションに戻るパスを格納する
	Session("backurl") = hdnBackURL

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle(toDate2(strNengetu) & "度 勤務表",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'月別勤怠表からデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0002(strShainCD,strNengetu))

	'レコード存在しない場合
	If rs1.RecordCount = 0 Then
		'勤怠情報を作成する
		Call insertProc()
		'月別勤怠表からデータ取り出し
		Set rs1 = session("kintai").Execute(SELWST0002(strShainCD,strNengetu))
	End If

	'承認状態チェック
	If rs1("SHOUNINSTATUS") = "2" Or rs1("SHOUNINSTATUS") = "3" Then
		Set rs1 = Nothing
		Response.Redirect "WST0001_1.asp?shaincd=" & strShainCD & "&nengetu=" & strNengetu
	End If

	'エラーメッセージ部分を表示する
	Call viewHeader()

	'日別勤怠表とスケジュール表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,1,strShainCD,strNengetu)

	'画面の項目に値をセット
	Call setScreenValue(rs,rs1)

	'行先掲示＆スケジュールを表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing
	Set rs1 = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(pRs1,pRs2)
'PROCEDURE機能：画面の項目に値をセット
'引数：
'				pRs1--日別勤怠表
'				pRs2--月別勤怠表
'****************************************************
Sub setScreenValue(pRs1,pRs2)

	cnt = 1
	pRs1.MoveFirst
	'日別勤怠表から画面項目の格納変数に代入
	Do until pRs1.EOF
		If (pRs1("NENGAPI") - strDateFrom) >= 0 And (pRs1("NENGAPI") - strDateTo) <= 0  Then
			lblNengapi(cnt)				= pRs1("NENGAPI")			'年月日
			lblYoubi(cnt)					= pRs1("YOUBI")				'曜日
			lblKyuujituflg(cnt)		= pRs1("KYUUJITUFLG")	'休日フラグ
			cmbKinmukbn(cnt)			= pRs1("KINMUKBN")		'勤務区分
			txtSTime(cnt)					= pRs1("STIME")				'出勤時間
			txtETime(cnt)					= pRs1("ETIME")				'退勤時間
			lblKinmuTime(cnt)			= pRs1("KINMUTIME")		'合計勤務時間
			lblZangyouTimeF(cnt)	= pRs1("ZANGYOUTIMEF")'普通残業
			lblZangyouTimeS(cnt)	= pRs1("ZANGYOUTIMES")'深夜残業
			lblZangyouTimeK(cnt)	= pRs1("ZANGYOUTIMEK")'休日残業
			lblShortageTime(cnt)	= pRs1("SHORTAGETIME")'不足時間
			lblTikokuTime(cnt)		= pRs1("TIKOKUTIME")	'遅刻時間
			lblDaikyuDate(cnt)		= pRs1("DAIKYUUDATE")	'代休日
			txtCMT(cnt)						= pRs1("CMT")					'備考
			txtPJTime(cnt,1)			= pRs1("PJTIME1")			'プロジェクト時間1
			txtPJTime(cnt,2)			= pRs1("PJTIME2")			'プロジェクト時間2
			txtPJTime(cnt,3)			= pRs1("PJTIME3")			'プロジェクト時間3
			txtPJTime(cnt,4)			= pRs1("PJTIME4")			'プロジェクト時間4
			txtPJTime(cnt,5)			= pRs1("PJTIME5")			'プロジェクト時間5
			txtPJTime(cnt,6)			= pRs1("PJTIME6")			'プロジェクト時間6
			txtPJTime(cnt,7)			= pRs1("PJTIME7")			'プロジェクト時間7
			txtPJTime(cnt,8)			= pRs1("PJTIME8")			'プロジェクト時間8
			txtPJTime(cnt,9)			= pRs1("PJTIME9")			'プロジェクト時間9
			txtPJTime(cnt,10)			= pRs1("PJTIME10")		'プロジェクト時間10
			cnt = cnt + 1
		End If
		pRs1.MoveNext
	Loop

	'月別勤怠表から画面項目の格納変数に代入
	Do until pRs2.EOF
		lblPJNO(1)					= pRs2("PJNO1")						'プロジェクトNo1
		lblPJNO(2)					= pRs2("PJNO2")						'プロジェクトNo2
		lblPJNO(3)					= pRs2("PJNO3")						'プロジェクトNo3
		lblPJNO(4)					= pRs2("PJNO4")						'プロジェクトNo4
		lblPJNO(5)					= pRs2("PJNO5")						'プロジェクトNo5
		lblPJNO(6)					= pRs2("PJNO6")						'プロジェクトNo6
		lblPJNO(7)					= pRs2("PJNO7")						'プロジェクトNo7
		lblPJNO(8)					= pRs2("PJNO8")						'プロジェクトNo8
		lblPJNO(9)					= pRs2("PJNO9")						'プロジェクトNo9
		lblPJNO(10)					= pRs2("PJNO10")					'プロジェクトNo10
		lblPJNM(1)					= pRs2("PJNM1")						'プロジェクト名称1
		lblPJNM(2)					= pRs2("PJNM2")						'プロジェクト名称2
		lblPJNM(3)					= pRs2("PJNM3")						'プロジェクト名称3
		lblPJNM(4)					= pRs2("PJNM4")						'プロジェクト名称4
		lblPJNM(5)					= pRs2("PJNM5")						'プロジェクト名称5
		lblPJNM(6)					= pRs2("PJNM6")						'プロジェクト名称6
		lblPJNM(7)					= pRs2("PJNM7")						'プロジェクト名称7
		lblPJNM(8)					= pRs2("PJNM8")						'プロジェクト名称8
		lblPJNM(9)					= pRs2("PJNM9")						'プロジェクト名称9
		lblPJNM(10)					= pRs2("PJNM10")					'プロジェクト名称10
		lblMPJTime(1)				= pRs2("PJTIME1")					'月間プロジェクト時間1
		lblMPJTime(2)				= pRs2("PJTIME2")					'月間プロジェクト時間2
		lblMPJTime(3)				= pRs2("PJTIME3")					'月間プロジェクト時間3
		lblMPJTime(4)				= pRs2("PJTIME4")					'月間プロジェクト時間4
		lblMPJTime(5)				= pRs2("PJTIME5")					'月間プロジェクト時間5
		lblMPJTime(6)				= pRs2("PJTIME6")					'月間プロジェクト時間6
		lblMPJTime(7)				= pRs2("PJTIME7")					'月間プロジェクト時間7
		lblMPJTime(8)				= pRs2("PJTIME8")					'月間プロジェクト時間8
		lblMPJTime(9)				= pRs2("PJTIME9")					'月間プロジェクト時間9
		lblMPJTime(10)			= pRs2("PJTIME10")				'月間プロジェクト時間10
		lblMKinmuTime				= pRs2("KINMUTIME")				'月間合計勤務時間
		lblMZangyouTimeF		= pRs2("ZANGYOUTIMEF")		'月間普通残業
		lblMZangyouTimeS		= pRs2("ZANGYOUTIMES")		'月間深夜残業
		lblMZangyouTimeK		= pRs2("ZANGYOUTIMEK")		'月間休日残業
		lblMShortageTime		= pRs2("SHORTAGETIME")		'月間不足時間
		lblMTikokuTime			= pRs2("TIKOKUTIME")			'月間遅刻時間
		cnt = cnt + 1
		pRs2.MoveNext
	Loop

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()

	cnt = 1
	'画面項目を変数に代入
	For cnt = 1 To UBOUND(lblNengapi)
		lblNengapi(cnt)				= trim(request.form("lblNengapi" & cnt))				'年月日
		lblYoubi(cnt)					= trim(request.form("lblYoubi" & cnt))					'曜日
		lblKyuujituflg(cnt)		= trim(request.form("lblKyuujituflg" & cnt))		'休日フラグ
		cmbKinmukbn(cnt)			= trim(request.form("cmbKinmukbn" & cnt))				'勤務区分
		txtSTime(cnt)					= trim(request.form("txtSTime" & cnt))					'出勤時間
		txtETime(cnt)					= trim(request.form("txtETime" & cnt))					'退勤時間
		lblKinmuTime(cnt)			= trim(request.form("lblKinmuTime" & cnt))			'合計勤務時間
		lblZangyouTimeF(cnt)	= trim(request.form("lblZangyouTimeF" & cnt))		'普通残業
		lblZangyouTimeS(cnt)	= trim(request.form("lblZangyouTimeS" & cnt))		'深夜残業
		lblZangyouTimeK(cnt)	= trim(request.form("lblZangyouTimeK" & cnt))		'休日残業
		lblShortageTime(cnt)	= trim(request.form("lblShortageTime" & cnt))		'不足時間
		lblTikokuTime(cnt)		= trim(request.form("lblTikokuTime" & cnt))			'遅刻時間
		lblDaikyuDate(cnt)		= trim(request.form("lblDaikyuDate" & cnt))			'代休日
		txtCMT(cnt)						= trim(request.form("txtCMT" & cnt))						'備考
		txtPJTime(cnt,1)			= trim(request.form("txtPJTime" & cnt & "-1"))	'プロジェクト時間1
		txtPJTime(cnt,2)			= trim(request.form("txtPJTime" & cnt & "-2"))	'プロジェクト時間2
		txtPJTime(cnt,3)			= trim(request.form("txtPJTime" & cnt & "-3"))	'プロジェクト時間3
		txtPJTime(cnt,4)			= trim(request.form("txtPJTime" & cnt & "-4"))	'プロジェクト時間4
		txtPJTime(cnt,5)			= trim(request.form("txtPJTime" & cnt & "-5"))	'プロジェクト時間5
		txtPJTime(cnt,6)			= trim(request.form("txtPJTime" & cnt & "-6"))	'プロジェクト時間6
		txtPJTime(cnt,7)			= trim(request.form("txtPJTime" & cnt & "-7"))	'プロジェクト時間7
		txtPJTime(cnt,8)			= trim(request.form("txtPJTime" & cnt & "-8"))	'プロジェクト時間8
		txtPJTime(cnt,9)			= trim(request.form("txtPJTime" & cnt & "-9"))	'プロジェクト時間9
		txtPJTime(cnt,10)			= trim(request.form("txtPJTime" & cnt & "-10"))	'プロジェクト時間10
	Next
	'月別勤怠表から画面項目の格納変数に代入
	For cnt = 1 To UBOUND(lblPJNO)
		lblPJNO(cnt)					= trim(request.form("lblPJNO" & cnt))						'プロジェクトNo1
		lblPJNM(cnt)					= trim(request.form("lblPJNM" & cnt))						'プロジェクト名称1
		lblMPJTime(cnt)				= trim(request.form("lblMPJTime" & cnt))				'月間プロジェクト時間1
	Next
	lblMKinmuTime						= trim(request.form("lblMKinmuTime"))						'月間合計勤務時間
	lblMZangyouTimeF				= trim(request.form("lblMZangyouTimeF"))				'月間普通残業
	lblMZangyouTimeS				= trim(request.form("lblMZangyouTimeS"))				'月間深夜残業
	lblMZangyouTimeK				= trim(request.form("lblMZangyouTimeK"))				'月間休日残業
	lblMShortageTime				= trim(request.form("lblMShortageTime"))				'月間不足時間
	lblMTikokuTime					= trim(request.form("lblMTikokuTime"))					'月間不足時間
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データの追加を行う
'引数：なし
'****************************************************
Sub insertProc()

	'エラー制御開始
'	On Error Resume Next

	'カレンダーマスタから日付データを取得
	Set rs = session("kintai").Execute(SELWSM0001_1(0,strNengetu))

	'レコード存在しない場合
	If rs.RecordCount = 0 Then
		errMSG = WSM0001E01
		'エラーメッセージ部分を表示する
		Call viewHeader()
		'処理を終了する
		response.End
	End If

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'日別、月別テーブル情報を一度削除する	
	session("kintai").Execute (DELWST0001(strShainCD,rs("NENGETU")))
	session("kintai").Execute (DELWST0002(strShainCD,rs("NENGETU")))

	'日別情報を作成する
	Do until rs.EOF
		'***SQL実行
		session("kintai").Execute (INSWST0001(strShainCD,rs("NENGAPI"),rs("NENGETU"),rs("NENDO"),rs("YOUBI"),rs("KYUUJITUFLG")))
		If Err <> 0 Then
			session("kintai").RollBackTrans
			errMSG = WST0001E1
			'エラーメッセージ部分を表示する
			Call viewHeader()
			'処理を終了する
			response.End
		End If
		rs.MoveNext
	Loop

	rs.MoveFirst

	'カレンダーマスタから月度平日日数を取得
	Set rs1 = session("kintai").Execute (SELWSM0001_5(strNengetu,0))
	fltStandardDay = rs1("CNT")

	'勤務条件マスタから標準勤務時間を取得
	Set rs1 = session("kintai").Execute(SELWSM0002_1(0,"01"))
	If Err <> 0 Then
		session("kintai").RollBackTrans
		errMSG = "勤務条件" & WSE0000
		'エラーメッセージ部分を表示する
		Call viewHeader()
		'処理を終了する
		response.End
	End If

	fltStandardTime = rs1("KIKAN") * fltStandardDay

	'月別情報を作成する
	'***SQL実行
	session("kintai").Execute (INSWST0002(strShaincd,strNengetu,rs("NENDO"),strBumonCD,fltStandardDay,fltStandardTime))
	If Err <> 0 Then
		session("kintai").RollBackTrans
		errMSG = WST0002E1
		'エラーメッセージ部分を表示する
		Call viewHeader()
		'処理を終了する
		response.End
	End If

	'** トランザクション終了
	session("kintai").CommitTrans

	'レコードセット開放
	Set rs = Nothing

	'エラー制御終了
	On Error Goto 0

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データの更新を行う
'引数：なし
'****************************************************
Sub updateProc()

	'画面の情報を取得する
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'画面の情報を基づき計算を行い、DBを更新する
	If executeUpdate() = False Then
		'ロールバック
		session("kintai").RollBackTrans
	Else
		'** トランザクション終了
		session("kintai").CommitTrans
	End If

	'エラー制御終了
	On Error Goto 0

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle(toDate2(strNengetu) & "度 勤務表",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'エラーメッセージ部分を表示する
	Call viewHeader()

	'日別勤怠表とスケジュール表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,1,strShainCD,strNengetu)

	'勤怠表を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'FUNCTION名：	executeUpdate()
'FUNCTION機能：画面の項目を基づき勤怠情報を計算する
'引数：なし
'リターン値：あり
'						True			--成功
'						False			--失敗
'****************************************************
Function executeUpdate()

	'リターン値をTrueに設定
	executeUpdate = True

	'代休テーブルの代休日を空にセット
	session("kintai").Execute (UPDWSM0005(strShainCD,strDateFrom,strDateTo))

	'代休テーブルの休日出勤日一度削除
	session("kintai").Execute (DELWSM0005(strShainCD,strDateFrom,strDateTo))

	'勤務条件マスタから標準勤務時間、標準開始時間を取得
	Set rs = session("kintai").Execute(SELWSM0002_1(0,"01"))
	fltStandardTime = rs("KIKAN")
	strStandardSTime = rs("STIME")

	'勤務条件マスタから午前標準勤務時間を取得
	Set rs = session("kintai").Execute(SELWSM0002_1(0,"02"))
	fltStandardTimeAM = rs("KIKAN")

	'勤務条件マスタから午後標準勤務時間を取得
	Set rs = session("kintai").Execute(SELWSM0002_1(0,"03"))
	fltStandardTimePM = rs("KIKAN")

	'週間日数分をロープ
	For cnt = 1 To intDateCNT
		'初期化
		lblKinmuTime(cnt) 		= 0			'勤務時間
		lblZangyouTimeF(cnt) 	= 0			'普通残業
		lblZangyouTimeS(cnt) 	= 0			'深夜残業
		lblZangyouTimeK(cnt) 	= 0			'休日残業
		lblShortageTime(cnt) 	= 0			'不足時間
		lblTikokuTime(cnt)	 	= 0			'遅刻時間
		lblDaikyuDate(cnt) = empty

		Select Case cmbKinmukbn(cnt)
		'有休、半休処理
		Case "2","4","5"
			'使用可能有休チェック
			If checkYukyuu(strShainCD,strNendo) = False Then
				'エラー処理
				executeUpdate = False
				Exit Function
			End If
			
			if StrComp(cmbKinmukbn(cnt), "2") = 0 then
				'有休の場合は標準勤務時間計上
				lblKinmuTime(cnt) = fltStandardTime
			elseif StrComp(cmbKinmukbn(cnt), "4") = 0 then
				'AM有休の場合はAM標準勤務時間計上
				lblKinmuTime(cnt) = fltStandardTimeAM
			else
				'PM有休の場合はPM標準勤務時間計上
				lblKinmuTime(cnt) = fltStandardTimePM
			end if
			
		'代休処理
		Case "3"
			'代休可能な休出日取得
			lblDaikyuDate(cnt) = getKyuudeDate(strShainCD,lblNengapi(cnt))
			If lblDaikyuDate(cnt) = Empty Then
				errMSG = toDate(lblNengapi(cnt)) & WSE0015
				'エラー処理
				executeUpdate = False
				Exit Function
			End If
			'代休設定
			If setDaikyuuDate(strShainCD,lblNengapi(cnt),lblDaikyuDate(cnt)) = False Then
				errMSG = "代休" & WSE0000
				'エラー処理
				executeUpdate = False
				Exit Function
			End If
			'代休の場合は標準勤務時間計上
			lblKinmuTime(cnt) = fltStandardTime
		End Select


		'画面の項目にﾁｪｯｸを行う
		If checkInput() = False Then
			executeUpdate = False
			Exit Function
		End If


		'出勤時間、退勤時間両方入力された場合勤務時間を取得
		If txtSTime(cnt) <> Empty And txtETime(cnt) <> Empty Then
			'タイムチェック
			If checkDate(txtSTime(cnt)) = False Or checkDate(txtETime(cnt)) = False Then
				'エラー処理
				errMSG = toDate(lblNengapi(cnt)) & WSE0013
				executeUpdate = False
				Exit Function
			End If

			'**********勤務時間計算*********
			'タイムマトリックスから勤務時間を取得
			If txtSTime(cnt) < txtETime(cnt) Then
				Set rs = session("kintai").Execute(SELWSM0003(0,txtSTime(cnt),txtETime(cnt)))
			Else
				Set rs = session("kintai").Execute(SELWSM0003_1(0,txtSTime(cnt),txtETime(cnt)))
			End If
			'勤務時間取得
			lblKinmuTime(cnt) = rs("KIKAN")
			
'			'勤務時間は7.5〜8の間は7.5として計算
'			If StrComp(lblKinmuTime(cnt), "7.5") > 0 And StrComp(lblKinmuTime(cnt), "8") <= 0 then
'				 lblKinmuTime(cnt) = 7.5
'			'8時間以上の場合は-0.5
'			ElseIf StrComp(lblKinmuTime(cnt), 8) > 0 Then
'				lblKinmuTime(cnt) = lblKinmuTime(cnt) - 0.5
'			end if

			'**********残業計算*********
			'土日の場合は休日残業計算
			If lblKyuujituflg(cnt) = 1 Then
			
				'6時間以上代休権を与える
				If lblKinmuTime(cnt) >= 6 Then
				
					lblDaikyuDate(cnt) = "代休可"
				
					'代休マスタ休出日存在チェック
					Set rs = session("kintai").Execute(SELWSM0005_4(strShainCD,lblNengapi(cnt)))
					If rs("CNT") = 0 Then
						'代休マスタに休出日追加
						session("kintai").Execute (INSWSM0005(strShainCD,lblNengapi(cnt),strNendo,strNengetu,lblZangyouTimeK(cnt),Empty))
					Else
						'代休マスタに休出日更新
						session("kintai").Execute (UPDWSM0005_2(strShainCD,lblNengapi(cnt),lblZangyouTimeK(cnt)))
					End If
				End If

				'休日残業
				if lblKinmuTime(cnt) < 6 Then
					'6時間未満の場合は休日残業計上
					lblZangyouTimeK(cnt) = lblKinmuTime(cnt)
				elseif (lblKinmuTime(cnt) - fltStandardTime) > 0 then
					'標準時間以上の場合は休日残業計上
					lblZangyouTimeK(cnt) = lblKinmuTime(cnt) - fltStandardTime
				end if

			'土日以外の場合は普通残業を計算
			Else
				'普通残業 = 勤務時間 - 標準勤務時間
				lblZangyouTimeF(cnt) = lblKinmuTime(cnt) - fltStandardTime
				'普通残業はマイナスの場合"0"とし、不足時間に代入
				If lblZangyouTimeF(cnt) < 0 Then
					lblShortageTime(cnt) = lblZangyouTimeF(cnt)
					lblZangyouTimeF(cnt) = 0
				End If
				'遅刻時間計算
				lblTikokuTime(cnt) = DateDiff("n",txtSTime(cnt),strStandardSTime) / 60
				If lblTikokuTime(cnt) > 0 Then
					lblTikokuTime(cnt) = 0
				End If
			End If

			'深夜残業を計算
			'タイムマトリックスから深夜残業を取得
			If txtSTime(cnt) < txtETime(cnt) Then
				Set rs = session("kintai").Execute(SELWSM0003_2(0,txtSTime(cnt),txtETime(cnt)))
			Else
				Set rs = session("kintai").Execute(SELWSM0003_3(0,txtSTime(cnt),txtETime(cnt)))
			End If
			
			'勤務時間取得
			lblZangyouTimeS(cnt) = rs("KIKAN")

			'普通残業時間＝普通残業時間（元）−深夜残業時間
			lblZangyouTimeF(cnt) = lblZangyouTimeF(cnt) - lblZangyouTimeS(cnt)
			if lblZangyouTimeF(cnt) < 0 then
				lblZangyouTimeF(cnt) = 0
			end if
			
			'レコードセットを開放
			Set rs = Nothing
		End If

		'******日別勤怠テーブル更新
		strSQL = UPDWST0001(strShaincd,lblNengapi(cnt),cmbKinmukbn(cnt),txtSTime(cnt),txtETime(cnt),lblKinmuTime(cnt),lblZangyouTimeF(cnt),lblZangyouTimeS(cnt),lblZangyouTimeK(cnt),lblShortageTime(cnt),lblTikokuTime(cnt),lblDaikyuDate(cnt),txtPJTime(cnt,1),txtPJTime(cnt,2),txtPJTime(cnt,3),txtPJTime(cnt,4),txtPJTime(cnt,5),txtPJTime(cnt,6),txtPJTime(cnt,7),txtPJTime(cnt,8),txtPJTime(cnt,9),txtPJTime(cnt,10),txtCMT(cnt))

		'***SQL実行
		session("kintai").Execute (strSQL)
		If Err <> 0 Then
			errMSG = WST0001E1
			executeUpdate = False
			Exit Function
		End If

	Next

	'日別勤怠テーブルからサマリー情報を取得
	Set rs = session("kintai").Execute(SELWST0001_2(strShainCD,strNengetu))
	'画面項目の格納変数に代入
	lblMKinmuTime			=	rs("KINMUTIME")			'月間合計勤務時間
	lblMZangyouTimeF	=	rs("ZANGYOUTIMEF")	'月間普通残業
	lblMZangyouTimeS	=	rs("ZANGYOUTIMES")	'月間深夜残業
	lblMZangyouTimeK	=	rs("ZANGYOUTIMEK")	'月間休日残業
	lblMShortageTime	=	rs("SHORTAGETIME")	'月間不足時間
	lblMTikokuTime		=	rs("TikokuTime")		'月間遅刻時間
	lblMPJTime(1)			=	rs("PJTIME1")				'月間プロジェクト時間
	lblMPJTime(2)			=	rs("PJTIME2")				'月間プロジェクト時間
	lblMPJTime(3)			=	rs("PJTIME3")				'月間プロジェクト時間
	lblMPJTime(4)			=	rs("PJTIME4")				'月間プロジェクト時間
	lblMPJTime(5)			=	rs("PJTIME5")				'月間プロジェクト時間
	lblMPJTime(6)			=	rs("PJTIME6")				'月間プロジェクト時間
	lblMPJTime(7)			=	rs("PJTIME7")				'月間プロジェクト時間
	lblMPJTime(8)			=	rs("PJTIME8")				'月間プロジェクト時間
	lblMPJTime(9)			=	rs("PJTIME9")				'月間プロジェクト時間
	lblMPJTime(10)		=	rs("PJTIME10")			'月間プロジェクト時間

	'初期化
	fltYuukyuuCNT = 0
	fltDaikyuuCNT = 0
	fltHankyuuCNT = 0
	fltKekkinCNT = 0
	fltSonotaCNT = 0

	'日別勤怠テーブルから有休、半休、代休、欠勤などのサマリー情報を取得
	Set rs = session("kintai").Execute(SELWST0001_3(strShainCD,strNengetu))
	Do until rs.EOF
		If rs("KINMUKBN") = 2 Then
			fltYuukyuuCNT = rs("CNT")														'有休回数
		ElseIf rs("KINMUKBN") = 3 Then
			fltDaikyuuCNT = rs("CNT")														'代休回数
		ElseIf rs("KINMUKBN") = 4 Or rs("KINMUKBN") = 5 Then
			fltHankyuuCNT = fltHankyuuCNT + rs("CNT")						'半休回数
		ElseIf rs("KINMUKBN") = 6 Then
			fltKekkinCNT 	= rs("CNT")														'欠勤回数
		ElseIf rs("KINMUKBN") = 7 Then
			fltSonotaCNT 	= rs("CNT")														'その他回数
		End If
		rs.MoveNext
	Loop

	'日別勤怠テーブル勤務日数を取得
	Set rs = session("kintai").Execute(SELWST0001_5(strShainCD,strNengetu))
	lblMKinmuDay = rs("CNT")

	strSQL = UPDWST0002(strShaincd,strNengetu,lblMKinmuDay,lblMKinmuTime,lblMZangyouTimeF,lblMZangyouTimeS,lblMZangyouTimeK,lblMShortageTime,lblMTikokuTime,fltYuukyuuCNT,fltHankyuuCNT,0,fltDaikyuuCNT,fltKekkinCNT,fltSonotaCNT,lblMPJTime(1),lblMPJTime(2),lblMPJTime(3),lblMPJTime(4),lblMPJTime(5),lblMPJTime(6),lblMPJTime(7),lblMPJTime(8),lblMPJTime(9),lblMPJTime(10),strSysDate)

	'***SQL実行
	session("kintai").Execute (strSQL)
	If Err <> 0 Then
		errMSG = WST0002E1
		executeUpdate = False
		Exit Function
	End If

	'レコードセット開放
	Set rs = Nothing

	'有休マスタ情報取得
	fltYuukyuuZan = 0
	Set rs = session("kintai").Execute(SELWSM0004_1(strShainCD,strNendo))

	If rs.RecordCount > 0 Then
		fltYuukyuuZan = rs("YUUKYUU") - fltYuukyuuCNT - (fltHankyuuCNT / 2)
		'有休マスタ更新
		'***SQL実行
		session("kintai").Execute (UPDWSM0004(strShainCD,strNendo,rs("YUUKYUU"),fltYuukyuuZan))
		If Err <> 0 Then
			errMSG = "有休マスタ" & WSE0000
			executeUpdate = False
			Exit Function
		End If
	End If

	'エラーメッセージが設定されている場合エラー処理を行う
	If errMSG <> Empty Then
		'リターン値をFlaseに設定
		executeUpdate = Flase
		'処理終了
		Exit Function
	End If
End Function
%>

<%
'****************************************************
'FUNCTION名：	checkInput()
'FUNCTION機能：画面の項目にﾁｪｯｸを行う
'引数：なし
'リターン値：あり
'						True			--成功
'						False			--失敗
'****************************************************
Function checkInput()
	checkInput = True
	'代休、有休、欠勤を選択した場合は出社時間、退社時間は入力不可
	If (cmbKinmukbn(cnt) = "2" Or cmbKinmukbn(cnt) = "3" Or cmbKinmukbn(cnt) = "6") And _
	(txtSTime(cnt) <> Empty Or txtETime(cnt) <> Empty) Then
		errMSG = WST0001E02
		checkInput = False
		Exit Function
	End If
End Function
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<TABLE WIDTH="960" BORDER="0">
		<TBODY>
			<TR>
				<TD WIDTH="580" HEIGHT="23"><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
				</TD>
				<TD><INPUT TYPE="BUTTON" VALUE="登録" onClick="SubmitWST0001(0)" class="s-btn">
					　<INPUT TYPE="BUTTON" VALUE="一ヶ月分確認" onClick="SubmitWST0001(1)" class="s-btn">
			 			<INPUT TYPE="BUTTON" VALUE="ﾌﾟﾛｼﾞｪｸﾄ設定" onClick="SubmitWST0001(2)" class="s-btn">
				</TD>
			</TR>
		</TBODY>
	</TABLE>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：勤怠入力
'引数：なし
'****************************************************
Sub viewDetail()
%>

<TD WIDTH="*%" VALIGN="TOP">
  <div class="list">

	<table class="l-tbl">
	<col width="5%">
	<col width="13%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="10%">
	<col width="30%">
	 <TR>
		<TH class="l-cellsec">勤怠<br>情報</TH>
		<TH class="l-cellsec">勤務区分</TH>
		<TH class="l-cellsec">出勤</TH>
		<TH class="l-cellsec">退勤</TH>
		<TH class="l-cellsec">稼働<br>時間</TH>
		<TH class="l-cellsec">普通<br>残業</TH>
		<TH class="l-cellsec">休日<br>残業</TH>
		<TH class="l-cellsec">深夜<br>残業</TH>
		<TH class="l-cellsec">不足<br>時間</TH>
<!--
		<TH class="l-cellsec">遅刻<br>時間</TH>
-->
		<TH class="l-cellsec">代休日</TH>
		<TH class="l-cellsec">備考</TH>
	 </TR>
	</TABLE>
	<table class="l-tbl">
	 <TR>
		<TH class="l-cellsec" width="5%">PJ<br>情報</TD>
<% For cnt = 1 To UBOUND(lblPJNO) %>
		<TH class="l-cellsec" width="9.5%"><% =lblPJNO(cnt) %><br><span class="txt-small"><% =mid(lblPJNM(cnt),1,12) %></span>
		<!-----------------------HIDDEN項目：PJNO、PJNM--------------------->
		<INPUT TYPE="HIDDEN" NAME="lblPJNO<%= cnt %>" VALUE="<% =lblPJNO(cnt) %>">
		<INPUT TYPE="HIDDEN" NAME="lblPJNM<%= cnt %>" VALUE="<% =lblPJNM(cnt) %>">
		</TD>
<% Next %>
	 </TR>
	</TABLE>
<% For cnt = 1 To intDateCNT
		strColor = getYoubiColor(lblYoubi(cnt),lblKyuujituflg(cnt)) %>
	<table class="l-tbl">
	<col width="5%">
	<col width="13%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="10%">
	<col width="30%">
	 <TR>
		<td class="l-cellodd"><font color="<%=strColor%>"><%=toDate4(lblNengapi(cnt))%></font>
		<!-----------------------HIDDEN項目：年月日、曜日、休日フラグ--------------------->
		<INPUT TYPE="HIDDEN" NAME="lblNengapi<%= cnt %>" VALUE="<% =lblNengapi(cnt) %>">
		<INPUT TYPE="HIDDEN" NAME="lblYoubi<%= cnt %>" VALUE="<% =lblYoubi(cnt) %>">
		<INPUT TYPE="HIDDEN" NAME="lblKyuujituFLG<%= cnt %>" VALUE="<% =lblKyuujituFLG(cnt) %>">
		</TD>
		<td class="l-cellodd">
			<SELECT NAME="cmbKinmukbn<%=cnt%>">
			<% For cnt1 = 1 To UBOUND(strKinmukbn)
					strFormat = Empty
				If cnt1 - cmbKinmukbn(cnt) = 0 Then	strFormat = "SELECTED" End If %>
				<OPTION VALUE="<%=cnt1%>" <%=strFormat%>><%=strKinmukbn(cnt1)%></OPTION>
			<% Next %>
			</SELECT>
		</TH>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imenum" SIZE="4" MAXLENGTH="5" NAME="txtSTime<%= cnt %>" onBlur="judgeTime(this)" VALUE="<%=txtSTime(cnt)%>"></TH>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imenum"" SIZE="4" MAXLENGTH="5" NAME="txtETime<%= cnt %>" onBlur="judgeTime(this)" VALUE="<%=txtETime(cnt)%>"></TH>
		<td class="l-celloddr"><%=toDecimal2(lblKinmuTime(cnt))%></TH>
		<td class="l-celloddr"><%=toDecimal2(lblZangyouTimeF(cnt))%></TH>
		<td class="l-celloddr"><%=toDecimal2(lblZangyouTimeK(cnt))%></TH>
		<td class="l-celloddr"><%=toDecimal2(lblZangyouTimeS(cnt))%></TH>
		<td class="l-celloddr"><%=toDecimal2(lblShortageTime(cnt))%></TH>
<!--
		<td class="l-cellodd"><%=toDecimal2(lblTikokuTime(cnt))%></TH>
-->
		<td class="l-cellodd"><%=toDate(lblDaikyuDate(cnt))%></TH>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imeon" NAME="txtCMT<%= cnt %>" VALUE="<%= txtCMT(cnt) %>" SIZE="40" MAXLENGTH="20">
		<!--HIDDEN項目：合計勤務時間、普通残業、深夜残業、休日残業、不足時間、遅刻時間、代休日-->
		<INPUT TYPE="HIDDEN" NAME="lblKinmuTime<%=cnt%>" 		VALUE="<%=lblKinmuTime(cnt)%>">
		<INPUT TYPE="HIDDEN" NAME="lblZangyouTimeF<%=cnt%>" VALUE="<%=lblZangyouTimeF(cnt)%>">
		<INPUT TYPE="HIDDEN" NAME="lblZangyouTimeK<%=cnt%>" VALUE="<%=lblZangyouTimeK(cnt)%>">
		<INPUT TYPE="HIDDEN" NAME="lblZangyouTimeS<%=cnt%>" VALUE="<%=lblZangyouTimeS(cnt)%>">
		<INPUT TYPE="HIDDEN" NAME="lblShortageTime<%=cnt%>" VALUE="<%=lblShortageTime(cnt)%>">
		<INPUT TYPE="HIDDEN" NAME="lblTikokuTime<%=cnt%>" 	VALUE="<%=lblTikokuTime(cnt)%>">
		<INPUT TYPE="HIDDEN" NAME="lblDaikyuDate<%=cnt%>" 	VALUE="<%=lblDaikyuDate(cnt)%>">
		</TH>
	 </TR>
	</TABLE>
	<table class="l-tbl">
		<TR>
			<TD class="l-celleven" width="5%">
			<font color="<%=strColor%>"><%=getYoubiKJ(lblYoubi(cnt))%></font>
			</TD>
		<%
			For cnt1 = 1 To UBOUND(lblPJNO)
			  If trim(lblPJNO(cnt1)) <> Empty Then %>
			<TD class="l-celleven" width="9.5%"><INPUT TYPE="TEXT"  class="txt_imenum" SIZE="5" NAME="txtPJTime<%=cnt%>-<%=cnt1%>" VALUE="<%= toEmpty(txtPJTime(cnt,cnt1)) %>" onBlur="judgeNumber(this)"></TD>
		<%	Else %>
			<TD class="l-celleven" width="9.5%"></TD>
		<%	End If 
			Next %>
		</TR>
	</TABLE>
<% Next %>
	<!------------------月間合計----------------->
	<table class="l-tbl">
	<col width="5%">
	<col width="13%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="6%">
	<col width="10%">
	<col width="30%">
	 <TR>
		<td class="l-cellodd">月間</td>
		<td class="l-cellodd"></td>
		<td class="l-cellodd"></td>
		<td class="l-cellodd"></td>
		<td class="l-celloddr"><%=toDecimal2(lblMKinmuTime)%></td>
		<td class="l-celloddr"><%=toDecimal2(lblMZangyouTimeF)%></td>
		<td class="l-celloddr"><%=toDecimal2(lblMZangyouTimeK)%></td>
		<td class="l-celloddr"><%=toDecimal2(lblMZangyouTimeS)%></td>
		<td class="l-celloddr"><%=toDecimal2(lblMShortageTime)%></td>
<!--		
		<td class="l-cellodd"><%=toDecimal2(lblMTikokuTime)%></td>
-->
		<td class="l-cellodd"></td>
		<td class="l-cellodd">
		<!--HIDDEN項目：合計勤務時間、普通残業、深夜残業、休日残業、不足時間、遅刻時間-->
		<INPUT TYPE="HIDDEN" NAME="lblMKinmuTime" 		VALUE="<%=lblMKinmuTime%>">
		<INPUT TYPE="HIDDEN" NAME="lblMZangyouTimeF" 	VALUE="<%=lblMZangyouTimeF%>">
		<INPUT TYPE="HIDDEN" NAME="lblMZangyouTimeS" 	VALUE="<%=lblMZangyouTimeS%>">
		<INPUT TYPE="HIDDEN" NAME="lblMZangyouTimeK" 	VALUE="<%=lblMZangyouTimeK%>">
		<INPUT TYPE="HIDDEN" NAME="lblMShortageTime" 	VALUE="<%=lblMShortageTime%>">
		<INPUT TYPE="HIDDEN" NAME="lblMTikokuTime" 		VALUE="<%=lblMTikokuTime%>">
		</td>
	 </TR>
	</TABLE>
	<table class="l-tbl">
	 <TR>
		<td class="l-celleven" width="5%">合計</TD>
			<% For cnt = 1 To UBOUND(lblPJNO) %>
			<td class="l-cellevenr" width="9.5%">
				<% If trim(lblPJNO(cnt)) <> Empty Then %>
				<%=toDecimal2(lblMPJTime(cnt))%>
				<!-----------------------HIDDEN項目：PJTime--------------------->
				<INPUT TYPE="HIDDEN" NAME="lblMPJTime<%= cnt %>" VALUE="<%=lblMPJTime(cnt) %>">
			</TD>
			<% 	End If %>
			<% Next %>
	 </TR>
	</TABLE>
  </div>
</TD>
<!--**********************勤怠入力表示(E)****************************-->
</TR>
</TABLE>
</CENTER>

<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" 	VALUE="<%=strShainCD%>">
<INPUT TYPE="HIDDEN" NAME="txtBumonCD" 	VALUE="<%=strBumonCD%>">
<INPUT TYPE="HIDDEN" NAME="txtNengetu" 	VALUE="<%=strNengetu%>">
<INPUT TYPE="HIDDEN" NAME="txtDateFrom" VALUE="<%=strDateFrom%>">
<INPUT TYPE="HIDDEN" NAME="txtDateTo" 	VALUE="<%=strDateTo%>">
<INPUT TYPE="HIDDEN" NAME="hdnBackURL" 	VALUE="<%=hdnBackURL%>">
</CENTER>
</FORM>
</BODY>
</HTML>


<%
End Sub
%>

<%
'****************************************************
'FUNCTION名：	getKyuudeDate(pShainCD,pNengapi)
'FUNCTION機能：代休可能な休出日取得
'引数：
'			pShainCD：社員コード
'			pNengapi：年月日
'****************************************************
Function getKyuudeDate(pShainCD,pNengapi)
	Dim lrs

	getKyuudeDate = Empty

	'利用可能な代休日の取得
	Set lrs = session("kintai").Execute(SELWSM0005_3(pShainCD,pNengapi))

	If lrs("KYUUDEDATE") = Empty Then
		Exit Function
	End If

	getKyuudeDate = lrs("KYUUDEDATE")

End Function
%>

<%
'****************************************************
'FUNCTION名：	setDaikyuuDate(pShainCD,pNengapi,pKyuudeDate)
'FUNCTION機能：代休日セット
'引数：
'			pShainCD：社員コード
'			pNengapi：年月日
'****************************************************
Function setDaikyuuDate(pShainCD,pNengapi,pKyuudeDate)

	setDaikyuuDate = True

	session("kintai").Execute (UPDWSM0005_1(pShainCD,pNengapi,pKyuudeDate))

	If Err <> 0 Then
		setDaikyuuDate = False
		Exit Function
	End If

End Function
%>


<%
'****************************************************
'FUNCTION名：	checkYukyuu(pShainCD,pNendo)
'FUNCTION機能：使用可能有休チェック
'引数：
'			pShainCD：社員コード
'****************************************************
Function checkYukyuu(pShainCD,pNendo)
	Dim lrs
	checkYukyuu = True

	'使用可能有休チェック
	Set lrs = session("kintai").Execute(SELWSM0004_1(pShainCD,pNendo))

	If lrs.RecordCount = 0 Then
		errMSG = WSE0014_1
		checkYukyuu = False
		Exit Function
	End If

	If lrs("YUUKYUUZAN") = 0 Then
		errMSG = toDate(lblNengapi(cnt)) & WSE0014
		checkYukyuu = False
		Exit Function
	End If

End Function
%>


