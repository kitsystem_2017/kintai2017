<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0006
'機能：行先掲示板
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>
<HTML onContextMenu="return false;">

<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.bottom.action="WST0006.asp";
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<%
	'変数定義
	Dim strMode									'実行モード
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim strSysDate							'システム年月日
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim strURL									'URL
	Dim strImage								'イメージ

	Dim cmbBumonCD							'部門コード

	'パラメーター取得
	Call getParameter()

	Call initProc()

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'フォームから取得から社員コードを取得
	strShainCD = trim(request.form("txtShainCD"))

	'フォームから取得から部門を取得
	cmbBumonCD = trim(request.form("cmbBumonCD"))

	'システム日付を取得
	strSysDate = Mid(Date,1,4) & Mid(Date,6,2) & Mid(Date,9,2)

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員マスタと行先テーブルからデータを取得
	Set rs = session("kintai").Execute(SELWST0004_1(cmbBumonCD))
	'詳細を表示
	Call viewDetail(rs)

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：行先掲示板表示
'引数：あり
'			pRs	:行先データ
'****************************************************
Sub viewDetail(pRs)
%>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD>
<%Call viewBumonCMB(cmbBumonCD)%><FONT SIZE="2">部門
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="_submit()" class="s-btn">
			</TD>
		</TR>
	</TABLE>
	<BR>
	<table class="l-tbl">
		<col width="20%">
		<col width="15%">
		<col width="20%">
		<col width="8%">
		<col width="30%">
		<col width="*%">
		<TR>
			<TH class="l-cellsec">社員番号</TH>
			<TH class="l-cellsec">氏名</TH>
			<TH class="l-cellsec">部門</TH>
			<TH class="l-cellsec">行先</TH>
			<TH class="l-cellsec">連絡方法</TH>
			<TH class="l-cellsec">記入日</TH>
		</TR>
		<TR>
			<TH class="l-cellsec" colspan="6">備考</TH>
		</TR>
	</TABLE>
  <div class="l-tbl-auto" style="height:500px">
	<table class="l-tbl">
		<col width="20%">
		<col width="15%">
		<col width="20%">
		<col width="8%">
		<col width="30%">
		<col width="*%">
	<%
		dim i
		dim cellClass,cellClassr
		i = 0
	%>
<%
	Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
	'スケジュール情報を取得
	Set rs1 = session("kintai").Execute(SELWST0003_2(pRs("SHAINCD"),strSysDate))
	strImage = Empty
	strURL = Empty
	If rs1.RecordCount > 0 Then
		strImage = CONST_IMAGE_SCHEDULE
		strURL = Empty
	End If
%>
		<TR>
			<TD class=<%=cellClass%>>
<!--
				<A HREF="WST0005.asp?shaincd=<%=strShainCD%>&toshaincd=<%=pRs("SHAINCD")%>">
					<%=pRs("SHAINCD")%>
				</A>
-->
				<%=pRs("SHAINCD")%>
			</TD>
			<TD class=<%=cellClass%>><%=pRs("SHAINNM")%></TD>
			<TD class=<%=cellClass%>><%=pRs("BUMONNM")%></TD>
			<TD class=<%=cellClass%>><%=strIkisakiArr(pRs("IKISAKIKBN"))%></TD>
			<TD class=<%=cellClass%>><%=pRs("RENRAKU")%></TD>
			<TD class=<%=cellClass%>><%=toDate(pRs("UPDATEDATE"))%></TD>
<!--
			<TD class=<%=cellClass%>><A HREF="WST0003_1.asp?shaincd=<%=pRs("SHAINCD")%>"><%=strImage%></A></TD>
-->
		</TR>
		<TR>
			<TD COLSPAN="6" class=<%=cellClass%>><%=pRs("BIKOU")%></TD>
		</TR>
<%	
	pRs.MoveNext
	Loop
%>

	</TABLE>
  </DIV>
</DIV>
</FORM>
</BODY>
</HTML>

<% End Sub %>

