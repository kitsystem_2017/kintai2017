<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST00011
'機能：精算申請一覧
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.bottom.action="WST0011.asp";
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strNendo							'年度
	Dim strMonth							'月
	Dim strNengetu							'年月
	Dim strSysDate							'システム年月日
	Dim strSQL								'SQL文
	Dim cnt									'カウント
	Dim strFormat							'書式
	Dim strURL								'URL
	Dim strURL1								'URL
	Dim hdnBackURL							'戻るパス

	'パラメーター取得
	Call getParameter()

	Call initProc()
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()
	Dim strTemp

	'アドレスバーからから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	If strShainCD = Empty Then
		'フォームから社員番号取得
		strShainCD = trim(request.form("txtShainCD"))
	End If

	'アドレスバーから取得から年度を取得
	strNendo = trim(request.querystring("nendo"))
	If strNendo = Empty Then
		'フォームから取得から年度を取得
		strNendo = trim(request.form("cmbNendo"))
	End If
	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = getSysNendo()
	end if

	'アドレスバーからから月を取得
	strMonth = trim(request.querystring("month"))
	If strMonth = Empty Then
		'フォームから取得から月を取得
		strMonth = trim(request.form("cmbMonth"))
	End If
	'月を取得できない場合はシステム日付を付与する
	if strMonth = empty then
		strMonth = CONST_SYSMONTH
	end if

	'年月
	strNengetu = strNendo & strMonth

	'戻るパス
 	hdnBackURL = "WST0011.asp?nendo=" & strNendo & "&month=" & strMonth

	'セッションに戻るパスを設定する
	Session("backurl") = hdnBackURL

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'要承認、代理承認一覧を表示
	Set rs = session("kintai").Execute(SELWST0006_2(strShainCD,strNengetu))


	'内容を表示
	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：各種精算申請一覧
'引数：あり
'			pRs:月別勤怠情報
'****************************************************
Sub viewDetail(pRs)
%>
<%
%>
	<span class="txt-gray1"><U>各種精算申請一覧</U></span>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="10%">
<%Call viewNendoCMB(strNendo)%>
				<FONT SIZE="2">年
			</TD>
			<TD WIDTH="10%">
<%Call viewMonthCMB(strMonth)%>
				<FONT SIZE="2">月
			</TD>
			<TD WIDTH="10%">
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="_submit()" class="s-btn">
			</TD>
			<TD WIDTH="*%">
			</TD>
		</TR>
	</TABLE>
	<table class="l-tbl">
		<col width="10%">
		<col width="15%">
		<col width="10%">
		<col width="10%">
		<col width="15%">
		<col width="10%">
		 <TR>
			<TH class="l-cellsec">申請日</TH>
			<TH class="l-cellsec">申請者</TH>
			<TH class="l-cellsec">申請種別</TH>
			<TH class="l-cellsec">承認状態</TH>
			<TH class="l-cellsec">承認者氏名</TH>
			<TH class="l-cellsec">承認日</TH>
		 </TR>
	</table>
	<table class="l-tbl">
		<col width="10%">
		<col width="15%">
		<col width="10%">
		<col width="10%">
		<col width="15%">
		<col width="10%">
	<%
		dim i
		dim cellClass,cellClassr
		i = 0
	%>
	<%Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
			'月間確認にリンク
		if pRs("SINSEICD") = strSINSEICD(1) then
		'	strURL = "WST0008_1.asp?shaincd=" & strShainCD & "&sinseidate=" & pRs("SINSEIDATE") & "&sinseicd=" & pRs("SINSEICD")
			strURL = "WST0008.asp?shaincd=" & strShainCD & "&sinseidate=" & pRs("SINSEIDATE") & "&sinseicd=" & pRs("SINSEICD")
		end if
		if pRs("SINSEICD") = strSINSEICD(2) then
		'	strURL = "WST0009_1.asp?shaincd=" & strShainCD & "&sinseidate=" & pRs("SINSEIDATE") & "&sinseicd=" & pRs("SINSEICD")
			strURL = "WST0009.asp?shaincd=" & strShainCD & "&sinseidate=" & pRs("SINSEIDATE") & "&sinseicd=" & pRs("SINSEICD")
		end if
		if pRs("SINSEICD") = strSINSEICD(3) then
		'	strURL = "WST0010_1.asp?shaincd=" & strShainCD & "&sinseidate=" & pRs("SINSEIDATE") & "&sinseicd=" & pRs("SINSEICD")
			strURL = "WST0010.asp?shaincd=" & strShainCD & "&sinseidate=" & pRs("SINSEIDATE") & "&sinseicd=" & pRs("SINSEICD")
		end if
	%>
		<TR>
			<TD class=<%=cellClassr%>><%=toDate1(pRs("SINSEIDATE"))%></TD>
			<TD class=<%=cellClass%>>
				<A HREF="<%=strURL%>"><%=getShainNM(pRs("SHAINCD"))%></A>
			</TD>
			<TD class=<%=cellClassr%> ><%=strSINSEINM(pRs("SINSEICD"))%></TD>
			<TD class=<%=cellClassr%> ><%=CONST_SHOUNINSTATUS(pRs("SHOUNINSTATUS"))%></TD>
			<TD class=<%=cellClassr%> ><%=pRs("SHOUNINSHANM")%></TD>
			<TD class=<%=cellClassr%> ><%=toDate1(pRs("SHOUNINDATE"))%></TD>
		</TR>
	<%pRs.MoveNext
		Loop%>
	</TABLE>
<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">

</CENTER>
</FORM>
</BODY>
</HTML>
<% End Sub %>

