<%@ Language=VBScript %>
<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WSM0002
'機能：勤務条件設定画面
'*********変更履歴*********
'日付			変更者		変更管理番号	理由
%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<HEAD>
<TITLE>WORKSHEET</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<%

	'変数定義
	Dim rs												'レコードセット
	Dim strMode										'実行モード
	Dim strSQL										'SQL文
	Dim cnt												'カウント
	Dim txtSTime(13)							'開始時間を格納する配列
	Dim txtETime(13)							'終了時間を格納する配列
	Dim lblTimeNM(13)							'時間名称
	Dim lblKikan(13)							'期間

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが作成の場合
	ElseIf strMode = "update" Then
		'作成処理を行う
		Call updateProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーから実行モードを取得
	strMode = request.querystring("mode")

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'マスタからデータを取得
	Set rs = session("kintai").Execute(SELWSM0002(0))

	'画面の項目に値をセット
	setScreenValue(rs)

	'カレンダーを表示する
	Call viewDetail()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データ更新プロセス
'引数：なし
'****************************************************
Sub updateProc()

	'画面の項目に値を取得
	getScreenValue()

	'データ更新を実行
	If executeUpdate() = False Then
		'ヘッダーを表示する
		Call viewDetail()
		Exit Sub
	End If

	'マスタからデータを取得
	Set rs = session("kintai").Execute(SELWSM0002(0))

	'画面の項目に値をセット
	setScreenValue(rs)

	'内容を表示する
	Call viewDetail()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(pRs)
'PROCEDURE機能：画面の項目に値をセット
'引数：
'				pRs--レコードセット
'****************************************************
Sub setScreenValue(pRs)
	For cnt = 1 To UBOUND(txtSTime)
		txtSTime(cnt)		= pRs("STIME")
		txtETime(cnt)		= pRs("ETIME")
		lblTimeNM(cnt)	= pRs("TIMENM")
		lblKikan(cnt)		= pRs("KIKAN")
		pRs.MoveNext
	Next
End Sub
%>
<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()
	For cnt = 1 To UBOUND(txtSTime)
		txtSTime(cnt) 	= trim(request.form("txtSTime" & cnt))
		txtETime(cnt) 	= trim(request.form("txtETime" & cnt))
		lblTimeNM(cnt) 	= trim(request.form("lblTimeNM" & cnt))
		lblKikan(cnt) 	= trim(request.form("lblKikan" & cnt))
	Next
End Sub
%>

<%
'****************************************************
'FUNCTION名：	executeUpdate()
'FUNCTION機能：画面の項目を基づき勤怠情報を計算する
'引数：なし
'リターン値：あり
'						True			--成功
'						False			--失敗
'****************************************************
Function executeUpdate()

	Dim fltKikan

	'リターン値をTrueに設定
	executeUpdate = True

	'エラー制御開始
'	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'タイムマトリックスの休憩フラグ、深夜残業フラグを無効にする
	session("kintai").Execute (UPDWSM0003(0))

	'休憩時間を更新
	For cnt = 4 To UBOUND(txtSTime)
		'開始時間、終了時間、両方入力がある場合DB更新を行う
'		If txtSTime(cnt) <> Empty And txtETime(cnt) <> Empty Then
			'***SQL実行
			'勤務条件マスタを更新
			session("kintai").Execute (UPDWSM0002(txtSTime(cnt),txtETime(cnt),getDateDiff(txtSTime(cnt),txtETime(cnt)),toString(cnt)))
			If Err <> 0 Then
				'エラーメッセージ設定
				errMSG = "勤務条件" & WSE0000
				Exit For
			End If

			'深夜残業時間の場合
			If cnt = 4 Then
				'タイムマトリックスの深夜残業フラグを有効にする
				If txtSTime(cnt) < txtETime(cnt) Then
					session("kintai").Execute (UPDWSM0003_2(0,txtSTime(cnt),txtETime(cnt)))
				Else
					session("kintai").Execute (UPDWSM0003_2(0,"00:00",txtETime(cnt)))
					session("kintai").Execute (UPDWSM0003_2(0,txtSTime(cnt),"24:00"))
				End If
				If Err <> 0 Then
					'エラーメッセージ設定
					errMSG = "タイムマトリックス" & WSE0000
					Exit For
				End If
			Else
				'休憩時間の場合
				'タイムマトリックスの休憩フラグを有効にする
				If txtSTime(cnt) < txtETime(cnt) Then
					session("kintai").Execute (UPDWSM0003_1(0,txtSTime(cnt),txtETime(cnt)))
				Else
					If txtSTime(cnt) <> Empty And txtETime(cnt) <> Empty Then
						session("kintai").Execute (UPDWSM0003_1(0,"00:00",txtETime(cnt)))
						session("kintai").Execute (UPDWSM0003_1(0,txtSTime(cnt),"24:00"))
					End If
				End If
				If Err <> 0 Then
					'エラーメッセージ設定
					errMSG = "タイムマトリックス" & WSE0000
					Exit For
				End If
			End If
'		End If
	Next

	'標準勤務時間、午前出勤、午後出勤の期間を再計算
	For cnt = 1 To 3
			'***SQL実行
			fltKikan = 0
			'タイムマトリックスから勤務時間を取得
			'開始時間、終了時間、両方入力がある場合DB更新を行う
			If txtSTime(cnt) <> Empty And txtETime(cnt) <> Empty Then
				If txtSTime(cnt) < txtETime(cnt) Then
					Set rs = session("kintai").Execute(SELWSM0003(0,txtSTime(cnt),txtETime(cnt)))
				Else
					Set rs = session("kintai").Execute(SELWSM0003_1(0,txtSTime(cnt),txtETime(cnt)))
				End If

				fltKikan = rs("KIKAN")
			End If

			'勤務条件マスタを更新
			session("kintai").Execute (UPDWSM0002(txtSTime(cnt),txtETime(cnt),fltKikan,toString(cnt)))
			If Err <> 0 Then
				'エラーメッセージ設定
				errMSG = "勤務条件" & WSE0000
				Exit For
			End If
	Next

	'レコードセット開放
	Set rs = Nothing

	'エラーメッセージが設定されている場合エラー処理を行う
	If errMSG <> Empty Then
		'ロールバック
		session("kintai").RollBackTrans
		'リターン値をFlaseに設定
		executeUpdate = Flase
		'処理終了
		Exit Function
	End If

	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

End Function
%>


<%
'****************************************************
'PROCEDURE名：  viewDetail()
'PROCEDURE機能：勤務条件設定を表示する
'****************************************************
Sub viewDetail()
%>
<body id="doc">
	<FORM NAME="bottom" METHOD="POST">
	<CENTER>
		<U><FONT SIZE="3" COLOR="BLUE"><B>勤務条件設定</FONT></U>
		<FONT COLOR="RED"> <%=errMSG%></FONT>
		<BR>
		<BR>
<div class="list">
	<table class="l-tbl" width="330">
		<col width="30%">
		<col width="15%">
		<col width="10%">
		<col width="10%">
		<col width="10%">
		<col width="25%">
			<TR ALIGN="CENTER">
				<TD></TD>
				<TD></TD>
				<TH class="l-cellsec">開始時間</TH>
				<TH class="l-cellsec">終了時間</TH>
				<TH class="l-cellsec">期間</TH>
				<TD></TD>
			</TR>
			<% For cnt = 1 To UBOUND(txtSTime) %>
			<TR ALIGN="CENTER">
				<TD></TD>
				<TH class="l-cellsec"><%=lblTimeNM(cnt)%>
					<INPUT TYPE="HIDDEN" NAME="lblTimeNM<%=cnt%>" VALUE="<%=lblTimeNM(cnt)%>">
				</TH>
				<TD class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imenum" NAME="txtSTime<%=cnt%>" VALUE="<%=trim(txtSTime(cnt))%>" onBlur="judgeTime(this)" SIZE="5" MAXLENGTH="5"></TD>
				<TD class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imenum" NAME="txtETime<%=cnt%>" VALUE="<%=trim(txtETime(cnt))%>" onBlur="judgeTime(this)" SIZE="5" MAXLENGTH="5"></TD>
				<TD class="l-cellodd"><%=toDecimal2(toEmpty(lblKikan(cnt)))%>　(H)</TD>
				<TD></TD>
			</TR>
			<% Next %>
		</TABLE>
		<BR>
		<INPUT TYPE="BUTTON" VALUE="確定" onClick="SubmitWSM0002()" class="s-btn">
		<BR>
</div>

	</CENTER></FONT>
	</FORM>
	</BODY>
	</HTML>
<%
End Sub
%>
