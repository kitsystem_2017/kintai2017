<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0010
'機能：行先掲示板
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(type){
	document.bottom.action="WSR0002.asp?mode=" + type;
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<%
	'変数定義
	Dim mode								'実行モード
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim strNendo								'年
	Dim strMonth								'月
	Dim strNengetu							'年月
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim strURL									'URL
	Dim strImage								'イメージ
	Dim strFormat								'フォーマート
	Dim strPJNO									'プロジェクトNO

	Dim cmbBumonCD							'部門コード
	Dim txtPJNO									'プロジェクトNO
	Dim rdo											'ラジオボックス

	'パラメーター取得
	Call getParameter()

	if mode="1" then
		Call downloadProc()
	else
		Call initProc()
	end if

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーからからモードを取得
	mode = trim(request.querystring("mode"))
	'フォームからから社員コードを取得
	strShainCD = trim(request.form("txtShainCD"))

	'フォームからから表示種別を取得
	rdo = trim(request.form("rdo"))
	If rdo = Empty Then
		rdo = "0"
	End If

	'フォームからから部門を取得
	cmbBumonCD = trim(request.form("cmbBumonCD"))

	'フォームからからプロジェクトNOを取得
	txtPJNO = trim(request.form("txtPJNO"))

	'フォームから取得から年度を取得
	strNendo = trim(request.form("cmbNendo"))
	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = GetCurrenNendo()
	end if

	'フォームからから月を取得
	strMonth = trim(request.form("cmbMonth"))
	'月を取得できない場合はシステム日付を付与する
	if strMonth = empty then
		strMonth = CONST_SYSMONTH
	end if

	strNengetu = strNendo & strMonth

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'ヘッダー表示
	Call viewHeader()

	'部門集計を表示
	If rdo = "0" Then
		'月別勤怠情報から部門毎PJNO,PJNM,PJTIMEの集計値を取得
		'部門集計
		Set rs = session("kintai").Execute(SELWST0002_2(cmbBumonCD))
		'詳細を表示
		Call viewDetail0(rs)

	'プロジェクト集計を表示
	ElseIf rdo = "1" Then
		'月別勤怠情報からプロジェクト毎のPJNO,PJNM,PJTIMEの集計値を取得
		'プロジェクト集計
		Set rs = session("kintai").Execute(SELWST0002_3(txtPJNO))

		'詳細を表示
		Call viewDetail1(rs)

	End If

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	downloadProc()
'PROCEDURE機能：ダウンロードを行う
'引数：なし
'****************************************************
Sub downloadProc()

	Response.Buffer = True
	Response.Charset="Shift-JIS"
'	Response.ContentType = "application/vnd.ms-excel"
	Response.ContentType = "application/x-download"

	Response.AddHeader "Content-Disposition","attachment inline;filename=" & strNendo & "年" & strMonth & "プロジェクト集計票.xls"

	'部門集計を表示
	If rdo = "0" Then
		'月別勤怠情報から部門毎PJNO,PJNM,PJTIMEの集計値を取得
		'部門集計
		Set rs = session("kintai").Execute(SELWST0002_2(cmbBumonCD))
		'詳細を表示
		Call viewDetail0(rs)

	'プロジェクト集計を表示
	ElseIf rdo = "1" Then
		'月別勤怠情報からプロジェクト毎のPJNO,PJNM,PJTIMEの集計値を取得
		'プロジェクト集計
		Set rs = session("kintai").Execute(SELWST0002_3(txtPJNO))

		'詳細を表示
		Call viewDetail1(rs)

	End If


	'レコードセットを開放
	Set rs = Nothing

	Response.Flush
	Response.end


End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：プロジェクト集計ヘッダー
'引数：なし
'****************************************************
Sub viewHeader()
%>
<link href="css/common.css" rel="stylesheet" type="text/css">
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
	<U><FONT COLOR="GRAY">プロジェクト集計</FONT></U>
</CENTER>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="10%">
<%Call viewNendoCMB(strNendo)%>
				<FONT SIZE="2">年
			</TD>
			<TD WIDTH="10%">

<%Call viewMonthCMB(strMonth)%>
				<FONT SIZE="2">月
			</TD>
			<TD WIDTH="50%">
<%If rdo = "0" Then strFormat = "CHECKED" Else strFormat = Empty End If%>
				<INPUT TYPE="RADIO" NAME="rdo" VALUE="0" <%=strFormat%>><FONT SIZE="2">部門集計
<%Call viewBumonCMB(cmbBumonCD)%>
<%If rdo = "1" Then strFormat = "CHECKED" Else strFormat = Empty End If%>
				<INPUT TYPE="RADIO" NAME="rdo" VALUE="1" <%=strFormat%>><FONT SIZE="2">プロジェクト集計
				<INPUT TYPE="TEXT" SIZE="10" MAXLENGTH="10" NAME="txtPJNO" VALUE="<%=txtPJNO%>" class="txt_imeoff">
			</TD>
			<TD WIDHT="*%">
				<INPUT TYPE="BUTTON" VALUE="表示"  onClick="_submit()" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="ダウンロード" onClick="_submit(1)" class="s-btn">
			</TD>
		</TR>
	</TABLE>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail0(pRs)
'PROCEDURE機能：部門集計表示
'引数：あり
'			pRs	:プロジェクト情報
'****************************************************
Sub viewDetail0(pRs)
	Dim strTemp
	Dim fltSum
%>
	<BR>
<div class="list">
	<table class="l-tbl">
		<col width="20%">
		<col width="8%">
		<col width="25%">
		<col width="20%">
		<col width="15%">
		<col width="*%">
		<TR>
			<TH class="l-cellsec">部門</TH>
			<TH class="l-cellsec">PJ番号</TH>
			<TH class="l-cellsec">PJ名称</TH>
			<TH class="l-cellsec">社員番号</TH>
			<TH class="l-cellsec">社員名称</TH>
			<TH class="l-cellsec">作業実績</TH>
		</TR>
	</table>
	
  <div class="l-tbl-auto" style="height:500px">
	<table class="l-tbl">
		<col width="20%">
		<col width="8%">
		<col width="25%">
		<col width="20%">
		<col width="15%">
		<col width="*%">
<%
	fltSum = 0
	strTemp = Empty
	dim i
	dim cellClass,cellClassr
	i = 0

	Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
		'合計を足す
		fltSum = fltSum + pRs("PJTIME")
		'部門コード記憶
		strTemp = pRs("BUMONCD")
%>
		<TR>
			<TD class=<%=cellClass%>><%=pRs("BUMONNM")%></TD>
			<TD class=<%=cellClass%>><%=pRs("PJNO")%></TD>
			<TD class=<%=cellClass%>><%=pRs("PJNM")%></TD>
			<TD class=<%=cellClass%>><FONT SIZE="2"><%=pRs("SHAINCD")%></TD>
			<TD class=<%=cellClass%>><%=pRs("SHAINNM")%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal2(pRs("PJTIME"))%></TD>
		</TR>
<%
		pRs.MoveNext
		'直前の部門コードはカレント部門コードと一致しない場合合計表示
		If pRs.EOF = True Then
%>
		<TR>
			<TH class="l-cellsec" COLSPAN="5"><FONT SIZE="2">部門合計</TH>
			<TD class="l-celloddr"><B><%=toDecimal2(fltSum)%></TD>
		</TR>
<%
		Else
			If pRs("BUMONCD") <> strTemp Then
%>
		<TR>
			<TH class="l-cellsec" COLSPAN="5"><FONT SIZE="2">部門合計</TH>
			<TD class= "l-celloddr"><B><%=toDecimal2(fltSum)%></TD>
		</TR>
<%
				'合計を初期化
				fltSum = 0
			End If
		End If
	Loop
%>

	</TABLE>
</div>
</FORM>
</BODY>
</HTML>

<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail1(pRs)
'PROCEDURE機能：プロジェクト集計表示
'引数：あり
'			pRs	:プロジェクト情報
'****************************************************
Sub viewDetail1(pRs)
%>
	<BR>
<div class="list">
	<table class="l-tbl">
		<col width="8%">
		<col width="25%">
		<col width="20%">
		<col width="20%">
		<col width="15%">
		<col width="*%">
		<TR>
			<TH class="l-cellsec">PJ番号</TH>
			<TH class="l-cellsec">PJ名称</TH>
			<TH class="l-cellsec">部門</TH>
			<TH class="l-cellsec">社員番号</TH>
			<TH class="l-cellsec">社員名称</TH>
			<TH class="l-cellsec">作業実績</TH>
		</TR>
	</table>
	
  <div class="l-tbl-auto" style="height:500px">
	<table class="l-tbl">
		<col width="8%">
		<col width="25%">
		<col width="20%">
		<col width="20%">
		<col width="15%">
		<col width="*%">

<%
	fltSum = 0
	strTemp = Empty
	dim i
	dim cellClass,cellClassr
	i = 0

	Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
		'プロジェクトNO記憶
		strTemp = pRs("PJNO")
		'合計を足す
		fltSum = fltSum + pRs("PJTIME")
%>
		<TR>
			<TD class=<%=cellClass%>><%=pRs("PJNO")%></TD>
			<TD class=<%=cellClass%>><%=pRs("PJNM")%></TD>
			<TD class=<%=cellClass%>><%=pRs("BUMONNM")%></TD>
			<TD class=<%=cellClass%>><%=pRs("SHAINCD")%></TD>
			<TD class=<%=cellClass%>><%=pRs("SHAINNM")%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal2(pRs("PJTIME"))%></TD>
		</TR>
<%
		pRs.MoveNext
		If pRs.EOF = True Then
%>
		<TR>
			<TH class="l-cellsec" COLSPAN="5"><FONT SIZE="2">プロジェクト合計</TH>
			<TD class="l-celloddr"><B><%=toDecimal2(fltSum)%></TD>
		</TR>
<%
		Else
			'直前のプロジェクトNOはカレントプロジェクトNOと一致しない場合合計表示
			If strTemp <> pRs("PJNO") Then
%>
		<TR>
			<TH class="l-cellsec" COLSPAN="5"><FONT SIZE="2">プロジェクト合計</TH>
			<TD class="l-celloddr"><B><%=toDecimal2(fltSum)%></TD>
		</TR>
<%
				'合計を初期化
				fltSum = 0
			End If
		End If
	Loop
%>

	</TABLE>
</div>
</FORM>
</BODY>
</HTML>

<%
End Sub
%>

