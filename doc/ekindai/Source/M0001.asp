<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：M0001
'機能：社員マスタメンテナンス
'*********変更履歴*********
'日付			変更者		変更管理番号	理由
'2007/10/12　　　　　　　　高　　　　　　「全部門」で検索できるように

%>

<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
'	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<%

	'変数定義
	Dim rs,rs1,rs2							'レコードセット
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim strColor								'カラー

	'画面項目の格納変数定義
	Dim txtShainCD							'社員ｺｰﾄﾞ
	Dim txtShainNM							'社員氏名
	Dim txtShainKN							'社員フリカナ
	Dim cmbBumonCD							'部門ｺｰﾄﾞ
	Dim cmbShainKBN							'社員区分
	Dim txtPWD									'ﾊﾟｽﾜｰﾄﾞ
	Dim cmbKengen								'権限
	Dim txtShouninshaCD					'承認者
	Dim txtShouninshaCD1				'代理承認者
	Dim txtBikou								'備考
	
	'メールアドレス 入社日 201404
	Dim txtMailAddr						'メールアドレス
	Dim txtNyusyaDate					'入社日
	
	Dim txtShainCDList()					'社員ｺｰﾄﾞ配列
	Dim txtShainNMList()					'社員氏名配列
	Dim txtShainKNList()					'社員フリカナ配列
	Dim cmbBumonCDList()					'部門ｺｰﾄﾞ配列
	Dim cmbShainKBNList()				'社員区分配列
	Dim txtPWDList()							'ﾊﾟｽﾜｰﾄﾞ配列
	Dim cmbKengenList()					'権限配列
	Dim txtShouninshaCDList()				'承認者配列
	Dim txtShouninshaCD1List()			'代理承認者配列
	Dim txtBikouList()							'備考配列
	
	'メールアドレス 入社日 201404
	Dim txtMailAddrList()						'メールアドレス配列
	Dim txtNyusyaDateList()					'入社日配列
	
	Dim chkSelList						'選択
	
	'社員区分
	Dim CONST_SHAINKBN_List(5)
	CONST_SHAINKBN_List(0) = ""
	CONST_SHAINKBN_List(1) = "プロパー"
	CONST_SHAINKBN_List(2) = "契約社員"
	CONST_SHAINKBN_List(3) = "出向者"
	CONST_SHAINKBN_List(4) = "退職者"
	CONST_SHAINKBN_List(5) = "その他"

	'権限
	Dim CONST_KENGEN_List(4)
	CONST_KENGEN_List(0) = ""
	CONST_KENGEN_List(1) = "一般利用"
	CONST_KENGEN_List(2) = "勤怠承認"
	CONST_KENGEN_List(3) = "総務担当"
	CONST_KENGEN_List(4) = "システム管理"
	
	'部門リスト取得
	Dim bumonCdList()
	Dim bumonNMList()
	Set rs = session("kintai").Execute(SELM0002())
	
	If rs.RecordCount = 0 then
		response.end
	End If
	
	Redim bumonCdList(rs.RecordCount)
	Redim bumonNMList(rs.RecordCount)
	bumonCdList(0) = ""
	bumonNMList(0) = "全部門"

	For cnt = 1 to rs.RecordCount
		bumonCdList(cnt) = rs("BUMONCD")
		bumonNMList(cnt) = rs("BUMONNM")
		rs.MoveNext
	Next

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合	'初期表示を行う
	If strMode = Empty Or strMode = "0" Then
		Call initProc()
	'実行モードが登録の場合、登録処理を行う
	elseIf strMode = "1" Then
		Call insertProc()
	'実行モードが更新の場合、更新処理を行う
	elseIf strMode = "2" Then
		Call updateProc()
	'実行モードが削除の場合
	elseIf strMode = "3" Then
		Call deleteProc()
	'検索処理を行う
	elseIf strMode = "4" Then
		Call searchProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'実行モードを取得
	strMode = request.querystring("mode")

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'画面の値をセット
	Call setScreenValue(Null)

	'共通処理
	Call commonProc()
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	searchProc()
'PROCEDURE機能：検索処理を行う
'引数：なし
'****************************************************
Sub searchProc()

	'画面の値を取得
	Call getScreenValue()
	
	'データ取得
	Set rs = session("kintai").Execute(SELM0001_2(txtShainCD,txtShainNM,cmbBumonCD,cmbKengen,cmbShainKBN))

	'画面の値をセット
	Call setScreenValue(rs)

	'共通処理
	Call commonProc()
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データの追加を行う
'引数：なし
'****************************************************
Sub insertProc()

	'画面の値を取得
	Call getScreenValue()

	If txtShainCD = Empty or txtShainNM = Empty or cmbBumonCD = Empty or (txtMailAddr = Empty and (cmbShainKBN = "1" or cmbShainKBN = "2")) Then
		errMSG = "必須項目を入力してください。"
		'画面の値をセット
		Call commonProc()
		Exit Sub
	End If
	
	'重複ﾁｪｯｸ
	Set rs = session("kintai").Execute(SELM0001(txtShainCD))
	If rs.RecordCount > 0 Then
		'Error処理
		errMSG = WSE0003_1
		Call commonProc()
		Exit Sub
	End If
	
	strSQL = INSM0001( _
		txtShainCD, _
		txtShainNM, _
		txtShainKN, _
		cmbShainKBN, _
		cmbBumonCD, _
		txtPWD, _
		cmbKengen, _
		0, _
		txtShouninshaCD, _
		"", _
		txtShouninshaCD1, _
		"", _
		txtBikou, _
		txtMailAddr, _
		txtNyusyaDate)

	'エラー制御開始
'	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans
	'***SQL実行
	session("kintai").Execute (strSQL)
	If Err <> 0 Then
		session("kintai").RollBackTrans
		'ヘッダー設定
		errMSG = "社員マスタ" & WSE0000
		Call commonProc()
		Exit Sub
	End If
	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

	'初期表示
	Call searchProc()

	'レコードセット開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データの更新を行う
'引数：なし
'****************************************************
Sub updateProc()
	Dim rShainNM,rShainNM1,rBumonCD,rBumonNM,rKengen
	
	'画面の値を取得
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	For cnt = 1 To UBOUND(txtShainCDList)
		'ﾁｪｯｸされている行のみ処理する
		If chkSelList(cnt) = "on" Then
			'If txtShainNMList(cnt) = Empty or cmbBumonCD = Empty Then
			If txtShainNMList(cnt) = Empty or (txtMailAddrList(cnt) = Empty and (cmbShainKBNList(cnt) = "1" or cmbShainKBNList(cnt) = "2")) Then
				errMSG = cnt & "番目の社員情報の必須項目を入力してください。"
				'画面の値をセット
				Call commonProc()
				Exit Sub
			End If
			Call getShainInfo(txtShouninshaCDList(cnt),rShainNM,rBumonCD,rBumonNM,rKengen)
			Call getShainInfo(txtShouninshaCD1List(cnt),rShainNM1,rBumonCD,rBumonNM,rKengen)
		
			strSQL = UPDM0001_2(txtShainCDList(cnt),txtShainNMList(cnt),txtShainKNList(cnt),cmbShainKBNList(cnt),cmbBumonCDList(cnt),txtPWDList(cnt),cmbKengenList(cnt),txtShouninshaCDList(cnt),rShainNM,txtShouninshaCD1List(cnt),rShainNM1,txtBikouList(cnt),txtMailAddrList(cnt),txtNyusyaDateList(cnt))

			'** トランザクション開始
			session("kintai").BeginTrans
			'***SQL実行
			session("kintai").Execute (strSQL)
			If Err <> 0 Then
				session("kintai").RollBackTrans
				'ヘッダー設定
				errMSG = "社員マスタ" & WSE0000
				Call commonProc()
				Exit Sub
			End If
			'** トランザクション終了
			session("kintai").CommitTrans

		End If
	Next

	'エラー制御終了
	On Error Goto 0

	'初期表示
	Call searchProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	deleteProc()
'PROCEDURE機能：メール削除を行う
'引数：なし
'****************************************************
Sub deleteProc()

	'画面の値を取得
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	For cnt = 1 To UBOUND(txtShainCDList)
		'ﾁｪｯｸされている行のみ処理する
		If chkSelList(cnt) = "on" Then
			strSQL = DELM0001(txtShainCDList(cnt))

			'** トランザクション開始
			session("kintai").BeginTrans
			'***SQL実行
			session("kintai").Execute (strSQL)
			If Err <> 0 Then
				session("kintai").RollBackTrans
				'ヘッダー設定
				errMSG = "社員マスタ" & WSE0000
				Call commonProc()
				Exit Sub
			End If
			'** トランザクション終了
			session("kintai").CommitTrans

		End If
	Next

	'エラー制御終了
	On Error Goto 0

	'初期表示
	Call searchProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	commonProc()
'PROCEDURE機能：共通処理
'引数：なし
'****************************************************
Sub commonProc()

	'ヘッダー部分を表示する
	Call viewHeader()

	'詳細を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()
	cnt								 = request.form("hdnCnt")						'カウント数
	txtShainCD				 = request.form("txtShainCD")				'社員ｺｰﾄﾞ
	txtShainNM				 = request.form("txtShainNM")				'社員氏名
	txtShainKN				 = request.form("txtShainKN")				'社員フリカナ
	cmbBumonCD				 = request.form("cmbBumonCD")				'部門ｺｰﾄﾞ
	cmbShainKBN				 = request.form("cmbShainKBN")			'社員区分
	txtPWD						 = request.form("txtPWD")						'ﾊﾟｽﾜｰﾄﾞ
	cmbKengen					 = request.form("cmbKengen")				'権限
	txtShouninshaCD		 = request.form("txtShouninshaCD")	'承認者
	txtShouninshaCD1	 = request.form("txtShouninshaCD1")	'代理承認者
	txtBikou					 = request.form("txtBikou")					'備考
	
	'メールアドレス 入社日 201404
	txtMailAddr					 = request.form("txtMailAddr")					'メールアドレス
	txtNyusyaDate				 = request.form("txtNyusyaDate")					'入社日

	Redim txtShainCDList(cnt)
	Redim txtShainNMList(cnt)
	Redim txtShainKNList(cnt)
	Redim cmbBumonCDList(cnt)
	Redim cmbShainKBNList(cnt)
	Redim txtPWDList(cnt)
	Redim cmbKengenList(cnt)
	Redim txtShouninshaCDList(cnt)
	Redim txtShouninshaCD1List(cnt)
	Redim txtBikouList(cnt)
	
	'メールアドレス 入社日 201404
	Redim txtMailAddrList(cnt)
	Redim txtNyusyaDateList(cnt)
	
	Redim chkSelList(cnt)
	For cnt = 1 To UBound(txtShainCDList)
		txtShainCDList(cnt)				 = request.form("hdnShainCD" & cnt)					'社員ｺｰﾄﾞ配列
		txtShainNMList(cnt)				 = request.form("txtShainNM" & cnt)					'社員氏名配列
		txtShainKNList(cnt)				 = request.form("txtShainKN" & cnt)					'社員フリカナ配列
		cmbBumonCDList(cnt)				 = request.form("cmbBumonCD" & cnt)					'部門ｺｰﾄﾞ配列
		cmbShainKBNList(cnt)				 = request.form("cmbShainKBN" & cnt)				'社員区分配列
		txtPWDList(cnt)						 = request.form("txtPWD" & cnt)							'ﾊﾟｽﾜｰﾄﾞ配列
		cmbKengenList(cnt)					 = request.form("cmbKengen" & cnt)					'権限配列
		txtShouninshaCDList(cnt)		 = request.form("txtShouninshaCDList" & cnt)		'承認者配列
		txtShouninshaCD1List(cnt)	 = request.form("txtShouninshaCD1List" & cnt)		'代理承認者配列
		txtBikouList(cnt)					 = request.form("txtBikou" & cnt)						'備考配列
	
		'メールアドレス 入社日 201404
		txtMailAddrList(cnt)					 = request.form("txtMailAddr" & cnt)						'メールアドレス配列
		txtNyusyaDateList(cnt)					 = request.form("txtNyusyaDate" & cnt)						'入社日配列
	
		chkSelList(cnt)					 = request.form("chkSel" & cnt)						'備考配列
	Next

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(rs)
'PROCEDURE機能：画面の項目に値をセット
'引数：なし
'****************************************************
Sub setScreenValue(rs)

'	'画面の項目に値をセット
'	txtShainCD				 = Empty							'社員ｺｰﾄﾞ
'	txtShainNM				 = Empty							'社員氏名
'	txtShainKN				 = Empty							'社員フリカナ
'	cmbBumonCD				 = Empty							'部門ｺｰﾄﾞ
'	cmbShainKBN				 = Empty							'社員区分
'	txtPWD						 = Empty							'ﾊﾟｽﾜｰﾄﾞ
'	cmbKengen					 = Empty							'権限
'	txtShouninshaCD		 = Empty							'承認者
'	txtShouninshaCD1	 = Empty							'代理承認者
'	txtBikou					 = Empty							'備考
	
	'メールアドレス 入社日 201404
'	txtMailAddr					 = Empty					'メールアドレス
'	txtNyusyaDate				 = Empty					'入社日

	cnt = 0
	If isNull(rs) = false Then
		cnt = rs.RecordCount
	End If

	Redim txtShainCDList(cnt)
	Redim txtShainNMList(cnt)
	Redim txtShainKNList(cnt)
	Redim cmbBumonCDList(cnt)
	Redim cmbShainKBNList(cnt)
	Redim txtPWDList(cnt)
	Redim cmbKengenList(cnt)
	Redim txtShouninshaCDList(cnt)
	Redim txtShouninshaCD1List(cnt)
	Redim txtBikouList(cnt)
	
	'メールアドレス 入社日 201404
	Redim txtMailAddrList(cnt)
	Redim txtNyusyaDateList(cnt)

	For cnt = 1 To UBound(txtShainCDList)
		txtShainCDList(cnt)				 = rs("SHAINCD")					'社員ｺｰﾄﾞ配列
		txtShainNMList(cnt)				 = rs("SHAINNM")					'社員氏名配列
		txtShainKNList(cnt)				 = rs("SHAINKN")					'社員フリカナ配列
		cmbBumonCDList(cnt)				 = rs("BUMONCD")					'部門ｺｰﾄﾞ配列
		cmbShainKBNList(cnt)				 = rs("SHAINKBN")					'社員区分配列
		txtPWDList(cnt)						 = rs("PWD")							'ﾊﾟｽﾜｰﾄﾞ配列
		cmbKengenList(cnt)					 = rs("KENGEN")						'権限配列
		txtShouninshaCDList(cnt)		 = rs("SHOUNINSHACD")			'承認者配列
		txtShouninshaCD1List(cnt)	 = rs("SHOUNINSHACD1")			'代理承認者配列
		txtBikouList(cnt)					 = rs("BIKOU")						'備考配列
	
		'メールアドレス 入社日 201404
		txtMailAddrList(cnt)				= rs("MAILADDR")						'メールアドレス
		txtNyusyaDateList(cnt)				= rs("NYUSYADATE")						'入社日
		
		rs.MoveNext
	Next

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<U><B><FONT SIZE="3" COLOR="BLUE">社員マスタ　メンテナンス</FONT></B></U>
	</CENTER>
<div class="list">

	<table class="l-tbl">
		<TBODY>
			<TR>
				<TD><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
			</TR>
		</TBODY>
	</table>
	<table class="l-tbl">
			<col width="10%">
			<col width="23%">
			<col width="10%">
			<col width="23%">
			<col width="10%">
			<col width="24%">
		<TR>
			<TH class="l-cellsec">社員コード<font color="ff0000">(*)</font></TH>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" NAME="txtShainCD" SIZE="40" MAXLENGTH="40" VALUE="<%=txtShainCD%>" class="txt_imeoff">
			</TD>
			<TH class="l-cellsec">氏名<font color="ff0000">(*)</font>
			</TH>
			<TD class="l-cellodd">
			<INPUT TYPE="TEXT" NAME="txtShainNM" SIZE="40" MAXLENGTH="40" VALUE="<%=txtShainNM%>" class="txt_imeon">
			</TD>
			<TH class="l-cellsec">所属<font color="ff0000">(*)</font></TH>
			<TD class="l-cellodd">
				<SELECT NAME="cmbBumonCD">

<%
		For cnt1 = 0 To UBound(bumonCDList)
			strFormat = Empyt
			If StrComp(cmbBumonCD,bumonCDList(cnt1)) = 0 Then
				strFormat = "SELECTED"
			End If
%>
						<OPTION <%=strFormat%> VALUE="<%=bumonCDList(cnt1)%>"><%=bumonNMList(cnt1)%></OPTION>
<%
		Next
%>
					</SELECT>
			</TD>


		</TR>
		<TR>
			<TH class="l-cellsec">区分</FONT>
			</TH>
			<TD class="l-cellodd">
				<SELECT NAME="cmbShainKBN">
<%
	For cnt = 0 To UBound(CONST_SHAINKBN_List)
			strFormat = Empyt
			If StrComp(cmbShainKBN,cnt) = 0 Then
				strFormat = "SELECTED"
			End If
%>
					<OPTION <%=strFormat%> VALUE="<%=cnt%>"><%=CONST_SHAINKBN_List(cnt)%></OPTION>
<%
	Next
%>
				</SELECT>
			</TD>
			<TH class="l-cellsec">フリカナ</FONT>
			</TH>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" NAME="txtShainKN" SIZE="40" MAXLENGTH="40" VALUE="<%=txtShainKN%>" class="txt_imeon">
			</TD>


			<TH class="l-cellsec">権限</TH>
			<TD class="l-cellodd">
					<SELECT NAME="cmbKengen">
<%
	For cnt = 0 To UBound(CONST_KENGEN_List)
			strFormat = Empyt
			If StrComp(cmbKengen,cnt) = 0 Then
				strFormat = "SELECTED"
			End If
%>
					<OPTION <%=strFormat%> VALUE="<%=cnt%>"><%=CONST_KENGEN_List(cnt)%></OPTION>
<%
		Next
%>
					</SELECT>
			</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">パスワード</FONT></TD>
			<TD class="l-cellodd"><INPUT NAME="txtPWD" SIZE="20" MAXLENGTH="20" VALUE="<%=txtPWD%>" class="txt"></TD>
			<TH class="l-cellsec">承認者</FONT></TD>
			<TD class="l-cellodd">
			<INPUT TYPE="TEXT" NAME="txtShouninshaCD" SIZE="33" MAXLENGTH="40" VALUE="<%=txtShouninshaCD%>" class="txt_imeoff">
			<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtShouninshaCD')" class="i-btn">
			</TD>
			<TH class="l-cellsec">代理承認者</FONT></TD>
			<TD class="l-cellodd">
			<INPUT TYPE="TEXT" NAME="txtShouninshaCD1" SIZE="33" MAXLENGTH="40" VALUE="<%=txtShouninshaCD1%>" class="txt_imeoff">
			<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtShouninshaCD1')" class="i-btn">
			</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">メールアドレス</FONT><font color="ff0000">(*)</font></TH>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" NAME="txtMailAddr" SIZE="30" MAXLENGTH="40" VALUE="<%=txtMailAddr%>" class="txt_imeon"></TD>
			<TH class="l-cellsec">入社日</FONT></TH>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" NAME="txtNyusyaDate" SIZE="10" MAXLENGTH="10" VALUE="<%=txtNyusyaDate%>" class="txt_imeon"> YYYY/MM/DD </TD>
			<TH class="l-cellsec">備考</FONT></TH>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" NAME="txtBikou" SIZE="40" MAXLENGTH="100" VALUE="<%=txtBikou%>" class="txt_imeon"></TD>
		</TR>
	<table>
	<table class="l-tbl">
		<TBODY>
			<TR>
				<TD WIDTH="50%">
				</TD>
				<TD WIDTH="*%">
				<INPUT TYPE="BUTTON" VALUE="検索" onClick="SubmitM0001(4)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="登録" onClick="SubmitM0001(1)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="更新" onClick="SubmitM0001(2)" class="s-btn">
			 	<INPUT TYPE="BUTTON" VALUE="削除" onClick="SubmitM0001(3)" class="s-btn">
				</TD>
			</TR>
		</TBODY>
	</table>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：社員一覧表示
'引数：あり
'****************************************************
Sub viewDetail()
%>
<div class="list">

	<table class="l-tbl">
		<col width="3%">
		<col width="20%">
		<col width="18%">
		<col width="18%">
		<col width="12%">
		<col width="17%">
		<col width="12%">
		<TR>
			<TH class="l-cellsec" ROWSPAN="2"></TH>
			<TH class="l-cellsec">社員コード<font color="ff0000">(*)</font></TH>
			<TH class="l-cellsec">氏名<font color="ff0000">(*)</font></TH>
			<TH class="l-cellsec">フリカナ</TH>
			<TH class="l-cellsec">区分</TH>
			<TH class="l-cellsec">所属<font color="ff0000">(*)</font></FONT></TH>
			<TH class="l-cellsec">権限</TH>
		</TR>
		<TR>
			<TH class="l-cellsec">パスワード</TH>
			<TH class="l-cellsec">承認者</TH>
			<TH class="l-cellsec">代理承認者</TH>
			<TH class="l-cellsec">メールアドレス<font color="ff0000">(*)</font></TH>
			<TH class="l-cellsec">入社日</TH>
			<TH class="l-cellsec">備考</TH>
		</TR>
	</table>

  <div class="l-tbl-auto" style="height:400px">
	<table class="l-tbl">
		<col width="3%">
		<col width="20%">
		<col width="18%">
		<col width="18%">
		<col width="10%">
		<col width="17%">
		<col width="14%">
<%
	For cnt = 1 To UBound(txtShainCDList)
		if cnt mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
%>
			<TR>
				<TD class=<%=cellClass%> rowspan="2"><%=cnt%><BR><INPUT TYPE="CHECKBOX" NAME="chkSel<%=cnt%>"></TD>
				<TD class=<%=cellClass%>><%= txtShainCDList(cnt) %></FONT>
				<!-----------------------HIDDEN項目：--------------------->
				<INPUT TYPE="HIDDEN" NAME="hdnShainCD<%=cnt%>" VALUE="<%=txtShainCDList(cnt)%>">
				</TD>
				<TD class=<%=cellClass%>><INPUT TYPE="TEXT" SIZE="30" MAXLENGTH="40" NAME="txtShainNM<%=cnt%>" VALUE="<%=txtShainNMList(cnt)%>" class="txt_imeon"></TD>
				<TD class=<%=cellClass%>><INPUT TYPE="TEXT" SIZE="30" MAXLENGTH="40" NAME="txtShainKN<%=cnt%>" VALUE="<%=txtShainKNList(cnt)%>" class="txt_imeon"></TD>
				<TD class=<%=cellClass%>>
					<SELECT NAME="cmbShainKBN<%=cnt%>">
<%
		For cnt1 = 1 To UBound(CONST_SHAINKBN_List)
			strFormat = Empyt
			If StrComp(cmbShainKBNList(cnt),cnt1) = 0 Then
				strFormat = "SELECTED"
			End If
%>
						<OPTION <%=strFormat%> VALUE="<%=cnt1%>"><%=CONST_SHAINKBN_List(cnt1)%></OPTION>
<%
		Next
%>
					</SELECT>
				</TD>
				<TD class=<%=cellClass%>>
					<SELECT NAME="cmbBumonCD<%=cnt%>">
<%
		For cnt1 = 1 To UBound(bumonCDList)
			strFormat = Empyt
			If StrComp(cmbBumonCDList(cnt),bumonCDList(cnt1)) = 0 Then
				strFormat = "SELECTED"
			End If
%>
						<OPTION <%=strFormat%> VALUE="<%=bumonCDList(cnt1)%>"><%=bumonNMList(cnt1)%></OPTION>
<%
		Next
%>
					</SELECT>
				</TD>
				<TD class=<%=cellClass%>>
					<SELECT NAME="cmbKengen<%=cnt%>">
<%
		For cnt1 = 1 To UBound(CONST_KENGEN_List)
			strFormat = Empyt
			If  StrComp(cmbKengenList(cnt),cnt1) = 0 Then
				strFormat = "SELECTED"
			End If
%>
						<OPTION <%=strFormat%> VALUE="<%=cnt1%>"><%=CONST_KENGEN_List(cnt1)%></OPTION>
<%
		Next
%>
					</SELECT>
				</TD>
			</TR>
			<TR>
				<TD class=<%=cellClass%>><INPUT SIZE="15" MAXLENGTH="15" NAME="txtPWD<%=cnt%>" VALUE="<%=txtPWDList(cnt)%>" class="txt_imeoff"></TD>
				<TD class=<%=cellClass%>>
				<INPUT TYPE="TEXT" SIZE="24" MAXLENGTH="40" NAME="txtShouninshaCDList<%=cnt%>" VALUE="<%=txtShouninshaCDList(cnt)%>" class="txt_imeoff">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtShouninshaCDList<%=cnt%>')" class="i-btn">
				</TD>
				<TD class=<%=cellClass%>>
				<INPUT TYPE="TEXT" SIZE="24" MAXLENGTH="40" NAME="txtShouninshaCD1List<%=cnt%>" VALUE="<%=txtShouninshaCD1List(cnt)%>" class="txt_imeoff">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtShouninshaCD1List<%=cnt%>')" class="i-btn">
				</TD>
				<TD class=<%=cellClass%>><INPUT TYPE="TEXT" SIZE="16" MAXLENGTH="40" NAME="txtMailAddr<%=cnt%>" VALUE="<%=txtMailAddrList(cnt)%>" class="txt_imeon"></TD>
				<TD class=<%=cellClass%>><INPUT TYPE="TEXT" SIZE="10" MAXLENGTH="20" NAME="txtNyusyaDate<%=cnt%>" VALUE="<%=txtNyusyaDateList(cnt)%>" class="txt_imeon"></TD>
				<TD class=<%=cellClass%>><INPUT TYPE="TEXT" SIZE="14" MAXLENGTH="100" NAME="txtBikou<%=cnt%>" VALUE="<%=txtBikouList(cnt)%>" class="txt_imeon"></TD>
			</TR>
<%
	Next
%>
	</table>
	</div>
</div>
<!-----------------------隠し項目--------------------->
<INPUT TYPE="HIDDEN" NAME="hdnCnt" VALUE="<% =UBound(txtShainCDList) %>">
</FORM>
</BODY>
</HTML>
<%
End Sub
%>


