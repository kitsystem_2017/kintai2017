<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WS0002
'機能：勤怠の入力
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<link href="css/common.css" rel="stylesheet" type="text/css">
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim strNengapi							'年月日
	Dim strNengetu							'年月
	Dim strNendo								'年度
	Dim strSysDate							'システム年月日

	'初期表示を行う
	Call initProc()

	response.end
%>


<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	If strNengetu = Empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'年月を取得できない場合はシステム日付を付与する
	If strNengetu = Empty then
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
	end if
	
	'****パラメータ取得******
	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = Empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle("お知らせ",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'エラーメッセージ部分を表示する
	Call viewHeader()


	'日別勤怠表とスケジュール表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,1,strShainCD,strNengetu)

	'行先掲示＆スケジュールを表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing
	Set rs1 = Nothing

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<TABLE WIDTH="960" BORDER="0">
		<TBODY>
			<TR>
			</TR>
		</TBODY>
	</TABLE>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：勤怠入力
'引数：なし
'****************************************************
Sub viewDetail()
%>

<TD WIDTH="*%" VALIGN="TOP" align="left">
  <div class="list">
	<table class="l-tbl">
	 <TR>
		 <TD>
		 	<span class="txt-emphasis">
				&diams;&nbsp;2013年9月1日<br>
			</span>
		 	<span class="txt-blue">
				<u>
				重要！メールサーバの変更について
				</u>
			</span>
		 	<span class="txt-black"><br>
				&nbsp;&nbsp;
				メールサーバが変わりました、いままで自社のメールサーバを使用していましたが、本日より
				レンタルサーバのメールサーバを導入しましたので、メールの設定を変更する必要があります。
				<br>
				●MicroSoft　OutLookなどといったメーラーでクライアントで受信する場合
				<br>
				アカウント名：いままでと同様。
				<br>
				パスワード：一律 symm01 と設定しています、WebMailにログインして、変更してください。
				<br>
				●WebMailの使用
				<br>
				URL:右上のリンクSymmetrixWebMailからアクセスできます。
				<br>
				アカウント名：いままでと同様。
				<br>
				パスワード：一律 symm01 と設定しています、WebMailにログインして、変更してください。
				<br>
				新しいサーバには以下のような便利な機能があります。
				<br>
				�@パスワードの更新。
				<br>
				�A転送の指定。
				<br>
				などなどがあります、いろいろ試してみてください。不明な点がありましたら、skatsu@symmetrix.co.jp(090-9857-3837)まで問い合わせてください。
			</span>
		 </TD>
	 </TR>	 <TR>
		 <TD>
		 	<span class="txt-emphasis">
				&diams;&nbsp;2010年9月14日<br>
			</span>
		 	<span class="txt-blue">
				<u>
				重要！メールサーバについて
				</u>
			</span>
		 	<span class="txt-black"><br>
				&nbsp;&nbsp;
				メールサーバが変わりました、いままで「PMail」というメールサーバを使用していましたが、本日より
				より強力な性能を持つメールサーバ「XMail」を導入しましたので、メールの設定を変更する必要があります。
				<br>
				●MicroSoft　OutLookなどといったメーラーでクライアントで受信する場合
				<br>
				アカウント名：今までユーザ名のみ（例：info）でしたが、今度からドメイン名も付ける必要があります（例：info@symmetrix.co.jp）。
				<br>
				パスワード：一律 symm01 と設定しています、WebMailにログインして、変更してください。
				<br>
				●WebMailの使用
				<br>
				URL: http://www.symmetrix.co.jp/xmail/（右上のリンクSymmetrixWebMailからアクセスできます。）
				<br>
				メールアドレス:今までユーザ名のみ（例：info）でしたが、今度からドメイン名も付ける必要があります（例：info@symmetrix.co.jp）。
				<br>
				パスワード：一律 symm01 と設定しています、WebMailにログインして、変更してください。
				<br>
				「XMail」のWebMailは以下のような便利な機能があります。
				<br>
				�@パスワードの更新。
				<br>
				�A転送の指定。
				<br>
				などなどがあります、いろいろ試してみてください。不明な点がありましたら、skatsu@symmetrix.co.jp(090-9857-3837)まで問い合わせてください。
			</span>
		 </TD>
	 </TR>
	<table class="l-tbl">
  </div>
</TD>
<!--**********************勤怠入力表示(E)****************************-->
</TR>
</TABLE>
</CENTER>

<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" 	VALUE="<%=strShainCD%>">
<INPUT TYPE="HIDDEN" NAME="txtBumonCD" 	VALUE="<%=strBumonCD%>">
<INPUT TYPE="HIDDEN" NAME="txtNengetu" 	VALUE="<%=strNengetu%>">
</CENTER>
</FORM>
</BODY>
</HTML>


<%
End Sub
%>



