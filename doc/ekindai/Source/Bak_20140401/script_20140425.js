<!---
//**********共通部分のスクリプト(S)**********
//window.document.onkeydown = getKeyCode;
//window.document.onmousedown = prohibitRightClick;
//キー制御
function getKeyCode() {
    var keycode = event.keyCode;
    var Alt = event.altKey;
    var Ctrl = event.ctrlKey;
    //Enterキー
    if(keycode == 13){
        //INPUTタグのtype=text,password,file,radio,checkboxでは、使用不可
        if(window.event.srcElement.tagName == "INPUT"){
            if(window.event.srcElement.type == "text" ||
               window.event.srcElement.type == "password" ||
               window.event.srcElement.type == "file" ||
               window.event.srcElement.type == "radio" ||
               window.event.srcElement.type == "checkbox"){
                event.keyCode = 0;
                return false;
            }
        }
        return true;
    }
    //BackSpaceキー
    if(keycode == 8){
        //INPUTタグのtype=text,password,fileでは、使用可能
        if(window.event.srcElement.tagName == "INPUT"){
            if((window.event.srcElement.type == "text" && window.event.srcElement.readOnly == false) || 
               (window.event.srcElement.type == "password" && window.event.srcElement.readOnly == false) ||
               window.event.srcElement.type == "file"){
                return true;
            }
        }
        //TEXTAREAタグでは、使用可能
        if(window.event.srcElement.tagName == "TEXTAREA" && window.event.srcElement.readOnly == false){
            return true;
        }
        event.keyCode = 0;
        return false;
    }
    //Altショートカットキー（backspace・←・→・HOME）

    if ( Alt ) {
      if ( keycode == 8 || keycode == 37 || keycode == 39 || keycode == 36 ) {
           alert("ショートカットキーは使用できません。");
           return false;
      }
    }
    //Ctrlショートカットキー（N）

    if ( Ctrl ) {
      if  ( keycode == 78 ) {
            alert("ショートカットキーは使用できません。");
            return false;
      }
    }
    //F5キー
    if(keycode == 116){
        event.keyCode = 0;
        return false;
    }
    //F11キー
    if(keycode == 122){
        event.keyCode = 0;
        return false;
    }
    //Escキー
    if(keycode == 27){
        event.keyCode = 0;
        return false;
    }
}

//右クリック禁止
function prohibitRightClick(){
  if(event.button == 2 || event.button == 3 || event.button == 6 || event.button == 7){
    alert("右クリックは使用できません。");
    return false;
  }
}

//タイムチェック
function judgeTime(elm){
	var errMSG = "入力エラー,'HHMM' or 'HH:MM'方式で入力してください。";
	var errMSG1 = "15分単位で入力してください。";
	var errMSG2 = "深夜24:00以上は00:00から入力してください。";
	var errMSG3 = "半角英数字で入力してください。"
	var str = elm.value;

	//未入力の場合は処理しない
	if(str.length == 0){
		return;
	}

	//全角文字チェック
	if (judgeZenkaku(elm)==false){
		return;
	};

	//4桁未満5桁以上はエラーとする
	if((str.length < 4) || (str.length > 5)){
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}

	//4桁はHH:MMに変換して返す
	if(str.length == 4){
		//":"が入っている場合
		if(str.indexOf(":") != -1){
			//HH:MMに変換して返す
			str = "0" + str
		//":"が入っていない場合
		}else{
			//HH:MMに変換して返す
			str = str.substr(0,2) + ":" + str.substr(2,2);
		}
	}
	//5桁の場合は
	if(str.length == 5){
		//3桁目は":"でない場合はエラー
		if(str.indexOf(":") != 2){
			alert(errMSG);
			elm.value = "";
			elm.focus();
			return;
		}

		j = 0;
		for (i = 0; i < str.length; i++){
			if (str.charAt(i) == ":"){
				j++;
			}
		}
		//":"が入っていない場合又二つ以上入っている場合エラー
		if(j == 0 || j > 1){
			alert(errMSG);
			elm.value = "";
			elm.focus();
			return;
		}
	}
	//15分刻みでない場合はエラー
	if((str.substr(3) != "00") && (str.substr(3) != "15") && (str.substr(3) != "30") && (str.substr(3) != "45")){
		alert(errMSG1);
		elm.value = "";
		elm.focus();
		return;
	};

	//24以上入力された場合はエラー
	if(str.substr(0,2) >= 24) {
		alert(errMSG2);
		elm.value = "";
		elm.focus();
		return;
	};

	//変換後の値をセット
	elm.value = str;
	return;
}
//数値チェック
function judgeNumber(elm){
	var str;
	var errMSG = "正しい数値を入力してください。";
	var errMSG1 = "0.25単位で入力してください。";
	var errMSG2 = "半角英数字で入力してください。"

	str = elm.value;

	//未入力の場合は処理しない
	if((str.length == 0) || (str == "0")){
		return;
	}

	//全角文字チェック
	if (judgeZenkaku(elm)==false){
		return;
	};

	//数値チェック
	if (isNaN(str)) {
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}
	//数値チェック
	if ((str % 0.25) != 0) {
		alert(errMSG1);
		elm.value = "";
		elm.focus();
		return;
	}
/*
	//"."が入っている場合
	i = str.indexOf(".");
	if(i != -1){
		if(str.substr(i+1).length == 1){
			str = elm.value + "0";
		}
	//"."が入っていない場合
	}else{
		//99.99に変換して返す
		str = elm.value + ".00";
	}
*/
	//変換後の値をセット
	elm.value = str;

	return;
}

//数値チェック
function judgeNumber2(elm){
	var str;
	var errMSG = "半角英数字で入力してください。\n日付項目は8桁(YYYYMMDD)で入力してください。";
	str = elm.value;

	//未入力の場合は処理しない
	if((str.length == 0) || (str == "0")){
		return;
	}

	//全角文字チェック
	if (judgeZenkaku(elm)==false){
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	};

	//数値チェック
	if (isNaN(str)) {
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}

	return;
}
//数値チェック
function judgeNumber1(elm){
	var str;
	var errMSG = "正しい数値を入力してください。";
	var errMSG1 = "0.25単位で入力してください。";
	var errMSG2 = "半角英数字で入力してください。"

	str = elm.value;

	//未入力の場合は処理しない
	if(str.length == 0){
		return;
	}

	//全角文字チェック
	if (judgeZenkaku(elm)==false){
		return;
	};

	//数値チェック
	if (isNaN(str)) {
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}

	return;
}


//全角判定
function judgeZenkaku(elm){
	var errMSG = "半角英数字で入力してください。"
	var dore;
	for(i=0; i<elm.value.length; i++){
		dore=escape(elm.value.charAt(i));
		if(dore.length>3 && dore.indexOf("%")!=-1){
			alert(errMSG);
			elm.value = "";
			elm.focus();
			return false;
		}
	}
	return true;
}

//すべてチェック（外す）
function checkAll(cnt){
	//すべてチェック
	if(document.bottom.chkall.checked == true){
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk"+i].checked = true;
		}
		return;
	}else{
		//すべて外す
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk"+i].checked = false;
		}
		return;
	}
}

//すべてチェック（外す）
function checkAll1(cnt){
	//すべてチェック
	if(document.bottom.chkall1.checked == true){
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk_1"+i].checked = true;
		}
		return;
	}else{
		//すべて外す
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk_1"+i].checked = false;
		}
		return;
	}
}


//社員検索画面開く
function _open(idx){
	nwin =window.open("T0001SW01.asp?idx="+idx,"T0001SW01","width=600,height=800,menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no");
	nwin.focus();
}

//プロジェクト情報検索画面開く
function _openWST0001SW001(idx){
	nwin =window.open("WST0001SW001.asp?idx="+idx,"WST0001SW001","width=600,height=800,menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no");
	nwin.focus();
}


//**********共通部分のスクリプト(E)**********

//**********勤怠入力画面のスクリプト(S)**********
function SubmitWST0001(type){
	//登録
	if(type == 0) {
		document.bottom.action="WST0001.asp?mode=update";
		document.bottom.submit();
		return;
	}
	//一ヶ月確認
	if(type == 1) {
		document.bottom.action="WST0001_1.asp";
		document.bottom.submit();
		return;
	}
	//プロジェクト設定
	if(type == 2) {
		document.bottom.action="WST0001_2.asp";
		document.bottom.submit();
		return;
	}
}

//**********勤怠入力画面のスクリプト(E)**********

//**********プロジェクト設定画面のスクリプト(S)**********
function SubmitWST0001_2(type){
	//クリア
	if(type == 3) {
		document.bottom.txtPJNO1.value = "";
		document.bottom.txtPJNO2.value = "";
		document.bottom.txtPJNO3.value = "";
		document.bottom.txtPJNO4.value = "";
		document.bottom.txtPJNO5.value = "";
		document.bottom.txtPJNO6.value = "";
		document.bottom.txtPJNO7.value = "";
		document.bottom.txtPJNO8.value = "";
		document.bottom.txtPJNO9.value = "";
		document.bottom.txtPJNO10.value = "";
		return;
	}
	//設定
	if(type == 1) {
		document.bottom.action="WST0001_2.asp?mode=1";
		document.bottom.submit();
		return;
	}
	//戻る
	if(type == 2) {
		document.bottom.action="WST0001_2.asp?mode=2";
		document.bottom.submit();
		return;
	}
}

//**********勤怠入力画面のスクリプト(E)**********

//**********勤務条件画面のスクリプト(S)**********
function SubmitWSM0002(){
	if(confirm('確定してもよろしいですか？')){
		document.bottom.action="WSM0002.asp?mode=update";
		document.bottom.submit();
	}
	else{}
}
//**********勤務条件画面のスクリプト(E)**********

//**********勤務条件画面のスクリプト(S)**********
function SubmitWST0005(type){
	document.bottom.target="bottom";
	document.bottom.action="WST0005.asp?mode="+type;
	document.bottom.submit();
	return;
}
//**********勤務条件画面のスクリプト(E)**********

function SubmitWSM0004(type){
	if (type == "1"){
		if(!confirm("登録してもよろしいですか？")){return;}
	}
	if (type == "2"){
		if(!confirm("更新してもよろしいですか？")){return;}
	}
	if (type == "3"){
		if(!confirm("削除してもよろしいですか？")){return;}
	}
	if (type == "4"){
		if(!confirm("前年度有休データを取込む場合は今年度の有休データは全部リセットされますが、実行してもよろしいですか？")){return;}
	}
	document.bottom.target="bottom";
	document.bottom.action="WSM0004.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********社員設定画面のスクリプト(S)**********
function SubmitM0001(type){
	if (type == "1"){
		if(!confirm("登録してもよろしいですか？")){return;}
	}
	if (type == "2"){
		if(!confirm("更新してもよろしいですか？")){return;}
	}
	if (type == "3"){
		if(!confirm("削除してもよろしいですか？")){return;}
	}
	if (type == "4"){
	}
	document.bottom.target="bottom";
	document.bottom.action="M0001.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********社員設定画面のスクリプト(E)**********

//**********部門設定画面のスクリプト(S)**********
function SubmitM0002(type){
	if (type == "1"){
		if(!confirm("登録してもよろしいですか？")){return;}
	}
	if (type == "2"){
		if(!confirm("更新してもよろしいですか？")){return;}
	}
	if (type == "3"){
		if(!confirm("削除してもよろしいですか？")){return;}
	}
	if (type == "4"){
	}
	document.bottom.target="bottom";
	document.bottom.action="M0002.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********部門設定画面のスクリプト(E)**********

//**********プロジェクト設定画面のスクリプト(S)**********
function SubmitM0003(type){
	if (type == "1"){
		if(!confirm("登録してもよろしいですか？")){return;}
	}
	if (type == "2"){
		if(!confirm("更新してもよろしいですか？")){return;}
	}
	if (type == "3"){
		if(!confirm("削除してもよろしいですか？")){return;}
	}
	if (type == "4"){
	}
	document.bottom.target="bottom";
	document.bottom.action="M0003.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********プロジェクト設定画面のスクリプト(E)**********

//**********部門設定画面のスクリプト(E)**********


//**********定期券の変更届出及び交通費の精算申請画面のスクリプト(S)**********
function SubmitWST0006(type){
	if (type == "1"){
		if(!confirm("精算申請してもよろしいですか？")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0008.asp?mode=update";
		document.bottom.submit();
	}
	if (type == "2"){
		if(!confirm("精算申請してもよろしいですか？")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0009.asp?mode=update";
		document.bottom.submit();
	}
	if (type == "3"){
		if(!confirm("精算申請してもよろしいですか？")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0010.asp?mode=update";
		document.bottom.submit();
	}
	return;
}

function SubmitWST0006_1(type,mode){

	if (mode == "3"){
		if(!confirm("承認してもよろしいですか？")){return;}
	}
	
	if (mode == "4"){
		if(!confirm("却下してもよろしいですか？")){return;}
	}

	if (type == "1"){
		document.bottom.target="bottom";
		document.bottom.action="WST0008_1.asp?mode="+mode;
		document.bottom.submit();
	}
	if (type == "2"){
		document.bottom.target="bottom";
		document.bottom.action="WST0009_1.asp?mode="+mode;
		document.bottom.submit();
	}
	if (type == "3"){
		document.bottom.target="bottom";
		document.bottom.action="WST0010_1.asp?mode="+mode;
		document.bottom.submit();
	}
	return;
}
//**********定期券の変更届出及び交通費の精算申請画面のスクリプト(E)**********

//**********週間報告画面のスクリプト(S)**********
function SubmitWST0007(type,elm){

	str = elm.value;

	//未入力の場合は処理しない
	if(str.length == 0){
		alert("報告先メールアドレスを入力してください。")
		return;
	}

	if (type == "0"){
		if(!confirm("週間報告してもよろしいですか？")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0013.asp?mode=update";
		document.bottom.submit();
	}
	return;
}

function SubmitWST0007_1(mode){

	if (mode == "3"){
		if(!confirm("承認してもよろしいですか？")){return;}
	}
	
	if (mode == "4"){
		if(!confirm("却下してもよろしいですか？")){return;}
	}

	document.bottom.action="WST0013.asp?mode="+mode;
	document.bottom.submit();

	return;
}

//**********週間報告画面のスクリプト(E)**********

function _back(BackURL){
	document.bottom.action=BackURL;
	document.bottom.submit();
	return;
}


//--->

