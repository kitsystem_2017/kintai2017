<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0013
'機能：週報
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<link href="css/common.css" rel="stylesheet" type="text/css">
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim strNengapi							'年月日
	Dim strNengetu							'年月
	Dim strNendo								'年度
	Dim strSysDate							'システム年月日
	Dim strSysTime							'システム年月日
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim strColor								'カラー
	Dim chkFlag									'ﾁｪｯｸフラグ
	Dim strDateFrom       			'週間の開始日
	Dim strDateTo								'週間の終了日
	Dim intDateCNT							'週間の日数


	'画面項目の格納変数定義
	Dim lblNengapi(7)						'年月日
	Dim lblYoubi(7)							'曜日
	Dim txtCMT(7)							'コメント
	Dim txtMAILTO							'メール送信先
	Dim txtBIKOU							'備考
	Dim lblHOUKOKUSHA						'報告者
	Dim lblHOUKOKUBI						'報告日
	Dim hidSHOUNINSTATUS					'承認状態 2:申請　3:承認　4:却下
	Dim hidSHOUNINSHACD					'承認者コード
	Dim lblSHOUNINSHANM					'承認者氏名
	Dim lblSHOUNINDATE					'承認日
	Dim txtSHOUNINCMT					'承認者コメント
	Dim strShounishacd					'承認者コード

	Dim hdnBackURL							'戻るパス
	Dim strKengen1
	
	'承認者のメールアドレス取得 20140424 Start
	Dim rs2,rs3
	Dim searchSHOUNINSHACD
	Dim ShounishaNMList(2)
	Dim ShounishaAddrList(2)
	Dim selected
	'承認者のメールアドレス取得 20140424 End

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが更新の場合
	else
		'スケジュール更新処理を行う
		Call updateProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()
	Dim strTemp
	Dim intYoubi

	'****パラメータ取得******
	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = Empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if

	'実行モードを取得
	strMode = request.querystring("mode")

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	If strNengetu = Empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'年月を取得できない場合はシステム日付を付与する
	If strNengetu = Empty then
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
	end if

	'アドレスバーから週間の開始日、終了日を取得
	strDateFrom = trim(request.querystring("DateFrom"))
	strDateTo = trim(request.querystring("DateTo"))
	'アドレスバーから週間の開始日、終了日を取得出来ない場合はフォームから取得
	If strDateFrom = Empty Then
		strDateFrom = trim(request.form("txtDateFrom"))
		strDateTo = trim(request.form("txtDateTo"))
	End If

	strSysDate = Mid(Date,1,4) & Mid(Date,6,2) & Mid(Date,9,2)

	strSysTime = replace(Mid(now,12),":","")

	'アドレスバーからもフォームからも週間の開始日、終了日取得できない場合
	If strDateFrom = Empty Then
		'システム日付の所在週の開始日、終了日を計算
		intYoubi = getWeekDay(Date())

		strDateFrom = strSysDate - intYoubi + 1
		strDateTo = strSysDate - intYoubi + 7
		If Left(strDateFrom,6) < Left(strSysDate,6) Or Right(strDateFrom,2) = "00" Then
			strDateFrom = Left(strSysDate,6) & "01"
		End If
	End If
	'月末の終了日を判定
	strTemp = GetMaxMonthDate(strDateTo)
	If (strDateTo - strTemp) > 0 Then
		strDateTo = strTemp
	End If

	'週間の日数計算
	if strDateTo <> Empty then
		intDateCNT = strDateTo - strDateFrom + 1
	end if
	
	'年度計算
	strNendo = Left(strNengetu,4)
	If Mid(strNengetu,5,2) < 4 Then
		strNendo = strNendo - 1
	End If
	
	'アドレスバーから承認者を取得
	strShounishacd = trim(request.querystring("shounishacd"))
	If strShounishacd = Empty then
		strShounishacd = trim(request.form("shounishacd"))
	end if

'	'戻るパス
'	hdnBackURL = "WST0013.asp?shaincd=" & strShainCD & "&nengetu=" & strNengetu & "&datefrom=" & strDateFrom & "&dateto=" & strDateTo
	hdnBackURL = Session("backurl")

	'セッションに戻るパスを格納する
'	Session("backurl") = hdnBackURL

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle(toDate2(strNengetu) & "週間報告",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'週報からデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0007_1(strShainCD,strNengetu,strDateFrom,strDateTo))

	'日別勤怠表とスケジュール表からデータ取り出し
'	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))
	'カレンダーマスタから日付データを取得
	Set rs = session("kintai").Execute(SELWSM0001_1(0,strNengetu))

	'レコード存在しない場合
	If rs1.RecordCount > 0 Then
		'画面の項目に値をセット
		Call setScreenValue(rs1)

'		'勤怠情報を作成する
'		Call insertProc()
'		'週報からデータ取り出し
'		Set rs1 = session("kintai").Execute(SELWST0007_1(strShainCD,strNengetu,strDateFrom,strDateTo))
	Else
		'画面の項目に値をセット
		Call setScreenValue1(rs)
	End If
	
	'承認者のメールアドレス取得 20140424 Start
	'報告者のデータ取得
	Set rs2 = Nothing
	Set rs3 = Nothing
	Set rs2 = session("kintai").Execute(SELM0001_2(strShainCD,strShainNM,strBumonCD,strKengen,Empty))
	searchSHOUNINSHACD = rs2("SHOUNINSHACD")
	Set rs3 = session("kintai").Execute(SELM0001_2(searchSHOUNINSHACD,Empty,Empty,Empty,Empty))
	
	ShounishaAddrList(1) = rs3("MAILADDR")
	ShounishaNMList(1) = rs3("SHAINNM")
	
	Set rs3 = Nothing
	
	searchSHOUNINSHACD = rs2("SHOUNINSHACD1")
	Set rs3 = session("kintai").Execute(SELM0001_2(searchSHOUNINSHACD,Empty,Empty,Empty,Empty))
	
	ShounishaAddrList(2) = rs3("MAILADDR")
	ShounishaNMList(2) = rs3("SHAINNM")
	
	Set rs3 = Nothing
	Set rs2 = Nothing
	'承認者のメールアドレス取得 20140424 End

	'承認状態チェック
'	If rs1("SHOUNINSTATUS") = "2" Or rs1("SHOUNINSTATUS") = "3" Then
'		Set rs1 = Nothing
'		Response.Redirect "WST0001_1.asp?shaincd=" & strShainCD & "&nengetu=" & strNengetu
'	End If


	errMSG = "承認状態：" & CONST_SHOUNINSTATUS(hidSHOUNINSTATUS) & "　承認者：" & lblSHOUNINSHANM

	'エラーメッセージ部分を表示する
	Call viewHeader()

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide1(rs,1,strShainCD,strNengetu)

	'行先掲示＆スケジュールを表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing
	Set rs1 = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(pRs1)
'PROCEDURE機能：画面の項目に値をセット
'引数：
'				pRs1--日別勤怠表
'				pRs2--月別勤怠表
'****************************************************
Sub setScreenValue(pRs1)

	cnt = 1
	pRs1.MoveFirst
	'日別勤怠表から画面項目の格納変数に代入
	Do until pRs1.EOF
		If (pRs1("NENGAPI") - strDateFrom) >= 0 And (pRs1("NENGAPI") - strDateTo) <= 0  Then
			lblNengapi(cnt)				= pRs1("NENGAPI")			'年月日
			lblYoubi(cnt)				= pRs1("YOUBI")				'曜日
			txtCMT(cnt)					= pRs1("CMT")	'コメント
			txtMAILTO					= pRs1("MAILTO")		'メール送信先
			txtBIKOU					= pRs1("BIKOU")				'備考
			lblHOUKOKUSHA				= getShainNM(pRs1("SHAINCD"))		'報告者
			lblHOUKOKUBI				= mid(pRs1("UPDATEDATE"),1,8)		'報告日
			hidSHOUNINSTATUS			= pRs1("SHOUNINSTATUS")		'承認状態 2:申請　3:承認　4:却下
			hidSHOUNINSHACD				= pRs1("SHOUNINSHACD")	'承認者コード
			lblSHOUNINSHANM				= pRs1("SHOUNINSHANM")	'承認者氏名
			lblSHOUNINDATE				= pRs1("SHOUNINDATE")	'承認日
			txtSHOUNINCMT				= pRs1("SHOUNINCMT")	'承認者コメント
			cnt = cnt + 1
		End If
		pRs1.MoveNext
	Loop



End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue1(pRs1)
'PROCEDURE機能：画面の項目に値をセット
'引数：
'				pRs1--日別勤怠表
'				pRs2--月別勤怠表
'****************************************************
Sub setScreenValue1(pRs1)

	if mid(strDateFrom,1,6) <> strNengetu then
		strDateFrom = strNengetu & "01"
	end if

	cnt = 1
	pRs1.MoveFirst
	'日別勤怠表から画面項目の格納変数に代入
	Do until pRs1.EOF
		If (pRs1("NENGAPI") - strDateFrom) >= 0 Then
			lblNengapi(cnt)				= pRs1("NENGAPI")			'年月日
			lblYoubi(cnt)				= pRs1("YOUBI")				'曜日
			if  pRs1("YOUBI") = 7 then
				strDateTo = pRs1("NENGAPI")
				'週間の日数計算
				intDateCNT = strDateTo - strDateFrom + 1
				Exit sub
			end if
			cnt = cnt + 1
		End If
		pRs1.MoveNext
	Loop


End Sub
%>


<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()

	cnt = 1
	'画面項目を変数に代入
	For cnt = 1 To UBOUND(lblNengapi)
		lblNengapi(cnt)				= trim(request.form("lblNengapi" & cnt))			'年月日
		lblYoubi(cnt)				= trim(request.form("lblYoubi" & cnt))				'曜日
		txtCMT(cnt)					= trim(request.form("txtCMT" & cnt))	'コメント
		txtMAILTO					= trim(request.form("txtMAILTO"))		'メール送信先
		txtBIKOU					= trim(request.form("txtBIKOU"))				'備考
'		lblHOUKOKUSHA				= trim(request.form(""))		'報告者
'		lblHOUKOKUBI				= trim(request.form(""))		'報告日
'		hidSHOUNINSTATUS			= trim(request.form(""))		'承認状態 2:申請　3:承認　4:却下
'		hidSHOUNINSHACD				= trim(request.form(""))	'承認者コード
'		lblSHOUNINSHANM				= trim(request.form(""))	'承認者氏名
'		lblSHOUNINDATE				= trim(request.form(""))	'承認日
		txtSHOUNINCMT				= trim(request.form("txtSHOUNINCMT"))	'承認者コメント
	Next

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データの追加を行う
'引数：なし
'****************************************************
Sub insertProc()

	'エラー制御開始
'	On Error Resume Next

	'カレンダーマスタから日付データを取得
	Set rs = session("kintai").Execute(SELWSM0001_1(0,strNengetu))

	'レコード存在しない場合
	If rs.RecordCount = 0 Then
		errMSG = WSM0001E01
		'エラーメッセージ部分を表示する
		Call viewHeader()
		'処理を終了する
		response.End
	End If

	'** トランザクション開始
'	session("kintai").RollBackTrans
'	session("kintai").BeginTrans

	'週報情報を作成する
	Do until rs.EOF
		If (rs("NENGAPI") - strDateFrom) >= 0 And (rs("NENGAPI") - strDateTo) <= 0  Then
			'***SQL実行
			session("kintai").Execute (INSWST0007(strShainCD,rs("NENGAPI"),rs("NENGETU"),rs("NENDO"),rs("YOUBI")))

			If Err <> 0 Then
				session("kintai").RollBackTrans
				errMSG = WST0001E1
				'エラーメッセージ部分を表示する
				Call viewHeader()
				'処理を終了する
				response.End
			End If
		End If
		rs.MoveNext
	Loop

	rs.MoveFirst

	'** トランザクション終了
'	session("kintai").CommitTrans

	'レコードセット開放
	Set rs = Nothing

	'エラー制御終了
	On Error Goto 0

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データの更新を行う
'引数：なし
'****************************************************
Sub updateProc()

	'画面の情報を取得する
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'画面の情報を基づき計算を行い、DBを更新する
	If executeUpdate() = False Then
		'ロールバック
		session("kintai").RollBackTrans
	Else
		'** トランザクション終了
		session("kintai").CommitTrans
	End If

	if strMode <> "update" then
		Response.Redirect hdnBackURL
	end if


	'エラー制御終了
	On Error Goto 0

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)
	
	
	
	'承認者のメールアドレス取得 20140424 Start
	'報告者のデータ取得
	Set rs2 = Nothing
	Set rs3 = Nothing
	Set rs2 = session("kintai").Execute(SELM0001_2(strShainCD,strShainNM,strBumonCD,strKengen,Empty))
	searchSHOUNINSHACD = rs2("SHOUNINSHACD")
	Set rs3 = session("kintai").Execute(SELM0001_2(searchSHOUNINSHACD,Empty,Empty,Empty,Empty))
	
	ShounishaAddrList(1) = rs3("MAILADDR")
	ShounishaNMList(1) = rs3("SHAINNM")
	
	Set rs3 = Nothing
	
	searchSHOUNINSHACD = rs2("SHOUNINSHACD1")
	Set rs3 = session("kintai").Execute(SELM0001_2(searchSHOUNINSHACD,Empty,Empty,Empty,Empty))
	
	ShounishaAddrList(2) = rs3("MAILADDR")
	ShounishaNMList(2) = rs3("SHAINNM")
	
	Set rs3 = Nothing
	Set rs2 = Nothing
	'承認者のメールアドレス取得 20140424 End

	'メール送信
	Call sendMail1(strShainNM,txtMAILTO)

	'週報からデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0007_1(strShainCD,strNengetu,strDateFrom,strDateTo))

	'画面の項目に値をセット
	Call setScreenValue(rs1)	'ヘッダを表示する

	Call viewTitle(toDate2(strNengetu) & "週間報告",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'エラーメッセージ部分を表示する
	Call viewHeader()

	'日別勤怠表とスケジュール表からデータ取り出し
'	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))
	'カレンダーマスタから日付データを取得
	Set rs = session("kintai").Execute(SELWSM0001_1(0,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide1(rs,1,strShainCD,strNengetu)

	'勤怠表を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'FUNCTION名：	executeUpdate()
'FUNCTION機能：画面の項目を基づき勤怠情報を計算する
'引数：なし
'リターン値：あり
'						True			--成功
'						False			--失敗
'****************************************************
Function executeUpdate()

	'リターン値をTrueに設定
	executeUpdate = True

	'画面の項目にﾁｪｯｸを行う
	if strMode = "update" then
		If checkInput() = False Then
			executeUpdate = False
			Exit Function
		End If
	end if

	'週報からデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0007_1(strShainCD,strNengetu,strDateFrom,strDateTo))

	'レコード存在しない場合
	If rs1.RecordCount = 0 Then
		'勤怠情報を作成する
		Call insertProc()
	End If

	'週間日数分をロープ
	For cnt = 1 To intDateCNT

		'******日別勤怠テーブル更新
'UPDWST0007_1 = "UPDATE " & CONST_T_SHUUHOU & " SET CMT='" & pCMT & "', MAILTO='" & pMAILTO & "', BIKOU='" & pBIKOU & "' WHERE SHAINCD='" & pShainCD & "' AND NENGAPI='" & pNENGAPI &"'"
		if strMode = "update" then
			strSQL = UPDWST0007_1(strShaincd,lblNengapi(cnt),txtCMT(cnt),txtMAILTO,txtBIKOU,strSysDate & strSysTime)
		else
'(pShainCD,pNENGAPI,pSHOUNINSTATUS,pSHOUNINSHACD,pSHOUNINSHANM,pSHOUNINDATE,pSHOUNINCMT)
			strSQL = UPDWST0007_2(strShaincd,lblNengapi(cnt),strMode,strShounishacd,getShainNM(strShounishacd),strSysDate,txtSHOUNINCMT)
		end if

		'***SQL実行
		session("kintai").Execute (strSQL)
		If Err <> 0 Then
			errMSG = WST0001E1
			executeUpdate = False
			Exit Function
		End If

	Next

End Function
%>

<%
'****************************************************
'FUNCTION名：	checkInput()
'FUNCTION機能：画面の項目にﾁｪｯｸを行う
'引数：なし
'リターン値：あり
'						True			--成功
'						False			--失敗
'****************************************************
Function checkInput()
	checkInput = True
	'
	If txtMAILTO = Empty Then
		errMSG = "報告先メールアドレスを指定してください。"
		checkInput = False
		Exit Function
	End If
End Function
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<TABLE WIDTH="960" BORDER="0">
		<TBODY>
			<TR>
				<TD WIDTH="480" HEIGHT="23"><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
				</TD>
<%
	strFormat = ""
	if strShounishacd <> "" then 
		strFormat = "DISABLED"
	end if
%>
					<TD><INPUT TYPE="BUTTON" VALUE="報告" onClick="SubmitWST0007(0,txtMAILTO)" <%=strFormat%> class="s-btn">
<%
	strFormat = ""
	if strShounishacd = "" then 
		strFormat = "DISABLED"
	end if
%>
<INPUT TYPE="BUTTON" VALUE="承認" onClick="SubmitWST0007_1(3)"  <%=strFormat%> class="s-btn">
<INPUT TYPE="BUTTON" VALUE="却下" onClick="SubmitWST0007_1(4)"  <%=strFormat%> class="s-btn">
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="_back('<%=hdnBackURL%>')"  <%=strFormat%> class="s-btn">
				</TD>
			</TR>
		</TBODY>
	</TABLE>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：週間報告入力
'引数：なし
'****************************************************
Sub viewDetail()
%>
<%
	strFormat = ""
	if strShounishacd <> "" then 
		strFormat = "DISABLED"
	end if
%>

<TD WIDTH="*%" VALIGN="TOP">
  <div class="list">
	<table class="l-tbl">
	<col width="95%">
	 <TR>
		<TH class="l-cellsec">報告先メールアドレス：</TH>
	 </TR>
	</TABLE>
	<table class="l-tbl">
	<col width="95%">
	 <TR>
		<td class="l-cellodd">
			<SELECT NAME="txtMAILTO">
			<%
				For cnt1 = 1 To UBound(ShounishaAddrList)
					selected = Empyt
					If StrComp(txtMAILTO,ShounishaAddrList(cnt1)) = 0 Then
						selected = "SELECTED"
					End If
			%>
					<OPTION <%=selected%> VALUE="<%=ShounishaAddrList(cnt1)%>"><%=ShounishaNMList(cnt1)%></OPTION>
			<%
				Next
			%>
			</SELECT>
		</td>
	 </TR>
	</TABLE>
<br>
	<table class="l-tbl">
	<col width="10%">
	<col width="85%">
	 <TR>
		<TH class="l-cellsec">日付</TH>
		<TH class="l-cellsec">作業内容</TH>
	 </TR>
	</TABLE>
<% For cnt = 1 To intDateCNT
'		strColor = getYoubiColor(lblYoubi(cnt),lblKyuujituflg(cnt)) %>
	<table class="l-tbl">
	<col width="10%">
	<col width="85%">
	 <TR>
		<td class="l-cellodd"><font color="<%=strColor%>"><%=toDate4(lblNengapi(cnt))%>(<%=getYoubiKJ(lblYoubi(cnt))%>)</font>

		<!-----------------------HIDDEN項目：年月日、曜日、休日フラグ--------------------->
		<INPUT TYPE="HIDDEN" NAME="lblNengapi<%= cnt %>" VALUE="<% =lblNengapi(cnt) %>">
		<INPUT TYPE="HIDDEN" NAME="lblYoubi<%= cnt %>" VALUE="<% =lblYoubi(cnt) %>">
		
		
		</TD>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imeon" NAME="txtCMT<%= cnt %>" VALUE="<%= txtCMT(cnt) %>" SIZE="100" MAXLENGTH="60" <%=strFormat%>>
		</TH>
	 </TR>
	</TABLE>
<% Next %>
		<INPUT TYPE="HIDDEN" NAME="shounishacd" VALUE="<% =strShounishacd %>">
<br>

	<table class="l-tbl">
	<col width="10%">
	<col width="10%">
	<col width="80%">
	 <TR>
		<TH class="l-cellsec">報告者</TH>
		<TH class="l-cellsec">報告日</TH>
		<TH class="l-cellsec">週間総括：（300文字以内）</TH>
	 </TR>
	</TABLE>
	<table class="l-tbl">
	<col width="10%">
	<col width="10%">
	<col width="80%">
	 <TR>
		<td class="l-cellodd"><% =lblHOUKOKUSHA %>
		</td>
		<td class="l-cellodd"><% =toDate(lblHOUKOKUBI) %>
		</td>
		<td class="l-cellodd">
			<TEXTAREA NAME="txtBIKOU" ROWS="4" COLS="80" WRAP="SOFT" <%=strFormat%>><% =txtBIKOU%></TEXTAREA>
		</td>
	 </TR>
	</TABLE>
<br>

	<table class="l-tbl">
	<col width="10%">
	<col width="10%">
	<col width="80%">
	 <TR>
		<TH class="l-cellsec">承認者</TH>
		<TH class="l-cellsec">承認日</TH>
		<TH class="l-cellsec">承認者のコメント：（300文字以内）</TH>
	 </TR>
	</TABLE>
	<table class="l-tbl">
	<col width="10%">
	<col width="10%">
	<col width="80%">
	 <TR>
		<td class="l-cellodd"><% =lblSHOUNINSHANM %>
		</td>
		<td class="l-cellodd"><% =toDate(lblSHOUNINDATE) %>
		</td>
		<td class="l-cellodd">
<%
	strFormat = ""
	if strShounishacd = "" then 
		strFormat = "DISABLED"
	end if
%>
			<TEXTAREA NAME="txtSHOUNINCMT" ROWS="4" COLS="80" WRAP="SOFT" <%=strFormat%>><% =txtSHOUNINCMT %></TEXTAREA>
		</td>
	 </TR>
	</TABLE>

  </div>
</TD>
<!--**********************勤怠入力表示(E)****************************-->
</TR>
</TABLE>
</CENTER>

<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" 	VALUE="<%=strShainCD%>">
<INPUT TYPE="HIDDEN" NAME="txtBumonCD" 	VALUE="<%=strBumonCD%>">
<INPUT TYPE="HIDDEN" NAME="txtNengetu" 	VALUE="<%=strNengetu%>">
<INPUT TYPE="HIDDEN" NAME="txtDateFrom" VALUE="<%=strDateFrom%>">
<INPUT TYPE="HIDDEN" NAME="txtDateTo" 	VALUE="<%=strDateTo%>">
<INPUT TYPE="HIDDEN" NAME="hdnBackURL" 	VALUE="<%=hdnBackURL%>">
</CENTER>
</FORM>
</BODY>
</HTML>


<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewSide1(pRs,pParent,pShainCD,pNengetu)
'PROCEDURE機能：画面のカレンダー、メッセージ部分を表示する
'引数：あり
'			1:pRs			--レコードセット
'			2:pParent --呼び元  1:勤怠入力画面	2:行先＆スケジュール
'****************************************************
Sub viewSide1(pRs,pParent,pShainCD,pNengetu)
	Dim strDateFrom				'開始期間
	Dim strDateTo					'終了期間
	Dim strFont						'フォント
	Dim strColor					'カラー
	Dim cnt,cnt1
	Dim lst								'レコードセット


	'システム年月日を取得
	strSysDate = mid(date,1,4) & mid(date,6,2) & mid(date,9,2)
	pRs.movefirst
%>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="168" ALIGN="CENTER" VALIGN="TOP">
				<!--**********************カレンダー表示(S)****************************-->
			  <div class="list">
				<table class="l-tbl">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
					<TR>
						<TH class="l-cellcenter" COLSPAN="8">
								<A HREF="WST0013.asp?shaincd=<%= pShainCD %>&nengetu=<%= changeNengetu(pNengetu,0) %>&nengapi=<%= changeNengetu(pNengetu,0)&"01" %>&shounishacd=<%=strShounishacd%>">
								<IMG SRC=IMG/PREV.GIf BORDER="0"></A>
							 <% =TODATE2(pNengetu) %> </FONT>
								<A HREF="WST0013.asp?shaincd=<%= pShainCD %>&nengetu=<%= changeNengetu(pNengetu,1) %>&nengapi=<%= changeNengetu(pNengetu,1)&"01" %>&shounishacd=<%=strShounishacd%>">
								<IMG SRC=IMG/Next.GIf  BORDER="0">
						</TH>
					</TR>
					<TR>
						<TD class="l-cellodd"></TD>
						<TD class="l-cellodd">月</TD>
						<TD class="l-cellodd">火</TD>
						<TD class="l-cellodd">水</TD>
						<TD class="l-cellodd">木</TD>
						<TD class="l-cellodd">金</TD>
						<TD class="l-cellodd" style="background-color:AQUA;background-image:none;">土</TD>
						<TD class="l-cellodd" style="background-color:#F08080;background-image:none;">日</TD>
					</TR>

					<%
					'日付情報を格納の二次元配列初期化
					For cnt = 1 To 6
						For cnt1 = 1 To 7
							strDateaArr(cnt,cnt1) = ""
							strKadokbnArr(cnt,cnt1) = 0
							strYoubiArr(cnt,cnt1) = 0
						Next
					Next

					cnt1 = 1

					'二次元配列にデータ代入
					For cnt = 1 To pRs.RecordCount
						If pRs("YOUBI") - 1 = 0 and cnt > 1 Then
							cnt1 = cnt1 + 1
						End If
						'二次元配列にセット
						cnt2 = pRs("YOUBI") + 0
						strDateaArr(cnt1,cnt2) = mid(pRs("NENGAPI"),7)
						strKadokbnArr(cnt1,cnt2) = pRs("KYUUJITUFLG")
						strYoubiArr(cnt1,cnt2) = pRs("YOUBI")
						'strYotei(cnt1,cnt2) = pRs("SEQ")
						pRs.moveNext
					Next
					For cnt = 1 To 6
						If strDateaArr(cnt,1) = emtpy and cnt > 1 Then
							exit for
						End If
						'週間開始日、終了日を算出
						strDateFrom = pNengetu & strDateaArr(cnt,1)
						If strDateFrom = pNengetu Then
							strDateFrom = pNengetu & "01"
						End If
						strDateTo = pNengetu & strDateaArr(cnt,7)
						If strDateTo = pNengetu Then
							strDateTo = GetMaxMonthDate(strDateTo)
						End If
						%>
						<TR>
						<TD class="l-cellodd">
							<A HREF="WST0013.asp?shaincd=<%= pShainCD %>&nengetu=<%= pNengetu %>&DateFrom=<%= strDateFrom %>&DateTo=<%= strDateTo %>&shounishacd=<%=strShounishacd%>">
							<IMG SRC=IMG/PEN.GIF WIDTH="15" HEIGHT="15" BORDER="0"></A>
						</TD>
						<% For cnt1 = 1 To 7
							'色を取得
								strFont = ""
								'スケジュールある場合
								'If strYotei(cnt,cnt1) > 0 Then strFont = "<B><FONT SIZE='2'>" End If
								'今日のフォント強調
								If pNengetu & strDateaArr(cnt,cnt1) = strSysDate Then strFont = strFont & "<FONT SIZE='4'>" End If
'								strColor = getYoubiColor(strYoubiArr(cnt,cnt1),strKadokbnArr(cnt,cnt1))
								strColor = ""
								if strYoubiArr(cnt,cnt1) = 6 then
									strColor = "style='background-color:AQUA;background-image:none;'"
								elseif strYoubiArr(cnt,cnt1) = 7 then
									strColor = "style='background-color:#F08080;background-image:none;'"
								elseif strKadokbnArr(cnt,cnt1) = 1 then
									strColor = "style='background-color:#F08080;background-image:none;'"
								end if
								%>
								<TD class="l-cellodd" <%=strColor%>>
<!--
									<A HREF="WST0013.asp?shaincd=<%= pShainCD %>&nengetu=<%= pNengetu %>&nengapi=<%= pNengetu & strDateaArr(cnt,cnt1) %>&shounishacd=<%=strShounishacd%>">
-->
									<%=strFont%><%= toNumber(strDateaArr(cnt,cnt1)) %></FONT></A>
								</TD>
								<% strDateaArr(cnt,cnt1) = ""
							Next %>
						</TR>
					<% Next %>
				</TABLE>
				</div>
				<BR>
				<!--**********************カレンダー表示(E)****************************-->


			</TD>
<%
End Sub
%>


