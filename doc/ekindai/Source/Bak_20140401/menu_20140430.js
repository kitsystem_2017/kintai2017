//各画面表示
function go(){
	//お知らせ
	if (document.top.cmbMenu.value=="0"){
		document.top.action="WST0000.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//勤務表入力
	if (document.top.cmbMenu.value=="1"){
		document.top.action="WST0001.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//勤務表一覧
	if (document.top.cmbMenu.value=="2"){
		document.top.action="WST0002.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//休暇情報一覧
	if (document.top.cmbMenu.value=="3"){
		document.top.action="WST0002_2.asp?mode=menu";
		document.top.submit();
//		document.bottom.focus();
	}
	//行先＆スケジュール
	if (document.top.cmbMenu.value=="4"){
		document.top.action="WST0003.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//社内メール
	if (document.top.cmbMenu.value=="5"){
		document.top.action="WST0005.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//行先掲示板
	if (document.top.cmbMenu.value=="6"){
		document.top.action="WST0006.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//年間カレンダー
	if (document.top.cmbMenu.value=="7"){
		document.top.action="WST0007.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//個人情報変更
	if (document.top.cmbMenu.value=="8"){
		document.top.action="T0001.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//交通費申請
	if (document.top.cmbMenu.value=="9"){
		document.top.action="WST0008.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//費用精算申請
	if (document.top.cmbMenu.value=="10"){
		document.top.action="WST0009.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//仮払い申請
	if (document.top.cmbMenu.value=="11"){
		document.top.action="WST0010.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//交通費・費用精算・仮払い申請
	if (document.top.cmbMenu.value=="12"){
		document.top.action="WST0011.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//週報
	if (document.top.cmbMenu.value=="13"){
		document.top.action="WST0013.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//勤務表承認
	if (document.top.cmbMenu.value=="20"){
		document.top.action="WST0002_1.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//交通費・費用精算・仮払い承認
	if (document.top.cmbMenu.value=="23"){
		document.top.action="WST0012.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//週報承認
	if (document.top.cmbMenu.value=="24"){
		document.top.action="WST0014.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//勤怠集計
	if (document.top.cmbMenu.value=="21"){
		document.top.action="WSR0001.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//プロジェクト集計
	if (document.top.cmbMenu.value=="22"){
		document.top.action="WSR0002.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//メンテナンスメニュー
	//カレンダー設定
	if (document.top.cmbMenu.value=="30"){
		document.top.action="WSM0001.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//勤務条件設定
	if (document.top.cmbMenu.value=="31"){
		document.top.action="WSM0002.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//有休付与
	if (document.top.cmbMenu.value=="32"){
		document.top.action="WSM0004.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//勤務表削除
	if (document.top.cmbMenu.value=="33"){
		document.top.action="delete.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//社員情報メンテナンス
	if (document.top.cmbMenu.value=="40"){
		document.top.action="M0001.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//部門情報メンテナンス
	if (document.top.cmbMenu.value=="41"){
		document.top.action="M0002.asp";
		document.top.submit();
//		document.bottom.focus();
	}
	//プロジェクト情報メンテナンス
	if (document.top.cmbMenu.value=="42"){
		document.top.action="M0003.asp";
		document.top.submit();
//		document.bottom.focus();
	}
}

//Help画面を表示
function go_help(){
	var wo;
	wo=window.open("systemguide.pdf","Help","width=800,height=600,menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no");
	wo.focus();
}
//ログアウト処理
function go_exit(){
//	if(confirm('ログオフしてもよろしいですか？')){
		document.top.action="logout.asp?mode=exit";
		document.top.target="_top";
		document.top.submit();
//	}
}
//イメージ変更
function flipImage(url){
	if (window.event.srcElement.tagName == "IMG" ) {
		window.event.srcElement.src = url;
	}

}

