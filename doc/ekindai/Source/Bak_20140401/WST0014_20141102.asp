<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST00014
'機能：精算申請承認
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.bottom.action="WST0014.asp";
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs											'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strNendo								'年度
	Dim strMonth								'月
	Dim strNengetu							'年月
	Dim strSysDate							'システム年月日
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim strFormat								'書式
	Dim strURL									'URL
	Dim strURL1									'URL
	Dim hdnBackURL							'戻るパス

	'パラメーター取得
	Call getParameter()

	Call initProc()
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()
	Dim strTemp

	'アドレスバーからから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	If strShainCD = Empty Then
		'フォームから社員番号取得
		strShainCD = trim(request.form("txtShainCD"))
	End If

	'アドレスバーから取得から年度を取得
	strNendo = trim(request.querystring("nendo"))
	If strNendo = Empty Then
		'フォームから取得から年度を取得
		strNendo = trim(request.form("cmbNendo"))
	End If
	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = getSysNendo()
	end if

	'アドレスバーからから月を取得
	strMonth = trim(request.querystring("month"))
	If strMonth = Empty Then
		'フォームから取得から月を取得
		strMonth = trim(request.form("cmbMonth"))
	End If
	'月を取得できない場合はシステム日付を付与する
	if strMonth = empty then
		strMonth = CONST_SYSMONTH
	end if

	'年月
	strNengetu = strNendo & strMonth

	'アドレスバーから承認者を取得
	if strShaincd = empty then
		strShaincd = trim(request.querystring("shounishacd"))
	end if

	'戻るパス
 	hdnBackURL = "WST0014.asp?nendo=" & strNendo & "&month=" & strMonth & "&shounishacd=" & strShaincd

	'セッションに戻るパスを設定する
	Session("backurl") = hdnBackURL

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'要承認、代理承認一覧を表示
'	response.write(SELWST0007_2(strNengetu))
'	response.end
	'週報情報取得 20140927 Start
	If strKengen = 4  then
	    Set rs = session("kintai").Execute(SELWST0007_2_1(strNengetu))
	Else
	    Set rs = session("kintai").Execute(SELWST0007_2_2(strNengetu, strShainCD))
	End If
	'週報情報取得 20140927 End


	'内容を表示
	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：交通費・費用精算・仮払い承認
'引数：あり
'			pRs:
'****************************************************
Sub viewDetail(pRs)
%>
<%
%>
	<span class="txt-gray1"><U>週間報告確認</U></span>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="10%">
<%Call viewNendoCMB(strNendo)%>
				<FONT SIZE="2">年
			</TD>
			<TD WIDTH="10%">
<%Call viewMonthCMB(strMonth)%>
				<FONT SIZE="2">月
			</TD>
			<TD WIDTH="10%">
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="_submit()" class="s-btn">
			</TD>
			<TD WIDTH="*%">
			</TD>
		</TR>
	</TABLE>
	<table class="l-tbl">
		<col width="11%">
		<col width="9%">
		<col width="15%">
		<col width="37%">
		<col width="8%">
		<col width="9%">
		<col width="11%">
		 <TR>
			<TH class="l-cellsec">報告日</TH>
			<TH class="l-cellsec">報告者</TH>
			<TH class="l-cellsec">期間</TH>
			<TH class="l-cellsec">メール送信先</TH>
			<TH class="l-cellsec">報告状態</TH>
			<TH class="l-cellsec">確認者氏名</TH>
			<TH class="l-cellsec">確認日</TH>
		 </TR>
	</table>
	<table class="l-tbl">
		<col width="11%">
		<col width="9%">
		<col width="15%">
		<col width="37%">
		<col width="8%">
		<col width="9%">
		<col width="11%">
	<%
		dim i
		dim cellClass,cellClassr
		i = 0
	%>
	<%Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
			'月間確認にリンク
			strURL = "WST0013.asp?shaincd=" & pRs("SHAINCD") & "&nengetu=" & pRs("NENGETU") & "&datefrom=" & pRs("DATEFROM") & "&dateto=" & pRs("DATETO") & "&shounishacd=" & strShaincd & "&kengen=4"
'			strURL = "WST0013.asp?shaincd=" & pRs("SHAINCD") & "&nengetu=" & pRs("NENGETU") & "&shounishacd=" & strShaincd & "&kengen=4"
	%>
		<TR>
			<TD class=<%=cellClass%> ><%=toDate1(mid(pRs("UPDATEDATE"),1,8))%></TD>
			<TD class=<%=cellClass%>>
				<A HREF="<%=strURL%>"><%=getShainNM(pRs("SHAINCD"))%></A>
			</TD>
			<TD class=<%=cellClass%> ><%=toDate3(mid(pRs("DATEFROM"),1,8))%>〜<%=toDate3(mid(pRs("DATETO"),1,8))%></TD>
			<TD class=<%=cellClass%> ><%=pRs("MAILTO")%></TD>
			<TD class=<%=cellClass%> ><%=CONST_HOUKOKUSTATUS(pRs("SHOUNINSTATUS"))%></TD>
			<TD class=<%=cellClass%> ><%=pRs("SHOUNINSHANM")%></TD>
			<TD class=<%=cellClass%> ><%=toDate1(pRs("SHOUNINDATE"))%></TD>
		</TR>
	<%pRs.MoveNext
		Loop%>
	</TABLE>
<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">

</CENTER>
</FORM>
</BODY>
</HTML>
<% End Sub %>

