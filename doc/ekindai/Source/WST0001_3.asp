<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0001_1
'機能：	印刷確認
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<!--<HTML onContextMenu="return false;">-->
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _print(){
	window.print();
	return;
}

//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員コードﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門コードﾞ
	Dim strBumonNM							'部門名称
	Dim strNengetu							'年月
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim strColor								'カラー
	Dim hdnViewType							'表示切替タイプ
	Dim hdnBackURL							'戻るパス
	Dim strShouninshaCD					'承認者コード
	Dim strShouninshaNM					'承認者氏名

	'エラー制御開始
'	On Error Resume Next

	'パラメーター取得
	Call getParameter()

	'初期表示を行う
	Call initProc(0)

	'エラー制御終了
	On Error Goto 0

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	if strShainCD = empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if

	'実行モードを取得
	strMode = request.querystring("mode")

	'表示切替タイプ
	hdnViewType = request.form("hdnViewType")
	If hdnViewType = Empty Then
		hdnViewType = 0
	Else
		hdnViewType = hdnViewType + 1
		If hdnViewType = 3 Then
			hdnViewType = 0
		End If
	End If

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	if strNengetu = empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'アドレスバーから承認者社員コードを取得
	strShouninshaCD = trim(request.querystring("shouninshacd"))
	if strShouninshaCD = empty then
		strShouninshaCD = trim(request.form("hdnShouninshaCD"))
	end if

	'アドレスバーから承認者社員氏名を取得
	strShouninshaNM = trim(request.querystring("shouninshanm"))
	if strShouninshaNM = empty then
		strShouninshaNM = trim(request.form("hdnShouninshaNM"))
	end if

	'戻るパスを取得
	'セッションより戻るパスを取得
	hdnBackURL = Session("backurl")
	If hdnBackURL = Empty Then
		hdnBackURL = trim(request.form("hdnBackURL"))
	End If

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc(pType)
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc(pType)

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle(toDate2(strNengetu) & "度 勤務表",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'日別勤怠表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001(strShainCD,strNengetu))

	'月別勤怠表からデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0002(strShainCD,strNengetu))

	'ヘッダー部分表示
	Call viewHeader(rs1("SHOUNINSTATUS"))

	'画面内容を表示
	Call viewDetail(rs,rs1,pType)

	'レコードセットを開放
	Set rs = Nothing
	Set rs1 = Nothing

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader(pShouninStatus)
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader(pShouninStatus)
%>

<div class="list">

	<table class="l-tbl">
			<col width="50%">
			<col width="50%">
			<TR>
				<TD><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%></B></FONT></TD>
				</TD>
				<TD align="right">
				</TD>
			</TR>
	</TABLE>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs,pRs1,pType)
'PROCEDURE機能：勤怠入力
'引数：あり
'			pRs	:日別勤怠情報
'			pRs1:月別勤怠情報
'****************************************************
Sub viewDetail(pRs,pRs1,pType)
%>
<div class="list">
	<table class="l-tbl">
		<col width="10%">
		<col width="10%">
		<col width="5%">
		<col width="5%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="8%">
		<col width="*%">
		<TR>
			<TH class="l-cellsec">日付</TH>
			<TH class="l-cellsec">勤務区分</TH>
			<TH class="l-cellsec">出勤</TH>
			<TH class="l-cellsec">退勤</TH>
			<TH class="l-cellsec">稼働<br>時間</TH>
			<TH class="l-cellsec"><FONT COLOR="BLUE">普通<br>残業</TH>
			<TH class="l-cellsec"><FONT COLOR="BLUE">休日<br>残業</TH>
			<TH class="l-cellsec"><FONT COLOR="BLUE">深夜<br>残業</TH>
			<TH class="l-cellsec"><FONT COLOR="RED">不足<br>時間</TH>
<!--
			<TH class="l-cellsec"><FONT COLOR="RED">遅刻<br>時間</TH>
-->
			<TH class="l-cellsec">代休日</TH>
			<TH class="l-cellsec">備考</TH>
		</TR>
<%
	Do Until pRs.EOF
		strColor = getYoubiFont(pRs("YOUBI"),pRs("KYUUJITUFLG"))
		If pType = 0 Or pType = 2 Then
%>
		<TR>
			<td class="l-cellodd">
				<FONT COLOR="<%=strColor%>"><%=toDate3(pRs("NENGAPI"))%>
			</TD>
			<td class="l-cellodd"><%=strKinmuKBN(pRs("KINMUKBN"))%></TH>
			<td class="l-cellodd"><%=pRs("STIME")%></TH>
			<td class="l-cellodd"><%=pRs("ETIME")%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("KINMUTIME"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("ZANGYOUTIMEF"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("ZANGYOUTIMEK"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("ZANGYOUTIMES"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("SHORTAGETIME"))%></TH>
<!--
			<td class="l-cellodd"><%=toDecimal2(pRs("TIKOKUTIME"))%></TH>
-->
			<td class="l-cellodd"><%=toDate(pRs("DAIKYUUDATE"))%></TH>
			<td class="l-cellodd"><%=pRs("CMT")%></FONT></TD>
		</TR>
<%
		End If
		pRs.MoveNext
	Loop
%>

	<!------------------月間合計----------------->
		<TR>
			<TH class="l-cellsec" ROWSPAN="2">月間合計</TD>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsecr"><%=toDecimal2(pRs1("KINMUTIME"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="BLUE"><%=toDecimal2(pRs1("ZANGYOUTIMEF"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="BLUE"><%=toDecimal2(pRs1("ZANGYOUTIMEK"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="BLUE"><%=toDecimal2(pRs1("ZANGYOUTIMES"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="RED"><%=toDecimal2(pRs1("SHORTAGETIME"))%></TD>
<!--
			<TH class="l-cellsec"><FONT COLOR="RED"><%=toDecimal2(pRs1("TIKOKUTIME"))%></TD>
-->
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec"></TD>
		</TR>
	</TABLE>

</div>
</CENTER>
</FORM>
</BODY>
</HTML>

<%
End Sub
%>


