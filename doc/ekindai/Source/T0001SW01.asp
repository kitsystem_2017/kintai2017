<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：T0001SW01
'機能：社員情報検索
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>社員検索</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.T0001SW01.action="T0001SW01.asp?mode=search";
	document.T0001SW01.submit();
	return;
}
function _close(){
	self.window.close();
}

function _set(){
	//レコードカウント取得
	cnt = document.T0001SW01.hidCnt.value;
	idx = document.T0001SW01.hdnIdx.value;
	var value;

	for(i = 0;i < cnt; i++){
		if (cnt == 1) {
			value = document.T0001SW01.hidShinCD.value;
		} else {
			if(document.T0001SW01.rdo[i].checked == true){
				value = document.T0001SW01.hidShinCD[i].value;
			}
		}
	}
	
	if (value == null){
		alert("選択してください");
		return;
	}
	
	opener.bottom.elements[idx].value=value;
	
	self.window.close();
	
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="T0001SW01" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim txtShainCD							'社員コードﾞ
	Dim txtShainNM							'社員氏名
	Dim cmbBumonCD							'部門コードﾞ
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim intIdx									'Indix

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = empty Then
		'初期表示を行う
		Call initProc()
	elseIf strMode = "search" Then
		'検索処理を行う
		Call searchProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーから社員番号取得
	intIdx = trim(request.querystring("idx"))
	If intIdx = Empty Then
		'フォームから社員番号取得
		intIdx = trim(request.form("hdnIdx"))
	End If

	'実行モードを取得
	strMode = request.querystring("mode")

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'ヘッダーを表示
	Call viewHeader()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	searchProc()
'PROCEDURE機能：検索を行う
'引数：なし
'****************************************************
Sub searchProc()

	'画面の項目を取得
	Call getScreenValue()

	'検索処理を行う
	if txtShainCD<>empty or txtShainNM <> empty or cmbBumonCD <> empty then
		Set rs = session("kintai").Execute(SELM0001_2(txtShainCD,txtShainNM,cmbBumonCD,2,0))
		'チェック
		If rs.RecordCount = 0 Then
			errMSG = WSE0009
			'ヘッダー表示
			viewHeader()
			Exit Sub
		End If

		'ヘッダー表示
		viewHeader()

		'検索結果を表示
		Call ViewDetail(rs)
	else
		'ヘッダー表示
		viewHeader()
	end if

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目を取得
'引数：なし
'****************************************************
Sub getScreenValue()

	cmbBumonCD = trim(request.form("cmbBumonCD"))
	txtShainCD = trim(request.form("txtShainCD"))
	txtShainNM = trim(request.form("txtShainNM"))

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：社員検索
'引数：あり
'			pRs	:社員情報
'****************************************************
Sub viewHeader()
%>
	<BR>
	<FONT SIZE="" COLOR="BLUE">社員検索<BR>
<div class="list">

	<table class="l-tbl">
		<TR ALIGN="LEFT">
			<TD WIDTH="200">
				<FONT COLOR="RED" SIZE="2"><%=errMSG%>
			</TD>
			<TD WIDTH="*" ALIGN="RIGHT">
				<INPUT TYPE="BUTTON" VALUE="検索" onClick="_submit()" class="i-btn">
				<INPUT TYPE="BUTTON" VALUE="確定" onClick="_set()" class="i-btn">
				<INPUT TYPE="BUTTON" VALUE="閉じる" onClick="_close()" class="i-btn">
			</TD>
		</TR>
	</TABLE>
	<BR>
	<table class="l-tbl">
		<TR>
			<TH class="l-cellsec">部門</TH>
			<TD class="l-cellodd" colspan="2">
<% Call viewBumonCMB(cmbBumonCD) %>
			</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">社員番号</TH>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" SIZE="20" NAME="txtShainCD" VALUE="<%=txtShainCD%>" class="txt_imeoff">
			</TD>
			<TD class="l-cellodd">(先頭一致検索)</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">社員氏名</TH>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" SIZE="20" NAME="txtShainNM" VALUE="<%=txtShainNM%>" class="txt_imeon">
			</TD>
			<TD class="l-cellodd">(部分一致検索)</TD>
		</TR>
	</TABLE>
	<!-----------------------HIDDEN項目：--------------------->
	<INPUT TYPE="HIDDEN" NAME="hdnIdx" VALUE="<%=intIdx%>">
	<BR>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：社員検索
'引数：あり
'			pRs	:社員情報
'****************************************************
Sub viewDetail(pRs)
%>
<div class="list">

	<table class="l-tbl">
		<col width="5%">
		<col width="40%">
		<col width="20%">
		<col width="35%">
		<TR>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec">社員番号</TD>
			<TH class="l-cellsec">社員氏名</TD>
			<TH class="l-cellsec">部門</TD>
		</TR>
	</table>
	<table class="l-tbl">
		<col width="5%">
		<col width="40%">
		<col width="20%">
		<col width="35%">
	<%
		dim i
		dim cellClass,cellClassr
		i = 0
	%>
<% Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
%>
		<TR>
			<TD class=<%=cellClass%>><INPUT TYPE="RADIO" NAME="rdo"></TD>
			<TD class=<%=cellClass%>><%=pRs("SHAINCD")%></TD>
			<TD class=<%=cellClass%>><%=pRs("SHAINNM")%></TD>
			<TD class=<%=cellClass%>><%=pRs("BUMONCD") & ":" & pRs("BUMONNM")%>
			<!-----------------------HIDDEN項目：--------------------->
			<INPUT TYPE="HIDDEN" NAME="hidShinCD" VALUE="<%=pRs("SHAINCD")%>">
			</TD>
		</TR>
<% pRs.MoveNext
	 Loop %>

	</TABLE>
	<!-----------------------HIDDEN項目：--------------------->
	<INPUT TYPE="HIDDEN" NAME="hidCnt" VALUE="<%=pRs.RecordCount%>">
</div>
</CENTER>
</FORM>
</BODY>
</HTML>

<%
End Sub
%>



