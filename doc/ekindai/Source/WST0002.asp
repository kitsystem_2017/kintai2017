<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0002
'機能：勤怠表の一覧
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.bottom.action="WST0002.asp";
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs											'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strNendo								'年度
	Dim strSysDate							'システム年月日
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim strFormat								'書式
	Dim strURL									'URL
	Dim hdnBackURL							'戻るパス

	'パラメーター取得
	Call getParameter()

	Call initProc()
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()
	Dim strTemp

	'****パラメータ取得******
	'アドレスバーから取得から社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	If strShainCD = Empty Then
		'フォームから社員番号取得
		strShainCD = trim(request.form("txtShainCD"))
	End IF

	'アドレスバーから取得から年度を取得
	strNendo = trim(request.querystring("nendo"))
	If strNendo = Empty Then
		'フォームから取得から年度を取得
		strNendo = trim(request.form("cmbNendo"))
	End If

	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = GetCurrenNendo()
	end if

	'戻るパス
	hdnBackURL = "WST0002.asp?nendo=" & strNendo

	'セッションに戻るパスを格納する
	Session("backurl") = hdnBackURL
	
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle(strNendo & "度 勤務表",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'月別勤怠表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0002_1(strShainCD,strNendo))

	'内容を表示
	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：勤怠一覧表示
'引数：あり
'			pRs:月別勤怠情報
'****************************************************
Sub viewDetail(pRs)
%>
<%
%>
<div class="list">

	<table class="l-tbl">
		<TR ALIGN="LEFT" BGCOLOR="">
			<TD WIDTH="80">
<%Call viewNendoCMB(strNendo)%>
			</TD>
			<TD>
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="_submit()" class="s-btn">
			</TD>
		</TR>
	</TABLE>
	<table class="l-tbl">
		<!--年月 -->
		<col width="8%">
		<!--標準日数 -->
		<col width="4%">
		<!--稼働日数 -->
		<col width="4%">
		<!--標準時間 -->
		<col width="4%">
		<!--稼働時間 -->
		<col width="5%">
		<!--普通残業 -->
		<col width="5%">
		<!--休日残業 -->
		<col width="5%">
		<!--深夜残業 -->
		<col width="5%">
		<!--不足時間 -->
		<col width="4%">
		<!--遅刻時間 -->
<!--
		<col width="4%">
-->
		<!--有休日数 -->
		<col width="4%">
		<!--代休日数 -->
		<col width="4%">
		<!--欠勤日数 -->
		<col width="4%">
		<!--その他日数 -->
		<col width="5%">
		<!--更新日 -->
		<col width="7%">
		<!--承認状況 -->
		<col width="5%">
		<!--申請日 -->
		<col width="7%">
		<!--承認日 -->
		<col width="7%">
		<!--承認者 -->
		<col width="*%">
		<TR>
<%Call viewKintaiHeader()%>
		</TR>
	<%
		dim i
		dim cellClass,cellClassr
		i = 0
	%>
	<%Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
			'月間確認にリンク
			strURL = "WST0001_1.asp?shaincd=" & pRs("SHAINCD") & "&nengetu=" & pRs("NENGETU")
			'承認フラグは未申請、差し戻しの場合勤怠入力にリンク
			If pRs("SHOUNINSTATUS") = "0" Or pRs("SHOUNINSTATUS") = "1" Or pRs("SHOUNINSTATUS") = "4" Then
				strURL = "WST0001.asp?shaincd=" & pRs("SHAINCD") & "&nengetu=" & pRs("NENGETU") & "&datefrom=" & pRs("NENGETU") & "01&dateto=" & pRs("NENGETU") & "07"
			End If%>
		<TR>
			<TD class=<%=cellClass%>>
				<A HREF=<%=strURL%>><%=toDate2(pRs("NENGETU"))%></A>
			</TD>
			<TD class=<%=cellClassr%>><%=toDecimal1(pRs("STANDARDDAY"))%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal1(pRs("KINMUDAY"))%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal2(pRs("STANDARDTIME"))%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal2(pRs("KINMUTIME"))%></TD>
			<TD class=<%=cellClassr%>><FONT COLOR="BLUE"><%=toDecimal2(pRs("ZANGYOUTIMEF"))%></TD>
			<TD class=<%=cellClassr%>><FONT COLOR="BLUE"><%=toDecimal2(pRs("ZANGYOUTIMEK"))%></TD>
			<TD class=<%=cellClassr%>><FONT COLOR="BLUE"><%=toDecimal2(pRs("ZANGYOUTIMES"))%></TD>
			<TD class=<%=cellClassr%>><FONT COLOR="RED"><%=toDecimal2(pRs("SHORTAGETIME"))%></TD>
<!--
			<TD class=<%=cellClassr%>><FONT COLOR="RED"><%=toDecimal2(pRs("TIKOKUTIME"))%></TD>
-->
			<TD class=<%=cellClassr%>><%=toDecimal1(pRs("YUUKYUUCNT"))%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal1(pRs("DAIKYUUCNT"))%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal1(pRs("KEKKINCNT"))%></TD>
			<TD class=<%=cellClassr%>><%=toDecimal1(pRs("SONOTACNT"))%></TD>
			<TD class=<%=cellClass%>><%=toDate(pRs("UPDATEDATE"))%></TD>
			<TD class=<%=cellClass%>><%=CONST_SHOUNINSTATUS(pRs("SHOUNINSTATUS"))%></TD>
			<TD class=<%=cellClass%>><%=toDate(pRs("SINSEIDATE"))%></TD>
			<TD class=<%=cellClass%>><%=toDate(pRs("SHOUNINDATE"))%></TD>
			<TD class=<%=cellClass%>><%=pRs("SHOUNINSHANM")%></TD>
		</TR>
	<%pRs.MoveNext
		Loop%>
	</TABLE>
<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">

</CENTER>
</FORM>
</BODY>
</HTML>
<% End Sub %>

