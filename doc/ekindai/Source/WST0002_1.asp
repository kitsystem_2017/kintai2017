<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0002_1
'機能：要承認勤怠表一覧
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.bottom.action="WST0002_1.asp";
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs											'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strNendo								'年度
	Dim strMonth								'月
	Dim strNengetu							'年月
	Dim strSysDate							'システム年月日
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim strFormat								'書式
	Dim strURL									'URL
	Dim strURL1									'URL
	Dim hdnBackURL							'戻るパス

	'パラメーター取得
	Call getParameter()

	Call initProc()
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()
	Dim strTemp

	'アドレスバーからから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	If strShainCD = Empty Then
		'フォームから社員番号取得
		strShainCD = trim(request.form("txtShainCD"))
	End If

	'アドレスバーから取得から年度を取得
	strNendo = trim(request.querystring("nendo"))
	If strNendo = Empty Then
		'フォームから取得から年度を取得
		strNendo = trim(request.form("cmbNendo"))
	End If
	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = getSysNendo()
	end if

	'アドレスバーからから月を取得
	strMonth = trim(request.querystring("month"))
	If strMonth = Empty Then
		'フォームから取得から月を取得
		strMonth = trim(request.form("cmbMonth"))
	End If
	'月を取得できない場合はシステム日付を付与する
	if strMonth = empty then
		strMonth = CONST_SYSMONTH
	end if

	'年月
	strNengetu = strNendo & strMonth

	'戻るパス
	hdnBackURL = "WST0002_1.asp?shaincd=" & strShainCD & "&nendo=" & strNendo & "&month=" & strMonth
	
 	'セッションに戻るパスを設定する
  Session("backurl") = hdnBackURL

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'要承認、代理承認一覧を表示
	'全社員の入力情報の取得 20140608 Start
	If strKengen = 4  then
	    Set rs = session("kintai").Execute(SELWST0002_4_1(strNengetu))
	Else
	    Set rs = session("kintai").Execute(SELWST0002_4_2(strShainCD,strNengetu))
	End If
	'全社員の入力情報の取得 20140608 End

	'内容を表示
	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：勤怠一覧表示
'引数：あり
'			pRs:月別勤怠情報
'****************************************************
Sub viewDetail(pRs)
%>
<%
%>
	<span class="txt-gray1"><U>勤怠表承認一覧</U></span>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="10%">
<%Call viewNendoCMB(strNendo)%>
				<FONT SIZE="2">年
			</TD>
			<TD WIDTH="10%">
<%Call viewMonthCMB(strMonth)%>
				<FONT SIZE="2">月
			</TD>
			<TD WIDTH="10%">
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="_submit()" class="s-btn">
			</TD>
			<TD WIDTH="*%">
			</TD>
		</TR>
	</TABLE>
	<table class="l-tbl">
	<table class="l-tbl">
		<!--氏名-->
		<col width="20%">
		<!--標準<BR>日数-->
		<col width="4%">
		<!--稼働<BR>日数-->
		<col width="4%">
		<!--標準<BR>時間-->
		<col width="4%">
		<!--稼働<BR>時間-->
		<col width="5%">
		<!--普通<BR>残業-->
		<col width="5%">
		<!--休日<BR>残業-->
		<col width="5%">
		<!--深夜<BR>残業-->
		<col width="5%">
		<!--不足時間 -->
		<col width="4%">
		<!--遅刻時間 -->
<!--
		<col width="4%">
-->
		<!--有休日数 -->
		<col width="4%">
		<!--代休日数 -->
		<col width="4%">
		<!--欠勤日数 -->
		<col width="4%">
		<!--その他日数 -->
		<col width="5%">
		<!--承認状況 -->
		<col width="5%">
		<!--申請日 -->
		<col width="6%">
		<!--承認日 -->
		<col width="6%">
		<!--承認者 -->
		<col width="*%">
		<TR>
<%Call viewKintaiHeader1()%>
		</TR>
	<%
		dim i
		dim cellClass,cellClassr
		i = 0
	%>
	<%Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
			'月間確認にリンク
			strURL = "WST0001_1.asp?shaincd=" & pRs("SHAINCD") & "&nengetu=" & pRs("NENGETU") & "&shouninshacd=" & strShainCD & "&shouninshanm=" & strShainNM
			strURL1 = "WST0002_2.asp?shaincd=" & pRs("SHAINCD")
	%>
		<TR>
			<TD class=<%=cellClass%>>
				<A HREF="<%=strURL%>"><%=pRs("SHAINCD")%></A>
			</TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("STANDARDDAY"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("KINMUDAY"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("STANDARDTIME"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("KINMUTIME"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("ZANGYOUTIMEF"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("ZANGYOUTIMEK"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("ZANGYOUTIMES"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("SHORTAGETIME"))%></TD>
<!--
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("TIKOKUTIME"))%></TD>
-->
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("YUUKYUUCNT"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("DAIKYUUCNT"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("KEKKINCNT"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("SONOTACNT"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=CONST_SHOUNINSTATUS(pRs("SHOUNINSTATUS"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=toDate(pRs("SINSEIDATE"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=toDate(pRs("SHOUNINDATE"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=pRs("SHOUNINSHANM")%></TD>
		</TR>
		<TR>
			<TD class=<%=cellClass%>>
				<A HREF=<%=strURL1%>><%=pRs("SHAINNM")%></A>
			</TD>
		</TR>
	<%pRs.MoveNext
		Loop%>
	</TABLE>
<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">

</CENTER>
</FORM>
</BODY>
</HTML>
<% End Sub %>

