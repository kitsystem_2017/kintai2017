<%@ Language=VBScript %>
<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>
<HTML onContextMenu="return false;">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<TITLE>日付設定実行</TITLE>
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(type){
	//戻る
	if(type == 0){
		document.bottom.action="WSM0001.asp";
		document.bottom.submit();
		return;
	}
	//更新
	if(type == 1){
		if(confirm('確定してもよろしいですか？')){
			document.bottom.action="WSM0001_1.asp?mode=1";
			document.bottom.submit();
		}
		return;
	}
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%

	'変数定義
	Dim rs												'レコードセット
	Dim strNengetu								'年月
	Dim strNengapi								'年月日
	Dim iniCalendarKBN						'カレンダー区分
	Dim intYoubi									'曜日
	Dim strBikou									'備考
	Dim intKyuujituFLG						'休日フラグを格納する配列
	Dim errMSG										'エラーメッセージ
	Dim strMode										'実行モード
	Dim strSQL										'SQL文
	Dim cnt,cnt1,cnt2								'カウント
	Dim strFormat									'書式
	Dim strColor									'カラー

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが作成の場合
	ElseIf strMode = "1" Then
		'カレンダー作成処理を行う
		Call updateProc()
	End If

	response.end
%>
<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーから年月、実行モード、カレンダー区分を取得
	strNengetu = request.querystring("nengetu")
	If strNengetu = Empty Then
		strNengetu = request.form("txtNengetu")
	End If
	strMode = request.querystring("mode")

'	iniCalendarKBN = request.querystring("calendarkbn")

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'カレンダーマスタからデータを取得
	Set rs = session("kintai").Execute(SELWSM0001_1(0,strNengetu))

	'ヘッダーを表示する
	Call viewHeader()

	'カレンダーを表示する
	Call viewDetail(rs)

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データ更新プロセス
'引数：なし
'****************************************************
Sub updateProc()

	'エラー制御開始
'	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollbackTrans
	session("kintai").BeginTrans

	cnt1 = request.form("hdnDataCnt")

	For cnt = 1 To cnt1
		strNengapi = request.form("hdnNengapi" & cnt)
		intKyuujituFLG = request.form("rdoKyuujituFLG" & cnt)
		strBikou = request.form("txtBikou" & cnt)

		'SQL実行
		session("kintai").Execute(UPDWSM0001(0,strNengapi,intKyuujituFLG,strBikou))

		'** エラーチェック
		If err <> 0 then
			'エラーメッセージを設定し、処理を終了
			errMSG = "カレンダー" & WSE0000
			'ロールバック
			session("kintai").RollbackTrans

			'カレンダーマスタからデータを取得
			Set rs = session("kintai").Execute(SELWSM0001_1(0,strNengetu))

			'ヘッダーを表示する
			Call viewHeader()

			'カレンダーを表示する
			Call viewDetail(rs)

			Exit Sub
		End If
	Next

	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

	'カレンダーマスタからデータを取得
	Set rs = session("kintai").Execute(SELWSM0001_1(0,strNengetu))

	'ヘッダーを表示する
	Call viewHeader()

	'カレンダーを表示する
	Call viewDetail(rs)

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：ヘッダー部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<FONT COLOR="BLUE"><B><U><%=toDate2(strNengetu)%> カレンダー設定画面</U></FONT>
	<TABLE WIDTH="500" BORDER="0">
		<TR>
			<TD WDITH="300"><%=errMSG%>　</TD>
			<TD WDITH="*" ALIGN="RIGHT">
				<INPUT TYPE="BUTTON" VALUE="確定" onClick="_submit(1)" class="s-btn1">
				<INPUT TYPE="BUTTON" VALUE="戻る" onClick="_submit(0)" class="s-btn1">
			</TD>
		</TR>
	</TABLE>
	</CENTER>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：  viewDetail(pRs)
'PROCEDURE機能：カレンダーを表示する
'****************************************************
Sub viewDetail(pRs)
%>
<div class="list">
	<table class="l-tbl">
		<col width="10%">
		<col width="10%">
		<col width="5%">
		<col width="5%">
		<col width="70%">
		<TR>
			<TH class="l-cellsec">日付</TH>
			<TH class="l-cellsec">曜日</TH>
			<TH class="l-cellsec">平日</TH>
			<TH class="l-cellsec">休日</TH>
			<TH class="l-cellsec">備考(30文字以下)</TH>
		</TR>
	</TABLE>
  <div class="l-tbl-auto" style="height:550px">
	<table class="l-tbl">
		<col width="10%">
		<col width="10%">
		<col width="5%">
		<col width="5%">
		<col width="70%">
<%
	For cnt = 1 To pRs.Recordcount
		strColor = getYoubiColor(rs("YOUBI"),rs("KYUUJITUFLG"))
%>
		<TR>
			<TD class="l-cellodd" BGCOLOR="<%=strColor%>"><%=toDate4(rs("NENGAPI"))%>
			<!----------------------------HIDDEN項目-------------------------->
			<INPUT TYPE="HIDDEN" NAME="hdnNengapi<%=cnt%>" VALUE="<%=pRS("NENGAPI")%>">
			</TD>
			<TD class="l-cellodd" BGCOLOR="<%=strColor%>"><%=getYoubiKJ(rs("YOUBI"))%></TD>
<%
		strFormat = Empty
		If rs("KYUUJITUFLG") = "0" Then strFormat = "CHECKED" End If
%>
			<TD class="l-cellodd" BGCOLOR="<%=strColor%>"><INPUT TYPE="RADIO" NAME="rdoKyuujituFLG<%=cnt%>" VALUE="0" <%=strFormat%>></TD>
<%
		strFormat = Empty
		If rs("KYUUJITUFLG") = "1" Then strFormat = "CHECKED" End If
%>
			<TD class="l-cellodd" BGCOLOR="<%=strColor%>"><INPUT TYPE="RADIO" NAME="rdoKyuujituFLG<%=cnt%>" VALUE="1" <%=strFormat%>></TD>
			<TD class="l-cellodd" BGCOLOR="<%=strColor%>">
				<INPUT TYPE="TEXT" MAXLENGTH="30" NAME="txtBikou<%=cnt%>" SIZE="60" VALUE="<%=pRS("CMT")%>">
			</TD>
		</TR>
<%
		pRS.MoveNext
	Next
%>

	</TABLE>
	<!----------------------------HIDDEN項目-------------------------->
	<INPUT TYPE="HIDDEN" NAME="hdnDataCnt" VALUE="<%=pRs.RecordCount%>">
	<INPUT TYPE="HIDDEN" NAME="txtYear" VALUE="<%=Left(strNengetu,4)%>">
	<INPUT TYPE="HIDDEN" NAME="txtNengetu" VALUE="<%=strNengetu%>">

  </div>
</div>
	</FORM>
	</BODY>
	</HTML>
<%
End Sub
%>


















