<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：M0001
'機能：社員マスタメンテナンス
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>

<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<%
	'変数定義
	Dim rs,rs1,rs2							'レコードセット
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim strColor								'カラー

	'画面項目の格納変数定義
	Dim txtPJNO							'プロジェクトｺｰﾄﾞ
	Dim txtPJNM							'プロジェクト名称
	Dim txtPJNOList()					'プロジェクトｺｰﾄﾞ配列
	Dim txtPJNMList()					'プロジェクト名称配列
	Dim chkSelList()						'選択
	
	'プロジェクトリスト取得
	Dim PJNOList()
	Dim PJNMList()

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合	'初期表示を行う
	If strMode = Empty Or strMode = "0" Then
		Call initProc()
	'実行モードが登録の場合、登録処理を行う
	elseIf strMode = "1" Then
		Call insertProc()
	'実行モードが更新の場合、更新処理を行う
	elseIf strMode = "2" Then
		Call updateProc()
	'実行モードが削除の場合
	elseIf strMode = "3" Then
		Call deleteProc()
	'検索処理を行う
	elseIf strMode = "4" Then
		Call searchProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'実行モードを取得
	strMode = request.querystring("mode")

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	Set rs = session("kintai").Execute(SELM0003_1())
	
	'画面の値をセット
	Call setScreenValue(rs)

	'共通処理
	Call commonProc()
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データの追加を行う
'引数：なし
'****************************************************
Sub insertProc()

	'画面の値を取得
	Call getScreenValue()

	If txtPJNO = Empty or txtPJNM = Empty Then
		errMSG = "必須項目を入力してください。"
		'画面の値をセット
		Call commonProc()
		Exit Sub
	End If
	
	'重複ﾁｪｯｸ
	Set rs = session("kintai").Execute(SELM0003(txtPJNO))
	If rs.RecordCount > 0 Then
		'Error処理
		errMSG = WSE0016
		Call commonProc()
		Exit Sub
	End If

	strSQL = INSM0003( _
		txtPJNO, _
		txtPJNM)

	'エラー制御開始
'	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans
	'***SQL実行
	session("kintai").Execute (strSQL)
	If Err <> 0 Then
		session("kintai").RollBackTrans
		'ヘッダー設定
		errMSG = "プロジェクトマスタ" & WSE0000
		Call commonProc()
		Exit Sub
	End If
	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

	'初期表示
	Call initProc()

	'レコードセット開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データの更新を行う
'引数：なし
'****************************************************
Sub updateProc()
	
	'画面の値を取得
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	For cnt = 1 To UBOUND(txtPJNOList)
		'ﾁｪｯｸされている行のみ処理する
		If chkSelList(cnt) = "on" Then
			If txtPJNMList(cnt) = Empty Then
				errMSG = "必須項目を入力してください。"
				'画面の値をセット
				Call commonProc()
				Exit Sub
			End If
		
			strSQL = UPDM0003(txtPJNOList(cnt),txtPJNMList(cnt))

			'** トランザクション開始
			session("kintai").BeginTrans
			'***SQL実行
			session("kintai").Execute (strSQL)
			If Err <> 0 Then
				session("kintai").RollBackTrans
				'ヘッダー設定
				errMSG = "プロジェクトマスタ" & WSE0000
				Call commonProc()
				Exit Sub
			End If
			'** トランザクション終了
			session("kintai").CommitTrans

		End If
	Next

	'エラー制御終了
	On Error Goto 0

	'初期表示
	Call initProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	deleteProc()
'PROCEDURE機能：メール削除を行う
'引数：なし
'****************************************************
Sub deleteProc()

	'画面の値を取得
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	For cnt = 1 To UBOUND(txtPJNOList)
		'ﾁｪｯｸされている行のみ処理する
		If chkSelList(cnt) = "on" Then
			strSQL = DELM0003(txtPJNOList(cnt))

			'** トランザクション開始
			session("kintai").BeginTrans
			'***SQL実行
			session("kintai").Execute (strSQL)
			If Err <> 0 Then
				session("kintai").RollBackTrans
				'ヘッダー設定
				errMSG = "プロジェクトマスタ" & WSE0000
				Call commonProc()
				Exit Sub
			End If
			'** トランザクション終了
			session("kintai").CommitTrans

		End If
	Next

	'エラー制御終了
	On Error Goto 0

	'初期表示
	Call initProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	commonProc()
'PROCEDURE機能：共通処理
'引数：なし
'****************************************************
Sub commonProc()

	'ヘッダー部分を表示する
	Call viewHeader()

	'詳細を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()
	cnt						 = request.form("hdnCnt")					'カウント数
	txtPJNO				 = request.form("txtPJNO")				'プロジェクトｺｰﾄﾞ
	txtPJNM				 = request.form("txtPJNM")				'プロジェクト

	Redim txtPJNOList(cnt)
	Redim txtPJNMList(cnt)
	Redim chkSelList(cnt)

	For cnt = 1 To UBound(txtPJNOList)
		txtPJNOList(cnt)				 = request.form("hdnPJNO" & cnt)					'プロジェクトｺｰﾄﾞ配列
		txtPJNMList(cnt)				 = request.form("txtPJNM" & cnt)					'プロジェクト配列
		chkSelList(cnt)					 = request.form("chkSel" & cnt)						'選択配列
	Next

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(rs)
'PROCEDURE機能：画面の項目に値をセット
'引数：なし
'****************************************************
Sub setScreenValue(rs)

	cnt = 0
	If isNull(rs) = false Then
		cnt = rs.RecordCount
	End If

	Redim txtPJNOList(cnt)
	Redim txtPJNMList(cnt)

	For cnt = 1 To UBound(txtPJNOList)
		txtPJNOList(cnt)				 = rs("PJNO")					'プロジェクトｺｰﾄﾞ配列
		txtPJNMList(cnt)				 = rs("PJNM")					'プロジェクト配列
		rs.MoveNext
	Next

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<U><B><FONT SIZE="3" COLOR="BLUE">プロジェクトマスタ　メンテナンス</FONT></B></U>
	</CENTER>
<div class="list">

	<table class="l-tbl">
		<TBODY>
			<TR>
				<TD><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
			</TR>
		</TBODY>
	</table>
	<table class="l-tbl">
			<col width="15%">
			<col width="10%">
			<col width="15%">
			<col width="60%">
		<TR>
			<TH class="l-cellsec">プロジェクトNO.<font color="ff0000">(*)</font></TH>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" NAME="txtPJNO" SIZE="10" MAXLENGTH="7" VALUE="<%=txtPJNO%>" class="txt_imeoff">
			</TD>
			<TH class="l-cellsec">プロジェクト名称<font color="ff0000">(*)</font>
			</TH>
			<TD class="l-cellodd">
			<INPUT TYPE="TEXT" NAME="txtPJNM" SIZE="60" MAXLENGTH="30" VALUE="<%=txtPJNM%>" class="txt_imeon">
			</TD>

		</TR>
	<table>
	<br>
	<table class="l-tbl">
		<TBODY>
			<TR>
				<TD WIDTH="60%">
				</TD>
				<TD WIDTH="*%">
<!--
				<INPUT TYPE="BUTTON" VALUE="検索" onClick="SubmitM0003(4)" class="s-btn">
-->
				<INPUT TYPE="BUTTON" VALUE="登録" onClick="SubmitM0003(1)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="更新" onClick="SubmitM0003(2)" class="s-btn">
			 	<INPUT TYPE="BUTTON" VALUE="削除" onClick="SubmitM0003(3)" class="s-btn">
				</TD>
			</TR>
		</TBODY>
	</table>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：社員一覧表示
'引数：あり
'****************************************************
Sub viewDetail()
%>
<div class="list">

	<table class="l-tbl">
		<col width="3%">
		<col width="15%">
		<col width="82%">
		<TR>
			<TH class="l-cellsec" ROWSPAN="2"></TH>
			<TH class="l-cellsec">プロジェクトNo.<font color="ff0000">(*)</font></TH>
			<TH class="l-cellsec">プロジェクト名称<font color="ff0000">(*)</font></TH>
		</TR>
	</table>

  <div class="l-tbl-auto" style="height:450px">
	<table class="l-tbl">
		<col width="3%">
		<col width="15%">
		<col width="82%">
<%
	For cnt = 1 To UBound(txtPJNOList)
	if cnt mod 2 <> 0 then
		cellClass = "l-cellodd"
		cellClassr = "l-celloddr"
	else
		cellClass = "l-celleven"
		cellClassr = "l-cellevenr"
	end if
%>
		<TR>
			<TD class=<%=cellClass%>><INPUT TYPE="CHECKBOX" NAME="chkSel<%=cnt%>"></TD>
			<TD class=<%=cellClass%>><%= txtPJNOList(cnt) %></FONT>
			<!-----------------------HIDDEN項目：--------------------->
			<INPUT TYPE="HIDDEN" NAME="hdnPJNO<%=cnt%>" VALUE="<%=txtPJNOList(cnt)%>">
			</TD>
			<TD class=<%=cellClass%>>
				<INPUT TYPE="TEXT" SIZE="60" MAXLENGTH="30" NAME="txtPJNM<%=cnt%>" VALUE="<%=txtPJNMList(cnt)%>" class="txt_imeon">
			</TD>
		</TR>
<%
	Next
%>
	</table>
	</div>
</div>
<!-----------------------隠し項目--------------------->
<INPUT TYPE="HIDDEN" NAME="hdnCnt" VALUE="<% =UBound(txtPJNMList) %>">
</FORM>
</BODY>
</HTML>
<%
End Sub
%>


