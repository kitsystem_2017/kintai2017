<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0001_1
'機能：	勤怠一ヶ月分確認
'				勤怠一ヶ月分申請
'				勤怠一ヶ月分承認or却下
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(type){
	//PJ情報表示しない
	if(type == 0) {
		document.bottom.action="WST0001_1.asp?mode=0";
		document.bottom.submit();
		return;
	}
	//PJ情報表示
	if(type == 1) {
		document.bottom.action="WST0001_1.asp?mode=1";
		document.bottom.submit();
		return;
	}
	//申請
	if(type == 2) {
		if(!confirm("申請してもよろしいですか？")){return;}
		document.bottom.action="WST0001_1.asp?mode=2";
		document.bottom.submit();
		return;
	}
	//承認
	if(type == 3) {
		if(!confirm("承認してもよろしいですか？")){return;}
		document.bottom.action="WST0001_1.asp?mode=3";
		document.bottom.submit();
		return;
	}
	//却下
	if(type == 4) {
		if(!confirm("却下してもよろしいですか？")){return;}
		document.bottom.action="WST0001_1.asp?mode=4";
		document.bottom.submit();
		return;
	}
}
function _back(BackURL){
	document.bottom.action=BackURL;
	document.bottom.submit();
	return;
}

function openPrintPage(pShainID,pNengetu){
var win1 = window.open("WST0001_3.asp?shaincd=" + pShainID + "&nengetu=" + pNengetu,"","left=0,top=0,width="+1024+",height="+768+",menubar=yes,toolbar=yes,location=no,directories=yes,status=yes,scrollbars=yes,resizable");
		return;
}

//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員コードﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門コードﾞ
	Dim strBumonNM							'部門名称
	Dim strNengetu							'年月
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim strColor								'カラー
	Dim hdnViewType							'表示切替タイプ
	Dim hdnBackURL							'戻るパス
	Dim strShouninshaCD					'承認者コード
	Dim strShouninshaNM					'承認者氏名

	'エラー制御開始
'	On Error Resume Next

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = "0" Or strMode = Empty Then
		'初期表示を行う
		Call initProc(0)
	elseIf strMode = "1" Then
		'スケジュールPJ情報表示処理を行う
		Call initProc(hdnViewType)
	elseIf strMode = "2" Then
		'スケジュール申請処理を行う
		Call applyProc()
	elseIf strMode = "3" Then
		'スケジュール承認処理を行う
		Call approveProc()
	elseIf strMode = "4" Then
		'スケジュール却下処理を行う
		Call rejectProc()
	End If

	'エラー制御終了
	On Error Goto 0

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	if strShainCD = empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if

	'実行モードを取得
	strMode = request.querystring("mode")

	'表示切替タイプ
	hdnViewType = request.form("hdnViewType")
	If hdnViewType = Empty Then
		hdnViewType = 0
	Else
		hdnViewType = hdnViewType + 1
		If hdnViewType = 3 Then
			hdnViewType = 0
		End If
	End If

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	if strNengetu = empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'アドレスバーから承認者社員コードを取得
	strShouninshaCD = trim(request.querystring("shouninshacd"))
	if strShouninshaCD = empty then
		strShouninshaCD = trim(request.form("hdnShouninshaCD"))
	end if

	'アドレスバーから承認者社員氏名を取得
	strShouninshaNM = trim(request.querystring("shouninshanm"))
	if strShouninshaNM = empty then
		strShouninshaNM = trim(request.form("hdnShouninshaNM"))
	end if

	'戻るパスを取得
	'セッションより戻るパスを取得
	hdnBackURL = Session("backurl")
	If hdnBackURL = Empty Then
		hdnBackURL = trim(request.form("hdnBackURL"))
	End If

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc(pType)
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc(pType)

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle(toDate2(strNengetu) & "度 勤務表",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'日別勤怠表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001(strShainCD,strNengetu))

	'月別勤怠表からデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0002(strShainCD,strNengetu))

	'ヘッダー部分表示
	Call viewHeader(rs1("SHOUNINSTATUS"))

	'画面内容を表示
	Call viewDetail(rs,rs1,pType)

	'レコードセットを開放
	Set rs = Nothing
	Set rs1 = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	applyProc()
'PROCEDURE機能：申請
'引数：なし
'****************************************************
Sub applyProc()

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'申請
	session("kintai").Execute(UPDWST0002_1(strShainCD,strNengetu,2,CONST_SYSDATE))

	If Err <> 0 Then
		session("kintai").RollBackTrans
		errMSG = "勤怠申請" & WSE0000
		'エラーメッセージ部分を表示する
		Call initProc(0)
		'処理を終了する
		response.End
	End If

	'** トランザクション終了
	session("kintai").CommitTrans

	Response.Redirect "WST0002.asp?shaincd=" & strShainCD & "&nendo=" & Left(strNengetu,4)

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	approveProc()
'PROCEDURE機能：承認
'引数：なし
'****************************************************
Sub approveProc()

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'申請
	session("kintai").Execute(UPDWST0002_2(strShainCD,strNengetu,3,CONST_SYSDATE,shrShouninshaCD,strShouninshaNM))

	If Err <> 0 Then
		session("kintai").RollBackTrans
		errMSG = "勤怠承認" & WSE0000
		'エラーメッセージ部分を表示する
		Call initProc(0)
		'処理を終了する
		response.End
	End If

	'** トランザクション終了
	session("kintai").CommitTrans

	Response.Redirect hdnBackURL

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	rejectProc()
'PROCEDURE機能：却下
'引数：なし
'****************************************************
Sub rejectProc()

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'申請
	session("kintai").Execute(UPDWST0002_2(strShainCD,strNengetu,4,CONST_SYSDATE,shrShouninshaCD,strShouninshaNM))

	If Err <> 0 Then
		session("kintai").RollBackTrans
		errMSG = "勤怠却下" & WSE0000
		'エラーメッセージ部分を表示する
		Call initProc(0)
		'処理を終了する
		response.End
	End If

	'** トランザクション終了
	session("kintai").CommitTrans

'	Server.Transfer(hdnBackURL)

	Response.Redirect hdnBackURL

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader(pShouninStatus)
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader(pShouninStatus)
%>

<div class="list">

	<table class="l-tbl" style="width:980px">
			<col width="40%">
			<col width="60%">
			<TR>
				<TD><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%></B></FONT></TD>
				</TD>
				<TD align="right">
					<INPUT TYPE="BUTTON" VALUE="表示切替" onClick="_submit(1)" class="s-btn">
<%
	If strShouninshaCD = Empty Then
		'申請中、承認済み申請ボタン使用不可
		If pShouninStatus = "2" Or pShouninStatus = "3" Then
			strFormat = "DISABLED"
		End If

		'勤怠集計表から遷移してきた場合使用不可
		If Left(hdnBackURL,7) = "WSR0001" Then
			strFormat = "DISABLED"
		End If

%>
					<INPUT TYPE="BUTTON" VALUE="申請" onClick="_submit(2)" <%=strFormat%> class="s-btn">
<%
	Else
		'申請中以外、承認、却下ボタン使用不可
		If pShouninStatus <> "2" Then
			strFormat = "DISABLED"
		End If
%>
					<INPUT TYPE="BUTTON" VALUE="承認" onClick="_submit(3)" <%=strFormat%> class="s-btn">
<%
		'承認済み、申請中以外却下ボタン使用不可
		strFormat = Empty
		If pShouninStatus <> "2" And pShouninStatus <> "3" Then
			strFormat = "DISABLED"
		End If
%>
		 			<INPUT TYPE="BUTTON" VALUE="却下" onClick="_submit(4)" <%=strFormat%> class="s-btn">
<%
	End If
	strFormat = Empty
	If hdnBackURL = Empty Then
		strFormat = "DISABLED"
		End If
%>
					<INPUT TYPE="BUTTON" VALUE="印刷確認" onClick="openPrintPage('<%=strShainCD%> ',' <%=strNengetu%>')"  class="s-btn" ID="ButtonPrint" NAME="ButtonPrint">
					<INPUT TYPE="BUTTON" VALUE="戻る" onClick="_back('<%=hdnBackURL%>')" <%=strFormat%> class="s-btn">
				</TD>
			</TR>
	</TABLE>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs,pRs1,pType)
'PROCEDURE機能：勤怠入力
'引数：あり
'			pRs	:日別勤怠情報
'			pRs1:月別勤怠情報
'****************************************************
Sub viewDetail(pRs,pRs1,pType)
%>
<div class="list">
<%
	If pType = 0 Or pType = 2 Then
%>

	<table class="l-tbl" style="width:980px">
		<col width="10%">
		<col width="10%">
		<col width="5%">
		<col width="5%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="8%">
		<col width="*%">
		<TR>
			<TH class="l-cellsec">日付</TH>
			<TH class="l-cellsec">勤務区分</TH>
			<TH class="l-cellsec">出勤</TH>
			<TH class="l-cellsec">退勤</TH>
			<TH class="l-cellsec">稼働<br>時間</TH>
			<TH class="l-cellsec"><FONT COLOR="BLUE">普通<br>残業</TH>
			<TH class="l-cellsec"><FONT COLOR="BLUE">休日<br>残業</TH>
			<TH class="l-cellsec"><FONT COLOR="BLUE">深夜<br>残業</TH>
			<TH class="l-cellsec"><FONT COLOR="RED">不足<br>時間</TH>
<!--
			<TH class="l-cellsec"><FONT COLOR="RED">遅刻<br>時間</TH>
-->
			<TH class="l-cellsec">代休日</TH>
			<TH class="l-cellsec">備考</TH>
		</TR>
	</TABLE>
<%
	End If

	If pType = 1 Or pType = 2 Then
%>
	<table class="l-tbl" style="width:980px">
		<col width="10%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<TR>
		<TH class="l-cellsec">PJ<br>情報</TD>
		<TH class="l-cellsec"><%=pRs1("PJNO1")%><br><%=Mid(pRs1("PJNM1"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO2")%><br><%=Mid(pRs1("PJNM2"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO3")%><br><%=Mid(pRs1("PJNM3"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO4")%><br><%=Mid(pRs1("PJNM4"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO5")%><br><%=Mid(pRs1("PJNM5"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO6")%><br><%=Mid(pRs1("PJNM6"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO7")%><br><%=Mid(pRs1("PJNM7"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO8")%><br><%=Mid(pRs1("PJNM8"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO9")%><br><%=Mid(pRs1("PJNM9"),1,12)%></TD>
		<TH class="l-cellsec"><%=pRs1("PJNO10")%><br><%=Mid(pRs1("PJNM10"),1,12)%></TD>
	 </TR>
	</TABLE>
<%
	End If
%>

  <div class="l-tbl-auto" style="height:390px">
<%
	Do Until pRs.EOF
		strColor = getYoubiFont(pRs("YOUBI"),pRs("KYUUJITUFLG"))
		If pType = 0 Or pType = 2 Then
%>
	<table class="l-tbl" style="width:980px">
		<col width="10%">
		<col width="10%">
		<col width="5%">
		<col width="5%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="8%">
		<col width="*%">
		<TR>
			<td class="l-cellodd">
				<FONT COLOR="<%=strColor%>"><%=toDate3(pRs("NENGAPI"))%>
			</TD>
			<td class="l-cellodd"><%=strKinmuKBN(pRs("KINMUKBN"))%></TH>
			<td class="l-cellodd"><%=pRs("STIME")%></TH>
			<td class="l-cellodd"><%=pRs("ETIME")%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("KINMUTIME"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("ZANGYOUTIMEF"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("ZANGYOUTIMEK"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("ZANGYOUTIMES"))%></TH>
			<td class="l-celloddr"><%=toDecimal2(pRs("SHORTAGETIME"))%></TH>
<!--
			<td class="l-cellodd"><%=toDecimal2(pRs("TIKOKUTIME"))%></TH>
-->
			<td class="l-cellodd"><%=toDate(pRs("DAIKYUUDATE"))%></TH>
			<td class="l-cellodd"><%=pRs("CMT")%></FONT></TD>
		</TR>
	</TABLE>
<%
		End If
		If pType = 1 Or pType = 2 Then 
%>
	<table class="l-tbl" style="width:980px">
		<col width="10%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<TR>
		<td class="l-celleven">
			<FONT COLOR="<%=strColor%>"><%=toDate3(pRs("NENGAPI"))%>
		</TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME1"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME2"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME3"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME4"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME5"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME6"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME7"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME8"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME9"))%></TD>
		<td class="l-cellevenr"><%=toDecimal2(pRs("PJTIME10"))%></TD>
		</TR>
	</TABLE>
<%
		End If
		pRs.MoveNext
	Loop
%>
  </div>
<%
	If pType = 0 Or pType = 2 Then 
%>
	<!------------------月間合計----------------->
	<table class="l-tbl" style="width:980px">
		<col width="10%">
		<col width="10%">
		<col width="5%">
		<col width="5%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="6%">
		<col width="8%">
		<col width="*%">
		<TR>
			<TH class="l-cellsec" ROWSPAN="2">月間合計</TD>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsecr"><%=toDecimal2(pRs1("KINMUTIME"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="BLUE"><%=toDecimal2(pRs1("ZANGYOUTIMEF"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="BLUE"><%=toDecimal2(pRs1("ZANGYOUTIMEK"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="BLUE"><%=toDecimal2(pRs1("ZANGYOUTIMES"))%></TD>
			<TH class="l-cellsecr"><FONT COLOR="RED"><%=toDecimal2(pRs1("SHORTAGETIME"))%></TD>
<!--
			<TH class="l-cellsec"><FONT COLOR="RED"><%=toDecimal2(pRs1("TIKOKUTIME"))%></TD>
-->
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec"></TD>
		</TR>
	</TABLE>
<%
	End If
	If pType = 1 Or pType = 2 Then 
%>
	<table class="l-tbl" style="width:980px">
		<col width="10%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<col width="9%">
		<TR>
			<TH class="l-cellsec">PJ情報<BR>月間合計</TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME1"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME2"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME3"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME4"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME5"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME6"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME7"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME8"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME9"))%></TD>
			<TH class="l-cellsec"><%=toDecimal2(pRs1("PJTIME10"))%></TD>
		</TR>
	</TABLE>
<%
	End If
%>
<!-----------------------HIDDEN項目：--------------------->
	<table class="l-tbl" style="width:980px">
		<TR>
		<TD>
		<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">
		<INPUT TYPE="HIDDEN" NAME="txtBumonCD" VALUE="<%=strBumonCD%>">
		<INPUT TYPE="HIDDEN" NAME="txtNengetu" VALUE="<%=strNengetu%>">
		<INPUT TYPE="HIDDEN" NAME="hdnViewType" VALUE="<%=hdnViewType%>">
		<INPUT TYPE="HIDDEN" NAME="hdnBackURL" 	VALUE="<%=hdnBackURL%>">
		<INPUT TYPE="HIDDEN" NAME="hdnShouninshaNM" 	VALUE="<%=strShouninshaNM%>">
		<INPUT TYPE="HIDDEN" NAME="hdnShouninshaCD" 	VALUE="<%=strShouninshaCD%>">
		</TR>
		</TD>
	</table>
</div>
</CENTER>
</FORM>
</BODY>
</HTML>

<%
End Sub
%>


