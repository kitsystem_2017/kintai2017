<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WS0003
'機能：行先＆スケジュール
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs								'レコードセット
	Dim cnt
	Dim strShainCD				'社員ｺｰﾄﾞ
	Dim strShainNM				'社員氏名
	Dim strBumonCD				'部門ｺｰﾄﾞ
	Dim strBumonNM				'部門名称
	Dim strKengen					'権限

	'パラメーター取得
	Call getParameter()

	'初期表示を行う
	Call initProc()

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle(toDate1(CONST_SYSDATE) & "　公開スケジュール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'公開スケジュール取得
	Set rs = session("kintai").Execute(SELWST0003_2(strShainCD,CONST_SYSDATE))

	'スケジュールを表示
	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：行先掲示＆スケジュールを表示
'引数：なし
'****************************************************
Sub viewDetail(pRs)
%>

	<TABLE WIDTH="450" BORDER="0" BGCOLOR="">
		<TR ALIGN="RIGHT" BGCOLOR="" HEIGHT="24">
			<TD><INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick=history.go(-1)></TD>
		</TR>
	</TABLE>
	<TABLE WIDTH="" BORDER="0" BGCOLOR="LIGHTBLUE">
		<COL WIDTH="30">
		<COL WIDTH="100">
		<COL WIDTH="100">
		<COL WIDTH="120">
		<COL WIDTH="100">
		<TR ALIGN="CENTER" BGCOLOR="LIGHTBLUE" HEIGHT="24">
			<TD><FONT SIZE="3"></FONT></TD>
			<TD><FONT SIZE="3">開始時間</FONT></TD>
			<TD><FONT SIZE="3">終了時間</FONT></TD>
			<TD><FONT SIZE="3">予定期間</FONT></TD>
			<TD><FONT SIZE="3">予定種別</FONT></TD>
		</TR>
		<TR ALIGN="CENTER" BGCOLOR="LIGHTBLUE" HEIGHT="24">
			<TD COLSPAN="5"><FONT SIZE="3">コメント</FONT></TD>
		</TR>
<%
	cnt = 1
	Do Until pRs.EOF
%>
		<TR ALIGN="CENTER" BGCOLOR="WHITE" HEIGHT="24">
			<TD><FONT SIZE="3"><%=cnt%></FONT></TD>
			<TD><FONT SIZE="3"><%=pRs("STIME")%></FONT></TD>
			<TD><FONT SIZE="3"><%=pRs("ETIME")%></FONT></TD>
			<TD ALIGN="RIGHT" ><FONT SIZE="3"><%=pRs("KIKAN")%>(H)</FONT></TD>
			<TD ALIGN="LEFT" ><FONT SIZE="3"><%=strIkisakiArr(pRs("YOTEIKBN"))%></FONT></TD>
		</TR>
		<TR ALIGN="CENTER" BGCOLOR="WHITE" HEIGHT="24">
			<TD COLSPAN="5" ALIGN="LEFT"><FONT SIZE="3"><%=pRs("BIKOU")%></FONT></TD>
		</TR>
<%
		cnt = cnt + 1
		pRs.MoveNext
	Loop
%>
	</TABLE>
</CENTER>
<BR>
<% 
End Sub
%>
