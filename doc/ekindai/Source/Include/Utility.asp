<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'GFN（株）承諾なくの改ざんは硬く禁じます。
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'==============================================
'ファイル名称：Const.asp
'ファイル機能：共通関数ファイル
'==============================================
%>
<!--#include file="SQL.asp"-->
<%
'SMTPサーバ
'CONST CONST_SMTP_SERVER					= "symmetrix.co.jp"
CONST CONST_SMTP_SERVER					= "smtp.lolipop.jp"
'CONST MAIL_FROM	="<info@symmetrix.co.jp>	info@symmetrix.co.jp:symm01	login"
CONST MAIL_FROM	="<info@symmetrix.co.jp>"
'****************************************************
'PROCEDURE名：	DBConnect()
'PROCEDURE機能：データベースに接続する
'引数：なし
'****************************************************
Sub DBConnect()
	If isempty(session("kintai")) Then
		set session("kintai") = server.CreateObject ("ADODB.Connection")
		session("kintai").Open dsn
		session("kintai").CursorLocation = dcr
'		session("kintai").ConnectionTimeout = "600"

	End If
End Sub

'****************************************************
'PROCEDURE名：	checkSession()
'PROCEDURE機能：セッション情報をチェックする
'引数：なし
'****************************************************
Sub checkSession()
	'セッション情報をチェックする
	if isEmpty(session("shainCD")) = true then
		Response.Redirect "Error.html"
	end if
End Sub


'****************************************************
'FUNCTION名：	getShainInfo(pShainCD,rShainNM,rBumonCD,rBumonNM,rKengen)
'FUNCTION機能：社員情報を取得
'引数：あり pShainCD：社員コード
'****************************************************
Function getShainInfo(pShainCD,rShainNM,rBumonCD,rBumonNM,rKengen)

	Dim l_rs

	Set l_rs = session("kintai").Execute(SELM0001(pShainCD))

	getShainInfo = False

	If l_rs.RecordCount > 0 Then
		rShainNM = l_rs("SHAINNM")
		rBumoncd = l_rs("BUMONCD")
		rBumonNM = l_rs("BUMONNM")
		rKengen = l_rs("KENGEN")
		getShainInfo = True
	End If
	Set l_rs = nothing

End Function

'****************************************************
'FUNCTION名：	getShainNM(pShainCD)
'FUNCTION機能：社員情報を取得
'引数：あり pShainCD：社員コード
'****************************************************
Function getShainNM(pShainCD)

	Dim l_rs

	Set l_rs = session("kintai").Execute(SELM0001(pShainCD))

	getShainNM = False

	If l_rs.RecordCount > 0 Then
		getShainNM = l_rs("SHAINNM")
	End If
	Set l_rs = nothing

End Function

'****************************************************
'FUNCTION名：  toString(Num)
'FUNCTION機能：1〜9までの数字の前に0を付加し、Stringにする
'引数：あり　Num：変換する数字
'****************************************************
Function toString(Num)

	toString = Num

	If Num<10 Then
		toString="0" & Num
	End If

End Function

'****************************************************
'FUNCTION名：  toNumber(pChar)
'FUNCTION機能：01〜09までのStringのを0除く
'引数：あり　aChar：変換する文字
'****************************************************
Function toNumber(pChar)
	Dim Str1

	Str1 = Mid(pChar,1,1)
	If Str1 = "0" Then
		toNumber = Mid(pChar,2)
	Else
		toNumber = pChar
	End If

End Function

'****************************************************
'FUNCTION名：  toDate(pDate)
'FUNCTION機能: 日付変換 YY/MM/DDに変換
'引数：あり　pDate：変換する文字
'****************************************************
Function toDate(pDate)
	If isNull(pDate) = True Then
		Exit Function
	End If

	If Trim(pDate) = Empty Or Len(pDate) <> 8 Then
		toDate = Trim(pDate)
	Else
		toDate = mid(pDate,3,2) & "/" & mid(pDate,5,2) & "/" & mid(pDate,7)
	End If
End Function

'****************************************************
'FUNCTION名：  toDate1(pDate)
'FUNCTION機能: 日付変換　YYYY年MM月DD日
'引数：あり　pDate：変換する文字
'****************************************************
Function toDate1(pDate)
	If isNull(pDate) Then
		Exit Function
	End If

	If Trim(pDate) = Empty Or Len(pDate) <> 8 Then
		toDate1 = pDate
	Else
		toDate1 = mid(pDate,1,4) & "年" & mid(pDate,5,2) & "月" & mid(pDate,7)& "日" 
	End If
End Function

'****************************************************
'FUNCTION名：  toDate2(pDate)
'FUNCTION機能: 日付変換　YYYY年MM月
'引数：あり　pDate：変換する文字
'****************************************************
Function toDate2(pDate)
	If isNull(pDate) Then
		Exit Function
	End If

	If Trim(pDate) = Empty Or Len(pDate) < 6 Then
		toDate2 = pDate
	Else
		toDate2 = Mid(pDate,1,4) & "年" & Mid(pDate,5,2) & "月"
	End If
End Function

'****************************************************
'FUNCTION名：  toDate3(pDate)
'FUNCTION機能: 日付変換　MM月DD日
'引数：あり　pDate：変換する文字
'****************************************************
Function toDate3(pDate)
	If isNull(pDate) Then
		Exit Function
	End If

	If Trim(pDate) = Empty Or Len(pDate) < 8 Then
		toDate3 = pDate
	Else
		toDate3 = Mid(pDate,5,2) & "月" & Mid(pDate,7) & "日"
	End If
End Function

'****************************************************
'FUNCTION名：  toDate4(pDate)
'FUNCTION機能: 日付変換　MM/DD
'引数：あり　pDate：変換する文字
'****************************************************
Function toDate4(pDate)
	If isNull(pDate) Then
		Exit Function
	End If

	If Trim(pDate) = Empty Or Len(pDate) < 8 Then
		toDate4 = pDate
	Else
		toDate4 = Mid(pDate,5,2) & "/" & Mid(pDate,7)
	End If
End Function

'****************************************************
'FUNCTION名：  toDate5(pDate)
'FUNCTION機能: 日付変換 YYYY/MM/DDに変換
'引数：あり　pDate：変換する文字
'****************************************************
Function toDate5(pDate)
	If isNull(pDate) = True Then
		Exit Function
	End If

	If Trim(pDate) = Empty Or Len(pDate) <> 8 Then
		toDate5 = Trim(pDate)
	Else
		toDate5 = mid(pDate,1,4) & "/" & mid(pDate,5,2) & "/" & mid(pDate,7)
	End If
End Function

'****************************************************
'Function名：	getYoubiKJ(aYobi)
'Function機能：和暦曜日を返す
'引数：あり aYobi：対象曜日
'****************************************************
Function getYoubiKJ(pYoubi)
	Select Case pYoubi
		Case "1": getYoubiKJ = "月"
		Case "2": getYoubiKJ = "火"
		Case "3": getYoubiKJ = "水"
		Case "4": getYoubiKJ = "木"
		Case "5": getYoubiKJ = "金"
		Case "6": getYoubiKJ = "土" 
		Case "7": getYoubiKJ = "日" 
	End Select
End Function

'****************************************************
'FUNCTION名：	getYoubiColor(pYoubi,pKyuujituFLG)
'FUNCTION機能：曜日を判定し、色を決める
'引数：あり pYoubi：対象曜日
'			pKyuujituFLG：対象稼動区分
'****************************************************
Function getYoubiColor(pYoubi,pKyuujituFLG)
	getYoubiColor = Empty

	If pYoubi = "6" Then
		getYoubiColor = "BLUE"
		Exit Function
	End If

	If pKyuujituFLG = "1" Then
		getYoubiColor = "F08080"
		Exit Function
	End If

End Function

'****************************************************
'FUNCTION名：	getYoubiFont(pYoubi,pKyuujituFLG)
'FUNCTION機能：曜日を判定し、色を決める
'引数：あり pYoubi：対象曜日
'			pKyuujituFLG：対象稼動区分
'****************************************************
Function getYoubiFont(pYoubi,pKyuujituFLG)
	getYoubiFont = "BLACK"

	If pYoubi = "6" Then
		getYoubiFont = "BLUE"
		Exit Function
	End If

	If pKyuujituFLG = "1" Then
		getYoubiFont = "RED"
		Exit Function
	End If

End Function

'****************************************************
'FUNCTION名：	GetMaxMonthDate(pNengapi)
'FUNCTION機能：月間の最大日を取得
'引数：あり 
'			pNengapi：年月日
'****************************************************
Function GetMaxMonthDate(pNengapi)
	GetMaxMonthDate = Left(pNengapi,6) & "31"
	Select case Mid(pNengapi,5,2)
	Case 02,04,06,09,11
		If Mid(pNengapi,5,2) = "02" Then
			If Left(pNengapi,4) mod 4 = 0 Then
				GetMaxMonthDate = Left(pNengapi,6) & "29"
			Else
				GetMaxMonthDate = Left(pNengapi,6) & "28"
			End If
		Else
			GetMaxMonthDate = Left(pNengapi,6) & "30"
		End If
	End Select
End Function

'****************************************************
'FUNCTION名：  getDateDiff(pTime1,pTime2)
'FUNCTION機能：時間１と時間２の時間差
'引数：	pTime1:時間１
'				pTime2:時間２
'戻り値：時間
'****************************************************
Function getDateDiff(pTime1,pTime2)
	If pTime1 = Empty Or pTime2 = Empty Then
		getDateDiff = 0
		Exit Function
	End If
	getDateDiff = DateDiff("n",pTime1,pTime2) / 60
	If getDateDiff < 0 Then
		getDateDiff = getDateDiff + 24
	End If
End Function

'****************************************************
'FUNCTION名：  getWeekDay(pNengapi)
'FUNCTION機能：日付から曜日を取得
'引数：	pNengapi:日付
'戻り値：曜日
'****************************************************
Function getWeekDay(pNengapi)
	getWeekDay = WeekDay(pNengapi)

	'日曜日以外は-1
	If getWeekDay - 1 <> 0 Then
		getWeekDay = getWeekDay - 1
	'日曜日を7に変更
	Else
		getWeekDay = 7
	End If
End Function

'****************************************************
'FUNCTION名：  toEmpty(pNumber)
'FUNCTION機能：数字を空文字に変換
'引数：あり　pNumber：変換する数字
'****************************************************
Function toEmpty(pNumber)
	toEmpty = pNumber
	If pNumber = "0" Then
		toEmpty = Empty
	End If
End Function

'****************************************************
'FUNCTION名：toDecimal2(pNumber)
'FUNCTION機能：小数点2位揃う
'引数：あり　pNumber：変換する数字
'****************************************************
Function toDecimal2(pNumber)
	dim intPos

	toDecimal2 = trim(pNumber)

	If isNull(toDecimal2) = True Or toDecimal2 = Empty Then
		toDecimal2 = Empty
		Exit Function
	End If

	If toDecimal2 = "0" Then
		toDecimal2 = Empty
		Exit Function
	End If

	If Left(toDecimal2,1) <> "." Then
		intPos = instr(1,pNumber,".")
	Else
		Exit Function
	End If

	If intPos = 0 Then
		toDecimal2 = toDecimal2 & ".00"
	Else
		If Len(Mid(toDecimal2,intPos)) = 2 Then
			toDecimal2 = toDecimal2 & "0"
		End If
	End If

End Function

'****************************************************
'FUNCTION名：toDecimal1(pNumber)
'FUNCTION機能：小数点1位揃う
'引数：あり　pNumber：変換する数字
'****************************************************
Function toDecimal1(pNumber)
	dim intPos

	toDecimal1 = trim(pNumber)

	If isNull(toDecimal1) = True Or toDecimal1 = Empty Then
		toDecimal1 = Empty
		Exit Function
	End If

	If toDecimal1 = "0" Then
		toDecimal1 = "0.0"
		Exit Function
	End If

	If Left(toDecimal1,1) <> "." Then
		intPos = instr(1,pNumber,".")
	Else
		Exit Function
	End If

	If intPos = 0 Then
		toDecimal1 = toDecimal1 & ".0"
	End If

End Function


'****************************************************
'FUNCTION名：  changeNengetu(pNengetu,pCase )
'FUNCTION機能：先月,次月の年月を取得
'引数：pNengetu：年月　pCase ：1：来月　0：先月
'****************************************************
Function changeNengetu(pNengetu,pCase )
	If pCase = 1 Then
		Select Case mid(pNengetu,5,2)
			Case "01": changeNengetu = mid(pNengetu,1,4) & "02"
			Case "02": changeNengetu = mid(pNengetu,1,4) & "03"
			Case "03": changeNengetu = mid(pNengetu,1,4) & "04"
			Case "04": changeNengetu = mid(pNengetu,1,4) & "05"
			Case "05": changeNengetu = mid(pNengetu,1,4) & "06"
			Case "06": changeNengetu = mid(pNengetu,1,4) & "07"
			Case "07": changeNengetu = mid(pNengetu,1,4) & "08"
			Case "08": changeNengetu = mid(pNengetu,1,4) & "09"
			Case "09": changeNengetu = mid(pNengetu,1,4) & "10"
			Case "10": changeNengetu = mid(pNengetu,1,4) & "11"
			Case "11": changeNengetu = mid(pNengetu,1,4) & "12"
			Case "12": changeNengetu = mid(pNengetu,1,4)+1 & "01"
		End Select
	Else
		Select Case mid(Nengetu,5,2)
			Case "01": changeNengetu = mid(pNengetu,1,4)-1 & "12"
			Case "02": changeNengetu = mid(pNengetu,1,4) & "01"
			Case "03": changeNengetu = mid(pNengetu,1,4) & "02"
			Case "04": changeNengetu = mid(pNengetu,1,4) & "03"
			Case "05": changeNengetu = mid(pNengetu,1,4) & "04"
			Case "06": changeNengetu = mid(pNengetu,1,4) & "05"
			Case "07": changeNengetu = mid(pNengetu,1,4) & "06"
			Case "08": changeNengetu = mid(pNengetu,1,4) & "07"
			Case "09": changeNengetu = mid(pNengetu,1,4) & "08"
			Case "10": changeNengetu = mid(pNengetu,1,4) & "09"
			Case "11": changeNengetu = mid(pNengetu,1,4) & "10"
			Case "12": changeNengetu = mid(pNengetu,1,4) & "11"
		End Select
	End If
End Function

'****************************************************
'FUNCTION名：  getCurrenNendo()
'FUNCTION機能：カレント年度を取得
'引数：なし
'****************************************************
Function getCurrenNendo()
	Dim strSysYear								'システム年
	strSysYear = Mid(date,1,4)

	GetCurrenNendo = strSysYear
	'四月前は前年度と見なす
	If Mid(date,6,2) - 4 < 0 Then
		GetCurrenNendo = GetCurrenNendo - 1
	End If

End Function

'****************************************************
'FUNCTION名：  getSysNendo()
'FUNCTION機能：システム年度を取得
'引数：なし
'****************************************************
Function getSysNendo()

	getSysNendo = Mid(date,1,4)

End Function


'****************************************************
'FUNCTION名：  toZero(pStr)
'FUNCTION機能：空文字を0に変換
'引数：あり　pStr：変換する文字
'****************************************************
Function toZero(pStr)
	If trim(pStr) = empty Then
		toZero = 0
	Else
		toZero = pStr
	End If
End Function

'****************************************************
'FUNCTION名：	checkDate(pDate)
'FUNCTION機能：デート型であるかどうかをチェック
'引数：あり　pDate：デート
'****************************************************
Function checkDate(pDate)
	checkDate = True
	If isNull(pDate) Then
		Exit Function
	End IF
	If Trim(pDate) = Empty Then
		Exit Function
	End IF
	If isDate(pDate) = False Then
		checkDate = False
	End If
End Function































'****************************************************
'FUNCTION名：  ToSpace(aChar)
'FUNCTION機能：空文字をスペースに変換
'引数：あり　aChar：変換する文字
'****************************************************
Function ToSpace(aChar)
	If isnull(aChar) = true Then
		ToSpace = "　"
	Else
		ToSpace = aChar
	End If

	If trim(aChar) = empty Then
		ToSpace = "　"
	Else
		ToSpace = aChar
	End If
End Function



'****************************************************
'FUNCTION名：  ToEmpty2(aNum)
'FUNCTION機能：数字を空文字に変換
'引数：あり　aNum：変換する数字
'****************************************************
Function ToEmpty2(aNum)
	If aNum = 0 Then
		ToEmpty2 = "　"
	Else
		ToEmpty2 = aNum
	End If
End Function



'****************************************************
'FUNCTION名：  Get_Color(aYmd)
'FUNCTION機能：年月日で色を決める
'引数：あり　aYmd：対象年月日
'****************************************************
Function Get_Color(aYmd)
	dim l_sqlstr,l_rs
	l_sqlstr="SELECT * FROM " & Const_M_Calendar &" WHERE YMD='" & aYmd & "' AND CLDCD='0'"
	set l_rs = session("kintai").Execute(l_sqlstr)

	Get_Color = "white"
	
	If l_rs("kadokbn") = 1 Then
		Get_Color = "tomato"
	End If
	If l_rs("yobi") = 7 Then
		Get_Color = "royalblue"
	End If
End Function

'****************************************************
'FUNCTION名：  Get_Color2(aShouninstate)
'FUNCTION機能：承認状況で色を決める
'引数：あり　aShouninstate：承認状況
'****************************************************
Function Get_Color2(aShouninstate)
	Get_Color2 = "black"
	If aShouninstate = 1 Then
		Get_Color2 = "green"
	ElseIf aShouninstate = 2 Then
		Get_Color2 = "blue"
	ElseIf aShouninstate = 3 Then
		Get_Color2 = "red"
	End If
End Function

'****************************************************
'FUNCTION名：  Get_Color3(pYoubi,pKyuujituFLG)
'FUNCTION機能：色を決める
'引数：あり　aShouninstate：承認状況
'****************************************************
Function Get_Color3(pYoubi,pKyuujituFLG)
	Get_Color3 = "white"
	If pKyuujituFLG = "1" Then
		Get_Color3 = "f08080"
	End If
	If pYoubi = 6 and pKyuujituFLG = "1" Then
		Get_Color3 = "aqua"
	End If
End Function


'****************************************************
'PROCEDURE名：	judge_yobi(aYobi,aKadokbn)
'PROCEDURE機能：曜日を判定し、色、祝日を決める
'引数：あり aYobi：対象曜日
'			aKadokbn：対象稼動区分
'****************************************************
Sub judge_yobi(aYobi,aKadokbn)
	w_Weekcolor = "white"
	If aKadokbn = "1" Then
		w_Weekcolor = "f08080"
	End If

	select Case aYobi
		Case "1": w_WeekH = "日" 
			If aKadokbn = "1" Then
				w_Weekcolor = "f08080"
			End If
		Case "2": w_WeekH = "月"
		Case "3": w_WeekH = "火"
		Case "4": w_WeekH = "水"
		Case "5": w_WeekH = "木"
		Case "6": w_WeekH = "金"
		Case "7": w_WeekH = "土" 
			If aKadokbn = "1" Then
				w_Weekcolor = "blue"
			End If
		Case "土": w_Weekcolor = "blue"

	End select
End Sub

'****************************************************
'FUNCTION名：	judge_yobi2(aYobi)
'FUNCTION機能：曜日を判定し、色、祝日を決める
'引数：あり aYobi：対象曜日
'			aKadokbn：対象稼動区分
'****************************************************
FUNCTION judge_yobi2(aYobi)
	select Case aYobi
		Case "日": w_WeekH = "1" 
		Case "月": w_WeekH = "2"
		Case "火": w_WeekH = "3"
		Case "水": w_WeekH = "4"
		Case "木": w_WeekH = "5"
		Case "金": w_WeekH = "6"
		Case "土": w_WeekH = "7" 
	End select

	judge_yobi2 = w_WeekH

End FUNCTION

'****************************************************
'FUNCTION名：  To_Char()
'FUNCTION機能：小数点2位揃う
'****************************************************
Function To_Char(Str)
	dim l_pos,l_len
	If isnull(Str) = true Then
		To_Char = Empty
		exit Function
	End If
	If Str = empty Or Str = "0" Then
		To_Char = "　"
		exit Function
	End If
	If mid(str,1,1) <> "." Then
		l_pos = instr(1,str,".")
	Else
		exit Function
	End If
	If l_pos = 0 Then
		To_Char = Str & ".00"
	Else
		l_len = len(mid(str,l_pos))
		If l_len = 2 Then
			To_Char = Str & "0"
		Else
			To_Char = Str
		End If
	End If
End Function

'****************************************************
'FUNCTION名：  To_Char1()
'FUNCTION機能：小数点1位揃う
'****************************************************
Function To_Char1(Str)
	dim l_pos,l_len
	If isnull(Str) = true Then
		To_Char1 = Empty
		exit Function
	End If
	If Str = empty Or Str = "0" Then
		To_Char1 = "0.0"
		exit Function
	End If
	If mid(str,1,1) <> "." Then
		l_pos = instr(1,str,".")
	Else
		exit Function
	End If
	If l_pos = 0 Then
		To_Char1 = Str & ".0"
	Else 
		To_Char1 = Str
	End If
End Function

'****************************************************
'FUNCTION名：  To_Char2()
'FUNCTION機能：小数点2位揃う
'****************************************************
Function To_Char2(Str)
	dim l_pos,l_len
	If isnull(Str) = true Then
		To_Char2 = Empty
		exit Function
	End If
	If Str = empty Or Str = "0" Then
		To_Char2 = "0.00"
		exit Function
	End If
	If mid(str,1,1) <> "." Then
		l_pos = instr(1,str,".")
	Else
		exit Function
	End If
	If l_pos = 0 Then
		To_Char2 = Str & ".00"
	Else
		l_len = len(mid(str,l_pos))
		If l_len = 2 Then
			To_Char2 = Str & "0"
		Else
			To_Char2 = Str
		End If
	End If
End Function

'****************************************************
'FUNCTION名：  Cut_Num(Str)
'FUNCTION機能：0.25、0.75をを切り捨て
'****************************************************
Function Cut_Num(Str)
	If isnull(Str) = true Then
		Cut_Num = Empty
		exit Function
	End If
	If Str = empty Or Str = "0" Then
		Cut_Num = Str
		exit Function
	End If
	If mid(str,1,1) <> "." Then
		l_pos = instr(1,str,".")
	Else
		exit Function
	End If
	If l_pos = 0 Then
		Cut_Num = Str
	Else
		If mid(str,l_pos) = ".25" Then
			Cut_Num = mid(str,1,l_pos-1) 
		ElseIf mid(str,l_pos) = ".75" Then
			Cut_Num = mid(str,1,l_pos) & "5" 
		Else
			Cut_Num = Str
		End If
	End If
End Function

'****************************************************
'FUNCTION名：  changeNengetu(Nengetu,aCase )
'FUNCTION機能：先月,次月の年月を取得
'引数：Nengetu：年月　aCase ：1：来月　0：先月
'****************************************************
Function ChangeNengetu(Nengetu,aCase )
	If aCase = 1 Then
		select Case mid(Nengetu,5,2)
			Case "01": ChangeNengetu = mid(Nengetu,1,4) & "02"
			Case "02": ChangeNengetu = mid(Nengetu,1,4) & "03"
			Case "03": ChangeNengetu = mid(Nengetu,1,4) & "04"
			Case "04": ChangeNengetu = mid(Nengetu,1,4) & "05"
			Case "05": ChangeNengetu = mid(Nengetu,1,4) & "06"
			Case "06": ChangeNengetu = mid(Nengetu,1,4) & "07"
			Case "07": ChangeNengetu = mid(Nengetu,1,4) & "08"
			Case "08": ChangeNengetu = mid(Nengetu,1,4) & "09"
			Case "09": ChangeNengetu = mid(Nengetu,1,4) & "10"
			Case "10": ChangeNengetu = mid(Nengetu,1,4) & "11"
			Case "11": ChangeNengetu = mid(Nengetu,1,4) & "12"
			Case "12": ChangeNengetu = mid(Nengetu,1,4)+1 & "01"
		End select
	Else
		select Case mid(Nengetu,5,2)
			Case "01": ChangeNengetu = mid(Nengetu,1,4)-1 & "12"
			Case "02": ChangeNengetu = mid(Nengetu,1,4) & "01"
			Case "03": ChangeNengetu = mid(Nengetu,1,4) & "02"
			Case "04": ChangeNengetu = mid(Nengetu,1,4) & "03"
			Case "05": ChangeNengetu = mid(Nengetu,1,4) & "04"
			Case "06": ChangeNengetu = mid(Nengetu,1,4) & "05"
			Case "07": ChangeNengetu = mid(Nengetu,1,4) & "06"
			Case "08": ChangeNengetu = mid(Nengetu,1,4) & "07"
			Case "09": ChangeNengetu = mid(Nengetu,1,4) & "08"
			Case "10": ChangeNengetu = mid(Nengetu,1,4) & "09"
			Case "11": ChangeNengetu = mid(Nengetu,1,4) & "10"
			Case "12": ChangeNengetu = mid(Nengetu,1,4) & "11"
		End select
	End If
End Function

'****************************************************
'PROCEDURE名：	CheckLen(aStr,aLen)
'PROCEDURE機能：長さをチェック
'****************************************************
Sub CheckLen(aStr,aLen)
	If LenB(aStr) > aLen Then
		Call SendMsg("コメント欄入力した文字列が長すぎます",aLen/2 & "文字以下にしてください。",3)
	End If
End Sub


'****************************************************
'PROCEDURE名：	Check_Daikyuu(aShaincd,aNengapi,aYakushokucd,aMibuncd)
'PROCEDURE機能：代休あるかどうかのチェック
'****************************************************
Sub Check_Daikyuu(aShaincd,aNengapi,aYakushokucd,aMibuncd)

	l_sqlstr = M_DaikyuuSql(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	set l_rs = session("kintai").Execute(l_sqlstr)
	If l_rs("KYUUDEBI") <> "" Then
		Call SendMsg(Mid(aNengapi,5,2) & "/" & Mid(aNengapi,7),"有休選択されましたが、利用可能な代休日があるため、代休を選択してください。",3)
	End If
End Sub

'****************************************************
'PROCEDURE名：	Check_Flex(aNengapi)
'PROCEDURE機能：フレックス使用期間のチェック
'****************************************************
Sub Check_Flex(aNengapi)

	If mid(aNengapi,5,2) > 10 Or mid(aNengapi,5,2) < 7 Then
'		Call SendMsg(Mid(aNengapi,5,2) & "/" & Mid(aNengapi,7),"Flexの使用可能期間は7月1日〜10月31日です。",3)
	End If
End Sub

'****************************************************
'PROCEDURE名：	Check_Kekkin(aShaincd,aNengapi,aYakushokucd,aMibuncd)
'PROCEDURE機能：欠勤を使用できるかどうかのチェック
'****************************************************
Sub Check_Kekkin(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	dim l_sqlstr,l_rs,l_Year
	l_sqlstr = M_DaikyuuSql(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	set l_rs = session("kintai").Execute(l_sqlstr)
	If l_rs("KYUUDEBI") <> "" Then
		Call SendMsg(Mid(aNengapi,5,2) & "/" & Mid(aNengapi,7),"使用可能な代休日があるため、欠勤使用できません。",3)
	End If

	l_Year = mid(aNengapi,1,4)
	If Mid(aNengapi,5,2) < 4 Then
		l_Year = l_Year - 1
	End If
	l_sqlstr = Sel_YuukyuuSql(aShaincd,l_Year)
	set l_rs = session("kintai").Execute(l_sqlstr)
	If l_rs("ZYUUKYUUZAN") + l_rs("YUUKYUUZAN") > 0 Then
		Call SendMsg(Mid(aNengapi,5,2) & "/" & Mid(aNengapi,7),"有休があるため、欠勤使用できません。",3)
	End If

End Sub



'****************************************************
'PROCEDURE名：Set_Daikyuubi(aShaincd,aNengapi,aYakushokucd,aMibuncd)
'PROCEDURE機能：代休日セット
'****************************************************
Sub Set_Daikyuubi(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	dim l_sqlstr,l_rs

	l_sqlstr = M_DaikyuuSql(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	set l_rs = session("kintai").Execute(l_sqlstr)
	If l_rs("KYUUDEBI") = "" Then
		Call SendMsg(Mid(aNengapi,5,2) & "/" & Mid(aNengapi,7),"代休選択されましたが、利用可能な代休日がありません。",3)
	Else
		w_Daikyuubi = l_rs("KYUUDEBI")
		l_sqlstr = "UPDATE " & Const_M_Daikyuu & " SET DAIKYUUBI='" & aNengapi &"' WHERE SHAINCD='" & aShaincd & "' AND KYUUDEBI='" & l_rs("KYUUDEBI") & "'"
		session("kintai").Execute (l_sqlstr)
		'** エラーチェック
		If err <> 0 Then
			'エラーメッセージ表示
			Call SendMsg("エラー発生,エラーNO：" & err,"代休マスタに代休行使日を書き込みできません、管理者に連絡してください。",3)
		End If
	End If
End Sub

'****************************************************
'PROCEDURE名：Set_Daikyuubi2(aShaincd,aNengapi,aYakushokucd,aMibuncd)
'PROCEDURE機能：代休日セット
'****************************************************
Sub Set_Daikyuubi2(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	dim l_Year
	dim l_sqlstr,l_rs,l_rs1

	l_sqlstr = M_DaikyuuSql(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	set l_rs = session("kintai").Execute(l_sqlstr)

	'代休日がある場合は代休日をセット
	If l_rs("KYUUDEBI") <> "" Then
		w_Daikyuubi = l_rs("KYUUDEBI")
		'代休テーブル更新
		l_sqlstr = "UPDATE " & Const_M_Daikyuu & " SET DAIKYUUBI='" & aNengapi &"' WHERE SHAINCD='" & aShaincd & "' AND KYUUDEBI='" & l_rs("KYUUDEBI") & "'"
		session("kintai").Execute (l_sqlstr)
		'** エラーチェック
		If err <> 0 Then
			'エラーメッセージ表示
			Call SendMsg("エラー発生,エラーNO：" & err,"代休マスタに代休行使日を書き込みできません、管理者に連絡してください。",3)
		End If
		'休暇テーブル更新
		l_sqlstr = "UPDATE " & Const_T_KYUUKA & " SET DAIKYUUBI='" & l_rs("KYUUDEBI") & "',YUUKYUU='' WHERE SHAINCD='" & aShaincd & "' AND YOTEIBI='" & aNengapi & "'"
		session("kintai").Execute (l_sqlstr)
		'** エラーチェック
		If err <> 0 Then
			'エラーメッセージ表示
			Call SendMsg("エラー発生,エラーNO：" & err,"休暇テーブルに代休行使日を書き込みできません、管理者に連絡してください。",3)
		End If

		session("kintai").Execute (l_sqlstr)
	'代休日がない場合は有休を使う
	Else
		'休暇テーブル更新
		l_sqlstr = "UPDATE " & Const_T_KYUUKA & " SET YUUKYUU='" & aNengapi & "', DAIKYUUBI='' WHERE SHAINCD='" & aShaincd & "' AND YOTEIBI='" & aNengapi & "'"
		session("kintai").Execute (l_sqlstr)
		'** エラーチェック
		If err <> 0 Then
			'エラーメッセージ表示
			Call SendMsg("エラー発生,エラーNO：" & err,"休暇テーブルに代休行使日を書き込みできません、管理者に連絡してください。",3)
		End If
	End If
End Sub

'****************************************************
'PROCEDURE名：Set_Hankyuu(aShaincd,aNengapi,aYakushokucd,aMibuncd)
'PROCEDURE機能：半休セット
'****************************************************
Sub Set_Hankyuu(aShaincd,aNengapi,aYakushokucd,aMibuncd)
	dim l_sqlstr,l_rs,l_Yoteibi
	'申請予定日を除く最大予定日取得
	'振替フラグを取得
	l_sqlstr = "SELECT FURIKAEFLG FROM " & Const_T_KYUUKA & " " _
	& "WHERE SHAINCD='" & aShaincd & "'" _
	& " AND YOTEIBI=(SELECT MAX(YOTEIBI) YOTEIBI FROM " & Const_T_KYUUKA & " " _
	& "WHERE SHAINCD='" & aShaincd & "' AND KINMUKBN IN('" & w_Kinmukbnarray(3) & "','" & w_Kinmukbnarray(4) & "')" _
	& " AND YOTEIBI<'" & aNengapi & "')"
	set l_rs = session("kintai").Execute (l_sqlstr)
	'振り替えフラグ更新
	If l_rs.RecordCount > 0 Then
		If l_rs("FURIKAEFLG") = 0 Then
			l_sqlstr = "UPDATE " & Const_T_KYUUKA & " SET FURIKAEFLG=1 WHERE SHAINCD='" & aShaincd & "' AND YOTEIBI='" & aNengapi & "'"
			session("kintai").Execute (l_sqlstr)
			'代休日(有休)セット
			Call Set_Daikyuubi2(aShaincd,aNengapi,aYakushokucd,aMibuncd)
		End If
	End If
End Sub

'****************************************************
'PROCEDURE名：Del_Hankyuu(aShaincd,aNengapi,aDaikyuubi)
'PROCEDURE機能：半休削除
'****************************************************
Sub Del_Hankyuu(aShaincd,aNengapi,aDaikyuubi)
	dim l_sqlstr,l_rs

	'振り替えの関係で順番でキャンセルしなければならない
	l_sqlstr = "SELECT MAX(YOTEIBI) YOTEIBI FROM " & Const_T_KYUUKA & " " _
	& "WHERE SHAINCD='" & aShaincd & "' AND KINMUKBN IN('" & w_Kinmukbnarray(3) & "','" & w_Kinmukbnarray(4) & "')"
	set l_rs = session("kintai").Execute (l_sqlstr)
	If l_rs("YOTEIBI") <> aNengapi Then
		Call Sendmsg(toDate(aNengapi),"半休をキャンセルする場合代休、有休の振替の関係で、日付大きい順でキャンセルしなければなりません。<br>" & toDate(l_rs("YOTEIBI")) & "からキャンセルしてください。",3)
	End If

	'代休に振り替えたら、代休テーブルの代休日を空に戻す
	If aDaikyuubi <> "" Then
		sqlstr = "UPDATE " & Const_M_Daikyuu & " SET DAIKYUUBI='' WHERE SHAINCD='" & aShaincd & "' AND DAIKYUUBI='" & aNengapi & "'"
		session("kintai").Execute (sqlstr)
	End If
End Sub

'****************************************************
'FUNCTION名：	sendMail()
'FUNCTION機能：メール送信を実行する
'引数：あり
'戻り値：あり
'				False--失敗
'				True --成功
'****************************************************
Function sendMail(SHAINNM,SINSEICD)

	Dim strTo     '宛先
	Dim strSub     'メール標題
	Dim strBody     'メール本文
	Dim strFile     '添付ファイル

	Dim result     '戻り値
	sendMail = true

	strTo = "<info@symmetrix.co.jp>"
'	strTo = "<skatsu@symmetrix.co.jp>"
	strSub = "交通費・費用精算・仮払い申請"
	strBody = "申請者：" & SHAINNM &  "　　申請種別：" & SINSEICD 
	strFile = ""

	Set bobj = Server.CreateObject("basp21") 
	result = bobj.SendMail(CONST_SMTP_SERVER,strTo,MAIL_FROM, strSub,strBody,strFile)

	If result <> "" Then
		Response.Write("メール送信エラー ：" & result)
'		Response.End
		sendMail = false
	End If

	End Function


%>

<%
'****************************************************
'FUNCTION名：	sendMail1()
'FUNCTION機能：メール送信を実行する
'引数：あり
'戻り値：あり
'				False--失敗
'				True --成功
'****************************************************
Function sendMail1(SHAINNM,MAILTO)

	Dim strTo     '宛先
	Dim strTo1     '宛先
	Dim strSub     'メール標題
	Dim strBody     'メール本文
	Dim strFile     '添付ファイル

	Dim result     '戻り値
	sendMail1 = true

	strTo = "<" + MAILTO & ">"

'	strTo = strTo & ",<info@symmetrix.co.jp>"
'	strTo = "<skatsu@symmetrix.co.jp>"
	
	strSub = "週報"
	strBody = "報告者：" & SHAINNM &  "、　　社内システムにて内容を確認してください。" 
	strFile = ""

	Set bobj = Server.CreateObject("basp21") 
	result = bobj.SendMail(CONST_SMTP_SERVER,strTo,MAIL_FROM, strSub,strBody,strFile)

	If result <> "" Then
		Response.Write("メール送信エラー ：" & result)
'		Response.End
		sendMail1 = false
	End If

End Function

'却下された場合に申請者へ送信 20140421 Start
'****************************************************
'FUNCTION名：	sendMailKyaka()
'FUNCTION機能：申請が却下された場合に申請者へメール送信を実行する
'引数：あり
'戻り値：あり
'				False--失敗
'				True --成功
'****************************************************
Function sendMailKyaka(SHAINNM,SINSEICD,sinseishaMAILADDR,ShounishaMAILADDR)

	Dim strTo     '宛先
	Dim strSub     'メール標題
	Dim strBody     'メール本文
	Dim strFile     '添付ファイル

	Dim result     '戻り値
	
	Dim strFrom    '承認者メールアドレス
	sendMail2 = true

	strTo = "<" & sinseishaMAILADDR & ">"
	strCc = "<r.nishijima@symmetrix.co.jp>"
	strFrom = "<" & ShounishaMAILADDR & ">"
	strSub = "交通費・費用精算・仮払い却下"
	
	strBody = "お疲れ様です、管理部の西島です。"
	strBody = strBody & vbCrLf
	strBody = strBody & vbCrLf & "申請が不備がありまして、却下しました。"
	strBody = strBody & vbCrLf & "お手数ですが、却下内容をご確認の上、"
	strBody = strBody & vbCrLf & "速やかに再申請するよう、よろしくお願いします。"
	strBody = strBody & vbCrLf & vbCrLf & "申請者：" & SHAINNM &  "　　申請種別：" & SINSEICD 
	
	strFile = ""

	Set bobj = Server.CreateObject("basp21") 
	result = bobj.SendMail(CONST_SMTP_SERVER,strTo,strFrom, strSub,strBody,strFile)

	If result <> "" Then
		Response.Write("メール送信エラー ：" & result)
		sendMail2 = false
	End If

	result = bobj.SendMail(CONST_SMTP_SERVER,strCc,strFrom, strSub,strBody,strFile)

	If result <> "" Then
		Response.Write("メール送信エラー ：" & result)
		sendMail2 = false
	End If

End Function
'却下された場合に申請者へ送信 20140421 End


Private Function formatKin(kin)
'    formatKin = Format(kin, "##,###")
	if kin = "" then
		formatKin = ""
	else
		formatKin = FormatCurrency(kin, 0, 0, 0, -1)
	end if
End Function

%>
