<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'GFN（株）承諾なくの改ざんは硬く禁じます。
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'==============================================
'ファイル名称：Const.asp
'ファイル機能：共通定数、変数、メッセージ定義
'==============================================
%>
<%
	'**************************データベース固定情報**************************
	CONST CONST_M_SHAIN					= "symmetrix.M0001"					'社員マスタ
	CONST CONST_M_BUMON					= "symmetrix.M0002"					'部門マスタ
	CONST CONST_M_PJ						= "symmetrix.M0003"					'プロジェクトマスタ
	CONST CONST_M_CALENDAR			= "symmetrix.WSM0001"				'カレンダーマスタ
	CONST CONST_M_WORKCONDITION	= "symmetrix.WSM0002"				'勤務条件マスタ
	CONST CONST_M_TIMEMATRIX		= "symmetrix.WSM0003"				'時間マトリックス
	CONST CONST_M_YUUKYUU				= "symmetrix.WSM0004"				'有休マスタ
	CONST CONST_M_DAIKYUU				= "symmetrix.WSM0005"				'代休マスタ
	CONST CONST_T_DAY						= "symmetrix.WST0001"				'日別勤怠表
	CONST CONST_T_MONTH					= "symmetrix.WST0002"				'月別勤怠表
	CONST CONST_T_SCHEDULE			= "symmetrix.WST0003"				'スケジュール表
	CONST CONST_T_IKISAKI				= "symmetrix.WST0004"				'行先表
	CONST CONST_T_MAIL					= "symmetrix.WST0005"				'メール
	CONST CONST_T_SEISAN					= "symmetrix.WST0006"				'交通費・費用精算・仮払いテーブル
	CONST CONST_T_SHUUHOU				= "symmetrix.WST0007"				'週報

	'**************************Font固定情報**************************
	CONST CONST_FONT_RED				= "<FONT COLOR='RED'>"
	CONST CONST_FONT_BLUE				= "<FONT COLOR='BLUE'>"

	'**************************Image固定情報**************************
	CONST CONST_IMAGE_MAIL				= "<IMG SRC='img/mail.gif' BORDER='0'>"
	CONST CONST_IMAGE_READ				= "<IMG SRC='img/read.gif' BORDER='0'>"
	CONST CONST_IMAGE_PEN					= "<IMG SRC='img/pen.gif' BORDER='0'>"
	CONST CONST_IMAGE_PREV				= "<IMG SRC='img/prev.gif' BORDER='0'>"
	CONST CONST_IMAGE_NEXT				= "<IMG SRC='img/next.gif' BORDER='0'>"
	CONST CONST_IMAGE_WARNING			= "<IMG SRC='img/warning.gif' BORDER='0'>"
	CONST CONST_IMAGE_WARNING1		= "<IMG SRC='img/warning1.gif' BORDER='0' HEIGHT='15' WIDTH='15'>"
	CONST CONST_IMAGE_MEMO				= "<IMG SRC='img/memo.gif' BORDER='0'>"
	CONST CONST_IMAGE_SCHEDULE		= "<IMG SRC='img/schedule.gif' BORDER='0'>"
	CONST CONST_IMAGE_POST		= "<IMG SRC='img/M01.gif' BORDER='0'>"

	'**************************システム情報***************************
	Dim CONST_SYSYEAR,CONST_SYSMONTH,CONST_SYSDATE,CONST_SYSNENGETU,CONST_SYSDAY

	CONST_SYSYEAR						= Mid(Date,1,4)
	CONST_SYSMONTH					= Mid(Date,6,2)
	CONST_SYSDATE						= Mid(Date,1,4) & Mid(Date,6,2) & Mid(Date,9,2)
	CONST_SYSNENGETU				= Mid(Date,1,4) & Mid(Date,6,2)
	CONST_SYSDAY						= Mid(Date,9,2)

	'**************************共通メッセージ定義**************************
	Const WSE0000 = "情報の処理は正常に終了できませんでした。"
	Const WSE0001 = "ログイン画面から社員コードを入力し、再度ログインしてください。"
	Const WSE0002 = "データベースに接続できないかまたデータベースエラー発生しました、管理者に連絡してください。ErrorCD:"
	Const WSE0003 = "該当ユーザの情報を取得できません。"
	Const WSE0003_1 = "該当ユーザの情報はすでに存在するため処理できません。"
	Const WSE0004 = "ログインできません、パスワードが違います。"
	Const WSE0005 = "開始時間、終了時間は一致しています。"
	Const WSE0006 = "スケジュールの時間が重複しています。"
	Const WSE0007 = "スケジュールを管理するデータベースエラー、管理者に報告してください。"
	Const WSE0008 = "選択してください。"
	Const WSE0009 = "情報取得できません。"
	Const WSE0010 = "承認権限がありません。"
	Const WSE0011 = "送信できませんでした。"
	Const WSE0012 = "該当データありません。"
	Const WSE0013 = "正しい時間を入力してください。"
	Const WSE0014 = "：有(半)休選択されましたが、利用可能な有休残数はありません。"
	Const WSE0014_1 = "有休付与されていません。"
	Const WSE0015 = "：代休選択されましたが、利用可能な休日出勤日はありません。"
	Const WSE0016 = "該当データがすでに存在しています。"
	
	Const WSM0001E01 = "カレンダー情報取得できません。"
	Const WST0001E01 = "日単位の勤怠情報をデータベースに登録できませんでした。"
	Const WST0001E02 = "代休、有休、欠勤を選択した場合は出勤時間、退勤時間を入力しないでください。"
	Const WST0002E01 = "月単位の勤怠情報をデータベースに登録できませんでした。"

	Const WSI000 = "処理が正常に完了しました。"


	'**************************グローバル変数定義**************************
	Dim strDateaArr(6,7)			'日付格納用配列
	Dim strKadokbnArr(6,7)		'稼動区分格納用配列
	Dim strYoubiArr(6,7)			'曜日格納用配列
	Dim strYotei(6,7)					'予定格納用配列

	Dim CONST_SHOUNINSTATUS(4)
	CONST_SHOUNINSTATUS(1) = ""
	CONST_SHOUNINSTATUS(2) = "<FONT SIZE='2' COLOR='GREEN'>申請中</FONT>"
	CONST_SHOUNINSTATUS(3) = "<FONT SIZE='2' COLOR='BLUE'>承認</FONT>"
	CONST_SHOUNINSTATUS(4) = "<FONT SIZE='2' COLOR='RED'>却下</FONT>"

	Dim CONST_HOUKOKUSTATUS(4)
	CONST_HOUKOKUSTATUS(1) = ""
	CONST_HOUKOKUSTATUS(2) = "<FONT SIZE='2' COLOR='GREEN'>報告済</FONT>"
	CONST_HOUKOKUSTATUS(3) = "<FONT SIZE='2' COLOR='BLUE'>確認</FONT>"
	CONST_HOUKOKUSTATUS(4) = "<FONT SIZE='2' COLOR='RED'>却下</FONT>"

	'行先
	Dim strIkisakiArr(10)
	strIkisakiArr(1) = ""
	strIkisakiArr(2) = "在席"
	strIkisakiArr(3) = "帰宅"
	strIkisakiArr(4) = "会議"
	strIkisakiArr(5) = "客先常駐"
	strIkisakiArr(6) = "客先訪問"
	strIkisakiArr(7) = "面会"
	strIkisakiArr(8) = "外出"
	strIkisakiArr(9) = "出張"
	strIkisakiArr(10) = "その他"

	'スケジュール予定
	Dim strYoteiArr(7)
	strYoteiArr(1) = ""
	strYoteiArr(2) = "会議"
	strYoteiArr(3) = "面会"
	strYoteiArr(4) = "客先訪問"
	strYoteiArr(5) = "打合せ"
	strYoteiArr(6) = "外出"
	strYoteiArr(7) = "その他"

	'勤務区分
	Dim strKinmukbn(7)
	strKinmukbn(1) = ""
	strKinmukbn(2) = "有休"
	strKinmukbn(3) = "代休"
	strKinmukbn(4) = "午前半休"
	strKinmukbn(5) = "午後半休"
	strKinmukbn(6) = "欠勤"
	strKinmukbn(7) = "その他"

	'申請種別  1：定期券・交通費　2：費用精算　3:仮払い
	Dim strSINSEICD(3)
	strSINSEICD(1) = "1"
	strSINSEICD(2) = "2"
	strSINSEICD(3) = "3"
	
	'申請名称  1：定期券・交通費　2：費用精算　3:仮払い
	Dim strSINSEINM(3)
	strSINSEINM(1) = "定期券・交通費"
	strSINSEINM(2) = "費用精算"
	strSINSEINM(3) = "仮払い"
	
	'路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他
	Dim strROOTArr(7)
	strROOTArr(1) = ""
	strROOTArr(2) = "JR"
	strROOTArr(3) = "地下鉄"
	strROOTArr(4) = "私鉄"
	strROOTArr(5) = "タクシー"
	strROOTArr(6) = "バス"
	strROOTArr(7) = "その他"
	
	
%>
