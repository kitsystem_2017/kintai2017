
//**********���ʕ����̃X�N���v�g(S)**********
//window.document.onkeydown = getKeyCode;
//window.document.onmousedown = prohibitRightClick;
//�L�[����
function getKeyCode() {
    var keycode = event.keyCode;
    var Alt = event.altKey;
    var Ctrl = event.ctrlKey;
    //Enter�L�[
    if(keycode == 13){
        //INPUT�^�O��type=text,password,file,radio,checkbox�ł́A�g�p�s��
        if(window.event.srcElement.tagName == "INPUT"){
            if(window.event.srcElement.type == "text" ||
               window.event.srcElement.type == "password" ||
               window.event.srcElement.type == "file" ||
               window.event.srcElement.type == "radio" ||
               window.event.srcElement.type == "checkbox"){
                event.keyCode = 0;
                return false;
            }
        }
        return true;
    }
    //BackSpace�L�[
    if(keycode == 8){
        //INPUT�^�O��type=text,password,file�ł́A�g�p�\
        if(window.event.srcElement.tagName == "INPUT"){
            if((window.event.srcElement.type == "text" && window.event.srcElement.readOnly == false) || 
               (window.event.srcElement.type == "password" && window.event.srcElement.readOnly == false) ||
               window.event.srcElement.type == "file"){
                return true;
            }
        }
        //TEXTAREA�^�O�ł́A�g�p�\
        if(window.event.srcElement.tagName == "TEXTAREA" && window.event.srcElement.readOnly == false){
            return true;
        }
        event.keyCode = 0;
        return false;
    }
    //Alt�V���[�g�J�b�g�L�[�ibackspace�E���E���EHOME�j

    if ( Alt ) {
      if ( keycode == 8 || keycode == 37 || keycode == 39 || keycode == 36 ) {
           alert("�V���[�g�J�b�g�L�[�͎g�p�ł��܂���B");
           return false;
      }
    }
    //Ctrl�V���[�g�J�b�g�L�[�iN�j

    if ( Ctrl ) {
      if  ( keycode == 78 ) {
            alert("�V���[�g�J�b�g�L�[�͎g�p�ł��܂���B");
            return false;
      }
    }
    //F5�L�[
    if(keycode == 116){
        event.keyCode = 0;
        return false;
    }
    //F11�L�[
    if(keycode == 122){
        event.keyCode = 0;
        return false;
    }
    //Esc�L�[
    if(keycode == 27){
        event.keyCode = 0;
        return false;
    }
}

//�E�N���b�N�֎~
function prohibitRightClick(){
  if(event.button == 2 || event.button == 3 || event.button == 6 || event.button == 7){
    alert("�E�N���b�N�͎g�p�ł��܂���B");
    return false;
  }
}

//�^�C���`�F�b�N
function judgeTime(elm){
	var errMSG = "���̓G���[,'HHMM' or 'HH:MM'�����œ��͂��Ă��������B";
	var errMSG1 = "15���P�ʂœ��͂��Ă��������B";
	var errMSG2 = "�[��24:00�ȏ��00:00������͂��Ă��������B";
	var errMSG3 = "���p�p�����œ��͂��Ă��������B"
	var str = elm.value;

	//�����͂̏ꍇ�͏������Ȃ�
	if(str.length == 0){
		return;
	}

	//�S�p�����`�F�b�N
	if (judgeZenkaku(elm)==false){
		return;
	};

	//4������5���ȏ�̓G���[�Ƃ���
	if((str.length < 4) || (str.length > 5)){
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}

	//4����HH:MM�ɕϊ����ĕԂ�
	if(str.length == 4){
		//":"�������Ă���ꍇ
		if(str.indexOf(":") != -1){
			//HH:MM�ɕϊ����ĕԂ�
			str = "0" + str
		//":"�������Ă��Ȃ��ꍇ
		}else{
			//HH:MM�ɕϊ����ĕԂ�
			str = str.substr(0,2) + ":" + str.substr(2,2);
		}
	}
	//5���̏ꍇ��
	if(str.length == 5){
		//3���ڂ�":"�łȂ��ꍇ�̓G���[
		if(str.indexOf(":") != 2){
			alert(errMSG);
			elm.value = "";
			elm.focus();
			return;
		}

		j = 0;
		for (i = 0; i < str.length; i++){
			if (str.charAt(i) == ":"){
				j++;
			}
		}
		//":"�������Ă��Ȃ��ꍇ����ȏ�����Ă���ꍇ�G���[
		if(j == 0 || j > 1){
			alert(errMSG);
			elm.value = "";
			elm.focus();
			return;
		}
	}
	//15�����݂łȂ��ꍇ�̓G���[
	if((str.substr(3) != "00") && (str.substr(3) != "15") && (str.substr(3) != "30") && (str.substr(3) != "45")){
		alert(errMSG1);
		elm.value = "";
		elm.focus();
		return;
	};

	//24�ȏ���͂��ꂽ�ꍇ�̓G���[
	if(str.substr(0,2) >= 24) {
		alert(errMSG2);
		elm.value = "";
		elm.focus();
		return;
	};

	//�ϊ���̒l���Z�b�g
	elm.value = str;
	return;
}
//���l�`�F�b�N
function judgeNumber(elm){
	var str;
	var errMSG = "���������l����͂��Ă��������B";
	var errMSG1 = "0.25�P�ʂœ��͂��Ă��������B";
	var errMSG2 = "���p�p�����œ��͂��Ă��������B"

	str = elm.value;

	//�����͂̏ꍇ�͏������Ȃ�
	if((str.length == 0) || (str == "0")){
		return;
	}

	//�S�p�����`�F�b�N
	if (judgeZenkaku(elm)==false){
		return;
	};

	//���l�`�F�b�N
	if (isNaN(str)) {
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}
	//���l�`�F�b�N
	if ((str % 0.25) != 0) {
		alert(errMSG1);
		elm.value = "";
		elm.focus();
		return;
	}
/*
	//"."�������Ă���ꍇ
	i = str.indexOf(".");
	if(i != -1){
		if(str.substr(i+1).length == 1){
			str = elm.value + "0";
		}
	//"."�������Ă��Ȃ��ꍇ
	}else{
		//99.99�ɕϊ����ĕԂ�
		str = elm.value + ".00";
	}
*/
	//�ϊ���̒l���Z�b�g
	elm.value = str;

	return;
}

//���l�`�F�b�N
function judgeNumber2(elm){
	var str;
	var errMSG = "���p�p�����œ��͂��Ă��������B\n���t���ڂ�8��(YYYYMMDD)�œ��͂��Ă��������B";
	str = elm.value;

	//�����͂̏ꍇ�͏������Ȃ�
	if((str.length == 0) || (str == "0")){
		return;
	}

	//�S�p�����`�F�b�N
	if (judgeZenkaku(elm)==false){
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	};

	//���l�`�F�b�N
	if (isNaN(str)) {
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}

	return;
}
//���l�`�F�b�N
function judgeNumber1(elm){
	var str;
	var errMSG = "���������l����͂��Ă��������B";
	var errMSG1 = "0.25�P�ʂœ��͂��Ă��������B";
	var errMSG2 = "���p�p�����œ��͂��Ă��������B"

	str = elm.value;

	//�����͂̏ꍇ�͏������Ȃ�
	if(str.length == 0){
		return;
	}

	//�S�p�����`�F�b�N
	if (judgeZenkaku(elm)==false){
		return;
	};

	//���l�`�F�b�N
	if (isNaN(str)) {
		alert(errMSG);
		elm.value = "";
		elm.focus();
		return;
	}

	return;
}


//�S�p����
function judgeZenkaku(elm){
	var errMSG = "���p�p�����œ��͂��Ă��������B"
	var dore;
	for(i=0; i<elm.value.length; i++){
		dore=escape(elm.value.charAt(i));
		if(dore.length>3 && dore.indexOf("%")!=-1){
			alert(errMSG);
			elm.value = "";
			elm.focus();
			return false;
		}
	}
	return true;
}

//���ׂă`�F�b�N�i�O���j
function checkAll(cnt){
	//���ׂă`�F�b�N
	if(document.bottom.chkall.checked == true){
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk"+i].checked = true;
		}
		return;
	}else{
		//���ׂĊO��
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk"+i].checked = false;
		}
		return;
	}
}

//���ׂă`�F�b�N�i�O���j
function checkAll1(cnt){
	//���ׂă`�F�b�N
	if(document.bottom.chkall1.checked == true){
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk_1"+i].checked = true;
		}
		return;
	}else{
		//���ׂĊO��
		for(i = 1;i <= cnt;i++){
			document.bottom.elements["chk_1"+i].checked = false;
		}
		return;
	}
}


//�Ј�������ʊJ��
function _open(idx){
	nwin =window.open("T0001SW01.asp?idx="+idx,"T0001SW01","width=600,height=800,menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no");
	nwin.focus();
}

//�v���W�F�N�g��񌟍���ʊJ��
function _openWST0001SW001(idx){
	nwin =window.open("WST0001SW001.asp?idx="+idx,"WST0001SW001","width=600,height=800,menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no");
	nwin.focus();
}


//**********���ʕ����̃X�N���v�g(E)**********

//**********�Αӓ��͉�ʂ̃X�N���v�g(S)**********
function SubmitWST0001(type){
	//�o�^
	if(type == 0) {
		document.bottom.action="WST0001.asp?mode=update";
		document.bottom.submit();
		return;
	}
	//�ꃖ���m�F
	if(type == 1) {
		document.bottom.action="WST0001_1.asp";
		document.bottom.submit();
		return;
	}
	//�v���W�F�N�g�ݒ�
	if(type == 2) {
		document.bottom.action="WST0001_2.asp";
		document.bottom.submit();
		return;
	}
}

//**********�Αӓ��͉�ʂ̃X�N���v�g(E)**********

//**********�v���W�F�N�g�ݒ��ʂ̃X�N���v�g(S)**********
function SubmitWST0001_2(type){
	//�N���A
	if(type == 3) {
		document.bottom.txtPJNO1.value = "";
		document.bottom.txtPJNO2.value = "";
		document.bottom.txtPJNO3.value = "";
		document.bottom.txtPJNO4.value = "";
		document.bottom.txtPJNO5.value = "";
		document.bottom.txtPJNO6.value = "";
		document.bottom.txtPJNO7.value = "";
		document.bottom.txtPJNO8.value = "";
		document.bottom.txtPJNO9.value = "";
		document.bottom.txtPJNO10.value = "";
		return;
	}
	//�ݒ�
	if(type == 1) {
		document.bottom.action="WST0001_2.asp?mode=1";
		document.bottom.submit();
		return;
	}
	//�߂�
	if(type == 2) {
		document.bottom.action="WST0001_2.asp?mode=2";
		document.bottom.submit();
		return;
	}
}

//**********�Αӓ��͉�ʂ̃X�N���v�g(E)**********

//**********�Ζ�������ʂ̃X�N���v�g(S)**********
function SubmitWSM0002(){
	if(confirm('�m�肵�Ă���낵���ł����H')){
		document.bottom.action="WSM0002.asp?mode=update";
		document.bottom.submit();
	}
	else{}
}
//**********�Ζ�������ʂ̃X�N���v�g(E)**********

//**********�Ζ�������ʂ̃X�N���v�g(S)**********
function SubmitWST0005(type){
	document.bottom.target="bottom";
	document.bottom.action="WST0005.asp?mode="+type;
	document.bottom.submit();
	return;
}
//**********�Ζ�������ʂ̃X�N���v�g(E)**********

function SubmitWSM0004(type){
	if (type == "1"){
		if(!confirm("�o�^���Ă���낵���ł����H")){return;}
	}
	if (type == "2"){
		if(!confirm("�X�V���Ă���낵���ł����H")){return;}
	}
	if (type == "3"){
		if(!confirm("�폜���Ă���낵���ł����H")){return;}
	}
	if (type == "4"){
		if(!confirm("�O�N�x�L�x�f�[�^���捞�ޏꍇ�͍��N�x�̗L�x�f�[�^�͑S�����Z�b�g����܂����A���s���Ă���낵���ł����H")){return;}
	}
	document.bottom.target="bottom";
	document.bottom.action="WSM0004.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********�Ј��ݒ��ʂ̃X�N���v�g(S)**********
function SubmitM0001(type){
	if (type == "1"){
		if(!confirm("�o�^���Ă���낵���ł����H")){return;}
	}
	if (type == "2"){
		if(!confirm("�X�V���Ă���낵���ł����H")){return;}
	}
	if (type == "3"){
		if(!confirm("�폜���Ă���낵���ł����H")){return;}
	}
	if (type == "4"){
	}
	document.bottom.target="bottom";
	document.bottom.action="M0001.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********�Ј��ݒ��ʂ̃X�N���v�g(E)**********

//**********����ݒ��ʂ̃X�N���v�g(S)**********
function SubmitM0002(type){
	if (type == "1"){
		if(!confirm("�o�^���Ă���낵���ł����H")){return;}
	}
	if (type == "2"){
		if(!confirm("�X�V���Ă���낵���ł����H")){return;}
	}
	if (type == "3"){
		if(!confirm("�폜���Ă���낵���ł����H")){return;}
	}
	if (type == "4"){
	}
	document.bottom.target="bottom";
	document.bottom.action="M0002.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********����ݒ��ʂ̃X�N���v�g(E)**********

//**********�v���W�F�N�g�ݒ��ʂ̃X�N���v�g(S)**********
function SubmitM0003(type){
	if (type == "1"){
		if(!confirm("�o�^���Ă���낵���ł����H")){return;}
	}
	if (type == "2"){
		if(!confirm("�X�V���Ă���낵���ł����H")){return;}
	}
	if (type == "3"){
		if(!confirm("�폜���Ă���낵���ł����H")){return;}
	}
	if (type == "4"){
	}
	document.bottom.target="bottom";
	document.bottom.action="M0003.asp?mode="+type;
	document.bottom.submit();
	return;
}

//**********�v���W�F�N�g�ݒ��ʂ̃X�N���v�g(E)**********

//**********����ݒ��ʂ̃X�N���v�g(E)**********


//**********������̕ύX�͏o�y�ь�ʔ�̐��Z�\����ʂ̃X�N���v�g(S)**********
function SubmitWST0006(type){
	if (type == "1"){
		if(!confirm("���Z�\�����Ă���낵���ł����H")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0008.asp?mode=update";
		document.bottom.submit();
	}
	if (type == "2"){
		if(!confirm("���Z�\�����Ă���낵���ł����H")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0009.asp?mode=update";
		document.bottom.submit();
	}
	if (type == "3"){
		if(!confirm("���Z�\�����Ă���낵���ł����H")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0010.asp?mode=update";
		document.bottom.submit();
	}
	return;
}

function SubmitWST0006_1(type,mode){

	if (mode == "3"){
		if(!confirm("���F���Ă���낵���ł����H")){return;}
	}
	
	if (mode == "4"){
		if(!confirm("�p�����Ă���낵���ł����H")){return;}
	}

	if (type == "1"){
		document.bottom.target="bottom";
		document.bottom.action="WST0008_1.asp?mode="+mode;
		document.bottom.submit();
	}
	if (type == "2"){
		document.bottom.target="bottom";
		document.bottom.action="WST0009_1.asp?mode="+mode;
		document.bottom.submit();
	}
	if (type == "3"){
		document.bottom.target="bottom";
		document.bottom.action="WST0010_1.asp?mode="+mode;
		document.bottom.submit();
	}
	return;
}
//**********������̕ύX�͏o�y�ь�ʔ�̐��Z�\����ʂ̃X�N���v�g(E)**********

//**********�T�ԕ񍐉�ʂ̃X�N���v�g(S)**********
function SubmitWST0007(type,elm){

	str = elm.value;

	//�����͂̏ꍇ�͏������Ȃ�
	if(str.length == 0){
		alert("�񍐐惁�[���A�h���X����͂��Ă��������B")
		return;
	}

	if (type == "0"){
		if(!confirm("�T�ԕ񍐂��Ă���낵���ł����H")){return;}
		document.bottom.target="bottom";
		document.bottom.action="WST0013.asp?mode=update";
		document.bottom.submit();
	}
	return;
}

function SubmitWST0007_1(mode,elm){

	str = elm.value;

	//�����͂̏ꍇ�͏������Ȃ�
	if(str.length == 0){
		alert("�m�F�҂̃R�����g����͂��Ă��������B")
		return;
	}

	if (mode == "3"){
		if(!confirm("���F���Ă���낵���ł����H")){return;}
	}
	
	if (mode == "4"){
		if(!confirm("�p�����Ă���낵���ł����H")){return;}
	}

	document.bottom.action="WST0013.asp?mode="+mode;
	document.bottom.submit();

	return;
}

//**********�T�ԕ񍐉�ʂ̃X�N���v�g(E)**********

function _back(BackURL){
	document.bottom.action=BackURL;
	document.bottom.submit();
	return;
}



