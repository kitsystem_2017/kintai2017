<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'GFN（株）承諾なくの改ざんは硬く禁じます。
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'==============================================
'ファイル名称：Content.asp
'ファイル機能：PegeのContent表示ファイル
'==============================================
%>

<%
'****************************************************
'PROCEDURE名：	viewTitle(pHeadLine,pBumonCD,pBumonNM,pShainCD,pShainNM)
'PROCEDURE機能：画面のヘッダを表示する
'引数：あり
'				1:ヘッダーラインﾞ
'				2:部門ｺｰﾄﾞ
'				3:部門名称
'				4:社員ｺｰﾄﾞ
'				5:社員氏名
'****************************************************
Sub viewTitle(pHeadLine,pBumonCD,pBumonNM,pShainCD,pShainNM)
%>
  <div class="list">
	<table class="l-tbl">
		<TR>
			<TD ALIGN="CENTER">
				<span class="txt-gray1"><U><% =pHeadLine %></U></span>
			</TD>
		</TR>
		<TR ALIGN="LEFT" VALIGN="TOP" NOWRAP>
			<TD ALIGN="LEFT">
				<span class="txt-gray">社員番号：<U><% =pShainCD %></U></span>&nbsp;&nbsp;&nbsp;
				<span class="txt-gray">氏名：<U><% =pShainNM %></U></span>&nbsp;&nbsp;&nbsp;
				<span class="txt-gray">所属：<U><% =pBumonCD %>&nbsp;<% =pBumonNM %></U></span>
			</TD>
		</TR>
	</TABLE>
  </div>
<%
End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewSide(pRs,pParent,pShainCD,pNengetu)
'PROCEDURE機能：画面のカレンダー、メッセージ部分を表示する
'引数：あり
'			1:pRs			--レコードセット
'			2:pParent --呼び元  1:勤怠入力画面	2:行先＆スケジュール
'****************************************************
Sub viewSide(pRs,pParent,pShainCD,pNengetu)
	Dim strDateFrom				'開始期間
	Dim strDateTo					'終了期間
	Dim strFont						'フォント
	Dim strColor					'カラー
	Dim cnt,cnt1
	Dim lst								'レコードセット


	'システム年月日を取得
	strSysDate = mid(date,1,4) & mid(date,6,2) & mid(date,9,2)
%>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="168" ALIGN="CENTER" VALIGN="TOP">
				<!--**********************カレンダー表示(S)****************************-->
			  <div class="list">
				<table class="l-tbl">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
				 <col width="12.5%">
					<TR>
						<TH class="l-cellcenter" COLSPAN="8">
							<% If pParent = 2 Then 'スケジュールの場合のみ表示%>
								<A HREF="WST0003.asp?shaincd=<%= pShainCD %>&nengetu=<%= changeNengetu(pNengetu,0) %>&nengapi=<%= changeNengetu(pNengetu,0)&"01" %>">
								<IMG SRC=IMG/PREV.GIf BORDER="0"></A>
							<% End If %>
							 <% =TODATE2(pNengetu) %> </FONT>
							<% If pParent = 2 Then 'スケジュールの場合のみ表示%>
								<A HREF="WST0003.asp?shaincd=<%= pShainCD %>&nengetu=<%= changeNengetu(pNengetu,1) %>&nengapi=<%= changeNengetu(pNengetu,1)&"01" %>">
								<IMG SRC=IMG/Next.GIf  BORDER="0">
							<% End If %>
						</TH>
					</TR>
					<TR>
						<TD class="l-cellodd"></TD>
						<TD class="l-cellodd">月</TD>
						<TD class="l-cellodd">火</TD>
						<TD class="l-cellodd">水</TD>
						<TD class="l-cellodd">木</TD>
						<TD class="l-cellodd">金</TD>
						<TD class="l-cellodd" style="background-color:AQUA;background-image:none;">土</TD>
						<TD class="l-cellodd" style="background-color:#F08080;background-image:none;">日</TD>
					</TR>

					<%
					'日付情報を格納の二次元配列初期化
					For cnt = 1 To 6
						For cnt1 = 1 To 7
							strDateaArr(cnt,cnt1) = ""
							strKadokbnArr(cnt,cnt1) = 0
							strYoubiArr(cnt,cnt1) = 0
						Next
					Next

					cnt1 = 1

					'二次元配列にデータ代入
					For cnt = 1 To pRs.RecordCount
						If pRs("YOUBI") - 1 = 0 and cnt > 1 Then
							cnt1 = cnt1 + 1
						End If
						'二次元配列にセット
						cnt2 = pRs("YOUBI") + 0
						strDateaArr(cnt1,cnt2) = mid(pRs("NENGAPI"),7)
						strKadokbnArr(cnt1,cnt2) = pRs("KYUUJITUFLG")
						strYoubiArr(cnt1,cnt2) = pRs("YOUBI")
						strYotei(cnt1,cnt2) = pRs("SEQ")
						pRs.moveNext
					Next
					For cnt = 1 To 6
						If strDateaArr(cnt,1) = emtpy and cnt > 1 Then
							exit for
						End If
						'週間開始日、終了日を算出
						strDateFrom = pNengetu & strDateaArr(cnt,1)
						If strDateFrom = pNengetu Then
							strDateFrom = pNengetu & "01"
						End If
						strDateTo = pNengetu & strDateaArr(cnt,7)
						If strDateTo = pNengetu Then
							strDateTo = GetMaxMonthDate(strDateTo)
						End If
						%>
						<TR>
						<TD class="l-cellodd">
							<A HREF="WST0001.asp?shaincd=<%= pShainCD %>&nengetu=<%= pNengetu %>&DateFrom=<%= strDateFrom %>&DateTo=<%= strDateTo %>">
							<IMG SRC=IMG/PEN.GIF WIDTH="15" HEIGHT="15" BORDER="0"></A>
						</TD>
						<% For cnt1 = 1 To 7
							'色を取得
								strFont = ""
								'スケジュールある場合
								If strYotei(cnt,cnt1) > 0 Then strFont = "<B><FONT SIZE='2'>" End If
								'今日のフォント強調
								If pNengetu & strDateaArr(cnt,cnt1) = strSysDate Then strFont = strFont & "<FONT SIZE='4'>" End If
'								strColor = getYoubiColor(strYoubiArr(cnt,cnt1),strKadokbnArr(cnt,cnt1))
								strColor = ""
								if strYoubiArr(cnt,cnt1) = 6 then
									strColor = "style='background-color:AQUA;background-image:none;'"
								elseif strYoubiArr(cnt,cnt1) = 7 then
									strColor = "style='background-color:#F08080;background-image:none;'"
								elseif strKadokbnArr(cnt,cnt1) = 1 then
									strColor = "style='background-color:#F08080;background-image:none;'"
								end if
								%>
								<TD class="l-cellodd" <%=strColor%>>
									<A HREF="WST0003.asp?shaincd=<%= pShainCD %>&nengetu=<%= pNengetu %>&nengapi=<%= pNengetu & strDateaArr(cnt,cnt1) %>">
									<%=strFont%><%= toNumber(strDateaArr(cnt,cnt1)) %></FONT></A>
								</TD>
								<% strDateaArr(cnt,cnt1) = ""
							Next %>
						</TR>
					<% Next %>
				</TABLE>
				</div>
				<BR>
				<!--**********************カレンダー表示(E)****************************-->

				<!--**********************メッセージ表示(S)****************************-->
<%
	'メールチェック
	lRs = session("kintai").Execute(SELWST0005_5(pShainCD))
	If lRs("CNT") > 0 Then
%>
			  <div class="list">
				<table class="l-tbl">
					<TR>
						<TD><%=CONST_IMAGE_POST%><span class="txt-attention">
								未読メッセージがあります,社内簡易メールにて確認してください。
						</span>
						</TD>
					</TR>
				</TABLE>
			</div>
<%
	End If
%>

<%
	'勤怠表却下チェック
	lRs = session("kintai").Execute(SELWST0002_6(pShainCD))
	If lRs("CNT") > 0 Then
%>
			  <div class="list">
				<table class="l-tbl">
					<TR>
						<TD><%=CONST_IMAGE_WARNING1%>
						<span class="txt-attention">
						勤務表の申請が却下されました、勤務表の一覧にて確認し修正した上再度申請してください。
						</span>
						</TD>
					</TR>
				</TABLE>
				</div>
<%
	End If
	Set lsr = Nothing
%>

				<!--**********************メッセージ表示(E)****************************-->

<%

	Call viewKinmuConfig()
%>


			</TD>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewBumonCMB(pBumonCD)
'PROCEDURE機能：部門情報を表示
'引数：あり
'			pBumonCD	:部門コード
'****************************************************
Sub viewBumonCMB(pBumonCD)
	Dim lRs
	Dim lstrFormat
	Set lRs = session("kintai").Execute(SELM0002())
%>

	<SELECT NAME="cmbBumonCD">
	<OPTION VALUE=""></OPTION>
<%
	 lstrFormat = Empty
	 If pBumonCD = "All" Then lstrFormat = "SELECTED"
%>
	<OPTION VALUE="All" <%=lstrFormat%>>全部門</OPTION>
<% Do Until lRs.EOF
	 lstrFormat = Empty
	 If Strcomp(pBumonCD,lRs("BUMONCD")) = 0 Then lstrFormat = "SELECTED"
%>
		<OPTION VALUE="<%=lRs("BUMONCD")%>" <%=lstrFormat%>>
			<%=lRs("BUMONCD") & ":" & lRs("BUMONNM")%>
		</OPTION>
<% lRs.MoveNext
	 Loop
%>
	</SELECT>
<%
	Set lRs = Nothing
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewNendoCMB(pNendo)
'PROCEDURE機能：カレンダー年度の情報を表示
'引数：あり
'			pNendo	:年度
'****************************************************
Sub viewNendoCMB(pNendo)
	Dim lRs
	Dim lstrFormat
	'カレンダーマスタから年度情報を取得
	Set lRs = session("kintai").Execute(SELWSM0001_3(0))
%>

	<SELECT NAME="cmbNendo">
<%Do Until lRs.EOF
	lstrFormat = Empty
	If Strcomp(pNendo,lRs("NENDO")) = 0 Then lstrFormat="SELECTED" End If%>
		<OPTION <%=lstrFormat%>><%=lRs("NENDO")%></OPTION>
<%lRs.MoveNext
	Loop%>
	</SELECT>
<%
	Set lRs = Nothing
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewMonthCMB(pMonth)
'PROCEDURE機能：月を表示
'引数：あり
'			pNendo	:月
'****************************************************
Sub viewMonthCMB(pMonth)
	Dim lcnt
	Dim lstrFormat
%>
	<SELECT NAME="cmbMonth">
<%
	For lcnt = 1 To 12
		lstrFormat = Empty
		If pMonth - lcnt = 0 Then
			lstrFormat="SELECTED" 
		End If
%>
	<OPTION <%=lstrFormat%>><%= toString(lcnt)%></OPTION>
<%
	Next
%>
	</SELECT>
<%
End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewKintaiHeader()
'PROCEDURE機能：勤怠表のヘッダーを表示
'引数：なし
'****************************************************
Sub viewKintaiHeader()
%>
	<TH class="l-cellsec">年月</TH>
	<TH class="l-cellsec">標準<BR>日数</TH>
	<TH class="l-cellsec">稼働<BR>日数</TH>
	<TH class="l-cellsec">標準<BR>時間</TH>
	<TH class="l-cellsec">稼働<BR>時間</TH>
	<TH class="l-cellsec"><FONT COLOR="BLUE">普通<BR>残業</TH>
	<TH class="l-cellsec"><FONT COLOR="BLUE">休日<BR>残業</TH>
	<TH class="l-cellsec"><FONT COLOR="BLUE">深夜<BR>残業</TH>
	<TH class="l-cellsec"><FONT COLOR="RED">不足<BR>時間</TH>
<!--
	<TH class="l-cellsec"><FONT COLOR="RED">遅刻<BR>時間</TH>
-->
	<TH class="l-cellsec">有休<BR>日数</TH>
	<TH class="l-cellsec">代休<BR>日数</TH>
	<TH class="l-cellsec">欠勤<BR>日数</TH>
	<TH class="l-cellsec">その他<BR>日数</TH>
	<TH class="l-cellsec">更新日</TH>
	<TH class="l-cellsec">承認<BR>状況</TH>
	<TH class="l-cellsec">申請日</TH>
	<TH class="l-cellsec">承認日</TH>
	<TH class="l-cellsec">承認者</TH>

<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewKintaiHeader1()
'PROCEDURE機能：勤怠表のヘッダーを表示
'引数：なし
'****************************************************
Sub viewKintaiHeader1()
%>
	<TH class="l-cellsec">社員番号</TH>
	<TH class="l-cellsec" rowspan="2">標準<BR>日数</TH>
	<TH class="l-cellsec" rowspan="2">稼働<BR>日数</TH>
	<TH class="l-cellsec" rowspan="2">標準<BR>時間</TH>
	<TH class="l-cellsec" rowspan="2">稼働<BR>時間</TH>
	<TH class="l-cellsec" rowspan="2"><FONT COLOR="BLUE">普通<BR>残業</TH>
	<TH class="l-cellsec" rowspan="2"><FONT COLOR="BLUE">休日<BR>残業</TH>
	<TH class="l-cellsec" rowspan="2"><FONT COLOR="BLUE">深夜<BR>残業</TH>
	<TH class="l-cellsec" rowspan="2"><FONT COLOR="RED">不足<BR>時間</TH>
<!--
	<TH class="l-cellsec" rowspan="2"><FONT COLOR="RED">遅刻<BR>時間</TH>
-->
	<TH class="l-cellsec" rowspan="2">有休<BR>日数</TH>
	<TH class="l-cellsec" rowspan="2">代休<BR>日数</TH>
	<TH class="l-cellsec" rowspan="2">欠勤<BR>日数</TH>
	<TH class="l-cellsec" rowspan="2">その他<BR>日数</TH>
	<TH class="l-cellsec" rowspan="2">承認<BR>状況</TH>
	<TH class="l-cellsec" rowspan="2">申請日</TH>
	<TH class="l-cellsec" rowspan="2">承認日</TH>
	<TH class="l-cellsec" rowspan="2">承認者</TH>
	</TR>
	<TR>
	<TH class="l-cellsec">氏名</TH>

<%
End Sub
%>


<%
'****************************************************
'PROCEDURE名：  viewKinmuConfig()
'PROCEDURE機能：勤務条件設定を表示する
'****************************************************
Sub viewKinmuConfig()
	'変数定義
	Dim lRs
	Dim txtSTime(13)							'開始時間を格納する配列
	Dim txtETime(13)							'終了時間を格納する配列
	Dim lblTimeNM(13)							'時間名称
	Dim lblKikan(13)							'期間

	'マスタからデータを取得
	Set lRs = session("kintai").Execute(SELWSM0002(0))


	For cnt = 1 To UBOUND(txtSTime)
		txtSTime(cnt)		= lRs("STIME")
		txtETime(cnt)		= lRs("ETIME")
		lblTimeNM(cnt)	= lRs("TIMENM")
		lblKikan(cnt)		= lRs("KIKAN")
		lRs.MoveNext
	Next


%>
<div class="list">
	<table class="l-tbl">
			<TR ALIGN="CENTER">
				<TH class="l-cellsec"><span class="txt-blue">勤務<br>条件</span></TH>
				<TH class="l-cellsec">開始<br>時間</TH>
				<TH class="l-cellsec">終了<br>時間</TH>
				<TH class="l-cellsec">時間<br>(H)</TH>
				<TD></TD>
			</TR>
			<TR>
				<TH class="l-cellsec">標準
				</TH>
				<TD class="l-celleven"><%=trim(txtSTime(1))%></TD>
				<TD class="l-celleven"><%=trim(txtETime(1))%></TD>
				<TD class="l-celleven"><%=toDecimal2(toEmpty(lblKikan(1)))%></TD>
				<TD></TD>
			</TR>
			<TR>
				<TH class="l-cellsec">午前
				</TH>
				<TD class="l-celleven"><%=trim(txtSTime(2))%></TD>
				<TD class="l-celleven"><%=trim(txtETime(2))%></TD>
				<TD class="l-celleven"><%=toDecimal2(toEmpty(lblKikan(2)))%></TD>
				<TD></TD>
			</TR>
			<TR>
				<TH class="l-cellsec">午後
				</TH>
				<TD class="l-celleven"><%=trim(txtSTime(3))%></TD>
				<TD class="l-celleven"><%=trim(txtETime(3))%></TD>
				<TD class="l-celleven"><%=toDecimal2(toEmpty(lblKikan(3)))%></TD>
				<TD></TD>
			</TR>
			<TR>
				<TH class="l-cellsec">深夜
				</TH>
				<TD class="l-celleven"><%=trim(txtSTime(4))%></TD>
				<TD class="l-celleven"><%=trim(txtETime(4))%></TD>
				<TD class="l-celleven"><%=toDecimal2(toEmpty(lblKikan(4)))%></TD>
				<TD></TD>
			</TR>
			<% For cnt = 5 To UBOUND(txtSTime) 
				if lblKikan(cnt) <> empty then %>
			<TR>
				<TH class="l-cellsec">休憩
				</TH>
				<TD class="l-celleven"><%=trim(txtSTime(cnt))%></TD>
				<TD class="l-celleven"><%=trim(txtETime(cnt))%></TD>
				<TD class="l-celleven"><%=toDecimal2(toEmpty(lblKikan(cnt)))%></TD>
				<TD></TD>
			</TR>
			<% end if
			Next %>
		</TABLE>
</div>
<%
End Sub
%>

