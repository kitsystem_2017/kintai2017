<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'GFN（株）承諾なくの改ざんは硬く禁じます。
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'==============================================
'ファイル名称：Main.asp
'ファイル機能：メインフレームを表示する
'==============================================
%>
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/Const.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/Utility.asp"-->
<%
	'DB接続
	Call DBConnect()
%>
<HTML onContextMenu="return false;">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<TITLE>勤怠システム</TITLE>
</HEAD>
<CENTER>
<%
	Dim rs						'レコードセット
	Dim strShainCD				'社員ｺｰﾄﾞ
	Dim strPWD					'パスワード
	Dim errMSG					'エラーメッセージ
	Dim strKengen				'権限
	errMSG = Empty

	'アドレスバーから社員ｺｰﾄﾞ、ﾊﾟｽﾜｰﾄﾞを取得
	strShainCD = request.form("txtShainCD")
	strPWD = request.form("txtPWD")

	If judgePWD() = False Then
		Response.Redirect "Error.html"
		Response.End
	End If
	
	Session("shainCD") = strShainCD
%>
<%
'****************************************************
'FUNCTION名：	judgePWD()
'FUNCTION機能：ﾊﾟｽﾜｰﾄﾞﾁｪｯｸ
'引数：あり pShainCD：社員コード
'****************************************************
Function judgePWD()

	judgePWD = True

	'社員ｺｰﾄﾞ取得できない場合
	If isNull(strShainCD) or strShainCD = Empty Then
		'エラーメッセージ設定
		errMSG = WSE0001
		judgePWD = False
		Exit Function
	End If

	'エラー制御開始
	On Error Resume Next
	'社員情報取得
	Set rs = session("kintai").Execute(SELM0001(strShainCD))

	'SQLエラーの場合
	If Err<>0 Then
		'エラーメッセージ設定
		errMSG = WSE0002 & Err
		judgePWD = False
		Exit Function
	End If

	'レコード取得できない場合
	If rs.RecordCount = 0 Then
		'エラーメッセージ設定
		errMSG = strShainCD & WSE0003
		judgePWD = False
		Exit Function
	Else
		'ﾊﾟｽﾜｰﾄﾞ認証
		If StrComp(strPWD,rs("PWD")) <> 0 Then
			'エラーメッセージ設定
			errMSG = WSE0004
			judgePWD = False
			Exit Function
		End If
		'権限取得
		strKengen = rs("KENGEN")
	End If

	'エラー制御終了
	On Error Goto 0

End Function
%>

</CENTER>

<FRAMESET ROWS="45,*" FRAMEBORDER="0" BORDER="0" FRAMESPACING="0">
<FRAME SCROLLING="" SRC="Menu-demo.asp?shaincd=<%=strShainCD%>&kengen=<%=strKengen%>" NAME="top">
<FRAME SRC="WST0001.asp?shaincd=<%=strShainCD%>" NAME="bottom">
</FRAMESET>
</HTML>


