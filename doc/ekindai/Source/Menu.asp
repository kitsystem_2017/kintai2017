<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
%>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<HTML onContextMenu="return false;">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<TITLE>Menu</TITLE>
<SCRIPT LANGUAGE="JavaScript" src="include/menu.js">
</SCRIPT>
</HEAD>
<%
	Dim strShainCD				'社員ｺｰﾄﾞ
	Dim strKengen					'権限
	Dim strNengapi				'年月日

	'パラメータ取得
	strShainCD = request.querystring("shaincd")
	strKengen = request.querystring("kengen")
	If strShainCD = Empty Then
		strShainCD = request.form("txtShainCD")
		strKengen = request.querystring("txtKengen")
	End If

	'認証
	If strShainCD=empty Then
		response.Write(WSE0001)
		response.End
	End If

	strNengapi = mid(date(),1,4) & "年" & mid(date(),6,2) & "月" & mid(date(),9,2) & "日"

%>

<body id="doc">
<div id="head">
	<div id="title">
		<h1>
		</h1>
<FORM NAME="top" METHOD="POST" TARGET="bottom">
<CENTER>
	<TABLE WIDTH="100%" BORDER=0 CELLSPACING=4 CELLPADDING=0 >
		<TR ALIGN="CENTER" WIDTH="">
			<TD WIDTH="10%" align="left">
				<IMG SRC='img/header_logo.jpg' BORDER='0' height='24' width='85'>
			</TD>
			<TD WIDTH="20%" align="left">
				<span class="txt-blue"><%= strNengapi %></span>
			</TD>
			<TD ALIGN="CENTER" HEIGHT="35" WIDTH="40%">
				<SELECT NAME="cmbMenu" ONCHANGE="go()">
					<OPTION VALUE="0" SELECTED>お知らせ</OPTION>
					<OPTION VALUE="1" SELECTED>勤務表入力</OPTION>
					<OPTION VALUE="2">勤務表一覧</OPTION>
					<OPTION VALUE="3">休暇情報</OPTION>
					<OPTION VALUE="">---------------</OPTION>
					<OPTION VALUE="4">行先＆スケジュール</OPTION>
					<OPTION VALUE="5">社内簡易メール</OPTION>
					<OPTION VALUE="6">行先掲示板</OPTION>
					<OPTION VALUE="7">年間カレンダー</OPTION>
					<OPTION VALUE="8">個人情報設定</OPTION>
					<OPTION VALUE="">---------------</OPTION>
					<OPTION VALUE="9">定期券・交通費の精算申請</OPTION>
					<OPTION VALUE="10">費用精算申請</OPTION>
					<OPTION VALUE="11">仮払い申請</OPTION>
					<OPTION VALUE="12">各種精算申請一覧</OPTION>
					<OPTION VALUE="">---------------</OPTION>
					<OPTION VALUE="13">週間報告</OPTION>
					<OPTION VALUE="15">週間報告一覧</OPTION>
			<% If strKengen = "2" or strKengen = "4" Then %>
					<OPTION VALUE="">---------------</OPTION>
					<OPTION VALUE="20">勤務表承認</OPTION>
					<OPTION VALUE="24">週間報告確認</OPTION>
			<% End If %>
			<% If strKengen = "3" or strKengen = "4" Then %>
					<OPTION VALUE="23">交通費・費用精算・仮払い承認</OPTION>
					<OPTION VALUE="">---------------</OPTION>
					<OPTION VALUE="21">勤怠集計表</OPTION>
					<OPTION VALUE="22">プロジェクト集計表</OPTION>
					<OPTION VALUE="">---------------</OPTION>
					<OPTION VALUE="30">カレンダー設定</OPTION>
					<OPTION VALUE="31">勤務条件設定</OPTION>
					<OPTION VALUE="32">有休管理</OPTION>
					<OPTION VALUE="">---------------</OPTION>
					<OPTION VALUE="40">社員情報設定</OPTION>
					<OPTION VALUE="41">部門情報設定</OPTION>
					<OPTION VALUE="42">プロジェクト情報設定</OPTION>
			<% End If %>
				</SELECT>
			<INPUT TYPE="BUTTON" VALUE="Go" onClick="go()" class="i-btn">
			</TD>
			<TD ALIGN="left" HEIGHT="35" WIDTH="15%">
				<INPUT TYPE="BUTTON" VALUE="Help" onClick="go_help()" class="i-btn">
				<INPUT TYPE="BUTTON" VALUE="Exit" onClick="go_exit()" class="i-btn">
			</TD>
			<TD ALIGN="right" HEIGHT="35" WIDTH="15%">
				<a href="https://tools.lolipop.jp/mail/" target="_blank">Symmetrix WebMail</a>
			</TD>
		</TR>
	</TABLE>
	<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">
	<INPUT TYPE="HIDDEN" NAME="txtKengen" VALUE="<%=strKengen%>">
</CENTER>
</FORM>
	</div>
</div>
</BODY>
