<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：T0001
'機能：勤怠表の一覧
'*********変更履歴*********
'日付			変更者		変更管理番号	理由
'2007-10-11               高　　　　　社員が自分の承認者を変更できないように

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<HEAD>
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(type){
	//クリア
	if(type == 2) {
		document.bottom.txtToShainCD1.value = "";
		document.bottom.txtToShainCD2.value = "";
		return;
	}
	if(!confirm('登録してもよろしいですか？')){
		return;
	}
	//承認者設定
	if(type == 0) {
		if((document.bottom.txtToShainCD1.value == document.bottom.txtToShainCD2.value) && document.bottom.txtToShainCD1.value != ""){
			alert("代理承認者は承認者と別々にしてください。");
			return;
		}
		document.bottom.action="T0001.asp?mode=0";
		document.bottom.submit();
		return;
	}
	//パスワード変更
	if(type == 1) {
		if(document.bottom.txtPWD.value != document.bottom.txtPWD1.value){
			alert("新しいパスワードは確認用パスワードと一致しません。");
			return;
		}
		document.bottom.action="T0001.asp?mode=1";
		document.bottom.submit();
		return;
	}
}

function _open(idx){
	nwin =window.open("T0001SW01.asp?idx="+idx,"T0001SW01","width=600,height=800,menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no");
	nwin.focus();
}

//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs,rs1								'レコードセット
	Dim strShainCD							'社員コードﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門コードﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen							'権限
	Dim strShouninshaCD						'承認者社員コードﾞ
	Dim strShouninshaNM						'承認者社員氏名
	Dim strSNBumonCD						'承認者部門コードﾞ
	Dim strSNBumonNM						'承認者部門名称
	Dim strSNKengen							'代理承認者権限
	Dim strShouninshaCD1					'代理承認者社員コードﾞ
	Dim strShouninshaNM1					'代理承認者社員氏名
	Dim strSNBumonCD1						'代理承認者部門コードﾞ
	Dim strSNBumonNM1						'代理承認者部門名称
	Dim strSNKengen1						'代理承認者権限
	Dim errMSG								'エラーメッセージ
	Dim strMode								'実行モード
	Dim strSQL								'SQL文
	Dim strFormat							'書式
	Dim strColor							'カラー
	Dim strPWD								'パスワード

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = empty Then
		'初期表示を行う
		Call initProc()
	elseIf strMode = "0" Then
		'承認者登録を行う
		Call updateProc(0)
	elseIf strMode = "1" Then
		'パスワード変更を行う
		Call updateProc(1)
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'フォームから社員番号取得
	strShainCD = trim(request.form("txtShainCD"))

	'実行モードを取得
	strMode = request.querystring("mode")

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("個人情報設定",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'社員マスタから承認者情報を取得
	Set rs = session("kintai").Execute(SELM0001_1(strShainCD))

	Do Until rs.EOF
		strShouninshaCD = rs("SHOUNINSHACD")
		strShouninshaCD1 = rs("SHOUNINSHACD1")
		rs.MoveNext
	Loop

	'承認者情報取得
	Call getShainInfo(strShouninshaCD,strShouninshaNM,strSNBumonCD,strSNBumonNM,strSNKengen)

	'代理承認社情報取得
	Call getShainInfo(strShouninshaCD1,strShouninshaNM1,strSNBumonCD1,strSNBumonNM1,strSNKengen1)

	'内容を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc(pType)
'PROCEDURE機能：登録を行う
'引数：なし
'****************************************************
Sub updateProc(pType)

	On Error Resume Next
	
	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("個人情報設定",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'承認者、代理承認者を取得
	strShouninshaCD = trim(request.form("txtToShainCD1"))
	strShouninshaCD1 = trim(request.form("txtToShainCD2"))

	'タイプは0の場合は承認者設定を行う
	If pType = "0" Then

		'承認者情報取得
		Call getShainInfo(strShouninshaCD,strShouninshaNM,strSNBumonCD,strSNBumonNM,strSNKengen)

		If strShouninshaCD <> Empty Then
			'存在チェック
			If strShouninshaNM = Empty Then
				errMSG = "承認者" & WSE0009
				viewDetail()
				Exit Sub
			End If
			'権限チェック
			If isNull(strSNKengen) = True Or strSNKengen = Empty Or strSNKengen = "0" Then
				errMSG = "承認者" & WSE0010
				viewDetail()
				Exit Sub
			End If
		End If

		'代理承認社情報取得
		Call getShainInfo(strShouninshaCD1,strShouninshaNM1,strSNBumonCD1,strSNBumonNM1,strSNKengen1)

		If strShouninshaCD1 <> Empty Then
			Call getShainInfo(strShouninshaCD1,strShouninshaNM1,strSNBumonCD1,strSNBumonNM1,strSNKengen1)
			'存在チェック
			If isNull(strShouninshaNM1) = True Or strShouninshaNM1 = Empty Then
				errMSG = "代理承認者" & WSE0009
				viewDetail()
				Exit Sub
			End If
			'権限チェック
			If isNull(strSNKengen1) = True Or strSNKengen1 = Empty Or strSNKengen1 = "0" Then
				errMSG = "代理承認者" & WSE0010
				viewDetail()
				Exit Sub
			End If
		End If

		'** トランザクション開始
'		session("kintai").RollBackTrans
		session("kintai").BeginTrans

		'社員マスタの承認者、代理承認者を更新
		session("kintai").Execute(UPDM0001(strShainCD,strShouninshaCD,strShouninshaCD1,strShouninshaNM,strShouninshaNM1))
		If Err <> 0 Then
			session("kintai").RollBackTrans
			errMSG = "承認者" & WSE0000
			'エラーメッセージ部分を表示する
			Call viewHeader()
			'処理を終了する
			response.End
		End If

		'** トランザクション終了
		session("kintai").CommitTrans


	'タイプは1の場合はパスワード変更を行う
	ElseIf pType = "1" Then
		'パスワードを取得
		strPWD = trim(request.form("txtPWD"))

		'** トランザクション開始
'		session("kintai").RollBackTrans
		session("kintai").BeginTrans

		'社員マスタのパスワードを更新
		session("kintai").Execute(UPDM0001_1(strShainCD,strPWD))
		If Err <> 0 Then
			session("kintai").RollBackTrans
			errMSG = "パスワード" & WSE0000
			'エラーメッセージ部分を表示する
			Call viewHeader()
			'処理を終了する
			response.End
		End If

		'** トランザクション終了
		session("kintai").CommitTrans

		'承認者情報取得
		Call getShainInfo(strShouninshaCD,strShouninshaNM,strSNBumonCD,strSNBumonNM,strSNKengen)
		'代理承認社情報取得
		Call getShainInfo(strShouninshaCD1,strShouninshaNM1,strSNBumonCD1,strSNBumonNM1,strSNKengen1)

	End If

	'初期表示する
	Call viewDetail()
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：勤怠一覧表示
'引数：あり
'			pRs	:承認者情報
'****************************************************
Sub viewDetail()
%>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD ALIGN="LEFT">
				<FONT COLOR="RED">　<%=errMSG%>
			</TD>
		</TR>
	</table>
	<table class="l-tbl">
		<col width="15%">
		<col width="10%">
		<col width="25%">
		<col width="15%">
		<col width="20%">
		<col width="15%">
		<TR>
			<TD></TD>
			<TD COLSPAN="4"><FONT SIZE="" COLOR="BLUE">承認者設定</TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec">社員コード</TD>
			<TH class="l-cellsec">氏名</TD>
			<TH class="l-cellsec">部門</TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TH class="l-cellsec">承認者</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" SIZE="30" MAXLENGTH="40" NAME="txtToShainCD1" VALUE="<%=strShouninshaCD%>" class="txt_imeoff" readonly="true">
				<!-- 修正ところ　コメントされた　　高　2007-10-11  -->
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtToShainCD1')" class="i-btn">
			</TD>
			<TD class="l-cellodd"><%=strShouninshaNM%></TD>
			<TD class="l-cellodd"><%=strSNBumonCD & "　" & strSNBumonNM%></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TH class="l-cellsec">代理承認者</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" SIZE="30" MAXLENGTH="40" NAME="txtToShainCD2" VALUE="<%=strShouninshaCD1%>" class="txt_imeoff" readonly="true">
				<!-- 修正ところ　コメントされた　　高　2007-10-11  -->
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtToShainCD2')" class="i-btn">
			</TD>
			<TD class="l-cellodd"><%=strShouninshaNM1%></TD>
			<TD class="l-cellodd"><%=strSNBumonCD1 & "　" & strSNBumonNM1%></TD>
			<TD></TD>
		</TR>
	</TABLE>
	<BR>
	<!-- 修正ところ　コメントされた　　高　2007-10-11  -->
	<INPUT TYPE="BUTTON" VALUE="登録" onClick="_submit(0)" class="s-btn">
	<INPUT TYPE="BUTTON" VALUE="クリア" onClick="_submit(2)" class="s-btn">
<BR>
<BR>
<BR>
<BR>
<HR>
<BR>
<BR>
	<table class="l-tbl">
		<col width="35%">
		<col width="15%">
		<col width="15%">
		<col width="35%">
		<TR>
			<TD></TD>
			<TD COLSPAN="2"><FONT SIZE="" COLOR="BLUE">パスワード変更</TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TH class="l-cellsec">新しいパスワード</TD>
			<TD class="l-cellodd"><INPUT TYPE="PASSWORD" SIZE="20" MAXLENGTH="10" NAME="txtPWD" class="txt_imeoff"></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TH class="l-cellsec">パスワード確認</TD>
			<TD class="l-cellodd"><INPUT TYPE="PASSWORD" SIZE="20" MAXLENGTH="10" NAME="txtPWD1" class="txt_imeoff"></TD>
			<TD></TD>
		</TR>
	</TABLE>
	<BR>
	<INPUT TYPE="BUTTON" VALUE="登録" onClick="_submit(1)" class="s-btn">
</div>
</CENTER>
<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" 	VALUE="<%=strShainCD%>">
</FORM>
</BODY>
</HTML>

<%
End Sub
%>


