<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WSM0004
'機能：有休付与、確認
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim strMode									'実行モード
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim strNendo								'年度
	Dim strSysDate							'システム年月日
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim rowCnt									'データカウント
	Dim strFoamat								'書式
	Dim errMSG									'エラーメッセージ

	'画面項目を格納する変数
	Dim cmbBumonCD							'部門コード
	Dim txtShainCD							'社員ｺｰﾄﾞ
	Dim txtBumonCD							'部門コード
	Dim txtYuukyuu							'有休
	Dim txtYuukyuuZan						'有休残
	Dim txtShainCD1							'社員ｺｰﾄﾞ
	Dim txtYuukyuu1							'有休
	Dim txtYuukyuuZan1					'有休残


	'パラメーター取得
	Call getParameter()

	If strMode = Empty Then
		'初期表示
		Call initProc()
	ElseIf strMode ="0" Then
		'サーチ処理
		Call searchProc()
	ElseIf strMode ="1" Then
		'登録処理
		Call insertProc()
	ElseIf strMode ="2" Then
		'更新処理
		Call updateProc()
	ElseIf strMode ="3" Then
		'削除処理
		Call deleteProc()
	ElseIf strMode ="4" Then
		'前年度データを取込処理
		Call importProc()
	End If
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'実行モードを取得
	strMode = request.querystring("mode")

	'フォームから取得から年度を取得
	strNendo = trim(request.form("cmbNendo"))

	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = GetCurrenNendo()
	end if

	'フォームから取得から部門を取得
	cmbBumonCD = trim(request.form("cmbBumonCD"))

	'フォームからデータカウントを取得
	rowCnt = trim(request.form("hdnRowCnt"))

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'ヘッダーを表示
	Call viewHeader()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	searchProc()
'PROCEDURE機能：DBからデータを抽出し表示する
'引数：なし
'****************************************************
Sub searchProc()

	'有休マスタから情報を取得
	Set rs = session("kintai").Execute(SELWSM0004(strNendo,cmbBumonCD))

	If rs.RecordCount = 0 Then
'		errMSG = CONST_FONT_RED & WSE0012
		'ヘッダーを表示
		Call viewHeader()
		Exit Sub
	End If

	'ヘッダーを表示
	Call viewHeader()

	'詳細を表示
	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：有休を付与する
'引数：なし
'****************************************************
Sub insertProc()
	On Error Resume Next
	
	'画面の項目を取得する
	Call getScreenValue()

	'社員情報取得
	Call getShainInfo(txtShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)
	'取得できない場合はエラー
	If strShainNM = Empty Then
		errMSG = CONST_FONT_RED & WSE0003
		'データを抽出し、再表示する
		Call searchProc()
		Exit Sub
	End If

	'有休マスタからデータ取得
	Set rs = session("kintai").Execute(SELWSM0004_1(txtShainCD,strNendo))
	'すでに存在している場合はエラー
	If rs.RecordCount > 0 Then
		errMSG = CONST_FONT_RED & WSE0003_1
		'データを抽出し、再表示する
		Call searchProc()
		Exit Sub
	End If

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans
	'Insert処理

	session("kintai").Execute(INSWSM0004(strNendo,txtShainCD,strBumonCD,toZero(txtYuukyuu),toZero(txtYuukyuuZan),0,0))
	'エラーの場合
	If Err <> 0 Then
		session("kintai").RollBackTrans
		errMSG = CONST_FONT_RED & "有休" & WSE0000
		'データを抽出し、再表示する
		Call searchProc()
		Exit Sub
	End If

	'** トランザクション終了
	session("kintai").CommitTrans

	'データを抽出し、再表示する
	Call searchProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：有休を更新する
'引数：なし
'****************************************************
Sub updateProc()
	On Error Resume Next
	'画面の項目を取得する
	Call getScreenValue()

	For cnt = 1 To rowCnt
		'チェックされているデータのみ処理
		If trim(request.form("chk" & cnt)) = "on" Then
			'Update処理
			txtShainCD1		 = trim(request.form("hdnShainCD" & cnt))						'社員ｺｰﾄﾞ
			txtYuukyuu1		 = trim(request.form("txtYuuKyuu" & cnt))						'有休
			txtYuukyuuZan1	 = trim(request.form("txtYuuKyuuZan" & cnt))					'有休残

			'** トランザクション開始
'			session("kintai").RollBackTrans
			session("kintai").BeginTrans
			session("kintai").Execute(UPDWSM0004(txtShainCD1,strNendo,toZero(txtYuukyuu1),toZero(txtYuukyuuZan1)))

			'エラーの場合
			If Err <> 0 Then
				session("kintai").RollBackTrans
				errMSG = CONST_FONT_RED & "有休" & WSE0000
				'データを抽出し、再表示する
				Call searchProc()
				Exit Sub
			End If
			'** トランザクション終了
			session("kintai").CommitTrans
		End If
	Next

	'データを抽出し、再表示する
	Call searchProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	deleteProc()
'PROCEDURE機能：有休を削除する
'引数：なし
'****************************************************
Sub deleteProc()

	For cnt = 1 To rowCnt
		'チェックされているデータのみ処理
		If trim(request.form("chk" & cnt)) = "on" Then
			txtShainCD1		 = trim(request.form("hdnShainCD" & cnt))						'社員ｺｰﾄﾞ
			'** トランザクション開始
'			session("kintai").RollBackTrans
			session("kintai").BeginTrans

			'削除処理
			session("kintai").Execute(DELWSM0004(strNendo,txtShainCD1))

			'エラーの場合
			If Err <> 0 Then
				session("kintai").RollBackTrans
				errMSG = CONST_FONT_RED & "有休" & WSE0000
				'データを抽出し、再表示する
				Call searchProc()
				Exit Sub
			End If
			'** トランザクション終了
			session("kintai").CommitTrans
		End If
	Next

	'データを抽出し、再表示する
	Call searchProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	importProc()
'PROCEDURE機能：前年度有休を取込する
'引数：なし
'****************************************************
Sub importProc()

	'前年度有休データを取得
	Set rs = session("kintai").Execute(SELWSM0004(strNendo - 1,cmbBumonCD))

	'前年度の有休付与、有休残を今年度の前年度有休付与、前年度有休残として追加
	Do Until rs.EOF
		'** トランザクション開始
'		session("kintai").RollBackTrans
		session("kintai").BeginTrans
		'削除処理
		session("kintai").Execute(DELWSM0004(strNendo,rs("SHAINCD")))

		'Insert処理
		session("kintai").Execute(INSWSM0004(strNendo,rs("SHAINCD"),rs("BUMONCD"),0,0,rs("YUUKYUU"),rs("YUUKYUUZAN")))

		'エラーの場合
		If Err <> 0 Then
			session("kintai").RollBackTrans
			errMSG = CONST_FONT_RED & "有休" & WSE0000
			'データを抽出し、再表示する
			Call searchProc()
			Exit Sub
		End If
		'** トランザクション終了
		session("kintai").CommitTrans
		rs.MoveNext
	Loop

	'データを抽出し、再表示する
	Call searchProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面情報を取得する
'引数：なし
'****************************************************
Sub getScreenValue()

	txtShainCD		 = trim(request.form("txtToShainCD1"))					'社員ｺｰﾄﾞ
	txtYuukyuu		 = trim(request.form("txtYuuKyuu"))							'有休
	txtYuukyuuZan	 = trim(request.form("txtYuuKyuuZan"))					'有休残

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：有休ヘッダー表示
'引数：なし
'****************************************************
Sub viewHeader()
	strFoamat = Empty
	'前年度有休データを取得
	Set rs1 = session("kintai").Execute(SELWSM0004(strNendo - 1,"All"))
	If rs1.RecordCount = 0 Then
		strFoamat = "DISABLED"
	End If
%>
	<CENTER><U><FONT COLOR="BLUE">有休管理</FONT></U>
	<TABLE WIDTH="700" BORDER="0" BGCOLOR="">
		<TR ALIGN="LEFT">
			<TD WIDTH="" HEIGHT="23"><FONT SIZE="3">　<%=errMSG%>
			</TD>
		</TR>
	</TABLE>

<div class="list">
	<table class="l-tbl">
		<col width="8%">
		<col width="20%">
		<col width="8%">
		<col width="5%">
		<col width="8%">
		<col width="5%">
		<col width="36%">
		<TR>
			<TH class="l-cellsec">年度
			</TH>
			<TD class="l-cellodd">
<%Call viewNendoCMB(strNendo)%>
			</TD>
			<TH class="l-cellsec">部門
			</TH>
			<TD class="l-cellodd" colspan="3" width="18%">
				<% Call viewBumonCMB(cmbBumonCD) %>
			</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="SubmitWSM0004(0)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="前年度データ取込" onClick="SubmitWSM0004(4)" <%=strFoamat%> class="s-btn1">
			</TD>
		</TR>
		<TR>
			<TD COLSAPN="5"><FONT SIZE="3" COLOR="BLUE">有休付与：
		</TR>
		<TR ALIGN="LEFT">
			<TH class="l-cellsec">社員番号<font color="ff0000">(*)</font>
			</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" NAME="txtToShainCD1" VALUE="<%=txtShainCD%>" SIZE="30" MAXLENGTH="40" class="txt_imeoff">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtToShainCD1')" class="i-btn">
			</TD>
			<TH class="l-cellsec">有休付与数</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" class="txt_imenum" NAME="txtYuukyuu" VALUE="<%=txtYuukyuu%>" SIZE="5" MAXLENGTH="5" onBlur="judgeNumber1(this)">
			</TD>
			<TH class="l-cellsec">
				有休残数
			</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" class="txt_imenum" NAME="txtYuukyuuZan" VALUE="<%=txtYuukyuuZan%>" SIZE="5" MAXLENGTH="5" onBlur="judgeNumber1(this)">
			</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="BUTTON" VALUE="登録" onClick="SubmitWSM0004(1)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="更新" onClick="SubmitWSM0004(2)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="削除" onClick="SubmitWSM0004(3)" class="s-btn">
			</TD>
		</TR>
	</TABLE>
	<BR>
</div>
<% End Sub %>
<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：有休一覧表示
'引数：あり
'			pRs	:有休データ
'****************************************************
Sub viewDetail(pRs)
%>
<div class="list">
	<table class="l-tbl">
		<col width="3%">
		<col width="20%">
		<col width="18%">
		<col width="18%">
		<col width="10%">
		<col width="10%">
		<col width="10%">
		<col width="10%">
		<TR>
			<TH class="l-cellsec"><INPUT TYPE="CHECKBOX" NAME="chkall" onClick="checkAll(<%=pRs.RecordCount%>)"></TH>
			<TH class="l-cellsec">社員番号</TH>
			<TH class="l-cellsec">氏名</TH>
			<TH class="l-cellsec">部門</TH>
			<TH class="l-cellsec">前年度有休付与</TH>
			<TH class="l-cellsec">前年度有休残数</TH>
			<TH class="l-cellsec">今年度有休付与</TH>
			<TH class="l-cellsec">今年度有休残数</TH>
		</TR>
	</table>
  <div class="l-tbl-auto" style="height:300px">
	<table class="l-tbl">
		<col width="3%">
		<col width="20%">
		<col width="18%">
		<col width="18%">
		<col width="10%">
		<col width="10%">
		<col width="10%">
		<col width="10%">
<%cnt = 1
	dim i
	dim cellClass,cellClassr
	i = 0
	Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
		'社員情報取得
		Call getShainInfo(pRs("SHAINCD"),strShainNM,strBumonCD,strBumonNM,strKengen)%>
		<TR>
			<TD class=<%=cellClass%>><INPUT TYPE="CHECKBOX" NAME="chk<%=cnt%>"></TD>
			<TD class=<%=cellClass%>><%=pRs("SHAINCD")%></TD>
			<TD class=<%=cellClass%>><%=strShainNM%></TD>
			<TD class=<%=cellClass%>><%=strBumonNM%></TD>
			<TD class=<%=cellClassr%>><%=pRs("ZYUUKYUU")%></TD>
			<TD class=<%=cellClassr%>><%=pRs("ZYUUKYUUZAN")%></TD>
			<TD class=<%=cellClass%>>
				<INPUT TYPE="TEXT" class="txt_imenum" NAME="txtYuukyuu<%=cnt%>" VALUE="<%=pRs("YUUKYUU")%>" SIZE="5" MAXLENGTH="5" onBlur="judgeNumber1(this)">
			</TD>
			<TD class=<%=cellClass%>>
				<INPUT TYPE="TEXT" class="txt_imenum" NAME="txtYuukyuuZan<%=cnt%>" VALUE="<%=pRs("YUUKYUUZAN")%>" SIZE="5" MAXLENGTH="5" onBlur="judgeNumber1(this)">
				<!-----------------------------HIDDEN項目------------------------------>
				<INPUT TYPE="HIDDEN" NAME="hdnShainCD<%=cnt%>" VALUE="<%=pRs("SHAINCD")%>">
			</TD>
		</TR>
<%cnt = cnt + 1
	pRs.MoveNext
	Loop%>
<!-----------------------------HIDDEN項目------------------------------>
<div style="display:none">
<INPUT TYPE="HIDDEN" NAME="hdnRowCnt" VALUE="<%=pRs.RecordCount%>">
</div>
	</table>
	</div>
</div>
</CENTER>
</FORM>
</BODY>
</HTML>

<% End Sub %>


