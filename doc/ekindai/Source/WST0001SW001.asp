<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0001SW001
'機能：プロジェクト情報検索
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>プロジェクト情報検索</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.WST0001SW001.action="WST0001SW001.asp?mode=search";
	document.WST0001SW001.submit();
	return;
}
function _close(){
	self.window.close();
}

function _set(){
	//レコードカウント取得
	cnt = document.WST0001SW001.hidCnt.value;
	idx = document.WST0001SW001.hdnIdx.value;
	var value;

	for(i = 0;i < cnt; i++){
		if (cnt == 1) {
			value = document.WST0001SW001.hidShinCD.value;
		} else {
			if(document.WST0001SW001.rdo[i].checked == true){
				value = document.WST0001SW001.hidShinCD[i].value;
			}
		}
	}
	
	if (value == null){
		alert("選択してください");
		return;
	}
	
	opener.bottom.elements[idx].value=value;
	
	self.window.close();
	
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="WST0001SW001" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim txtPJNO							'プロジェクトコードﾞ
	Dim txtPJNM							'プロジェクト名称
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim intIdx									'Indix

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = empty Then
		'初期表示を行う
		Call searchProc()
	elseIf strMode = "search" Then
		'検索処理を行う
		Call searchProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーからプロジェクト番号取得
	intIdx = trim(request.querystring("idx"))
	If intIdx = Empty Then
		'フォームからプロジェクト番号取得
		intIdx = trim(request.form("hdnIdx"))
	End If

	'実行モードを取得
	strMode = request.querystring("mode")

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'ヘッダーを表示
	Call viewHeader()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	searchProc()
'PROCEDURE機能：検索を行う
'引数：なし
'****************************************************
Sub searchProc()

	'画面の項目を取得
	Call getScreenValue()

	'検索処理を行う
	Set rs = session("kintai").Execute(SELM0003_1())

	'チェック
	If rs.RecordCount = 0 Then
		errMSG = WSE0009
		'ヘッダー表示
		viewHeader()
		Exit Sub
	End If

	'ヘッダー表示
	viewHeader()

	'検索結果を表示
	Call ViewDetail(rs)

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目を取得
'引数：なし
'****************************************************
Sub getScreenValue()

	txtPJNO = trim(request.form("txtPJNO"))
	txtPJNM = trim(request.form("txtPJNM"))

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：プロジェクト検索
'引数：あり
'			pRs	:プロジェクト情報
'****************************************************
Sub viewHeader()
%>
	<BR>
	<FONT SIZE="" COLOR="BLUE">プロジェクト設定</FONT><BR>
<div class="list">

	<table class="l-tbl">
		<TR ALIGN="LEFT">
			<TD WIDTH="200">
				<FONT COLOR="RED" SIZE="2"><%=errMSG%>
			</TD>
			<TD WIDTH="*" ALIGN="RIGHT">
<!--
				<INPUT TYPE="BUTTON" VALUE="検索" onClick="_submit()" class="i-btn">
-->
				<INPUT TYPE="BUTTON" VALUE="確定" onClick="_set()" class="i-btn">
				<INPUT TYPE="BUTTON" VALUE="閉じる" onClick="_close()" class="i-btn">
			</TD>
		</TR>
	</TABLE>
<!--
	<BR>
	<table class="l-tbl">
		<TR>
			<TH class="l-cellsec">プロジェクト番号</TH>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" SIZE="20" NAME="txtPJNO" VALUE="<%=txtPJNO%>" class="txt_imeoff">
			</TD>
			<TD class="l-cellodd">(先頭一致検索)</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">プロジェクト名称</TH>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" SIZE="20" NAME="txtPJNM" VALUE="<%=txtPJNM%>" class="txt_imeon">
			</TD>
			<TD class="l-cellodd">(部分一致検索)</TD>
		</TR>
	</TABLE>
-->
	<!-----------------------HIDDEN項目：--------------------->
	<INPUT TYPE="HIDDEN" NAME="hdnIdx" VALUE="<%=intIdx%>">
	<BR>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：プロジェクト検索
'引数：あり
'			pRs	:プロジェクト情報
'****************************************************
Sub viewDetail(pRs)
%>
<div class="list">

	<table class="l-tbl">
		<col width="5%">
		<col width="20%">
		<col width="75%">
		<TR>
			<TH class="l-cellsec"></TD>
			<TH class="l-cellsec">プロジェクト番号</TD>
			<TH class="l-cellsec">プロジェクト名称</TD>
		</TR>
	</table>
	<table class="l-tbl">
		<col width="5%">
		<col width="20%">
		<col width="75%">
<% Do Until pRs.EOF %>
		<TR>
			<TD class="l-cellodd"><INPUT TYPE="RADIO" NAME="rdo"></TD>
			<TD class="l-cellodd"><%=pRs("PJNO")%></TD>
			<TD class="l-cellodd"><%=pRs("PJNM")%>
			<!-----------------------HIDDEN項目：--------------------->
			<INPUT TYPE="HIDDEN" NAME="hidShinCD" VALUE="<%=pRs("PJNO")%>">
			</TD>
		</TR>
<% pRs.MoveNext
	 Loop %>

	</TABLE>
	<!-----------------------HIDDEN項目：--------------------->
	<INPUT TYPE="HIDDEN" NAME="hidCnt" VALUE="<%=pRs.RecordCount%>">
</div>
</CENTER>
</FORM>
</BODY>
</HTML>

<%
End Sub
%>



