<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WS0002
'機能：社内メール
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<%
	'変数定義
	Dim rs,rs1,rs2								'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim strFormat1							'書式
	Dim strColor								'カラー
	Dim strToShainCD						'宛先社員コード
	Dim strToShainNM						'宛先社員氏名
	Dim strToBumonCD						'宛先部門ｺｰﾄﾞ
	Dim strToBumonNM						'宛先部門名称
	Dim strToKengen							'宛先権限
	Dim intWarningFLG						'注目フラグ
	Dim strSRC									'イメージのSRC
	Dim strSRC1									'イメージのSRC
	Dim fltSEQ									'SEQ
	Dim strDateTime							'時刻
	Dim rowCnt									'データカウント

	'画面項目の格納変数定義
	Dim rdoTo										'送信先区分
	Dim chkWarningFLG						'注目フラグ
	Dim txtTitle								'タイトル
	Dim txtMail									'メール内容

	'パラメーター取得
	Call getParameter()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = Empty Or strMode = "0" Then
		'画面の項目に値をセット
		Call setScreenValue()
		'初期表示を行う
		Call initProc(0)
	'実行モードが受信表示の場合
	elseIf strMode = "1" Then
		'受信表示処理を行う
		Call initProc(1)
	'実行モードが送信表示の場合
	elseIf strMode = "2" Then
		'送信表示処理を行う
		Call initProc(2)
	'実行モードが送信の場合
	elseIf strMode = "3" Then
		'送信処理を行う
		Call sendProc()
	elseIf strMode = "4" Then
		'返信表示を行う
		Call initProc(4)
	elseIf strMode = "5" Then
		'受信一覧削除を行う
		Call deleteProc(5)
	elseIf strMode = "6" Then
		'送信一覧削除を行う
		Call deleteProc(6)
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'****パラメータ取得******
	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = Empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if

	'実行モードを取得
	strMode = request.querystring("mode")

	'アドレスバーからSEQを取得
	fltSEQ = trim(request.querystring("seq"))

	'宛先、タイトル取得
	If strMode = "4" Then
		strToShainCD = trim(request.form("hdnToShainCD"))
		txtTitle = trim(request.form("hdnTitle"))
	End If

	strDateTime = Date & " " & Time

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc(pType)
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc(pType)

	'画面項目をクリア

	If pType = "1" Then
		'未読フラグを更新
		'エラー制御開始
'		On Error Resume Next

		'** トランザクション開始
'		session("kintai").RollbackTrans
		session("kintai").BeginTrans
		
		session("kintai").Execute(UPDWST0005(strShainCD,fltSEQ,strDateTime))
		If Err <> 0 Then
			'ロールバック
			session("kintai").RollbackTrans
			'エラー処理
			errMSG = "受信" & WSE0009
			'終了処理する
			Call finalProc()
		End If
		'** トランザクション終了
		session("kintai").CommitTrans

	End If

	'タイトルを表示する
	Call viewTitle("社内簡易メール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'受信メール一覧取得
	Set rs = session("kintai").Execute(SELWST0005(strShainCD))

	'送信メール一覧取得
	Set rs1 = session("kintai").Execute(SELWST0005_1(strShainCD))

	'メールを表示
	Call viewDetail(pType,rs,rs1)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	sendProc()
'PROCEDURE機能：メール送信を行う
'引数：なし
'****************************************************
Sub sendProc()

	'画面から項目を取得
	Call getScreenValue()
	
	if txtTitle = Empty then
		errMSG = "タイトルを入力してください。"
		'終了処理する
		Call finalProc()
		Exit sub
	end if
	
	'注目フラグ
	intWarningFLG = 0
	If chkWarningFLG = "on" Then
		intWarningFLG = 1
	End If

	'エラー制御開始
	On Error Resume Next

	'最大SEQ取得
	Set rs = session("kintai").Execute(SELWST0005_2(strShainCD))
	fltSEQ = rs("SEQ")

	'** トランザクション開始
'	session("kintai").RollbackTrans
	session("kintai").BeginTrans

	'個人送信の場合
	If rdoTo = "0" Then
		'社員情報取得
		Call getShainInfo(strToShainCD,strToShainNM,strToBumonCD,strToBumonNM,strToKengen)
		If strToShainNM = Empty Then
			'ロールバック
			session("kintai").RollbackTrans
			'エラー処理
			errMSG = "該当社員" & WSE0009
			'終了処理する
			Call finalProc()
			Exit sub
		End If

		fltSEQ = Replace(fltSEQ,"""",Empty) + 1
'response.write(INSWST0005(strShainCD,fltSEQ,strShainNM,strBumonCD,strDateTime,strToShainNM,strToShainCD,strToBumonCD,txtTitle,Left(txtMail,300),intWarningFLG,0,0,0))
'response.end 
		'SQL実行
		session("kintai").Execute (INSWST0005(strShainCD,fltSEQ,strShainNM,strBumonCD,strDateTime,strToShainNM,strToShainCD,strToBumonCD,txtTitle,Left(txtMail,300),intWarningFLG,0,0,0))

		If Err <> 0 Then
			'ロールバック
			session("kintai").RollbackTrans
			'エラー処理
			errMSG = "個人" & WSE0011
			'終了処理する
			Call finalProc()
			Exit sub
		End If

	'部門送信の場合
	ElseIf rdoTo = "1" Then

		'部門の所属社員を取得
		Set rs = session("kintai").Execute(SELM0002_1(strToBumonCD))
		If rs.RecordCount = 0 Then
			'ロールバック
			session("kintai").RollbackTrans
			'エラー処理
			errMSG = "該当部門の社員" & WSE0009
			'終了処理する
			Call finalProc()
			Exit sub
		End If

		'全員送信処理
		Do Until rs.EOF
			fltSEQ = Replace(fltSEQ,"""",Empty) + 1

			'SQL実行
			session("kintai").Execute (INSWST0005(strShainCD,fltSEQ,strShainNM,strBumonCD,strDateTime,rs("SHAINNM"),rs("SHAINCD"),rs("BUMONCD"),txtTitle,Left(txtMail,300),intWarningFLG,0,0,0))

			rs.MoveNext
		Loop

		If Err <> 0 Then
			'ロールバック
			session("kintai").RollbackTrans
			'エラー処理
			errMSG = "部門" & WSE0011
			'終了処理する
			Call finalProc()
			Exit sub
		End If

		'レコードセットを開放
		Set rs = Nothing
	End If

	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

	Call finalProc()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	finalProc()
'PROCEDURE機能：終了処理を行う
'引数：なし
'****************************************************
Sub finalProc()

	'画面から項目を取得
	Call getScreenValue()

	'タイトルを表示する
	Call viewTitle("社内簡易メール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'受信メール取得
	Set rs = session("kintai").Execute(SELWST0005(strShainCD))

	'送信信メール取得
	Set rs1 = session("kintai").Execute(SELWST0005_1(strShainCD))

	'メールを表示
	Call viewDetail("0",rs,rs1)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	deleteProc(pType)
'PROCEDURE機能：メール削除を行う
'引数：なし
'****************************************************
Sub deleteProc(pType)

	'画面から項目を取得
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollbackTrans
	session("kintai").BeginTrans

	'受信メール削除
	If pType = "5" Then
		'データカウントを取得
		rowCnt = trim(request.form("hdnRowCnt"))

		For cnt = 1 To rowCnt
			If trim(request.form("chk" & cnt)) = "on" Then

				fltSEQ = trim(request.form("hdnSEQ" & cnt))						'SEQ

				'受信削除フラグを更新
				session("kintai").Execute (UPDWST0005_1(strShainCD,fltSEQ))

				'削除実行
				session("kintai").Execute (DELWST0005(strShainCD,fltSEQ))

				If Err <> 0 Then
					'ロールバック
					session("kintai").RollbackTrans
					'エラー処理
					errMSG = "個人" & WSE0011
					'終了処理する
					Call finalProc()
				End If

			End If
		Next

	'送信メール削除
	ElseIf pType = "6" Then
		'データカウントを取得
		rowCnt = trim(request.form("hdnRowCnt1"))

		For cnt = 1 To rowCnt
			If trim(request.form("chk_1" & cnt)) = "on" Then

				fltSEQ = trim(request.form("hdnSEQ_1" & cnt))						'SEQ
				'送信削除フラグを更新
				session("kintai").Execute (UPDWST0005_2(strShainCD,fltSEQ))

				'削除実行
				session("kintai").Execute (DELWST0005(strShainCD,fltSEQ))

				If Err <> 0 Then
					'ロールバック
					session("kintai").RollbackTrans
					'エラー処理
					errMSG = "個人" & WSE0011
					'終了処理する
					Call finalProc()
				End If

			End If
		Next

	End If

	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

	'タイトルを表示する
	Call viewTitle("社内簡易メール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'受信メール取得
	Set rs = session("kintai").Execute(SELWST0005(strShainCD))

	'送信信メール取得
	Set rs1 = session("kintai").Execute(SELWST0005_1(strShainCD))

	'メールを表示
	Call viewDetail("0",rs,rs1)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue()
'PROCEDURE機能：画面の項目に値をセット
'引数：なし
'****************************************************
Sub setScreenValue()
	rdoTo					= Empty					'送信先区分
	strToShainCD	= Empty							'宛先社員コード
	strToBumonCD	= Empty							'宛先部門コード
	chkWarningFLG	= Empty							'注目フラグ
	txtTitle			= Empty						'タイトル
	txtMail				= Empty						'メール内容
	'アドレスバーから宛先を取得
	strToShainCD = trim(request.querystring("toshaincd"))

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()
	rdoTo			= trim(request.form("rdoTo"))					'送信先区分
	strToShainCD	= trim(request.form("txtToShainCD1"))			'宛先社員コード
	strToBumonCD	= trim(request.form("cmbBumonCD"))				'宛先部門コード
	chkWarningFLG	= trim(request.form("chkWarningFLG"))			'注目フラグ
	txtTitle		= trim(request.form("txtTitle"))				'タイトル
	txtMail			= trim(request.form("txtMail"))					'メール内容

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データの追加を行う
'引数：なし
'****************************************************
Sub insertProc()


End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="*"><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
			</TD>
		</TR>
	</TABLE>
</div>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail(pType,pRs,pRs1)
'PROCEDURE機能：受信送信一覧表示
'引数：あり
'			rs	:	受信データ
'			rs1	:	送信データ
'****************************************************
Sub viewDetail(pType,pRs,pRs1)
%>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="53%" VALIGN="TOP">
				<table class="l-tbl">
					<col width="5%">
					<col width="10%">
					<col width="25%">
					<col width="45%">
					<col width="15%">
					<TR ALIGN="LEFT" BGCOLOR="">
						<FONT SIZE="" COLOR="BLUE">受信一覧：</FONT>
					</TR>
					<TR>
						<TH class="l-cellsec">
							<INPUT TYPE="CHECKBOX" NAME="chkall" onClick="checkAll(<%=pRs.RecordCount%>)">
						</TH>
						<TH class="l-cellsec"><INPUT TYPE="BUTTON" VALUE="削除" onClick="SubmitWST0005(5)" class="i-btn"></TH>
						<TH class="l-cellsec"><FONT SIZE="2">差出人</TH>
						<TH class="l-cellsec"><FONT SIZE="2">件名</TH>
						<TH class="l-cellsec">受信日付
						<!-------------------------HIDDEN項目------------------------------>
						<INPUT TYPE="HIDDEN" NAME="hdnRowCnt" VALUE="<%=pRs.RecordCount%>">
						</TH>
					</TR>
				</TABLE>
				<div class="l-tbl-auto" style="height:250">
					<table class="l-tbl">
						<col width="5%">
						<col width="10%">
						<col width="25%">
						<col width="45%">
						<col width="15%">
<%
	cnt = 1
	Do Until pRs.EOF
		'既読、未読のイメージを決定
		If pRs("READFLG") = 0 Then
			strSRC = "<IMG SRC='img/mail.gif' BORDER='0'>"
		Else
			strSRC = "<IMG SRC='img/read.gif' BORDER='0'>"
		End If
		'重要のイメージを決定
		strSRC1 = Empty
		If pRs("WARNINGFLG") = 1 Then
			strSRC1 = "<IMG SRC='img/warning.gif'>"
		End If
%>
						<TR>
							<TD class="l-cellodd">
								<INPUT TYPE="CHECKBOX" NAME="chk<%=cnt%>">
							</TD>
							<TD class="l-cellodd">
								<A HREF="WST0005.asp?mode=1&shaincd=<%=strShainCD%>&seq=<%=(pRs("SEQ"))%>">
								<%=strSRC%></A>
								<%=strSRC1%>
							</TD>
							<TD class="l-cellodd">
								<%=LEFT(pRs("SHAINNM"),10)%>
							</TD>
							<TD class="l-cellodd">
								<A HREF="WST0005.asp?mode=1&shaincd=<%=strShainCD%>&seq=<%=(pRs("SEQ"))%>">
								<%=LEFT(pRs("TITLE"),30)%></A>
							</TD>
							<TD class="l-cellodd"><%=LEFT(pRs("SENDTIME"),10)%>
							<!-------------------------HIDDEN項目------------------------------>
							<INPUT TYPE="HIDDEN" NAME="hdnSEQ<%=cnt%>" VALUE="<%=pRs("SEQ")%>">
							</TD>
						</TR>
<%
	cnt = cnt + 1
	pRs.MoveNext
	Loop
%>
					</TABLE>
				</DIV>
				<table class="l-tbl">
					<col width="5%">
					<col width="10%">
					<col width="25%">
					<col width="45%">
					<col width="15%">
					<TR ALIGN="LEFT" BGCOLOR="">
						<FONT SIZE="" COLOR="BLUE">送信一覧：</FONT>
					</TR>
					<TR>
						<TH class="l-cellsec">
							<INPUT TYPE="CHECKBOX" NAME="chkall1" onClick="checkAll1(<%=pRs1.RecordCount%>)">
						</TD>
						<TH class="l-cellsec"><INPUT TYPE="BUTTON" VALUE="削除" onClick="SubmitWST0005(6)" class="i-btn"></TH>
						<TH class="l-cellsec">宛先</TH>
						<TH class="l-cellsec">件名</TH>
						<TH class="l-cellsec">送信日付
							<!-------------------------HIDDEN項目------------------------------>
							<INPUT TYPE="HIDDEN" NAME="hdnRowCnt1" VALUE="<%=pRs1.RecordCount%>">
						</TH>
					</TR>
				</TABLE>
				<div class="l-tbl-auto" style="height:250">
					<table class="l-tbl">
						<col width="5%">
						<col width="10%">
						<col width="25%">
						<col width="45%">
						<col width="15%">
<%
	cnt = 1
	Do Until pRs1.EOF
		If pRs1("READFLG") = 0 Then
			strSRC = "<IMG SRC='img/mail.gif' BORDER='0'>"
		Else
			strSRC = "<IMG SRC='img/read.gif' BORDER='0'>"
		End If
		'重要のイメージを決定
		strSRC1 = Empty
		If pRs1("WARNINGFLG") = 1 Then
			strSRC1 = "<IMG SRC='img/warning.gif'>"
		End If
%>
						<TR>
							<TD class="l-cellodd">
								<INPUT TYPE="CHECKBOX" NAME="chk_1<%=cnt%>">
							</TD>
							<TD class="l-cellodd">
								<A HREF="WST0005.asp?mode=2&shaincd=<%=strShainCD%>&seq=<%=(pRs1("SEQ"))%>">
								<%=strSRC%></A>
								<%=strSRC1%></TD>
							<TD class="l-cellodd">
								<%=LEFT(pRs1("TOSHAINNM"),10)%>
							</TD>
							<TD class="l-cellodd">
								<A HREF="WST0005.asp?mode=2&shaincd=<%=strShainCD%>&seq=<%=(pRs1("SEQ"))%>">
								<%=LEFT(pRs1("TITLE"),30)%></A>
							</TD>
							<TD class="l-cellodd"><%=LEFT(pRs1("SENDTIME"),10)%>
								<!-------------------------HIDDEN項目------------------------------>
								<INPUT TYPE="HIDDEN" NAME="hdnSEQ_1<%=cnt%>" VALUE="<%=pRs1("SEQ")%>">
							</TD>
						</TR>
<%
	cnt = cnt + 1
	pRs1.MoveNext
	Loop
%>
					</TABLE>
				</DIV>
			</TD>
			<TD WIDTH="*%" ALIGN="CENTER" VALIGN="TOP">
<%
	'初期は新規メール表示
	If pType = "0" Or pType = "4" Then
%>
				<table class="l-tbl">
					<col width="17%">
					<col width="*%">
					<TR ALIGN="LEFT">
						<TD COLSPAN="2"><FONT SIZE="3" COLOR="BLUE">新規メール：</TD>
					</TR>
					<TR>
						<TH class="l-cellsec" ROWSPAN="2"><FONT SIZE="2"><B>宛先：</TH>
						<TD class="l-cellodd">
<%
	strFormat = Empty
	strFormat1 = Empty
	If rdoTo = "1" Then strFormat1 = "CHECKED" Else strFormat = "CHECKED" End If
%>
							<INPUT TYPE="RADIO" NAME="rdoTo" VALUE="0" <%=strFormat%>><FONT SIZE="2">個人
							<INPUT TYPE="TEXT" NAME="txtToShainCD1" VALUE="<%=strToShainCD%>" onBlur="judgeZenkaku(this)" MAXLENGTH="40" SIZE="30" class="txt_imeoff" readonly="true">
							<INPUT TYPE="BUTTON" VALUE=">>" onClick="_open('txtToShainCD1')" class="i-btn">
						</TD>
					</TR>
					<TR>
						<TD class="l-cellodd">
							<INPUT TYPE="RADIO" NAME="rdoTo" VALUE="1" <%=strFormat1%>><FONT SIZE="2">部門
<% Call viewBumonCMB(strToBumonCD)%>
						</TD>
					</TR>
					<TR ALIGN="LEFT">
						<TH class="l-cellsec"><B>件名：</TD>
						<TD class="l-cellodd">
							<INPUT TYPE="TEXT" NAME="txtTitle" VALUE="<%=txtTitle%>" MAXLENGTH="30" SIZE="60" class="txt_imeon">
							重要<INPUT TYPE="CHECKBOX" NAME="chkWarningFLG">
						</TD>
					</TR>
					<TR ALIGN="LEFT">
						<TH class="l-cellsec">内容：<BR>
							
						</TH>
						<TD class="l-cellodd">
							<TEXTAREA NAME="txtMail" COLS="50" ROWS="15" class="txt_imeon"><%=txtMail%></TEXTAREA>
						</TD>
					<TR ALIGN="CENTER">
						<TD COLSPAN="2">
							<INPUT TYPE="BUTTON" VALUE="送信" onClick="SubmitWST0005(3)" class="s-btn">
						</TD>
					</TR>
				</TABLE>
<%
	'受信内容を表示
	ElseIf pType = "1" Then
		'受信メール取得
		Set rs2 = session("kintai").Execute(SELWST0005_3(strShainCD,fltSEQ))
		Do Until rs2.EOF
%>
				<table class="l-tbl">
					<col width="17%">
					<col width="*%">
					<TR ALIGN="LEFT">
						<FONT SIZE="3" COLOR="BLUE">受信メール：
					</TR>
					<TR>
						<TH class="l-cellsec">差出人：</TH>
						<TD class="l-cellodd"><%=rs2("SHAINNM")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">件名：</TH>
						<TD class="l-cellodd"><%=rs2("TITLE")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">受信時刻：</TH>
						<TD class="l-cellodd"><%=rs2("SENDTIME")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">開封時刻：</TH>
						<TD class="l-cellodd"><%=rs2("READTIME")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">内容：</TH>
						<TD class="l-cellodd">
							<TEXTAREA NAME="txtMail" COLS="50" ROWS="15" class="txt_imeon" readonly="true"><%=rs2("DETAIL")%></TEXTAREA>
							
						</TD>
					<TR ALIGN="CENTER">
						<TD COLSPAN="2">
							<INPUT TYPE="BUTTON" VALUE="返信" onClick="SubmitWST0005(4)" class="s-btn">
							<INPUT TYPE="BUTTON" VALUE="新規作成" onClick="SubmitWST0005(0)" class="s-btn">
							<!-----------------------HIDDEN項目：--------------------->
							<INPUT TYPE="HIDDEN" NAME="hdnToShainCD" VALUE="<%=rs2("SHAINCD")%>">
							<INPUT TYPE="HIDDEN" NAME="hdnTitle" VALUE="Re：<%=rs2("TITLE")%>">
						</TD>
					</TR>
				</TABLE>

<%
			Exit Do
		Loop
%>

<%
	'送信内容を表示
	ElseIf pType = "2" Then
		'送信メール取得
		Set rs2 = session("kintai").Execute(SELWST0005_4(strShainCD,fltSEQ))
		Do Until rs2.EOF
%>
				<table class="l-tbl">
					<col width="17%">
					<col width="*%">
					<TR ALIGN="LEFT">
						<FONT SIZE="3" COLOR="BLUE">送信済みメール：
					</TR>
					<TR>
						<TH class="l-cellsec">宛先：</TH>
						<TD class="l-cellodd"><%=rs2("TOSHAINNM")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">件名：</TH>
						<TD class="l-cellodd"><%=rs2("TITLE")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">送信時刻：</TH>
						<TD class="l-cellodd"><%=rs2("SENDTIME")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">開封時刻：</TH>
						<TD class="l-cellodd"><%=rs2("READTIME")%></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">内容</TH>
						<TD class="l-cellodd">
							<TEXTAREA NAME="txtMail" COLS="50" ROWS="15" class="txt_imeon" readonly="true"><%=rs2("DETAIL")%></TEXTAREA>
						</TD>
					</TR>
						<TD COLSPAN="2"><INPUT TYPE="BUTTON" VALUE="新規作成" onClick="SubmitWST0005(0)" class="s-btn"></TD>
						</TD>
				</TABLE>
<%
			rs2.MoveNext
		Loop
	End If
%>
			</TD>
		</TR>
	</TABLE>
</div>

<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">
</CENTER>
</FORM>
</BODY>
</HTML>
<%
End Sub
%>


