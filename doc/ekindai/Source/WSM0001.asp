<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WSM0001
'機能：カレンダーマスタメンテ表示画面
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<TITLE>WORKSHEET</TITLE>
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _onchange(){
	var year = document.bottom.txtYear.value;
	document.bottom.action="WSM0001.asp?year="+year;
	document.bottom.submit();
}

function _submit(){
	if(!confirm('新規作成してもよろしいですか？')){
		return;
	}
	document.bottom.target="bottom";
	document.bottom.action="WSM0001.asp?mode=insert";
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%

	'変数定義
	Dim rs												'レコードセット
	Dim	strNendo									'年度
	Dim strNengetu								'年月
	Dim strNengapi								'年月日
	Dim intYoubi									'曜日
	Dim intKyuujituFLG						'休日フラグを格納する配列
	Dim strSysYear								'システム年
	Dim errMSG										'エラーメッセージ
	Dim strMode										'実行モード
	Dim cnt,cnt1,cnt2								'カウント
	Dim strFormat									'書式
	Dim strColor									'カラー
	Dim strNengapiArr(12,6,7)			'年月日格納する配列
	Dim intKyuujituFLGArr(12,6,7)	'休日フラグを格納する配列
	Dim intYoubiArr(12,6,7)				'曜日を格納する配列
	Dim strNengetuArr(12)						'年月を格納する配列

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが作成の場合
	ElseIf strMode = "insert" Then
		'カレンダー作成処理を行う
		Call insertProc()
	End If

	response.end
%>
<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'フォームから年度を取得
	strNendo = request.form("txtYear")

	'フォームから年度を取得できない場合はアドレスバーから取得
	If strNendo = Empty Then
		strNendo = request.querystring("year")
	End If

	'実行モードを取得
	strMode = request.querystring("mode")

	'年度取得できない場合はシステム年を付与
	strSysYear = Mid(date,1,4)
	If strNendo = Empty Then
		strNendo = GetCurrenNendo()
	End If

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'カレンダーマスタからデータを取得
	Set rs = session("kintai").Execute(SELWSM0001(0,strNendo))

	'データ取得できない場合作成ボタンを表示し、処理を終了する
	If rs.RecordCount = 0 Then
		'ヘッダーを表示する
		viewTitle(Empty)
		Exit Sub
	End If

	'画面の項目に値をセット
	Call setScreenValue(rs)

	'ヘッダーを表示する
	Call viewTitle("DISABLED")

	'カレンダーを表示する
	Call viewDetail()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データ追加プロセス
'引数：なし
'****************************************************
Sub insertProc()

	'カレンダーデータ追加を実行
	If executeInsert() = False Then
		'エラーメッセージを設定し、処理を終了
		errMSG = "カレンダー" & WSE0000
		'ヘッダーを表示する
		viewTitle(Empty)
		Exit Sub
	End If

	'カレンダーマスタからデータを取得
	Set rs = session("kintai").Execute(SELWSM0001(0,strNendo))

	'画面の項目に値をセット
	Call setScreenValue(rs)

	'ヘッダーを表示する
	Call viewTitle("DISABLED")

	'カレンダーを表示する
	Call viewDetail()
End Sub
%>

<%
'****************************************************
'FUNCTION名：	executeInsert()
'FUNCTION機能：データ追加を実行する
'引数：なし
'戻り値：あり
'				False--失敗
'				True --成功
'****************************************************
Function executeInsert()

	'戻り値をTrueに設定
	executeInsert = True

	'エラー制御開始
	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollbackTrans
	session("kintai").BeginTrans

	'一度該当年度のデータを削除する
	session("kintai").Execute (DELWSM0001(0,strNendo))

	'エラー発生した場合処理を終了する
	If Err <> 0 Then
		'ロールバック
		session("kintai").RollbackTrans
		'戻り値をFalseに設定
		executeInsert = False
		Exit Function
	End If

	'カレンダーマスタにデータを追加
	For cnt = 4 To 15
		If cnt > 12 Then
			'来年に加算
			strNendo = strNendo + 1
			strNengetu = strNendo & toString((cnt - 12))
			'年度を戻す
			strNendo = strNendo - 1
		Else
			strNengetu = strNendo & toString(cnt)
		End If

		Select Case Mid(strNengetu,5,2)
		Case "01","03","05","07","08","10","12"
			For cnt1 = 1 To 31
				'年月日、曜日、休日を算出
				Call getCalendar(cnt1)

				'SQL実行
				If executeInsertSub() = False Then
					'ロールバック
					session("kintai").RollbackTrans
					'戻り値をFalseに設定
					executeInsert = False
					Exit Function
				End If
			Next
		Case "02","04","06","09","11"
			If Mid(strNengetu,5,2) = "02" Then
				'潤年を算出
				If Mid(strNengetu,1,4) mod 4 = 0 Then
					cnt2 = 29
				Else
					cnt2 = 28
				End If
			else
				cnt2 = 30
			End If

			For cnt1 = 1 To cnt2
				'年月日、曜日、休日を算出
				Call getCalendar(cnt1)

				'SQL実行
				If executeInsertSub() = False Then
					'ロールバック
					session("kintai").RollbackTrans
					'戻り値をFalseに設定
					executeInsert = False
					Exit Function
				End If
			Next
		End Select
	Next

	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

End Function
%>

<%
'****************************************************
'PROCEDURE名：	getCalendar(pCnt)
'PROCEDURE機能：年月日、曜日、休日を算出
'引数：あり
'			pCnt--カウント
'****************************************************
Sub getCalendar(pCnt)

	Dim strTemp

	'年月日算出
	strNengapi =  strNengetu & toString(pCnt)
	'曜日を算出
	strTemp = Mid(strNengetu,1,4) & "/" & Mid(strNengetu,5,2) & "/" & toString(pCnt)

	intYoubi = getWeekDay(strTemp)

	intKyuujituFLG = 0
	'土日に休日フラグを立てる
	If intYoubi = 6 Or intYoubi = 7 Then
		intKyuujituFLG = 1
	End If

End Sub
%>

<%
'****************************************************
'FUNCTION名：	executeInsertSub()
'FUNCTION機能：データ追加を実行するサブルーチン
'引数：あり
'戻り値：あり
'				False--失敗
'				True --成功
'****************************************************
Function executeInsertSub()

	'戻り値をTrueに設定
	executeInsertSub = True

	'SQL実行
	session("kintai").Execute(INSWSM0001(0,strNendo,strNengetu,strNengapi,intYoubi,intKyuujituFLG))

	'エラーチェック
	If Err <> 0 Then
		'戻り値をFalseに設定
		executeInsertSub = False
		Exit Function
	End If

End Function
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(pRs)
'PROCEDURE機能：画面の項目に値をセット
'引数：
'				pRs--カレンダーデータ
'****************************************************
Sub setScreenValue(pRs)

	'日別勤怠表から画面項目の格納変数に代入 一次元配列から三次元配列へ変更
	cnt  = 1
	cnt1 = 1
	cnt2 = 1
	Do until pRs.EOF
		If cnt1 > 6 Then
			cnt1 = 1
		End If
		'カレント年月と直前の年月を比較
		If pRs("NENGETU") <> strNengetu Then
			strNengetuArr(cnt) = pRs("NENGETU")
			'月が変わった場合、月をカウントし、初期化する
			cnt = cnt + 1
			cnt1 = 1
		End If

		'月曜日の場合週をカウントする（月の始めは月曜日の場合カウントしない）
		If pRs("YOUBI") - 1 = 0 And Right(pRs("NENGAPI"),2) <> "01" Then
			cnt1 = cnt1 + 1
		End If

		'三次元配列にセット
		cnt2 = pRs("YOUBI")
		strNengapiArr(cnt-1,cnt1,cnt2) = RIGHT(pRs("NENGAPI"),2)			'年月日格納する配列
		intKyuujituFLGArr(cnt-1,cnt1,cnt2) = pRs("KYUUJITUFLG")		'休日フラグを格納する配列
		intYoubiArr(cnt-1,cnt1,cnt2) = pRs("YOUBI")									'曜日を格納する配列

		'カレント年月を記憶
		strNengetu = pRs("NENGETU")

		'次のレコードに移動
		pRs.moveNext
	Loop
End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewTitle(pAble)
'PROCEDURE機能：ヘッダー部分を表示する
'引数：あり
'			pAble		-「作成」ボタン使用可能かどうか
'****************************************************
Sub viewTitle(pAble)
%>
	<FORM NAME="bottom" METHOD="POST">
	<CENTER>
	<U><B><FONT SIZE="3" COLOR="BLUE">会社年間カレンダー設定</FONT></B></U>
	<TABLE WIDTH="800" BORDER="0" CELLSPACING="0" CELLPADDING="0">
		<TR>
			<TD>
				<SELECT NAME="txtYear" onChange="_onchange()">
				<% For cnt = -3 To 5
						strFormat = Empty
					  If strSysYear + cnt - strNendo = 0 Then strFormat ="SELECTED" End If %>
						<OPTION <%=strFormat%> VALUE="<%=strSysYear + cnt%>"><%=strSysYear + cnt%></OPTION>
					End If
				<% Next %>
				</SELECT>年度&nbsp;&nbsp;&nbsp;
				<INPUT TYPE="BUTTON" <%=pAble%> VALUE="カレンダー作成" onClick="_submit()" class="s-btn1">
			</TD>
		</TR>
	</TABLE>
	</CENTER>
	<BR>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：  viewDetail()
'PROCEDURE機能：カレンダーを表示する
'****************************************************
Sub viewDetail()
%>
	<CENTER>
	<TABLE WIDTH="800" BORDER="0" CELLSPACING="0" CELLPADDING="4" BGCOLOR="">
<% For cnt = 1 To 12
	If cnt mod 4 = 1 Then %>
		<TR ALIGN="CENTER">
<% End If%>
		<TD ALIGN="LEFT" VALIGN="TOP">
			<TABLE WIDTH="" BORDER="0" CELLSPACING="0" CELLPADDING="0" BGCOLOR="">
				<TR ALIGN="CENTER">
					<TD COLSPAN="7" WIDTH="">
						<A HREF="WSM0001_1.asp?nengetu=<%=strNengetuArr(cnt)%>&calendarkbn=0"><FONT SIZE="3"><B><%=toDate2(strNengetuArr(cnt))%></B></A>
					</TD>
				</TR>
				<TR ALIGN="CENTER" HEIGHT="22">
					<TD WIDTH="22"><FONT SIZE="3" COLOR="">月</B></FONT></TD>
					<TD WIDTH="22"><FONT SIZE="3" COLOR="">火</B></FONT></TD>
					<TD WIDTH="22"><FONT SIZE="3" COLOR="">水</B></FONT></TD>
					<TD WIDTH="22"><FONT SIZE="3" COLOR="">木</B></FONT></TD>
					<TD WIDTH="22"><FONT SIZE="3" COLOR="">金</B></FONT></TD>
					<TD WIDTH="22" BGCOLOR=""><FONT SIZE="3" COLOR="BLUE">土</B></FONT></TD>
					<TD WIDTH="22" BGCOLOR=""><FONT SIZE="3" COLOR="RED">日</B></FONT></TD>
				</TR>

	 <% For cnt1 = 1 To 6 %>
				<TR ALIGN="CENTER" HEIGHT="22">
		 <% For cnt2 = 1 To 7
					strFont = getYoubiFont(intYoubiArr(cnt,cnt1,cnt2),intKyuujituFLGArr(cnt,cnt1,cnt2)) %>
					<TD WIDTH=""> 
						<FONT SIZE="3" COLOR="<%=strFont%>"><%=toNumber(strNengapiArr(cnt,cnt1,cnt2))%></FONT>
					</TD>
		 <% Next %>
				</TR>
	 <% Next %>
			</TABLE>
		</TD>
<% If cnt mod 4 = 0 Then %>
	</TR>
<% End If %>
<% Next %>
</TABLE>
<%
End Sub
%>

