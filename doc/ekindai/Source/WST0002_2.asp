<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0002_2
'機能：休暇情報の一覧
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(){
	document.bottom.action="WST0002_2.asp";
	document.bottom.submit();
	return;
}
function _back(BackURL){
	document.bottom.action=BackURL;
	document.bottom.submit();
	return;
}

//--->
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim rs											'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strNendo								'年度
	Dim strSysDate							'システム年月日
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim strFormat								'書式
	Dim strURL									'URL
	Dim hdnBackURL							'戻るパス
	Dim fltYuukyuuFuyo					'有休付与数
	Dim fltYuukyuuCNT						'有休使用数
	Dim fitHankyuuCNT						'半休使用数
	Dim fltYuukyuuZan						'有休残数
	Dim fltDaikyuuCNT						'代休使用数
	Dim fltDaikyuuZan						'代休使用可能残数
	Dim fltKekkinCNT						'欠勤日数
	Dim fltSonotaCNT						'そのた日数

	'パラメーター取得
	Call getParameter()

	Call initProc()
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()
	Dim strTemp

	'****パラメータ取得******
	'アドレスバーから取得から社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	If strShainCD = Empty Then
		'フォームから社員番号取得
		strShainCD = trim(request.form("txtShainCD"))
	End IF

	'アドレスバーから取得から年度を取得
	strNendo = trim(request.querystring("nendo"))
	If strNendo = Empty Then
		'フォームから取得から年度を取得
		strNendo = trim(request.form("cmbNendo"))
	End If

	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = GetCurrenNendo()
	end if

	'モード取得
	mode = trim(request.querystring("mode"))
	if mode = "menu" then 
		'セッションクリア
		Session("backurl") = Empty
	end if

	'戻るパス
'	hdnBackURL = "WST0002_2.asp?nendo=" & strNendo
	'戻るパスを取得
	'セッションより戻るパスを取得
	hdnBackURL = Session("backurl")

	If hdnBackURL = Empty Then
		hdnBackURL = trim(request.form("hdnBackURL"))
	End If

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle(strNendo & "度 休暇状況一覧",strBumonCD,strBumonNM,strShainCD,strShainNM)


	'初期化
	fltYuukyuuFuyo = 0							'有休付与数
	fltYuukyuuCNT	 = 0							'有休使用数
	fitHankyuuCNT	 = 0							'半休使用数
	fltYuukyuuZan	 = 0							'有休残数
	fltDaikyuuCNT	 = 0							'代休使用数
	fltDaikyuuZan	 = 0							'代休使用可能残数
	fltKekkinCNT	 = 0							'欠勤日数
	fltSonotaCNT	 = 0							'そのた日数

	'有休マスタから有休使用付与、有休残数を取得
	Set rs = session("kintai").Execute(SELWSM0004_1(strShainCD,strNendo))
	'有休付与数,有休残数に代入
	Do Until rs.EOF
		fltYuukyuuFuyo = rs("YUUKYUU")
		fltYuukyuuZan = rs("YUUKYUUZAN")
		rs.MoveNext
	Loop

	'レコードセット開放
	Set rs = Nothing

	'代休マスタから代休可能残数を取得
	Set rs = session("kintai").Execute(SELWSM0005_1(strShainCD,strNendo))

	'代休使用可能残数に代入
	Do Until rs.EOF
		fltDaikyuuZan = rs("CNT")
		rs.MoveNext
	Loop
	'レコードセット開放
	Set rs = Nothing

	'月別勤怠表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0002_1(strShainCD,strNendo))

	'有休使用数に代入
	Do Until rs.EOF
		fltYuukyuuCNT = rs("YUUKYUUCNT")
		'代給使用数に代入
		fitHankyuuCNT = rs("HANKYUUCNT")
		'半休使用数に代入
		fltDaikyuuCNT = rs("DAIKYUUCNT")
		'欠勤使用数に代入
		fltKekkinCNT = rs("KEKKINCNT")
		'その他使用数に代入
		fltSonotaCNT = rs("SONOTACNT")
		rs.MoveNext
	Loop
	'有休＝有休＋代休
	If fitHankyuuCNT > 0 Then
		fltYuukyuuCNT = fltYuukyuuCNT + fitHankyuuCNT/2
	End If
	'レコードセット開放
	Set rs = Nothing

	'内容を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：勤怠一覧表示
'引数：なし
'****************************************************
Sub viewDetail()
%>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="10%">
<%Call viewNendoCMB(strNendo)%>
			</TD>
			<TD WIDTH="30%">
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="_submit()" class="s-btn">
<%
	If hdnBackURL = Empty Then
		strFormat = "DISABLED"
		End If
%>
					<INPUT TYPE="BUTTON" VALUE="戻る" onClick="_back('<%=hdnBackURL%>')" <%=strFormat%> class="s-btn">
			</TD>
			<TD WIDTH="*%">
			</TD>
		</TR>
	</TABLE>

	<table class="l-tbl">
		<TR>
			<TD>
				<table class="l-tbl">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="*%">
					<TR>
						<TH class="l-cellsec" COLSPAN="7">年度合計</TD>
						<TD></TD>
					 </TR>
					<TR>
						<TH class="l-cellsec">有休付与</TH>
						<TH class="l-cellsec">有休使用日数</TH>
						<TH class="l-cellsec">有休残数</TH>
						<TH class="l-cellsec">代休使用日数</TH>
						<TH class="l-cellsec">代休可能残数</TH>
						<TH class="l-cellsec">欠勤使用日数</TH>
						<TH class="l-cellsec">その他日数</TH>
						<TD></TD>
					</TR>

					<TR>
						<TD class="l-celloddr"><%=fltYuukyuuFuyo%></TD>
						<TD class="l-celloddr"><%=fltYuukyuuCNT%></TD>
						<TD class="l-celloddr"><%=fltYuukyuuZan%></TD>
						<TD class="l-celloddr"><%=fltDaikyuuCNT%></TD>
						<TD class="l-celloddr"><%=fltDaikyuuZan%></TD>
						<TD class="l-celloddr"><%=fltKekkinCNT%></TD>
						<TD class="l-celloddr"><%=fltSonotaCNT%></TD>
					</TR>
				</TABLE>
				<BR>
			<%
				'日別勤怠テーブル年間休暇データを取得
				Set rs = session("kintai").Execute(SELWST0001_4(strShainCD,strNendo))
			%>

				<table class="l-tbl">
				<col width="10%">
				<col width="10%">
				<col width="30%">
				<col width="*%">
					<TR>
						<TH class="l-cellsec" COLSPAN="3">有休、半休一覧</TH>
						<TD></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">勤怠種別</TH>
						<TH class="l-cellsec">行使日</TH>
						<TH class="l-cellsec">コメント</TH>
						<TD></TD>
					</TR>
			<%
				Do Until rs.EOF
					If rs("KINMUKBN") = 2 Or rs("KINMUKBN") = 4 Or rs("KINMUKBN") = 5 Then
			%>
					<TR>
						<TD class="l-cellodd"><%=strKinmukbn(rs("KINMUKBN"))%></TD>
						<TD class="l-cellodd"><%=toDate(rs("NENGAPI"))%></TD>
						<TD class="l-cellodd"><%=rs("CMT")%></TD>
					</TR>
			<%
					ElseIf rs("KINMUKBN") = 6 Then
						Exit Do
					End If
					rs.MoveNext
				Loop
			%>
				</TABLE>
				<BR>

			<%
				'代休マスタと日別勤怠テーブルからデータを取得
				Set rs1 = session("kintai").Execute(SELWSM0005_2(strNendo,strShainCD))
			%>

				<table class="l-tbl">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="30%">
				<col width="*%">
					<TR>
						<TH class="l-cellsec" COLSPAN="4">代休一覧</TH>
						<TD></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">勤怠種別</TH>
						<TH class="l-cellsec">休日出勤日</TH>
						<TH class="l-cellsec">行使日</TH>
						<TH class="l-cellsec">コメント</TH>
						<TD></TD>
					</TR>
			<%
				Do Until rs1.EOF
			%>
					<TR>
						<TD class="l-cellodd">代休</TD>
						<TD class="l-cellodd"><%=toDate(rs1("KYUUDEDATE"))%></TD>
						<TD class="l-cellodd"><%=toDate(rs1("DAIKYUUDATE"))%></TD>
						<TD class="l-cellodd"><%=rs1("CMT")%></TD>
					</TR>
			<%
					rs1.MoveNext
				Loop
			%>
				</TABLE>
				<BR>

				<table class="l-tbl">
				<col width="10%">
				<col width="10%">
				<col width="30%">
				<col width="*%">
					<TR>
						<TH class="l-cellsec" COLSPAN="3">欠勤一覧</TH>
						<TD></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">勤怠種別</TH>
						<TH class="l-cellsec">行使日</TH>
						<TH class="l-cellsec">コメント</TH>
						<TD></TD>
					</TR>
			<%
				Do Until rs.EOF
					If rs("KINMUKBN") = 6 Then
			%>
					<TR>
						<TD class="l-cellodd">欠勤</TD>
						<TD class="l-cellodd"><%=toDate(rs("NENGAPI"))%></TD>
						<TD class="l-cellodd"><%=rs("CMT")%></TD>
						<TD></TD>
					</TR>
			<%
					ElseIf rs("KINMUKBN") = 7 Then
						Exit Do
					End If
					rs.MoveNext
				Loop
			%>
				</TABLE>
				<BR>

				<table class="l-tbl">
				<col width="10%">
				<col width="10%">
				<col width="30%">
				<col width="*%">
					<TR>
						<TH class="l-cellsec" COLSPAN="3">その他一覧</TD>
						<TD></TD>
					</TR>
					<TR>
						<TH class="l-cellsec">勤怠種別</TD>
						<TH class="l-cellsec">行使日</TD>
						<TH class="l-cellsec">コメント</TD>
						<TD></TD>
					</TR>
			<%
				Do Until rs.EOF
					If rs("KINMUKBN") = 7 Then
			%>
					<TR>
						<TD class="l-cellodd">その他</TD>
						<TD class="l-cellodd"><%=toDate(rs("NENGAPI"))%></TD>
						<TD class="l-cellodd"><%=rs("CMT")%></TD>
						<TD></TD>
					</TR>
			<%
					End If
					rs.MoveNext
				Loop
			%>
				</TABLE>
				<BR>
			</TD>
		</TR>
	</TABLE>
</div>
<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">
<INPUT TYPE="HIDDEN" NAME="hdnBackURL" 	VALUE="<%=hdnBackURL%>">

</CENTER>
</FORM>
</BODY>
</HTML>
<% 
	End Sub
%>

