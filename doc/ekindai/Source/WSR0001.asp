<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WSR0001
'機能：勤怠表集計
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<HTML onContextMenu="return false;">
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(type){
	document.bottom.action="WSR0001.asp?mode=" + type;
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>
<%
	'変数定義
	Dim rs											'レコードセット
	Dim mode								'実行モード
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strNendo								'年度
	Dim strMonth								'月
	Dim strNengetu							'年月
	Dim strSQL									'SQL文
	Dim cnt											'カウント
	Dim strURL									'URL
	Dim strURL1									'URL
	Dim hdnBackURL							'戻るパス
	Dim cmbBumonCD							'部門コード

	'パラメーター取得
	Call getParameter()
	
	if mode="1" then
		Call downloadProc()
	else
		Call initProc()
	end if
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーからからモードを取得
	mode = trim(request.querystring("mode"))
	'アドレスバーからから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	If strShainCD = Empty Then
		'フォームから社員番号取得
		strShainCD = trim(request.form("txtShainCD"))
	End If

	'アドレスバーから取得から年度を取得
	strNendo = trim(request.querystring("nendo"))
	If strNendo = Empty Then
		'フォームから取得から年度を取得
		strNendo = trim(request.form("cmbNendo"))
	End If
	'年度を取得できない場合はシステム日付を付与する
	if strNendo = empty then
		strNendo = GetCurrenNendo()
	end if

	'アドレスバーからから月を取得
	strMonth = trim(request.querystring("month"))
	If strMonth = Empty Then
		'フォームから取得から月を取得
		strMonth = trim(request.form("cmbMonth"))
	End If
	'月を取得できない場合はシステム日付を付与する
	if strMonth = empty then
		strMonth = CONST_SYSMONTH
	end if

	'年月
	strNengetu = strNendo & strMonth

	'アドレスバーから取得から部門を取得
	cmbBumonCD = trim(request.querystring("bumoncd"))
	If cmbBumonCD = Empty Then
		'フォームから取得から部門を取得
		cmbBumonCD = trim(request.form("cmbBumonCD"))
	End If

	'戻るパス
 	hdnBackURL = "WSR0001.asp?bumoncd=" & cmbBumonCD & "&nendo=" & strNendo & "&month=" & strMonth
 	
 	'セッションに戻るパスを設定する
  Session("backurl") = hdnBackURL

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'月別勤怠情報から部門単位データを取得
	Set rs = session("kintai").Execute(SELWST0002_5(cmbBumonCD,strNengetu))

	'内容を表示
	Call viewHead()

	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	downloadProc()
'PROCEDURE機能：ダウンロードを行う
'引数：なし
'****************************************************
Sub downloadProc()

	'月別勤怠情報から部門単位データを取得
	Set rs = session("kintai").Execute(SELWST0002_5(cmbBumonCD,strNengetu))

	Response.Buffer = True
	Response.Charset="Shift-JIS"
'	Response.ContentType = "application/vnd.ms-excel"
	Response.ContentType = "application/x-download"

	Response.AddHeader "Content-Disposition","attachment inline;filename=" & strNendo & "年" & strMonth & "月勤怠集計票.xls"

	'内容を表示
	Call viewDetail(rs)

	'レコードセットを開放
	Set rs = Nothing

	Response.Flush
	Response.end


End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewHead()
'PROCEDURE機能：勤怠一覧表示
'引数：あり
'			pRs:月別勤怠情報
'****************************************************
Sub viewHead()
%>
<link href="css/common.css" rel="stylesheet" type="text/css">
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
	<U><FONT COLOR="GRAY">勤怠集計表</font></U>
</CENTER>
<div class="list">

	<table class="l-tbl">
		<TR>
			<TD WIDTH="10%">
<%Call viewNendoCMB(strNendo)%>
				<FONT SIZE="2">年
			</TD>
			<TD WIDTH="10%">
<%Call viewMonthCMB(strMonth)%>
				<FONT SIZE="2">月
			</TD>
			<TD WIDTH="50%">
<%Call viewBumonCMB(cmbBumonCD)%>
				<FONT SIZE="2">部門
				<INPUT TYPE="BUTTON" VALUE="表示" onClick="_submit()" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="ダウンロード" onClick="_submit(1)" class="s-btn">
			</TD>
			<TD WIDTH="*%">
			</TD>
		</TR>
	</TABLE>
<% End Sub %>


<%
'****************************************************
'PROCEDURE名：	viewDetail(pRs)
'PROCEDURE機能：勤怠一覧表示
'引数：あり
'			pRs:月別勤怠情報
'****************************************************
Sub viewDetail(pRs)
%>
	<table class="l-tbl">
		<!--氏名-->
		<col width="20%">
		<!--標準<BR>日数-->
		<col width="4%">
		<!--稼働<BR>日数-->
		<col width="4%">
		<!--標準<BR>時間-->
		<col width="5%">
		<!--稼働<BR>時間-->
		<col width="5%">
		<!--普通<BR>残業-->
		<col width="4%">
		<!--休日<BR>残業-->
		<col width="4%">
		<!--深夜<BR>残業-->
		<col width="4%">
		<!--不足時間 -->
		<col width="5%">
		<!--遅刻時間 -->
<!--		
		<col width="4%">
-->
		<!--有休日数 -->
		<col width="4%">
		<!--代休日数 -->
		<col width="4%">
		<!--欠勤日数 -->
		<col width="4%">
		<!--その他日数 -->
		<col width="5%">
		<!--承認状況 -->
		<col width="5%">
		<!--申請日 -->
		<col width="6%">
		<!--承認日 -->
		<col width="6%">
		<!--承認者 -->
		<col width="*%">
		<TR>
<%Call viewKintaiHeader1()%>
		</TR>
	</TABLE>
	
  <div class="l-tbl-auto" style="height:500px">
	<table class="l-tbl">
		<!--氏名-->
		<col width="20%">
		<!--標準<BR>日数-->
		<col width="4%">
		<!--稼働<BR>日数-->
		<col width="4%">
		<!--標準<BR>時間-->
		<col width="5%">
		<!--稼働<BR>時間-->
		<col width="5%">
		<!--普通<BR>残業-->
		<col width="4%">
		<!--休日<BR>残業-->
		<col width="4%">
		<!--深夜<BR>残業-->
		<col width="4%">
		<!--不足時間 -->
		<col width="5%">
		<!--遅刻時間 -->
<!--
		<col width="4%">
-->
		<!--有休日数 -->
		<col width="4%">
		<!--代休日数 -->
		<col width="4%">
		<!--欠勤日数 -->
		<col width="4%">
		<!--その他日数 -->
		<col width="5%">
		<!--承認状況 -->
		<col width="5%">
		<!--申請日 -->
		<col width="6%">
		<!--承認日 -->
		<col width="6%">
		<!--承認者 -->
		<col width="*%">
		<TR>
	<%
		dim i
		dim cellClass,cellClassr
		i = 0
	%>
	<%Do Until pRs.EOF
		i = i +1
		if i mod 2 <> 0 then
			cellClass = "l-cellodd"
			cellClassr = "l-celloddr"
		else
			cellClass = "l-celleven"
			cellClassr = "l-cellevenr"
		end if
			'月間確認にリンク
			strURL = "WST0001_1.asp?shaincd=" & pRs("SHAINCD") & "&nengetu=" & pRs("NENGETU")
			strURL1 = "WST0002_2.asp?shaincd=" & pRs("SHAINCD")
	%>
		<TR>
			<TD class=<%=cellClass%>>
				<FONT SIZE="2"><A HREF=<%=strURL%>><%=pRs("SHAINCD")%></A></FONT>
			</TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("STANDARDDAY"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("KINMUDAY"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("STANDARDTIME"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("KINMUTIME"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("ZANGYOUTIMEF"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("ZANGYOUTIMEK"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("ZANGYOUTIMES"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("SHORTAGETIME"))%></TD>
<!--
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal2(pRs("TIKOKUTIME"))%></TD>
-->
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("YUUKYUUCNT"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("DAIKYUUCNT"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("KEKKINCNT"))%></TD>
			<TD class=<%=cellClassr%> rowspan="2"><%=toDecimal1(pRs("SONOTACNT"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=CONST_SHOUNINSTATUS(pRs("SHOUNINSTATUS"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=toDate(pRs("SINSEIDATE"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=toDate(pRs("SHOUNINDATE"))%></TD>
			<TD class=<%=cellClass%> rowspan="2"><%=pRs("SHOUNINSHANM")%></TD>
		</TR>
		<TR>
			<TD class=<%=cellClass%>><A HREF=<%=strURL1%>><%=pRs("SHAINNM")%></A></TD>
		</TR>



	<%pRs.MoveNext
		Loop%>
	</TABLE>
  </div>
</div>
<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<%=strShainCD%>">

</CENTER>
</FORM>
</BODY>
</HTML>
<% End Sub %>

