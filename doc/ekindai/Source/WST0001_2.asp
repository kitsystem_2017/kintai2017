<%@ Language=VBScript %>
<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST0001_2
'機能：プロジェクト設定
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>
<HTML onContextMenu="return false;">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">
<CENTER>
<%
	'変数定義
	Dim strPJNOArr(10)
	Dim strPJNMArr(10)
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strBumonCD							'部門ｺｰﾄﾞ
	Dim strBumonNM							'部門名称
	Dim strKengen								'権限
	Dim strNengetu							'年月
	Dim errMSG									'エラーメッセージ
	Dim strMode									'実行モード
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim strColor								'カラー
	Dim strDateFrom       			'週間の開始日
	Dim strDateTo								'週間の終了日
	Dim hdnBackURL							'戻るパス

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが更新の場合
	ElseIf strMode = "1" Then
		'更新処理を行う
		Call updateProc()
	ElseIf strMode = "2" Then
		'戻るを行う
		Response.Redirect hdnBackURL
	End If
	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'実行モードを取得
	strMode = request.querystring("mode")

	'社員番号を取得
	strShainCD = trim(request.form("txtShainCD"))

	'アドレス取得
	strNengetu = trim(request.form("txtNengetu"))

	'アドレスバーから週間の開始日、終了日を取得
	strDateFrom = trim(request.form("txtDateFrom"))
	strDateTo = trim(request.form("txtDateTo"))

	'戻るパスを取得
	hdnBackURL = trim(request.form("hdnBackURL"))

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle(toDate2(strNengetu) & "度 プロジェクト設定",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'****PJNO情報取得
	Set rs = session("kintai").Execute(SELWST0002(strShaincd,strNengetu))

	Do Until rs.EOF
		'PJ情報を取得
		strPJNOArr(1) = rs("PJNO1")
		strPJNOArr(2) = rs("PJNO2")
		strPJNOArr(3) = rs("PJNO3")
		strPJNOArr(4) = rs("PJNO4")
		strPJNOArr(5) = rs("PJNO5")
		strPJNOArr(6) = rs("PJNO6")
		strPJNOArr(7) = rs("PJNO7")
		strPJNOArr(8) = rs("PJNO8")
		strPJNOArr(9) = rs("PJNO9")
		strPJNOArr(10) = rs("PJNO10")

		strPJNMArr(1) = rs("PJNM1")
		strPJNMArr(2) = rs("PJNM2")
		strPJNMArr(3) = rs("PJNM3")
		strPJNMArr(4) = rs("PJNM4")
		strPJNMArr(5) = rs("PJNM5")
		strPJNMArr(6) = rs("PJNM6")
		strPJNMArr(7) = rs("PJNM7")
		strPJNMArr(8) = rs("PJNM8")
		strPJNMArr(9) = rs("PJNM9")
		strPJNMArr(10) = rs("PJNM10")
		rs.MoveNext
	Loop

	Call viewDetail()

End Sub
%>
<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データの更新を行う
'引数：なし
'****************************************************
Sub updateProc()

	Dim strTempArr(10),strTempArr1(10)

	'画面の入力値を取得
	strPJNOArr(1) = request.form("txtPJNO1")
	strPJNOArr(2) = request.form("txtPJNO2")
	strPJNOArr(3) = request.form("txtPJNO3")
	strPJNOArr(4) = request.form("txtPJNO4")
	strPJNOArr(5) = request.form("txtPJNO5")
	strPJNOArr(6) = request.form("txtPJNO6")
	strPJNOArr(7) = request.form("txtPJNO7")
	strPJNOArr(8) = request.form("txtPJNO8")
	strPJNOArr(9) = request.form("txtPJNO9")
	strPJNOArr(10) = request.form("txtPJNO10")

	strPJNMArr(1) = getPJNM(strPJNOArr(1))
	strPJNMArr(2) = getPJNM(strPJNOArr(2))
	strPJNMArr(3) = getPJNM(strPJNOArr(3))
	strPJNMArr(4) = getPJNM(strPJNOArr(4))
	strPJNMArr(5) = getPJNM(strPJNOArr(5))
	strPJNMArr(6) = getPJNM(strPJNOArr(6))
	strPJNMArr(7) = getPJNM(strPJNOArr(7))
	strPJNMArr(8) = getPJNM(strPJNOArr(8))
	strPJNMArr(9) = getPJNM(strPJNOArr(9))
	strPJNMArr(10) = getPJNM(strPJNOArr(10))


	'エラーがなければ、SQL実行
	If errMSG = Empty then
		'PJNO整理する
		cnt1 = 1
		For cnt = 1 To 10
			'空のPJNOを無視
			If strPJNOArr(cnt) <> Empty Then
				strTempArr(cnt1) = strPJNOArr(cnt)
				strTempArr1(cnt1) = strPJNMArr(cnt)
				cnt1 = cnt1 + 1
			End If
		Next

		For cnt = 1 To 10
			strPJNOArr(cnt) = strTempArr(cnt)
			strPJNMArr(cnt) = strTempArr1(cnt)
		Next

		'エラー制御開始
'		On Error Resume Next

		'** トランザクション開始
		'session("kintai").RollbackTrans
		session("kintai").BeginTrans

		'SQL実行
		session("kintai").Execute (UPDWST0002_3(strShainCD,strNengetu,strPJNOArr(1),strPJNMArr(1),strPJNOArr(2),strPJNMArr(2),strPJNOArr(3),strPJNMArr(3),strPJNOArr(4),strPJNMArr(4),strPJNOArr(5),strPJNMArr(5),strPJNOArr(6),strPJNMArr(6),strPJNOArr(7),strPJNMArr(7),strPJNOArr(8),strPJNMArr(8),strPJNOArr(9),strPJNMArr(9),strPJNOArr(10),strPJNMArr(10)))
		'** エラーチェック
		If err <> 0 then
			session("kintai").RollbackTrans
			errMSG = "プロジェクト" & WSE0000
		Else
			'** トランザクション終了
			 session("kintai").CommitTrans
		End If

		'エラー制御終了
		On Error Goto 0
	End If

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle(toDate2(strNengetu) & "度 プロジェクト設定",strBumonCD,strBumonNM,strShainCD,strShainNM)

	Call viewDetail()

End Sub

%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：詳細表示
'引数：なし
'****************************************************
Sub viewDetail()
%>
	<font size='2' color='red'>　<% =errMSG %></font>
<div class="list">

	<table class="l-tbl">
		<col width="25%">
		<col width="3%">
		<col width="15%">
		<col width="32%">
		<col width="25%">
		<TR>
			<TD></TD>
			<TH class="l-cellsec">NO</TH>
			<TH class="l-cellsec">プロジェクトNo.</TH>
			<TH class="l-cellsec">プロジェクト名称</TH>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-cellodd">1</TD>
			<TD class="l-cellodd">
				<INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO1" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(1) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO1')" class="i-btn">
			</TD>
			<TD class="l-cellodd"><% =strPJNMArr(1) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-celleven">2</TD>
			<TD class="l-celleven">
				<INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO2" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(2) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO2')" class="i-btn">
			</TD>
			<TD class="l-celleven"><% =strPJNMArr(2) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-cellodd">3</TD>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO3" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(3) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO3')" class="i-btn">
			</TD>
			<TD class="l-cellodd"><% =strPJNMArr(3) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-celleven">4</TD>
			<TD class="l-celleven"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO4" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(4) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO4')" class="i-btn">
			</TD>
			<TD class="l-celleven"><% =strPJNMArr(4) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-cellodd">5</TD>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO5" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(5) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO5')" class="i-btn">
			</TD>
			<TD class="l-cellodd"><% =strPJNMArr(5) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-celleven">6</TD>
			<TD class="l-celleven"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO6" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(6) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO6')" class="i-btn">
			</TD>
			<TD class="l-celleven"><% =strPJNMArr(6) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-cellodd">7</TD>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO7" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(7) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO7')" class="i-btn">
			</TD>
			<TD class="l-cellodd"><% =strPJNMArr(7) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-celleven">8</TD>
			<TD class="l-celleven"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO8" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(8) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO8')" class="i-btn">
			</TD>
			<TD class="l-celleven"><% =strPJNMArr(8) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-cellodd">9</TD>
			<TD class="l-cellodd"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO9" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(9) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO9')" class="i-btn">
			</TD>
			<TD class="l-cellodd"><% =strPJNMArr(9) %></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD></TD>
			<TD class="l-celleven">10</TD>
			<TD class="l-celleven"><INPUT TYPE="TEXT" SIZE="12" MAXLENGTH="7" NAME="txtPJNO10" class="txt_imeoff" readonly="true"
													onBlur="judgeZenkaku(this)" VALUE="<% =strPJNOArr(10) %>">
				<INPUT TYPE="BUTTON" VALUE=">>" onClick="_openWST0001SW001('txtPJNO10')" class="i-btn">
			</TD>
			<TD class="l-celleven"><% =strPJNMArr(10) %></TD>
			<TD></TD>
		</TR>
	</TABLE><BR>
	<input type="button" value="確定" onClick="SubmitWST0001_2(1)" class="s-btn">
	<input type="button" value="クリア" onClick="SubmitWST0001_2(3)" class="s-btn">
	<input type="button" value="戻る" onClick="SubmitWST0001_2(2)" class="s-btn"><br>
</div>
	<!-----------------------HIDDEN項目：--------------------->
	<INPUT TYPE="HIDDEN" NAME="txtShainCD" 	VALUE="<%=strShainCD%>">
	<INPUT TYPE="HIDDEN" NAME="txtBumonCD" 	VALUE="<%=strBumonCD%>">
	<INPUT TYPE="HIDDEN" NAME="txtNengetu" 	VALUE="<%=strNengetu%>">
	<INPUT TYPE="HIDDEN" NAME="txtDateFrom" VALUE="<%=strDateFrom%>">
	<INPUT TYPE="HIDDEN" NAME="txtDateTo" 	VALUE="<%=strDateTo%>">
	<INPUT TYPE="HIDDEN" NAME="hdnBackURL" 	VALUE="<%=hdnBackURL%>">

<%
	'レコードセット開放
	Set rs = Nothing
End Sub
%>

<%
'****************************************************
'FUNCTION名：  getPJNM(pPJNO)
'FUNCTION機能：PJ名称を取得
'引数：pPJNO--プロジェクトナンバー
'		pPJKBN--0:マスタ、1:一般
'****************************************************
Function getPJNM(pPJNO)
	Dim lrs
	getPJNM = ""

	If trim(pPJNO) = Empty then
		Exit Function
	End If

	Set lrs = session("kintai").Execute(SELM0003(pPJNO))

	If lrs.RecordCount = 0 then
		errMSG = pPJNO & "：該当プロジェクト" & "WSE0009"
	Else
		getPJNM = replace(lrs("PJNM"),"'","’")
	End If

	Set lrs = Nothing

End Function

%>
