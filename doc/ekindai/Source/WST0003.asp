<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WS0003
'機能：行先＆スケジュール
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>

<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(type){
	document.bottom.target="bottom";
	//行先登録
	if(type == 0) {
		document.bottom.action="WST0003.asp?mode=ikisaki";
		document.bottom.submit();
		return;
	}
	//スケジュール登録
	if(type == 1) {
		document.bottom.action="WST0003.asp?mode=insert";
		document.bottom.submit();
		return;
	}
	//更新
	if(type == 2) {
		document.bottom.action="WST0003.asp?mode=update";
		document.bottom.submit();
		return;
	}
	//削除
	if(type == 3) {
		document.bottom.action="WST0003.asp?mode=delete";
		document.bottom.submit();
		window.close();
	}
}
//--->
</SCRIPT>
</HEAD>
<HTML onContextMenu="return false;">
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs								'レコードセット
	Dim strShainCD				'社員ｺｰﾄﾞ
	Dim strShainNM				'社員氏名
	Dim strBumonCD				'部門ｺｰﾄﾞ
	Dim strBumonNM				'部門名称
	Dim strKengen					'権限
	Dim strNengapi				'年月日
	Dim strNengetu				'年月
	Dim strSysDate				'システム年月日
	Dim errMSG						'ヘッダー
	Dim strMode						'実行モード
	Dim strSQL						'SQL文
	Dim cnt,cnt1
	Dim intSEQ						'SEQ
	Dim strSTime					'開始時間
	Dim strETime					'終了時間
	Dim fltKikan					'期間
	Dim strFormat					'書式
	Dim chkFlag						'ﾁｪｯｸフラグ
	Dim intKoukaiFLG			'公開フラグ

	'画面項目の格納変数定義
	Dim lblModifyDate				'記入日
	Dim cmbIkisaki					'行先
	Dim txtRenraku					'連絡
	Dim txtIkisakiCMT				'コメント
	Dim chkKoukai						'公開
	Dim cmbSTimeH						'開始時
	Dim cmbSTimeM						'開始分
	Dim cmbETimeH						'終了時
	Dim cmbETimeM						'終了分
	Dim cmbYotei						'予定種別
	Dim txtScheduleCMT			'コメント
	Dim chkSelArr()					'選択
	Dim chkKoukaiArr()			'公開
	Dim cmbSTimeHArr()			'開始時
	Dim cmbSTimeMArr()			'開始分
	Dim cmbETimeHArr()			'終了時
	Dim cmbETimeMArr()			'終了分
	Dim lblKikanArr()				'期間
	Dim cmbYoteiArr()				'予定種別
	Dim txtScheduleCMTArr()	'コメント
	Dim hdnSEQArr()					'SEQ隠し項目

	'パラメーター取得
	Call getParameter()

	'*********実行モードを判定する*********
	'実行モードが空の場合
	If strMode = empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが行先の場合
	elseIf strMode = "ikisaki" Then
		'行先掲示を処理
		Call entryIkisakiProc()
	'実行モードが追加の場合
	elseIf strMode = "insert" Then
		'スケジュール追加処理を行う
		Call insertProc()
	'実行モードが更新の場合
	elseIf strMode = "update" Then
		'スケジュール更新処理を行う
		Call updateProc()
	'実行モードが削除の場合
	elseIf strMode = "delete" Then
		'スケジュール削除処理を行う
		Call deleteProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = empty Then
		strShainCD = trim(request.form("txtShainCD"))
	End If

	'フォームから部門ｺｰﾄﾞを取得
	strBumonCD = trim(request.form("txtBumonCD"))

	'実行モードを取得
	strMode = request.querystring("mode")

	'システム年月日を取得
	strSysDate = mid(date,1,4) & mid(date,6,2) & mid(date,9,2)

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))

	'アドレスバーから年月を取得出来ない場合はフォームから取得
	If strNengetu = empty Then
		strNengetu = trim(request.form("txtNengetu"))
	End If

	'年月を取得できない場合はシステム日付を付与する
	If strNengetu = empty Then
		strNengetu = mid(date,1,4) & mid(date,6,2)
	End If

	'アドレスバーから年月日を取得
	strNengapi = trim(request.querystring("nengapi"))

	'アドレスバーから年月日を取得出来ない場合はフォームから取得
	If strNengapi = empty Then
		strNengapi = trim(request.form("txtNengapi"))
	End If

	'年月日を取得できない場合はシステム日付を付与する
	If strNengapi = empty Then
		strNengapi = strSysDate
	End If

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("行先掲示＆スケジュール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'カレンダーマスタとスケジュール表からデータ取り出し
	Set rs = session("kintai").Execute(SELWSM0001_2(strShainCD,0,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	If rs.RecordCount = 0 Then
		errMSG = WSM0001E01
		'ヘッダー部分を表示する
		Call viewHeader()
		response.end
	End if

	'ヘッダー部分を表示する
	Call viewHeader()

	Call viewSide(rs,2,strShainCD,strNengetu)

	'レコードセットを開放
	Set rs = Nothing

	'行先表からデータを取り出し
	Set rs = session("kintai").Execute(SELWST0004(strShainCD))
	'画面項目の格納変数に代入
	Do until rs.EOF
		lblModifyDate = rs("UPDATEDATE") 
		cmbIkisaki = rs("IKISAKIKBN") 
		txtRenraku = rs("RENRAKU") 
		txtIkisakiCMT = rs("BIKOU") 
		rs.MoveNext
	Loop

	'レコードセットを開放
	Set rs = Nothing

	'画面の項目に値をセット
	Call setScreenValue()

	'行先掲示＆スケジュールを表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	entryIkisakiProc()
'PROCEDURE機能：行先の登録を行う
'引数：なし
'****************************************************
Sub entryIkisakiProc()

	Call getScreenValue()

	'行先表からデータを取り出し
	Set rs = session("kintai").Execute(SELWST0004(strShainCD))

	'データが存在していない場合はInsert
	If rs.RecordCount = 0 Then
		strSQL = INSWST0004(strShainCD,strBumonCD,cmbIkisaki,txtRenraku,txtIkisakiCMT,strSysDate)
	'データが存在している場合はUpdate
	else
		strSQL = UPDWST0004(strShainCD,strBumonCD,cmbIkisaki,txtRenraku,txtIkisakiCMT,strSysDate)
	End If

	'エラー制御開始
	On Error Resume Next

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans
	'***SQL実行
	session("kintai").Execute (strSQL)
	If Err <> 0 Then
		'ヘッダー設定
		errMSG = "行先" & WSE0000
		session("kintai").RollBackTrans
	End If
	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("行先掲示＆スケジュール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'日別就業表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,2,strShainCD,strNengetu)

	'レコードセットを開放
	Set rs = Nothing

	'記入日更新
	lblModifyDate = strSysDate

	'行先掲示＆スケジュールを表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：スケジュールの登録を行う
'引数：なし
'****************************************************
Sub insertProc()

	'画面の値を取得
	Call getScreenValue()

	strSTime = cmbSTimeH & ":" & cmbSTimeM
	strETime = cmbETimeH & ":" & cmbETimeM

'	'**********必須ﾁｪｯｸ*********
'	If inputCheck(strSTime,strETime,tgtSTime,tgtETime) = False Then
'		'エラー処理
'		Call errorHandler()
'		Exit Sub
'	End If

	'開始時間＝終了時間の場合はエラー
	If strSTime = strETime Then
		errMSG = WSE0005
		Call errorHandler()
		Exit Sub
	End If

	'重複ﾁｪｯｸ
	Set rs = session("kintai").Execute(SELWST0003(strShainCD,strNengapi))
	Do Until rs.EOF
		If (DateDiff("n",rs("STIME"),strSTime) >= 0 And  DateDiff("n",rs("ETIME"),strSTime) < 0) _
			Or (DateDiff("n",rs("STIME"),strETime) > 0 And  DateDiff("n",rs("ETIME"),strETime) <= 0) _
			Or (DateDiff("n",rs("STIME"),strSTime) <= 0 And  DateDiff("n",rs("ETIME"),strETime) >= 0) _
			Or (DateDiff("n",rs("ETIME"),strSTime) > 0 And  DateDiff("n",rs("ETIME"),strETime) > 0 And DateDiff("n",strSTime,strETime) < 0) Then
			'ヘッダー設定
			errMSG = WSE0006
			'レコードセットを開放
			Set rs = Nothing
			'エラー処理
			Call errorHandler()
			Exit Sub
		End If
		rs.MoveNext
	Loop

	'レコードセット開放
	Set rs = Nothing

	'スケジュールテーブルから最大SEQ取得
	Set rs = session("kintai").Execute(SELWST0003_1(strShainCD,strNengapi))

	'SEQ付与
	intSEQ = 1
	If isNull(rs("SEQ")) = False Then
		intSEQ = rs("SEQ") + 1
	End If

	'期間計算
	fltKikan = DateDiff("n",strSTime,strETime) / 60
	If fltKikan < 0 Then
		fltKikan = fltKikan + 24
	End If

	'レコードセット開放
	Set rs = Nothing

	intKoukaiFLG = 0
	If chkKoukai = "on" Then intKoukaiFLG = 1

	strSQL = INSWST0003(strShaincd,strNengapi,intSEQ,intKoukaiFLG,strSTime,strETime,fltKikan,cmbYotei,left(txtScheduleCMT,100) )

	'エラー制御開始
	On Error Resume Next

	'** トランザクション開始
'	session("kintai").CommitTrans
	session("kintai").BeginTrans
	'***SQL実行
	session("kintai").Execute (strSQL)
	If Err <> 0 Then
		'ヘッダー設定
		errMSG = strShainCD & WSE0007
		Call errorHandler()
		Exit Sub
	End If
	'** トランザクション終了
	session("kintai").CommitTrans

	'エラー制御終了
	On Error Goto 0

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("行先掲示＆スケジュール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'日別就業表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,2,strShainCD,strNengetu)

	'レコードセットを開放
	Set rs = Nothing

	'画面の項目に値をセット
	Call setScreenValue()

	'行先掲示＆スケジュールを表示
	Call viewDetail()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：スケジュールの更新を行う
'引数：なし
'****************************************************
Sub updateProc()

	'画面の値を取得
	Call getScreenValue()

	'ﾁｪｯｸフラグを初期化
	chkFlag = False

	For cnt = 1 To UBOUND(cmbSTimeHArr)
		'ﾁｪｯｸされている行のみ処理する
		If chkSelArr(cnt) = "on" Then
			'ﾁｪｯｸフラグをセット
			chkFlag = True
			strSTime = cmbSTimeHArr(cnt) & ":" & cmbSTimeMArr(cnt)
			strETime = cmbETimeHArr(cnt) & ":" & cmbETimeMArr(cnt)

			'**********必須ﾁｪｯｸ*********
			'開始時間＝終了時間の場合はエラー
			If strSTime = strETime Then
				errMSG = WSE0005
				Call errorHandler()
				Exit Sub
			End If

			'期間計算
			fltKikan = DateDiff("n",strSTime,strETime) / 60
			If fltKikan < 0 Then
				fltKikan = fltKikan + 24
			End If

			intKoukaiFLG = 0
			If chkKoukaiArr(cnt) = "on" Then intKoukaiFLG = 1

			strSQL = UPDWST0003(strShainCD,strNengapi,hdnSEQArr(cnt),intKoukaiFLG,strSTime,strETime,fltKikan,cmbYoteiArr(cnt),txtScheduleCMTArr(cnt))

			'エラー制御開始
		'	On Error Resume Next

			'** トランザクション開始
		'	session("kintai").CommitTrans
			session("kintai").BeginTrans
			'***SQL実行
			session("kintai").Execute (strSQL)
			If Err <> 0 Then
				'ヘッダー設定
				errMSG = strShainCD & WSE0007
			End If
			'** トランザクション終了
			session("kintai").CommitTrans

			'エラー制御終了
			On Error Goto 0
		End If
	Next

	If chkFlag = False Then
		'ヘッダー設定
		errMSG = WSE0008
		'レコードセットを開放
		Set rs = Nothing
		'エラー処理
		Call errorHandler()
		Exit Sub
	End If

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("行先掲示＆スケジュール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'日別就業表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,2,strShainCD,strNengetu)

	'レコードセットを開放
	Set rs = Nothing

	'画面の項目に値をセット
	Call setScreenValue()

	'行先掲示＆スケジュールを表示
	Call viewDetail()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	deleteProc()
'PROCEDURE機能：スケジュールの削除を行う
'引数：なし
'****************************************************
Sub deleteProc()

	'画面の値を取得
	Call getScreenValue()

	'ﾁｪｯｸフラグを初期化
	chkFlag = False

	For cnt = 1 To UBOUND(cmbSTimeHArr)
		'ﾁｪｯｸされている行のみ処理する
		If chkSelArr(cnt) = "on" Then
			'ﾁｪｯｸフラグをセット
			chkFlag = True

			strSQL = DELWST0003(strShainCD,strNengapi,hdnSEQArr(cnt))

			'エラー制御開始
		'	On Error Resume Next

			'** トランザクション開始
		'	session("kintai").CommitTrans
			session("kintai").BeginTrans
			'***SQL実行
			session("kintai").Execute (strSQL)
			If Err <> 0 Then
				'ヘッダー設定
				errMSG = strShainCD & WSE0007
			End If
			'** トランザクション終了
			session("kintai").CommitTrans

			'エラー制御終了
			On Error Goto 0
		End If
	Next

	If chkFlag = False Then
		'ヘッダー設定
		errMSG = WSE0008
		'レコードセットを開放
		Set rs = Nothing
		'エラー処理
		Call errorHandler()
		Exit Sub
	End If

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("行先掲示＆スケジュール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'日別就業表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,2,strShainCD,strNengetu)

	'レコードセットを開放
	Set rs = Nothing

	'画面の項目に値をセット
	Call setScreenValue()

	'行先掲示＆スケジュールを表示
	Call viewDetail()

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面から入力値取得
'引数：なし
'****************************************************
Sub getScreenValue()
	cnt									= request.form("hdnCnt")						'スケジュールのカウント数
	lblModifyDate 			= request.form("lblModifyDate")			'記入日
	cmbIkisaki 					= request.form("cmbIkisaki")				'行先
	txtRenraku 					= request.form("txtRenraku")				'連絡
	txtIkisakiCMT	 			= request.form("txtIkisakiCMT")			'コメント
	chkKoukai 					= request.form("chkKoukai")					'公開
	cmbSTimeH 					= request.form("cmbSTimeH")					'開始時
	cmbSTimeM 					= request.form("cmbSTimeM")					'開始分
	cmbETimeH 					= request.form("cmbETimeH")					'終了時
	cmbETimeM 					= request.form("cmbETimeM")					'終了分
	cmbYotei 						= request.form("cmbYotei")					'予定種別
	txtScheduleCMT 			= request.form("txtScheduleCMT")		'コメント
	ReDim cmbSTimeHArr(cnt)
	ReDim cmbSTimeMArr(cnt)
	ReDim cmbETimeHArr(cnt)
	ReDim cmbETimeMArr(cnt)
	ReDim cmbYoteiArr(cnt)
	ReDim txtScheduleCMTArr(cnt)
	ReDim chkKoukaiArr(cnt)
	ReDim hdnSEQArr(cnt)
	ReDim chkSelArr(cnt)
	ReDim lblKikanArr(cnt)

	For cnt = 1 To UBOUND(cmbSTimeHArr)
		chkSelArr(cnt) 					= request.form("chkSel" & cnt)						'選択
		chkKoukaiArr(cnt)				= request.form("chkKoukai" & cnt)					'公開
		cmbSTimeHArr(cnt) 			= request.form("cmbSTimeH" & cnt)					'開始時
		cmbSTimeMArr(cnt) 			= request.form("cmbSTimeM" & cnt)					'開始分
		cmbETimeHArr(cnt) 			= request.form("cmbETimeH" & cnt)					'終了時
		cmbETimeMArr(cnt) 			= request.form("cmbETimeM" & cnt)					'終了分
		lblKikanArr(cnt) 				= request.form("lblKikan" & cnt)					'期間
		cmbYoteiArr(cnt) 				= request.form("cmbYotei" & cnt)					'予定種別
		txtScheduleCMTArr(cnt) 	= request.form("txtScheduleCMT" & cnt)		'コメント
		hdnSEQArr(cnt)					= request.form("hdnSEQ" & cnt)						'SEQ
	Next
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue()
'PROCEDURE機能：画面の項目に値をセット
'引数：なし
'****************************************************
Sub setScreenValue()

	chkKoukai 					= Empty					'公開
	cmbSTimeH 					= Empty					'開始時
	cmbSTimeM 					= Empty					'開始分
	cmbETimeH 					= Empty					'終了時
	cmbETimeM 					= Empty					'終了分
	cmbYotei 						= Empty					'予定種別
	txtScheduleCMT 			= Empty		'コメント

	'スケジュール表からデータを取り出し
	Set rs = session("kintai").Execute(SELWST0003(strShainCD,strNengapi))
	'画面項目の格納変数に代入
	cnt = rs.RecordCount
	ReDim cmbSTimeHArr(cnt)
	ReDim cmbSTimeMArr(cnt)
	ReDim cmbETimeHArr(cnt)
	ReDim cmbETimeMArr(cnt)
	ReDim cmbYoteiArr(cnt)
	ReDim txtScheduleCMTArr(cnt)
	ReDim chkKoukaiArr(cnt)
	ReDim hdnSEQArr(cnt)
	ReDim chkSelArr(cnt)
	ReDim lblKikanArr(cnt)
	cnt = 1

	Do until rs.EOF

		chkKoukaiArr(cnt) = "off"
		If rs("KOUKAIFLG") = "1" Then chkKoukaiArr(cnt) = "on"							'公開
		cmbSTimeHArr(cnt) 			= Left(rs("STIME"),2) 			'開始時
		cmbSTimeMArr(cnt) 			= Right(rs("STIME"),2) 			'開始分
		cmbETimeHArr(cnt) 			= Left(rs("ETIME"),2) 			'終了時
		cmbETimeMArr(cnt) 			= Right(rs("ETIME"),2) 			'終了分
		lblKikanArr(cnt) 				= rs("KIKAN")								'期間
		cmbYoteiArr(cnt) 				= rs("YOTEIKBN")						'予定種別
		txtScheduleCMTArr(cnt) 	= rs("BIKOU")									'コメント
		hdnSEQArr(cnt) 					= rs("SEQ")									'SEQ隠し項目
		cnt = cnt + 1
		rs.MoveNext
	Loop

	'レコードセットを開放
	Set rs = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：ヘッダー部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<TABLE WIDTH="960" BORDER="0">
		<TBODY>
			<TR>
				<TD WIDTH="580" HEIGHT="23"><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
				</TD>
			</TR>
		</TBODY>
	</TABLE>
<%
End Sub
%>

<%
'****************************************************
'FUNCTION名：	inputCheck(pSTime,pETime,ptgtSTime,ptgtETime)
'FUNCTION機能：ヘッダー部分を表示する
'引数：あり
'				pSTime			--開始時間
'				pETime			--終了時間
'				ptgtSTime		--ターゲット開始時間
'				ptgtETime		--ターゲット終了時間
'****************************************************
FUNCTION inputCheck(pSTime,pETime,ptgtSTime,ptgtETime)
	inputCheck = False
	If strSTime = strETime Then
		errMSG = WSE0005
		inputCheck = True
	End If
End FUNCTION
%>

<%
'****************************************************
'PROCEDURE名：	errorHandler()
'PROCEDURE機能：エラー発生時の処理
'引数：なし
'****************************************************
Sub errorHandler()
	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'タイトルを表示する
	Call viewTitle("行先掲示＆スケジュール",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'ヘッダー部分を表示する
	Call viewHeader()

	'日別就業表からデータ取り出し
	Set rs = session("kintai").Execute(SELWST0001_1(strShainCD,strNengetu))

	'カレンダー部分、メッセージ部分を表示する
	Call viewSide(rs,2,strShainCD,strNengetu)

	'レコードセットを開放
	Set rs = Nothing

	'行先掲示＆スケジュールを表示
	Call viewDetail()

End Sub
%>


<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：行先掲示＆スケジュールを表示
'引数：なし
'****************************************************
Sub viewDetail()
%>

<!--**********************カレンダー入力表示(S)****************************-->

<TD WIDTH="*%" VALIGN="TOP">
<div class="list">

	<table class="l-tbl">
		<col width="10%">
		<col width="20%">
		<col width="10%">
		<col width="20%">
		<col width="10%">
		<col width="30%">
		<TR ALIGN="LEFT">
			<TD COLSPAN="3" ALIGN="LEFT">
				<FONT SIZE="3" COLOR="BLUE">行先掲示：</FONT></B>　
				<FONT SIZE="2">記入日　<% =toDate1(lblModifyDate) %>
				<INPUT TYPE="HIDDEN" NAME="lblModifyDate" VALUE="<% =lblModifyDate %>">
			</TD>
			<TD COLSPAN="3" ALIGN="RIGHT">
				<INPUT TYPE="BUTTON" VALUE="登録" onClick="_submit(0)" class="s-btn">
			</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">行先</TH>
			<TD class="l-cellodd">
				<SELECT NAME="cmbIkisaki">
					<% For cnt = 1 To UBound(strIkisakiArr)
						 	strFormat = Empty 
							If (cnt - cmbIkisaki) = 0 Then strFormat = "SELECTED" End If %>
					<OPTION VALUE="<%= cnt %>" <%= strFormat %>><%= strIkisakiArr(cnt)%></OPTION>") 
					<% Next %>
				</SELECT>
			</TD>
			<TH class="l-cellsec">連絡方法</TH>
			<TD COLSPAN="3" class="l-cellodd" width="60%">
				<INPUT TYPE="TEXT" SIZE="60" MAXLENGTH="60"  NAME="txtRenraku" VALUE="<% =txtRenraku %>" class="txt_imeon">
			</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">コメント</FONT></TH>
			<TD COLSPAN="5" class="l-cellodd">
				<TEXTAREA NAME="txtIkisakiCMT" ROWS="2" COLS="66" WRAP="SOFT"><% =txtIkisakiCMT %></TEXTAREA>
			</TD>
		</TR>
	</TABLE>
	<BR>
	<table class="l-tbl">
		<col width="10%">
		<col width="20%">
		<col width="10%">
		<col width="20%">
		<col width="10%">
		<col width="30%">
		<TR>
			<TD COLSPAN="6" ><span class="txt-gray1"><%=toDate1(strNengapi)%></span>
		</TR>
		<TR ALIGN="LEFT">
			<TD COLSPAN="2" ALIGN="LEFT"><FONT SIZE="3" COLOR="BLUE">スケジュール追加：</FONT></TD>
			<TD>
<!--
				<FONT SIZE="2">公開</FONT>
				<% strFormat = Empty 
						If chkKoukai = "on" Then strFormat = "CHECKED" End If %>
					<INPUT TYPE="CHECKBOX" NAME="chkKoukai" <% =strFormat %>>
-->
			</TD>
			<TD COLSPAN="3" ALIGN="RIGHT">
				<INPUT TYPE="BUTTON" VALUE="登録" onClick="_submit(1)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="更新" onClick="_submit(2)" class="s-btn">
				<INPUT TYPE="BUTTON" VALUE="削除" onClick="_submit(3)" class="s-btn">
			</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">開始時間</TH>
			<TD class="l-cellodd">
				<SELECT NAME="cmbSTimeH">
					<% For cnt = 0 To 23
						 	strFormat = Empty 
							If (cmbSTimeH - cnt) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt)%></OPTION>") 
					<% Next %>
				</SELECT>：
				<SELECT NAME="cmbSTimeM">
					<% For cnt = 0 To 45 step 15
						 	strFormat = Empty 
							If (cmbSTimeM - cnt) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt)%></OPTION>") 
					<% Next %>
					%>
				</SELECT>
			</TD>
			<TH class="l-cellsec">終了時間</TH>
			<TD class="l-cellodd">
				<SELECT NAME="cmbETimeH">
					<% For cnt = 0 To 23
						 	strFormat = Empty 
							If (cmbETimeH - cnt) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt)%></OPTION>") 
					<% Next %>
					%>
				</SELECT>：
				<SELECT NAME="cmbETimeM">
					<% For cnt = 0 To 45 step 15
						 	strFormat = Empty 
							If (cmbETimeM - cnt) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt)%></OPTION>") 
					<% Next %>
					%>
				</SELECT>
			</TD>
			<TH class="l-cellsec">予定種別</TH>
			<TD class="l-cellodd">
				<SELECT NAME="cmbYotei">
					<% For cnt = 1 To UBound(strYoteiArr)
						 	strFormat = Empty 
							If (cmbYotei - cnt) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION VALUE="<%= cnt %>" <%= strFormat %>><%= strYoteiArr(cnt)%></OPTION>") 
					<% Next %>
				</SELECT>
			</TD>
		</TR>
		<TR>
			<TH class="l-cellsec">コメント</TH>
			<TD COLSPAN="5" class="l-cellodd">
				<TEXTAREA NAME="txtScheduleCMT" ROWS="2" COLS="95" WRAP="SOFT" MAXLENGTH="200"><% =txtScheduleCMT %></TEXTAREA>
		</TR>
	</TABLE>

	<table class="l-tbl">
		<COL WIDTH="5%">
		<COL WIDTH="5%">
		<COL WIDTH="20%">
		<COL WIDTH="20%">
		<COL WIDTH="15%">
		<COL WIDTH="15%">
		<COL WIDTH="*%">
		<TR>
			<TH class="l-cellsec" ROWSPAN="2">選択</TH>
			<TH class="l-cellsec" ROWSPAN="2">No.</TH>
			<TH class="l-cellsec">開始時間</TH>
			<TH class="l-cellsec">終了時間</TH>
			<TH class="l-cellsec">予定期間</TH>
			<TH class="l-cellsec">予定種別</TH>
			<TH class="l-cellsec"></TH>
<!--
			<TH class="l-cellsec">公開</TH>
-->
		</TR>
		<TR>
			<TH class="l-cellsec" COLSPAN="5">コメント</TH>
		</TR>
	</TABLE>
  <div class="l-tbl-auto" style="height:258px">
	<table class="l-tbl">
		<COL WIDTH="5%">
		<COL WIDTH="5%">
		<COL WIDTH="20%">
		<COL WIDTH="20%">
		<COL WIDTH="15%">
		<COL WIDTH="15%">
		<COL WIDTH="*%">
		<% For cnt = 1 To UBound(cmbSTimeHArr) %>
		<TR>
			<TD ROWSPAN="2" class="l-cellodd">
				<% strFormat = Empty 
				If chkSelArr(cnt) = "on" Then strFormat = "CHECKED" End If %>
				<INPUT TYPE="CHECKBOX" NAME="chkSel<%= cnt %>" <% =strFormat %>>
			</TD>
			<TD ROWSPAN="2" class="l-cellodd">
				<% =cnt %>
				<!--**********************隠し項目：SEQ************************-->
				<INPUT TYPE="HIDDEN" NAME="hdnSEQ<%= cnt %>" VALUE="<% =hdnSEQArr(cnt) %>">
			</TD>
			<TD class="l-cellodd">
				<SELECT NAME="cmbSTimeH<%= cnt %>">
					<% For cnt1 = 0 To 23
						 	strFormat = Empty 
							If (cmbSTimeHArr(cnt) - cnt1) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt1)%></OPTION>") 
					<% Next %>
				</SELECT>：
				<SELECT NAME="cmbSTimeM<%= cnt %>">
					<% For cnt1 = 0 To 45 step 15
						 	strFormat = Empty 
							If (cmbSTimeMArr(cnt) - cnt1) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt1)%></OPTION>") 
					<% Next %>
				</SELECT>
			</TD>
			<TD class="l-cellodd">
				<SELECT NAME="cmbETimeH<%= cnt %>">
					<% For cnt1 = 0 To 23
						 	strFormat = Empty 
							If (cmbETimeHArr(cnt) - cnt1) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt1)%></OPTION>") 
					<% Next %>
				</SELECT>：
				<SELECT NAME="cmbETimeM<%= cnt %>">
					<% For cnt1 = 0 To 45 step 15
						 	strFormat = Empty 
							If (cmbETimeMArr(cnt) - cnt1) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION <%= strFormat %>><%= toString(cnt1)%></OPTION>") 
					<% Next %>
				</SELECT>
			</TD>
			<TD class="l-cellodd">
				<FONT SIZE="2"><% =lblKikanArr(cnt) %> H</FONT>
				<!--**********************隠し項目：期間************************-->
				<INPUT TYPE="HIDDEN" NAME="lblKikan<%= cnt %>" VALUE="<% =lblKikanArr(cnt) %>">
			</TD>
			<TD class="l-cellodd">
				<SELECT NAME="cmbYotei<%= cnt %>">
					<% For cnt1 = 1 To UBound(strYoteiArr)
						 	strFormat = Empty 
							If (cmbYoteiArr(cnt) - cnt1) = 0 Then strFormat = "SELECTED" End If %>
							<OPTION VALUE="<%= cnt1 %>" <%= strFormat %>><%= strYoteiArr(cnt1)%></OPTION>") 
					<% Next %>
				</SELECT>
			</TD>
			<TD class="l-cellodd">
<!--
				<% strFormat = Empty 
						If chkKoukaiArr(cnt) = "on" Then strFormat = "CHECKED" End If %>
				<INPUT TYPE="CHECKBOX" NAME="chkKoukai<%= cnt %>" <% =strFormat %>>
-->
			</TD>
		</TR>
		<TR>
			<TD COLSPAN="5" class="l-cellodd" width="90%">
				<TEXTAREA NAME="txtScheduleCMT<%= cnt %>" ROWS="2" COLS="90" WRAP="SOFT"><% =txtScheduleCMTArr(cnt) %></TEXTAREA>
			</TD>
		</TR>
		<% Next %>
	</TABLE>
  </DIV>
</div>
</TD>
<!--**********************カレンダー入力表示(E)****************************-->

		</TR>
	</TABLE>
<BR>
<!-----------------------隠し項目--------------------->
<INPUT TYPE="HIDDEN" NAME="hdnCnt" VALUE="<% =UBound(cmbSTimeHArr) %>">
<INPUT TYPE="HIDDEN" NAME="txtShainCD" VALUE="<% =strShainCD %>">
<INPUT TYPE="HIDDEN" NAME="txtBumonCD" VALUE="<% =strBumonCD %>">
<INPUT TYPE="HIDDEN" NAME="txtNengetu" VALUE="<% =strNengetu %>">
<INPUT TYPE="HIDDEN" NAME="txtNengapi" VALUE="<% =strNengapi %>">
</HTML>
<% 
End Sub
%>
