<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST009
'機能：費用精算の申請
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<link href="css/common.css" rel="stylesheet" type="text/css">
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>
<!--
<HTML onContextMenu="return false;">
-->
<HTML>
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
</HEAD>
<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式

	Dim txtSINSEIDATE(15)							'申請日
	Dim txtSINSEICD(15)								'申請種別　1：定期券・費用精算　2：費用精算　3:仮払い
	Dim txtNENGETU(15)								'年月
	Dim txtSEQ(15)									'行
	Dim txtROOT(15)									'路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他
	Dim txtEKIFROM(15)								'出発駅
	Dim txtEKITO(15)								'到着駅
	Dim txtHASSEIDATE(15)							'費用発生日
	Dim txtKOUTUUHI(15)								'費用精算（円）
	Dim txtSHUKUHAKUHI(15)							'宿泊費（円）
	Dim txtKOUSAIHI(15)								'交際費（円）
	Dim txtSONOTAHI(15)								'その他の費用（円）
	Dim txtIKISAKI(15)								'行先
	Dim txtUTIWAKE(15)								'使用目的・内訳・事由
	Dim txtBIKOU(15)								'備考
	Dim txtUPDATEDATE(15)							'更新日
	Dim txtSHOUNINSTATUS(15)						'承認状態 2:申請　3:承認　4:却下
	Dim txtSHOUNINSHACD(15)							'承認者コード
	Dim txtSHOUNINSHANM(15)							'承認者氏名
	Dim txtSHOUNINDATE(15)							'承認日
	
	Dim paramSinseidate

	'パラメーター取得
'	Call getParameter()
	'****パラメータ取得******
	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = Empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if
	
	paramSinseidate = trim(request.querystring("sinseidate"))
	If paramSinseidate = Empty then
		paramSinseidate = trim(request.form("hdnParamSinseidate"))
	end if

	'実行モードを取得
	strMode = trim(request.querystring("mode"))

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	If strNengetu = Empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'年月を取得できない場合はシステム日付を付与する
	If strNengetu = Empty then
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
	end if

	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが更新の場合
	else 
		'費用精算更新処理を行う
		Call updateProc()
	End If

	response.end
%>

<%
'****************************************************
'PROCEDURE名：	getParameter()
'PROCEDURE機能：アドレスバーor画面からパラメーター取得
'引数：なし
'****************************************************
Sub getParameter()

	'****パラメータ取得******
	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = Empty then
		strShainCD = trim(request.form("txtShainCD"))
	end if

	'実行モードを取得
	strMode = trim(request.querystring("mode"))

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	If strNengetu = Empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'年月を取得できない場合はシステム日付を付与する
	If strNengetu = Empty then
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
	end if
	
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle("費用の精算申請",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'費用精算・費用精算・仮払いデータ取り出し
	If paramSinseidate = Empty Then
		Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,replace(Date(),"/",""),strSINSEICD(2)))
	Else
		Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,paramSinseidate,strSINSEICD(2)))
	End If

	'レコード存在する場合
	If rs1.RecordCount > 0 Then

		'画面の項目に値をセット
		Call setScreenValue(rs1)

		'承認状態チェック
		If rs1("SHOUNINSTATUS") = "2" Or rs1("SHOUNINSTATUS") = "3" Then
			Set rs1 = Nothing
			If paramSinseidate = Empty Then
				Response.Redirect "WST0009_1.asp?shaincd=" & strShainCD & "&sinseidate=" & replace(Date(),"/","") & "&sinseicd=" & strSINSEICD(2)
			Else
				Response.Redirect "WST0009_1.asp?shaincd=" & strShainCD & "&sinseidate=" & paramSinseidate & "&sinseicd=" & strSINSEICD(2)
			End If
		End If

	End If

	'エラーメッセージ部分を表示する
	Call viewHeader()

	'詳細を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing
	Set rs1 = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(pRs1)
'PROCEDURE機能：画面の項目に値をセット
'引数：
'				pRs1-
'****************************************************
Sub setScreenValue(pRs1)

	cnt = 0
	pRs1.MoveFirst

	'画面項目の格納変数に代入
	For cnt = 1 To 15
'		 = pRs1("SHAINCD")							'社員コード
		txtSINSEIDATE(cnt) = pRs1("SINSEIDATE")						'申請日
		txtSINSEICD(cnt) = pRs1("SINSEICD")						'申請種別　1：定期券・費用精算　2：費用精算　3:仮払い
		txtSEQ(cnt) = pRs1("SEQ")								'行
		txtNENGETU(cnt) = pRs1("NENGETU")							'年月
		txtROOT(cnt) = pRs1("ROOT")							'路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他
		txtEKIFROM(cnt) = pRs1("EKIFROM")							'出発駅
		txtEKITO(cnt) = pRs1("EKITO")							'到着駅
		txtHASSEIDATE(cnt) = toDate5(pRs1("HASSEIDATE"))						'費用発生日
		if pRs1("KOUTUUHI") = 0 then 
			txtKOUTUUHI(cnt) = ""
		else
			txtKOUTUUHI(cnt) = pRs1("KOUTUUHI")						'費用精算（円）
		end if
		if pRs1("SHUKUHAKUHI") = 0 then 
			txtSHUKUHAKUHI(cnt) = ""
		else
			txtSHUKUHAKUHI(cnt) = pRs1("SHUKUHAKUHI")						'宿泊費（円）
		end if
		if pRs1("KOUSAIHI") = 0 then 
			txtKOUSAIHI(cnt) = ""
		else
			txtKOUSAIHI(cnt) = pRs1("KOUSAIHI")						'交際費（円）
		end if
		if pRs1("SONOTAHI") = 0 then 
			txtSONOTAHI(cnt) = ""
		else
			txtSONOTAHI(cnt) = pRs1("SONOTAHI")						'その他の費用（円）
		end if
		txtIKISAKI(cnt) = pRs1("IKISAKI")							'行先
		txtUTIWAKE(cnt) = pRs1("UTIWAKE")							'使用目的・内訳・事由
		txtBIKOU(cnt) = pRs1("BIKOU")							'備考
		txtUPDATEDATE(cnt) = pRs1("UPDATEDATE")						'更新日
		txtSHOUNINSTATUS(cnt) = pRs1("SHOUNINSTATUS")					'承認状態 2:申請　3:承認　4:却下
		txtSHOUNINSHACD(cnt) = pRs1("SHOUNINSHACD")					'承認者コード
		txtSHOUNINSHANM(cnt) = pRs1("SHOUNINSHANM")					'承認者氏名
		txtSHOUNINDATE(cnt) = pRs1("SHOUNINDATE")						'承認日
		if cnt < 15 then
			pRs1.MoveNext
		end if
	next

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setUpdateScreenValue(pRs1)
'PROCEDURE機能：更新の画面の項目に値をセット
'引数：
'				pRs1-
'****************************************************
Sub setUpdateScreenValue(pRs1)

	cnt = 0
	pRs1.MoveFirst

	'画面項目の格納変数に代入
	For cnt = 1 To 15
'		 = pRs1("SHAINCD")							'社員コード
		txtSINSEIDATE(cnt) = pRs1("SINSEIDATE")						'申請日
		txtSINSEICD(cnt) = pRs1("SINSEICD")						'申請種別　1：定期券・費用精算　2：費用精算　3:仮払い
		txtSEQ(cnt) = pRs1("SEQ")								'行
		txtNENGETU(cnt) = pRs1("NENGETU")							'年月
		txtUPDATEDATE(cnt) = pRs1("UPDATEDATE")						'更新日
		txtSHOUNINSTATUS(cnt) = pRs1("SHOUNINSTATUS")					'承認状態 2:申請　3:承認　4:却下
		txtSHOUNINSHACD(cnt) = pRs1("SHOUNINSHACD")					'承認者コード
		txtSHOUNINSHANM(cnt) = pRs1("SHOUNINSHANM")					'承認者氏名
		txtSHOUNINDATE(cnt) = pRs1("SHOUNINDATE")						'承認日
		if cnt < 15 then
			pRs1.MoveNext
		end if
	next

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()

	cnt = 1
	'画面項目を変数に代入
	For cnt = 1 To UBOUND(txtSINSEIDATE)
		txtSINSEIDATE(cnt) = trim(request.form("txtSINSEIDATE" & cnt))						'申請日
		txtSINSEICD(cnt) = trim(request.form("txtSINSEICD" & cnt))						'申請種別　1：定期券・費用精算　2：費用精算　3:仮払い
		txtSEQ(cnt) = cnt								'行
'		txtNENGETU(cnt) = trim(request.form("txtNENGETU" & cnt))							'年月
		txtROOT(cnt) = trim(request.form("txtROOT" & cnt))							'路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他
		txtEKIFROM(cnt) = trim(request.form("txtEKIFROM" & cnt))							'出発駅
		txtEKITO(cnt) = trim(request.form("txtEKITO" & cnt))							'到着駅
		txtHASSEIDATE(cnt) = trim(request.form("txtHASSEIDATE" & cnt))						'費用発生日
		txtKOUTUUHI(cnt) = trim(request.form("txtKOUTUUHI" & cnt))						'費用精算（円）
		txtSHUKUHAKUHI(cnt) = trim(request.form("txtSHUKUHAKUHI" & cnt))						'宿泊費（円）
		txtKOUSAIHI(cnt) = trim(request.form("txtKOUSAIHI" & cnt))						'交際費（円）
		txtSONOTAHI(cnt) = trim(request.form("txtSONOTAHI" & cnt))						'その他の費用（円）
		txtIKISAKI(cnt) = trim(request.form("txtIKISAKI" & cnt))							'行先
		txtUTIWAKE(cnt) = trim(request.form("txtUTIWAKE" & cnt))							'使用目的・内訳・事由
		txtBIKOU(cnt) = trim(request.form("txtBIKOU" & cnt))							'備考
		txtUPDATEDATE(cnt) = trim(request.form("txtUPDATEDATE" & cnt))						'更新日
		txtSHOUNINSTATUS(cnt) = trim(request.form("txtSHOUNINSTATUS" & cnt))					'承認状態 2:申請　3:承認　4:却下
		txtSHOUNINSHACD(cnt) = trim(request.form("txtSHOUNINSHACD" & cnt))					'承認者コード
		txtSHOUNINSHANM(cnt) = trim(request.form("txtSHOUNINSHANM" & cnt))					'承認者氏名
		txtSHOUNINDATE(cnt) = trim(request.form("txtSHOUNINDATE" & cnt))						'承認日
	Next
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データの追加を行う
'引数：なし
'****************************************************
Sub insertProc()

	'エラー制御開始
'	On Error Resume Next

	'費用精算・費用精算・仮払いデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,replace(Date(),"/",""),strSINSEICD(2)))

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'日別情報を作成する
	For cnt = 1 To UBOUND(txtSINSEIDATE)
		'***SQL実行
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
		session("kintai").Execute (INSWST0006(strShainCD,replace(Date(),"/",""),strSINSEICD(2),txtSEQ(cnt),strNengetu,txtROOT(cnt),txtEKIFROM(cnt),txtEKITO(cnt),replace(txtHASSEIDATE(cnt),"/",""),txtKOUTUUHI(cnt),txtSHUKUHAKUHI(cnt),txtKOUSAIHI(cnt),txtSONOTAHI(cnt),txtIKISAKI(cnt),txtUTIWAKE(cnt),txtBIKOU(cnt),replace(Date(),"/",""),"2","","",""))
		If Err <> 0 Then
			session("kintai").RollBackTrans
			errMSG = WST0001E1
			'エラーメッセージ部分を表示する
			Call viewHeader()
			'処理を終了する
			response.End
		End If
	Next

	'** トランザクション終了
	session("kintai").CommitTrans

	'レコードセット開放
	Set rs = Nothing

	'エラー制御終了
	On Error Goto 0

	'確認画面を表示
	Response.Redirect "WST0009_1.asp?shaincd=" & strShainCD & "&sinseidate=" & replace(Date(),"/","") & "&sinseicd=" & strSINSEICD(2)

response.end

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データの更新を行う
'引数：なし
'****************************************************
Sub updateProc()

	'画面の情報を取得する
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

	'費用精算・費用精算・仮払いデータ取り出し
	If paramSinseidate = Empty Then
		Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,replace(Date(),"/",""),strSINSEICD(2)))
	Else
		Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,paramSinseidate,strSINSEICD(2)))
	End If
	Set rs2 = Nothing

	'レコード存在する場合
	If rs1.RecordCount = 0 Then

		'メール送信を行う
		Call sendMail(getShainNM(strShainCD),strSINSEINM(2))

		'insertを行う
		Call insertProc()
	'却下の状態編集可 20140417 Start
	Else

		'画面の項目に値をセット
		Call setUpdateScreenValue(rs1)

		'** トランザクション開始
		session("kintai").BeginTrans
		
		For cnt = 1 To UBOUND(txtSINSEIDATE)
			'***SQL実行
			Set rs2 = session("kintai").Execute (SELWST0006_4(strShainCD,txtSINSEIDATE(cnt),txtSINSEICD(cnt),txtSEQ(cnt),txtNENGETU(cnt),txtUPDATEDATE(cnt),txtSHOUNINSTATUS(cnt),txtSHOUNINSHACD(cnt),txtSHOUNINSHANM(cnt),txtSHOUNINDATE(cnt)))
			
			If(rs2.RecordCount > 0) Then
				If paramSinseidate = Empty Then
					session("kintai").Execute (UPDWST0006_4(strShainCD,replace(Date(),"/",""),txtSINSEICD(cnt),txtSEQ(cnt),txtNENGETU(cnt),replace(Date(),"/",""),txtSHOUNINSTATUS(cnt),txtSHOUNINSHACD(cnt),txtSHOUNINSHANM(cnt),replace(txtSHOUNINDATE(cnt),"/",""),"","","",replace(txtHASSEIDATE(cnt),"/",""),"",txtBIKOU(cnt),"","",txtSONOTAHI(cnt),txtIKISAKI(cnt),txtUTIWAKE(cnt),replace(Date(),"/","")))
				Else
					session("kintai").Execute (UPDWST0006_4(strShainCD,paramSinseidate,txtSINSEICD(cnt),txtSEQ(cnt),txtNENGETU(cnt),replace(Date(),"/",""),txtSHOUNINSTATUS(cnt),txtSHOUNINSHACD(cnt),txtSHOUNINSHANM(cnt),replace(txtSHOUNINDATE(cnt),"/",""),"","","",replace(txtHASSEIDATE(cnt),"/",""),"",txtBIKOU(cnt),"","",txtSONOTAHI(cnt),txtIKISAKI(cnt),txtUTIWAKE(cnt),txtUPDATEDATE(cnt)))
				End If
			
			End If
			
			If Err <> 0 Then
				session("kintai").RollBackTrans
				errMSG = WST0001E1
				'エラーメッセージ部分を表示する
				Call viewHeader()
				'処理を終了する
				response.End
			End If
		Next

		'** トランザクション終了
		session("kintai").CommitTrans
		
		Set rs1 = Nothing
		Set rs2 = Nothing
		
		'却下の状態編集可 20140417 End
		
	End If

	'確認画面を表示
	If paramSinseidate = Empty Then
		Response.Redirect "WST0009_1.asp?shaincd=" & strShainCD & "&sinseidate=" & replace(Date(),"/","") & "&sinseicd=" & strSINSEICD(2)
	Else
		Response.Redirect "WST0009_1.asp?shaincd=" & strShainCD & "&sinseidate=" & paramSinseidate & "&sinseicd=" & strSINSEICD(2)
	End If


End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<TABLE WIDTH="960" BORDER="0">
		<TBODY>
			<TR>
				<TD WIDTH="660" HEIGHT="23"><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
				</TD>
				<TD> 
	<span class="txt-emphasis">
	申請日：
<%
if txtSINSEIDATE(1) ="" then
	response.write(todate1(replace(date(),"/","")))
else
	response.write(toDate1(txtSINSEIDATE(1)))
end if
%>
	</span>
<INPUT TYPE="BUTTON" VALUE="申請" onClick="SubmitWST0006(2)" class="s-btn">
				</TD>
			</TR>
		</TBODY>
	</TABLE>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：費用精算・費用精算・仮払いを表示
'引数：なし
'****************************************************
Sub viewDetail()
%>

<TD WIDTH="*%" VALIGN="TOP">
  <div class="list">
	<table class="l-tbl">
	<col width="10%">
	<col width="30%">
	<col width="20%">
	<col width="10%">
	<col width="30%">
	 <TR>
		<TH class="l-cellsec">日　付</TH>
		<TH class="l-cellsec">費用内訳</TH>
		<TH class="l-cellsec">プロジェクト名</TH>
		<TH class="l-cellsec">金　額（円）</TH>
		<TH class="l-cellsec">備　　考</TH>
	 </TR>
	</TABLE>
	<table class="l-tbl">
	<col width="10%">
	<col width="30%">
	<col width="20%">
	<col width="10%">
	<col width="30%">
<% For cnt = 1 To 15 %>
	 <TR>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imeoff" SIZE="13" MAXLENGTH="10" NAME="txtHASSEIDATE<%=(cnt)%>" onBlur="judgeNumber2(this)" VALUE="<%=txtHASSEIDATE(cnt)%>"></td>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imeon" SIZE="45" MAXLENGTH="45" NAME="txtUTIWAKE<%=(cnt)%>" VALUE="<%=txtUTIWAKE(cnt)%>"></td>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imeon" SIZE="20" MAXLENGTH="20" NAME="txtIKISAKI<%=(cnt)%>" VALUE="<%=txtIKISAKI(cnt)%>"></td>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imenum" SIZE="10" MAXLENGTH="10" NAME="txtSONOTAHI<%=(cnt)%>" onBlur="judgeNumber2(this)" VALUE="<%=txtSONOTAHI(cnt)%>"></td>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imeon" SIZE="45" MAXLENGTH="45" NAME="txtBIKOU<%=(cnt)%>" VALUE="<%=txtBIKOU(cnt)%>"></td>
	 </TR>
<% Next %>
	</table> 
</CENTER>

<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtShainCD" 	VALUE="<%=strShainCD%>">
<INPUT TYPE="HIDDEN" NAME="txtBumonCD" 	VALUE="<%=strBumonCD%>">
<INPUT TYPE="HIDDEN" NAME="txtNengetu" 	VALUE="<%=strNengetu%>">
<INPUT TYPE="HIDDEN" NAME="hdnBackURL" 	VALUE="<%=hdnBackURL%>">
<INPUT TYPE="HIDDEN" NAME="hdnParamSinseidate" 	VALUE="<%=paramSinseidate%>">
<INPUT TYPE="HIDDEN" NAME="mode" 	VALUE="update">
</CENTER>
</FORM>
</BODY>
</HTML>


<%
End Sub
%>

