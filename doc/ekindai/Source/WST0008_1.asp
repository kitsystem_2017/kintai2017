<%
'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
'▲GFN（株）承諾なくの改ざんは硬く禁じます。 ▲
'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
'ファイル名称：WST008
'機能：交通費の申請
'*********変更履歴*********
'日付			変更者		変更管理番号	理由

%>
<link href="css/common.css" rel="stylesheet" type="text/css">
<!--#include file="include/Const.asp"-->
<!--#include file="include/Utility.asp"-->
<!--#include file="include/DBSpec.asp"-->
<!--#include file="include/SQL.asp"-->
<!--#include file="include/WebContent.asp"-->
<%
	'セッション情報をチェックする
	Call checkSession()

	'DB接続
	Call DBConnect()
%>
<!--
<HTML onContextMenu="return false;">
-->
<HTML>
<TITLE>WORKSHEET</TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
<link href="css/common.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="include/script.js">
</SCRIPT>
<SCRIPT LANGUAGE='JavaScript'>
<!---
function _submit(type){
	document.bottom.action="WST0008_1.asp?mode=" + type;
	document.bottom.submit();
	return;
}
//--->
</SCRIPT>
</HEAD>


<body id="doc">
<FORM NAME="bottom" METHOD="POST">

<%
	'変数定義
	Dim rs,rs1									'レコードセット
	Dim strShainCD							'社員ｺｰﾄﾞ
	Dim strShainNM							'社員氏名
	Dim strSQL									'SQL文
	Dim cnt,cnt1								'カウント
	Dim strFormat								'書式
	Dim hdnBackURL


	Dim txtSINSEIDATE(15)							'申請日
	Dim txtSINSEICD(15)								'申請種別　1：定期券・交通費　2：費用精算　3:仮払い
	Dim txtNENGETU(15)								'年月
	Dim txtSEQ(15)									'行
	Dim txtROOT(15)									'路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他
	Dim txtEKIFROM(15)								'出発駅
	Dim txtEKITO(15)								'到着駅
	Dim txtHASSEIDATE(15)							'費用発生日
	Dim txtKOUTUUHI(15)								'交通費（円）
	Dim txtSHUKUHAKUHI(15)							'宿泊費（円）
	Dim txtKOUSAIHI(15)								'交際費（円）
	Dim txtSONOTAHI(15)								'その他の費用（円）
	Dim txtIKISAKI(15)								'行先
	Dim txtUTIWAKE(15)								'使用目的・内訳・事由
	Dim txtBIKOU(15)								'備考
	Dim txtUPDATEDATE(15)							'更新日
	Dim txtSHOUNINSTATUS(15)						'承認状態 2:申請　3:承認　4:却下
	Dim txtSHOUNINSHACD(15)							'承認者コード
	Dim txtSHOUNINSHANM(15)							'承認者氏名
	Dim txtSHOUNINDATE(15)							'承認日

	Dim txtSHOUNINCMT(15)							'承認コメント
	
	Dim strSinseidate								'申請日
	Dim strSinseisbt									'申請種別
	Dim strKengen
	Dim strShounishacd
	
	Dim totalKOUTUUHI
	Dim totalSHUKUHAKUHI
	Dim totalKOUSAIHI
	Dim totalSONOTAHI
	
	'却下された場合に申請者へ送信 20140421 Start
	Dim sinseishaMAILADDR							'申請者メールアドレス
	Dim shouninshaMAILADDR							'承認者メールアドレス
	'却下された場合に申請者へ送信 20140421 End

	'****パラメータ取得******
	'アドレスバーから社員番号を取得
	strShainCD = trim(request.querystring("shaincd"))
	'アドレスバーから社員番号を取得出来ない場合はフォームから取得
	If strShainCD = Empty then
	'	strShainCD = trim(request.form("txtShainCD"))
		strShainCD = trim(request.form("txtSinSeiShainCD"))
	end if

	'アドレスバーから承認者を取得
	strShounishacd = trim(request.querystring("shounishacd"))
	If strShounishacd = Empty then
		strShounishacd = trim(request.form("shounishacd"))
	end if

	'アドレスバーから申請日を取得
	strSinseidate = trim(request.querystring("sinseidate"))
	If strSinseidate = Empty then
		strSinseidate = trim(request.form("sinseidate"))
	end if

	'アドレスバーから申請種別を取得
	strSinseisbt = trim(request.querystring("sinseicd"))

	'実行モードを取得
	strMode = trim(request.querystring("mode"))

	'アドレスバーから年月を取得
	strNengetu = trim(request.querystring("nengetu"))
	'アドレスバーから年月を取得出来ない場合はフォームから取得
	If strNengetu = Empty then
		strNengetu = trim(request.form("txtNengetu"))
	end if

	'年月を取得できない場合はシステム日付を付与する
	If strNengetu = Empty then
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
	end if
	
	'ダウンロード機能追加 20140414 Start
	if strMode = "1" then
		Call downloadProc()
		response.end
	end if
	'ダウンロード機能追加 20140414 End

	'実行モードが空の場合
	If strMode = Empty Then
		'初期表示を行う
		Call initProc()
	'実行モードが更新の場合
	else 
		'交通費更新処理を行う
		Call updateProc()
	End If

	response.end
%>


<%
'****************************************************
'PROCEDURE名：	initProc()
'PROCEDURE機能：画面の初期表示を行う
'引数：なし
'****************************************************
Sub initProc()

	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'ヘッダを表示する
	Call viewTitle("定期券の変更届出及び交通費の精算申請",strBumonCD,strBumonNM,strShainCD,strShainNM)

	'交通費・費用精算・仮払いデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,strSinseidate,strSinseisbt))

	'画面の項目に値をセット
	setScreenValue(rs1)
	
	'エラーメッセージ部分を表示する
	Call viewHeader()

	'詳細を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs = Nothing
	Set rs1 = Nothing

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	setScreenValue(pRs1)
'PROCEDURE機能：画面の項目に値をセット
'引数：
'				pRs1-
'****************************************************
Sub setScreenValue(pRs1)

	cnt = 0
	pRs1.MoveFirst

	totalKOUTUUHI = 0
	totalSHUKUHAKUHI = 0
	totalKOUSAIHI = 0
	totalSONOTAHI = 0


	'画面項目の格納変数に代入
	For cnt = 1 To 15
'		 = pRs1("SHAINCD")							'社員コード
		txtSINSEIDATE(cnt) = pRs1("SINSEIDATE")						'申請日
		txtSINSEICD(cnt) = pRs1("SINSEICD")						'申請種別　1：定期券・交通費　2：費用精算　3:仮払い
		txtSEQ(cnt) = pRs1("SEQ")								'行
		txtNENGETU(cnt) = pRs1("NENGETU")							'年月
		txtROOT(cnt) = pRs1("ROOT")							'路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他
		txtEKIFROM(cnt) = pRs1("EKIFROM")							'出発駅
		txtEKITO(cnt) = pRs1("EKITO")							'到着駅
		txtHASSEIDATE(cnt) = toDate5(pRs1("HASSEIDATE"))						'費用発生日
		if pRs1("KOUTUUHI") = 0 then 
			txtKOUTUUHI(cnt) = ""
		else
			txtKOUTUUHI(cnt) = pRs1("KOUTUUHI")						'交通費（円）
			totalKOUTUUHI = totalKOUTUUHI + txtKOUTUUHI(cnt)
		end if
		if pRs1("SHUKUHAKUHI") = 0 then 
			txtSHUKUHAKUHI(cnt) = ""
		else
			txtSHUKUHAKUHI(cnt) = pRs1("SHUKUHAKUHI")						'宿泊費（円）
			totalSHUKUHAKUHI = totalSHUKUHAKUHI + txtSHUKUHAKUHI(cnt)
		end if
		if pRs1("KOUSAIHI") = 0 then 
			txtKOUSAIHI(cnt) = ""
		else
			txtKOUSAIHI(cnt) = pRs1("KOUSAIHI")						'交際費（円）
			totalKOUSAIHI = totalKOUSAIHI + txtKOUSAIHI(cnt)
		end if
		if pRs1("SONOTAHI") = 0 then 
			txtSONOTAHI(cnt) = ""
		else
			txtSONOTAHI(cnt) = pRs1("SONOTAHI")						'その他の費用（円）
			totalSONOTAHI = totalSONOTAHI + txtSONOTAHI(cnt)
		end if
		txtIKISAKI(cnt) = pRs1("IKISAKI")							'行先
		txtUTIWAKE(cnt) = pRs1("UTIWAKE")							'使用目的・内訳・事由
		txtBIKOU(cnt) = pRs1("BIKOU")							'備考
		txtUPDATEDATE(cnt) = pRs1("UPDATEDATE")						'更新日
		txtSHOUNINSTATUS(cnt) = pRs1("SHOUNINSTATUS")					'承認状態 2:申請　3:承認　4:却下
		txtSHOUNINSHACD(cnt) = pRs1("SHOUNINSHACD")					'承認者コード
		txtSHOUNINSHANM(cnt) = pRs1("SHOUNINSHANM")					'承認者氏名
		txtSHOUNINDATE(cnt) = pRs1("SHOUNINDATE")						'承認日

		txtSHOUNINCMT(cnt) = pRs1("SHOUNINCMT")						'承認コメント

		if cnt < 15 then
			pRs1.MoveNext
		end if
	next

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	getScreenValue()
'PROCEDURE機能：画面の項目に値を取得
'引数：なし
'****************************************************
Sub getScreenValue()

	cnt = 1
	'画面項目を変数に代入
	For cnt = 1 To UBOUND(txtSINSEIDATE)
		txtSINSEIDATE(cnt) = trim(request.form("txtSINSEIDATE" & cnt))						'申請日
		txtSINSEICD(cnt) = trim(request.form("txtSINSEICD" & cnt))						'申請種別　1：定期券・交通費　2：費用精算　3:仮払い
		txtSEQ(cnt) = cnt								'行
'		txtNENGETU(cnt) = trim(request.form("txtNENGETU" & cnt))							'年月
		txtROOT(cnt) = trim(request.form("txtROOT" & cnt))							'路線　1:JR　2:地下鉄　3:私鉄　4:タクシー　5:バス　6:その他
		txtEKIFROM(cnt) = trim(request.form("txtEKIFROM" & cnt))							'出発駅
		txtEKITO(cnt) = trim(request.form("txtEKITO" & cnt))							'到着駅
		txtHASSEIDATE(cnt) = trim(request.form("txtHASSEIDATE" & cnt))						'費用発生日
		txtKOUTUUHI(cnt) = trim(request.form("txtKOUTUUHI" & cnt))						'交通費（円）
		txtSHUKUHAKUHI(cnt) = trim(request.form("txtSHUKUHAKUHI" & cnt))						'宿泊費（円）
		txtKOUSAIHI(cnt) = trim(request.form("txtKOUSAIHI" & cnt))						'交際費（円）
		txtSONOTAHI(cnt) = trim(request.form("txtSONOTAHI" & cnt))						'その他の費用（円）
		txtIKISAKI(cnt) = trim(request.form("txtIKISAKI" & cnt))							'行先
		txtUTIWAKE(cnt) = trim(request.form("txtUTIWAKE" & cnt))							'使用目的・内訳・事由
		txtBIKOU(cnt) = trim(request.form("txtBIKOU" & cnt))							'備考
		txtUPDATEDATE(cnt) = trim(request.form("txtUPDATEDATE" & cnt))						'更新日
		txtSHOUNINSTATUS(cnt) = trim(request.form("txtSHOUNINSTATUS" & cnt))					'承認状態 2:申請　3:承認　4:却下
		txtSHOUNINSHACD(cnt) = trim(request.form("txtSHOUNINSHACD" & cnt))					'承認者コード
		txtSHOUNINSHANM(cnt) = trim(request.form("txtSHOUNINSHANM" & cnt))					'承認者氏名
		txtSHOUNINDATE(cnt) = trim(request.form("txtSHOUNINDATE" & cnt))						'承認日
		txtSHOUNINCMT(cnt) = trim(request.form("txtSHOUNINCMT" & cnt))						'承認コメント

	Next
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	insertProc()
'PROCEDURE機能：データの追加を行う
'引数：なし
'****************************************************
Sub insertProc()

	'エラー制御開始
'	On Error Resume Next

	'交通費・費用精算・仮払いデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,replace(Date(),"/",""),strSINSEICD(1)))

	'** トランザクション開始
'	session("kintai").RollBackTrans
	session("kintai").BeginTrans

	'日別情報を作成する
	For cnt = 1 To UBOUND(txtSINSEIDATE)
		'***SQL実行
		strNengetu = Mid(Date,1,4) & Mid(Date,6,2)
		session("kintai").Execute (INSWST0006(strShainCD,replace(Date(),"/",""),strSINSEICD(1),txtSEQ(cnt),strNengetu,txtROOT(cnt),txtEKIFROM(cnt),txtEKITO(cnt),replace(txtHASSEIDATE(cnt),"/",""),txtKOUTUUHI(cnt),txtSHUKUHAKUHI(cnt),txtKOUSAIHI(cnt),txtSONOTAHI(cnt),txtIKISAKI(cnt),txtUTIWAKE(cnt),txtBIKOU(cnt),replace(Date(),"/",""),"2","","",""))
		If Err <> 0 Then
			session("kintai").RollBackTrans
			errMSG = WST0001E1
			'エラーメッセージ部分を表示する
			Call viewHeader()
			'処理を終了する
			response.End
		End If
	Next

	'** トランザクション終了
	session("kintai").CommitTrans

	'レコードセット開放
	Set rs = Nothing

	'エラー制御終了
	On Error Goto 0
response.end

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	updateProc()
'PROCEDURE機能：データの更新を行う
'引数：なし
'****************************************************
Sub updateProc()
	'画面の情報を取得する
	Call getScreenValue()

	'エラー制御開始
'	On Error Resume Next

'	'交通費・費用精算・仮払いデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,replace(Date(),"/",""),strSINSEICD(1)))

'	'レコード存在する場合
'	If rs1.RecordCount = 0 Then

'		'insertを行う
'		Call insertProc()
		
'	else
		'updateを行う
		'交通費・費用精算・仮払いデータ取り出し
		Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,strSinseidate,strSINSEICD(1)))
		'** トランザクション開始
'		session("kintai").RollBackTrans
		session("kintai").BeginTrans

		'日別情報を作成する

		session("kintai").Execute (UPDWST0006(rs1("SHAINCD"),strSinseidate,rs1("SINSEICD"),strMode,strShounishacd,getShainNM(strShounishacd),replace(Date(),"/",""),txtSHOUNINCMT(1)))
		
		If Err <> 0 Then
			session("kintai").RollBackTrans
			errMSG = WST0001E1
			'エラーメッセージ部分を表示する
			Call viewHeader()
			'処理を終了する
			response.End
		End If

		'** トランザクション終了
		session("kintai").CommitTrans

		'レコードセット開放
		Set rs = Nothing
		
		'却下された場合に申請者へ送信 20140421 Start
		If strMode = "4" Then

		    '申請者のメールアドレスを取得
		    Set rs = session("kintai").Execute (SELM0001_3(rs1("SHAINCD")))
		    sinseishaMAILADDR = rs("MAILADDR")
		
			'承認者のメールアドレスを取得
			Set rs = Nothing
			Set rs = session("kintai").Execute (SELM0001_3(strShounishacd))
			shouninshaMAILADDR = rs("MAILADDR")
		
			'メール送信を行う
			Call sendMailKyaka(getShainNM(strShainCD),strSINSEINM(1),sinseishaMAILADDR,shouninshaMAILADDR)
			
			'レコードセット開放
			Set rs = Nothing
		End If
		'却下された場合に申請者へ送信 20140421 End
		
		hdnBackURL = Session("backurl")
		Response.Redirect hdnBackURL

		'エラー制御終了
		On Error Goto 0
'	End If

End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewHeader()
'PROCEDURE機能：エラーメッセージ部分を表示する
'引数：なし
'****************************************************
Sub viewHeader()
%>
	<CENTER>
	<TABLE WIDTH="960" BORDER="0">
		<TBODY>
			<TR>
<%
if txtSHOUNINSTATUS(1) = "2" then
	errMSG = "精算申請中"
elseif txtSHOUNINSTATUS(1) = "3" then
	errMSG = "承認済み"
elseif txtSHOUNINSTATUS(1) = "4" then
	errMSG = "却下されました"
end if
%>
				<TD WIDTH="420" HEIGHT="23"><FONT COLOR="RED" SIZE="2"><B>　<%=errMSG%><B></FONT></TD>
				</TD>
				<TD> 
	<span class="txt-emphasis">
	申請日：
<%
if txtSINSEIDATE(1) ="" then
	response.write(todate1(replace(date(),"/","")))
else
	response.write(toDate1(txtSINSEIDATE(1)))
end if
%>
	</span>
<%
	'アドレスバーから権限を取得
	strKengen1 = trim(request.querystring("kengen"))

if strKengen1 = "4" then 
%>
<INPUT TYPE="BUTTON" VALUE="承認" onClick="SubmitWST0006_1(1,3)" class="s-btnsyounin">
<INPUT TYPE="BUTTON" VALUE="却下" onClick="SubmitWST0006_1(1,4)" class="s-btnsyounin">
<!-- <INPUT TYPE="BUTTON" VALUE="ダウンロード" onClick="_submit(1)" class="s-btndownload"> -->
<INPUT TYPE="BUTTON" VALUE="印刷" onClick="window.print();" class="s-btnsyounin">
<%
end if
	hdnBackURL = Session("backurl")
	if mid(hdnBackURL,1,7) = "WST0011" or mid(hdnBackURL,1,7) = "WST0012"  then
		strFormat = ""
	else
		strFormat = "DISABLED"
	end if
%>
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="_back('<%=hdnBackURL%>')"  <%=strFormat%> class="s-btnsyounin">

				</TD>
			</TR>
		</TBODY>
	</TABLE>
<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	viewDetail()
'PROCEDURE機能：交通費・費用精算・仮払いを表示
'引数：なし
'****************************************************
Sub viewDetail()
%>

  <div class="list">
	<table class="l-tbl">
	<col width="15%">
	<col width="*%">
	 <TR>
		<TH class="l-cellsec">承認者のコメント</TH>
<%
if strShounishacd = "" then 
%>
		<td class="l-cellodd"><%=txtSHOUNINCMT(1)%></td>
<%
else
%>
		<td class="l-cellodd"><INPUT TYPE="TEXT" class="txt_imeon" SIZE="150" MAXLENGTH="100" NAME="txtSHOUNINCMT1" VALUE="<%=txtSHOUNINCMT(1)%>"></td>
<%
end if
%>

	 </TR>
	 </table>

 	<span class="txt-blue1">
		<u>
	定期券変更
		</u>
	</span>
	<table class="l-tbl">
	<col width="10%">
	<col width="15%">
	<col width="15%">
	<col width="10%">
	<col width="13%">
	<col width="30%">
	 <TR>
		<TH class="l-cellsec">路　線</TH>
		<TH class="l-cellsec">出　発　駅</TH>
		<TH class="l-cellsec">到　着　駅</TH>
		<TH class="l-cellsec">金　額（円）</TH>
		<TH class="l-cellsec">変　更　日　付</TH>
		<TH class="l-cellsec">変　更　事　由</TH>
	 </TR>
	</TABLE>
	
	<table class="l-tbl">
	<col width="10%">
	<col width="15%">
	<col width="15%">
	<col width="10%">
	<col width="13%">
	<col width="30%">
	 <TR>
		<td class="l-cellodd"><%=txtROOT(1)%></td>
		<td class="l-cellodd"><%=txtEKIFROM(1)%></td>
		<td class="l-cellodd"><%=txtEKITO(1)%></td>
		<td class="l-cellodd"><%=formatKin(txtKOUTUUHI(1))%></td>
		<td class="l-cellodd"><%=txtHASSEIDATE(1)%></td>
		<td class="l-cellodd"><%=txtBIKOU(1)%></td>
	 </TR>
	</table> 
	<br>
	 	<span class="txt-blue1">
		<u>
	交通費精算
		</u>
	</span>
	<table class="l-tbl">
	<col width="10%">
	<col width="15%">
	<col width="15%">
	<col width="10%">
	<col width="13%">
	<col width="30%">
	 <TR>
		<TH class="l-cellsec">路　線</TH>
		<TH class="l-cellsec">出　発　駅</TH>
		<TH class="l-cellsec">到　着　駅</TH>
		<TH class="l-cellsec">金　額（円）</TH>
		<TH class="l-cellsec">日　付</TH>
		<TH class="l-cellsec">備　　考</TH>
	 </TR>
	</TABLE>
	<table class="l-tbl">
	<col width="10%">
	<col width="15%">
	<col width="15%">
	<col width="10%">
	<col width="13%">
	<col width="30%">
<% For cnt = 2 To 15 %>
	 <TR>
		<td class="l-cellodd"><%=txtROOT(cnt)%></td>
		<td class="l-cellodd"><%=txtEKIFROM(cnt)%></td>
		<td class="l-cellodd"><%=txtEKITO(cnt)%></td>
		<td class="l-cellodd"><%=formatKin(txtKOUTUUHI(cnt))%></td>
		<td class="l-cellodd"><%=txtHASSEIDATE(cnt)%></td>
		<td class="l-cellodd"><%=txtBIKOU(cnt)%></td>
	 </TR>
<% Next %>
	 <TR>
		<TH class="l-cellsec" colspan = "3">合計</TH>
		<td class="l-cellodd"><%=formatKin(totalKOUTUUHI)%></td>
		<td class="l-cellodd"></td>
		<td class="l-cellodd"></td>
	 </TR>
	</table> 
</CENTER>

<!-----------------------HIDDEN項目：--------------------->
<INPUT TYPE="HIDDEN" NAME="txtSinSeiShainCD" 	VALUE="<%=strShainCD%>">
<INPUT TYPE="HIDDEN" NAME="txtBumonCD" 	VALUE="<%=strBumonCD%>">
<INPUT TYPE="HIDDEN" NAME="txtNengetu" 	VALUE="<%=strNengetu%>">
<INPUT TYPE="HIDDEN" NAME="hdnBackURL" 	VALUE="<%=hdnBackURL%>">
<INPUT TYPE="HIDDEN" NAME="shounishacd" 	VALUE="<%=strShounishacd%>">
<INPUT TYPE="HIDDEN" NAME="sinseidate" 	VALUE="<%=strSinseidate%>">
<INPUT TYPE="HIDDEN" NAME="strSinseisbt" 	VALUE="<%=strSinseisbt%>">
</CENTER>
</FORM>
</BODY>
</HTML>


<%
End Sub
%>

<%
'****************************************************
'PROCEDURE名：	downloadProc()
'PROCEDURE機能：ダウンロードを行う
'引数：なし
'****************************************************
Sub downloadProc()

	'申請種別
	strSinseisbt = trim(request.form("strSinseisbt"))
	
	'社員情報取得
	Call getShainInfo(strShainCD,strShainNM,strBumonCD,strBumonNM,strKengen)

	'交通費・費用精算・仮払いデータ取り出し
	Set rs1 = session("kintai").Execute(SELWST0006_1(strShainCD,strSinseidate,strSinseisbt))
'	Response.Buffer = True
'	Response.Charset="Shift-JIS"
'	Response.ContentType = "application/vnd.ms-excel"
'	Response.ContentType = "application/x-download"

	Response.ContentType = "application/octet-stream"
'	Response.AddHeader "Content-Disposition","attachment inline;filename=" & strNendo & "年" & strMonth & "交通費.xls"
	Response.AddHeader "Content-Disposition","Attachment;filename=" & strShainNM & "_" & strNengetu & "_交通費.xls"

	'画面の項目に値をセット
	setScreenValue(rs1)

	'詳細を表示
	Call viewDetail()

	'レコードセットを開放
	Set rs1 = Nothing

	Response.Flush
	Response.end


End Sub
%>

