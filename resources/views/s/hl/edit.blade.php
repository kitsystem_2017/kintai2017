<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('s/hl')}}"  method="POST">
        <input type="hidden" name="userid" value="{{$userid}}">
        <input type="hidden" name="niki" value="{{$nengetu}}">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-condensed table-responsive" style="margin-bottom:0px;">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}費用精算</span><!--タイトル-->

                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:center;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="btn-group pull-right" >
                                                    <!--button name="yes-button" value="1"  class="btn btn-sm btn-warning">   <a href="/s/hl/sn{{$Ymd_Y}}{{$Ymd_M}}">承認</a></button>-->
                                                    <!--<button  class="btn btn-sm btn-warning"><a href="/s/hl/kk{{$Ymd_Y}}{{$Ymd_M}}">却下</a></button>-->
                                                     <button name ="button-sn" id="button-sn" 　data-toggle="modal"  onclick="javaScript:confirm('承認しても宜しいですか？')"  value="sn" class="btn btn-sm btn-warning" >承認</button> 
                                                     <button name ="button-kk" id="button-kk"  data-toggle="modal"  onclick="javaScript:confirm('却下しても宜しいですか？')"  value="kk" class="btn btn-sm btn-warning" >却下</button> 
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="border: solid #FF8000;">
                                    <thead>
                                    <td>承認者のコメント</td>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><textarea cols="125" name="confirm1">{{$confirm1}}</textarea></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;">
                                    <thead>
                                    <tr>
                                        <th width="15%"  style="text-align:center; background: #EEEEEE;">日付</th>
                                        <th width="15%"  style="text-align:center; background: #EEEEEE;">行先</th>
                                        <th width="20%"  style="text-align:center; background: #EEEEEE;">内容</th>
                                        <th width="10%"  style="text-align:center; background: #EEEEEE;">交通費（円）</th>
                                        <th width="10%"  style="text-align:center; background: #EEEEEE;">宿泊費（円）</th>
                                        <th width="10%"  style="text-align:center; background: #EEEEEE;">その他（円）</th>
                                        <th width="20%"  style="text-align:center; background: #EEEEEE;">備考</th>
                                    </tr>
                                    </thead></table>
                                <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:270px;border: solid #FF8000;border-top:0;"><!--スクロール-->
                                    <table id = "tableH" class="table table-striped table-bordered table-condensed table-responsive">
                                        <tbody>
                                        @foreach( $day_list as $l )
                                            <tr>
                                                <td width="15%"><fieldset>
                                                        {{ $l->SINSEIDATE }}
                                                    </fieldset></td>
                                                <td width="15%"><fieldset>
                                                        {{ $l->IKISAKI }}
                                                    </fieldset></td>
                                                <td width="20%"><fieldset>
                                                        {{ $l->UTIWAKE }}
                                                    </fieldset></td>
                                                <td width="10%"><fieldset>
                                                        {{ $l->KOUTUUHI }}
                                                    </fieldset></td>
                                                <td width="10%"><fieldset>
                                                        {{ $l->SHUKUHAKUHI }}
                                                    </fieldset></td>
                                                <td width="10%"><fieldset>
                                                        {{ $l->SONOTAHI }}
                                                    </fieldset></td>
                                                <td width="20%"><fieldset>
                                                        {{ $l->BIKOU }}
                                                    </fieldset></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>


                                <table class="table table-striped table-bordered table-condensed table-responsive" style="border: 3px solid #FF8000;border-top:0;">
                                    <thead>
                                    <tr>
                                        <th width="15%" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                                        <th width="15%" style ="background: #EEEEEE;">-</th>
                                        <th width="20%" style ="background: #EEEEEE;">-</th>
                                        <th width="10%" style ="background: #EEEEEE;">{{$sum_koutuuhi}}</th><!--金額（円）-->
                                        <th width="10%" style ="background: #EEEEEE;">{{$sum_shukuhakuhi}}</th><!--金額（円）-->
                                        <th width="10%" style ="background: #EEEEEE;">{{$sum_sonotahi}}</th><!--金額（円）-->
                                        <th width="20%" style ="background: #EEEEEE;">-</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                                <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                                <br>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div></div>

    </form>




@endsection