<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('s/hg')}}"  method="POST">
        <input type="hidden" name="userid" value="{{$userid}}">
        <input type="hidden" name="niki" value="{{$nengetu}}">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-condensed table-responsive" style="margin-bottom:0px;">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}費用精算</span><!--タイトル-->

                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:center;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="btn-group pull-right" >
                                                   <!--button  name="yes-button" value="1" class="btn btn-sm btn-warning">   <a href="/s/hg/sn{{$Ymd_Y}}{{$Ymd_M}}">承認</a></button>-->
                                                   <!--<button  class="btn btn-sm btn-warning"><a href="/s/hg/kk{{$Ymd_Y}}{{$Ymd_M}}">却下</a></button>-->
                                                   
                                                    <button name ="button-sn" onclick="return shonin()"  data-toggle = "modal" value="sn" class="btn btn-sm btn-warning" >承認</button>

                                                    <button name ="button-kk" type = "submit" 　data-toggle="modal" onclick="return kyakka()"  value="kk" class="btn btn-sm btn-warning" >却下</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="border: solid #FF8000;">
                                    <thead>
                                    <td>承認者のコメント</td>
                                    </thead>
                                    <tbody>
                                    <tr>
                    </div>
                                        <td><textarea cols="125" name="confirm1">{{$confirm1}}</textarea></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;"><!--勤務表入力項目-->
                                    <thead>
                                    <tr>
                                        <th width="25%"  style="text-align:center; background: #EEEEEE;">日付</th>
                                        <th width="35%" style="text-align:center; background: #EEEEEE;">費用内訳</th>
                                        <th width="15%" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                        <th width="25%" style="text-align:center; background: #EEEEEE;">備考</th>
                                    </tr>
                                    </thead></table>
                                <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:300px;border: solid #FF8000;border-top:0"><!--スクロール-->
                                    <table class="table table-striped table-bordered table-condensed table-responsive">
                                        <tbody>
                                        @foreach( $day_list as $l )
                                            <tr>
                                                <td width="25%"><fieldset>
                                                        {{ $l->SINSEIDATE }}
                                                    </fieldset></td>
                                                <td width="35%"><fieldset>
                                                        {{ $l->UTIWAKE }}
                                                    </fieldset></td>
                                                <td width="15%"><fieldset>
                                                        {{ $l->SONOTAHI }}
                                                    </fieldset></td>
                                                <td width="25%"><fieldset>
                                                        {{ $l->BIKOU }}
                                                    </fieldset></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <table class="table table-striped table-bordered table-condensed table-responsive" style="border: 3px solid #FF8000;border-top:0;">
                                    <thead>
                                    <tr>
                                        <th width="25%" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                                        <th width="35%" style ="background: #EEEEEE;">-</th>
                                        <th width="15%" style ="background: #EEEEEE;">{{$sum_sonotahi}}</th><!--金額（円）-->
                                        <th width="25%" style ="background: #EEEEEE;">-</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>

                            <div class="col-md-2">
                                <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                                <br>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div></div>
<script>
			   function shonin() //承認
		   {
			   
			   rct = confirm('承認しても宜しいですか？')
			   if(rct == true)
			   {
				   
			   }
			   else
			   {
				   return false;
			   }
			
		   }
		   
		     function kyakka()　//却下
		   {
			   
			   rct = confirm('却下しても宜しいですか？')
			   if(rct == true)
			   {
				   
			   }
			   else
			   {
				   return false;
			   }
			
		   }
		   
			</script>
    </form>




@endsection