<!-- 勤務決定画面　申請あり　項目変更できない　disabled -->
@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-condensed table-responsive" style="margin-bottom:0px;" >
                                <thead>

                                <tr>
                                    <th>
                                        <span style="font-size:25px; align:left; margin-right: 480px">費用承認</span>
                                    </th>
                                    <th><!--年度-->
                                        <div class="btn-group pull-right" >
                                            <span style="font-size:15px">年度:</span>  
                                             <select id="nendo" class="form-canvas" style="width :60px; height :30px">
                                                <option value="">--</option>
                                                <option value="{{$kotoshi}}" @if($kotoshi == $nendo){{'selected'}}@endif>{{$kotoshi}}</option>
                                                <option value="{{$kotoshi-1}}" @if($kotoshi-1 == $nendo){{'selected'}}@endif>{{$kotoshi-1}}</option>
                                                <option value="{{$kotoshi-2}}" @if($kotoshi-2 == $nendo){{'selected'}}@endif>{{$kotoshi-2}}</option>
                                                <option value="{{$kotoshi-3}}" @if($kotoshi-3 == $nendo){{'selected'}}@endif>{{$kotoshi-3}}</option>
                                                <option value="{{$kotoshi-4}}" @if($kotoshi-4 == $nendo){{'selected'}}@endif>{{$kotoshi-4}}</option>
                                            </select>
                                            <span style="font-size:15px">月度:</span>
                                            <select id="getudo"  class="form-canvas" style="width :60px; height :30px">
                                                <option value="">--</option>
                                                <option value="01" @if('01' == $getudo){{'selected'}}@endif>1月</option>
                                                <option value="02" @if('02' == $getudo){{'selected'}}@endif>2月</option>
                                                <option value="03" @if('03' == $getudo){{'selected'}}@endif>3月</option>
                                                <option value="04" @if('04' == $getudo){{'selected'}}@endif>4月</option>
                                                <option value="05" @if('05' == $getudo){{'selected'}}@endif>5月</option>
                                                <option value="06" @if('06' == $getudo){{'selected'}}@endif>6月</option>
                                                <option value="07" @if('07' == $getudo){{'selected'}}@endif>7月</option>
                                                <option value="08" @if('08' == $getudo){{'selected'}}@endif>8月</option>
                                                <option value="09" @if('09' == $getudo){{'selected'}}@endif>9月</option>
                                                <option value="10" @if('10' == $getudo){{'selected'}}@endif>10月</option>
                                                <option value="11" @if('11' == $getudo){{'selected'}}@endif>11月</option>
                                                <option value="12" @if('12' == $getudo){{'selected'}}@endif>12月</option>
                                            </select>
                                        </div>
                                    </th>
                                    <th>
                                        <button id="getYearMonth" class="btn btn-xs btn-warning" style="width: 80px;height :30px">表示</button>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table  class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px; border: solid #FF8000;"><!--勤務表入力項目-->
                                <thead>
                                <tr>
                                    <th width="75">年月</th>
                                    <th width="100">申請日</th>
                                    <th width="75">申請者</th>
                                    <th width="75">費用類別</th>
                                    <th width="75">費用総額</th>
                                    <th width="75">承認状況</th>
                                    <th width="75">承認者</th>
                                    <th width="75">承認日</th>
                                </tr>
                                </thead>
                                @if(count($arrhlist)>0)
                                    @foreach( $arrhlist as $key=>$value )
                                        <tr>
                                            @if ($value["SINSEICD"]=="1")
                                                <td width="75"><span><a href="/s/hk/{{substr($value["SHINSEIM"],0,4)."-".substr($value["SHINSEIM"],4,2)."+".$value["SHAINCD"]}}/edit">{{$value["SHINSEIM"]}}</a></span></td>
                                            @endif
                                            @if ($value["SINSEICD"]=="2")
                                                <td width="75"><span><a href="/s/hg/{{substr($value["SHINSEIM"],0,4)."-".substr($value["SHINSEIM"],4,2)."+".$value["SHAINCD"]}}/edit">{{$value["SHINSEIM"]}}</a></span></td>
                                            @endif
                                            @if ($value["SINSEICD"]=="3")
                                                <td width="75"><span><a href="/s/hl/{{substr($value["SHINSEIM"],0,4)."-".substr($value["SHINSEIM"],4,2)."+".$value["SHAINCD"]}}/edit">{{$value["SHINSEIM"]}}</a></span></td>
                                            @endif
                                            <td width="100"><span>{{$value["SHINSEIDATE"]}}</span></td>
                                                <td width="75"><span>{{$value["NAME"]}}</span>
                                                @if($value["SINSEICD"]=="1")
                                                    <td width="85"><span>定期券、交通費</span></td>
                                                @endif
                                                @if($value["SINSEICD"]=="2")
                                                    <td width="85"><span>精算費用</span></td>
                                                @endif
                                                @if($value["SINSEICD"]=="3")
                                                    <td width="85"><span>仮支払</span></td>
                                                @endif
                                                <td width="75"><span>{{$value["HIYOUST"]}}</span></td>
                                            <td width="75"><span ><font color="red">{{$value["SHOUNINSTATUS"]}}</font></span></td>
                                                <td width="75"><span>{{$value["SHOUNINSHANM"]}}</span></td>
                                                <td width="75"><span>{{$value["SHOUNINDATE"]}}</span></td>
                                        </tr>
                                    @endforeach
                                @endif

                            </table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example"><!--スクロール-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
    <script>
        $(document).ready(function(){
            $('#getYearMonth').unbind();
            $('#getYearMonth').bind('click', function(e) {
                e.preventDefault();
                var nendo=document.getElementById("nendo").value;
                var getudo=document.getElementById("getudo").value;
                window.location.pathname = '/s/sh/'+nendo+ getudo;
            });
        });
    </script>
@endsection