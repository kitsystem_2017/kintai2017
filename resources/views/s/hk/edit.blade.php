<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('s/hk')}}"  method="POST">
        <input type="hidden" name="userid" value="{{$userid}}">
        <input type="hidden" name="niki" value="{{$nengetu}}">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-condensed table-responsive" style="margin-bottom:0px;">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}費用精算</span><!--タイトル-->

                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:center;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="btn-group pull-right" >
                                                    <!--<button  name="yes-button" value="1"  class="btn btn-sm btn-warning">   <a href="/s/hk/sn{{$Ymd_Y}}{{$Ymd_M}}">承認</a></button>
                                                    <button  class="btn btn-sm btn-warning"><a href="/s/hk/kk{{$Ymd_Y}}{{$Ymd_M}}">却下</a></button>-->
													<button name ="button-sn" id="button-sn"　 data-toggle="modal" onclick="javaScript:confirm('承認しても宜しいですか？');" value="sn" class="btn btn-sm btn-warning" >承認</button>
                                                    <button name ="button-kk" id="button-kk" 　data-toggle="modal" onclick="javaScript:confirm('却下しても宜しいですか？');"  value="kk" class="btn btn-sm btn-warning" >却下</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="border: solid #FF8000;">
                                    <thead>
                                    <td>承認者のコメント</td>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><textarea cols="125" name="confirm1">{{$confirm1}}</textarea></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div  class="text-center">
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <div class="col-md-10">
                                    <table class="table table-striped table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;">
                                        <thead>
                                        <tr>
                                            <!-- update takahashi 1997/8/3 colspan=7 → 6 -->
                                            <th class="h3" colspan="6">定期券変更</th>
                                        </tr>
                                        <tr>
                                            <th width="120" style="text-align:center; background: #EEEEEE;">路線</th>
                                            <th width="120" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                            <th width="120" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                            <th width="160" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                            <th width="120" style="text-align:center; background: #EEEEEE;">変更日付</th>
                                            <th width="*" style="text-align:center; background: #EEEEEE;">変更事由</th>
                                        </tr>
                                        </thead></table>
                                    <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:120px;border: solid #FF8000;border-top:0;"><!--スクロール-->
                                        <table id = "tableK" class="table table-striped table-bordered table-condensed table-responsive">
                                            <tbody>
                                            @foreach( $Teki_list as $T )
                                                <tr>
                                                    <td width="120">
                                                        @if('01'== $T->ROOT){{'JR'}}
                                                        @elseif('02'== $T->ROOT){{'地下鉄'}}
                                                        @elseif('03'== $T->ROOT){{'私鉄'}}
                                                        @elseif('04'== $T->ROOT){{'タクシー'}}
                                                        @elseif('05'== $T->ROOT){{'バス'}}
                                                        @elseif('06'== $T->ROOT){{'その他'}}
                                                        @endif
                                                    </td>
                                                    <td width="120"><fieldset>
                                                            {{ $T->EKIFROM }}
                                                        </fieldset></td>
                                                    <td width="120"><fieldset>
                                                            {{ $T->EKITO }}
                                                        </fieldset></td>
                                                    <td width="160"><fieldset>
                                                            {{ $T->KOUTUUHI }}
                                                        </fieldset></td>
                                                    <td width="120"><fieldset>
                                                            {{ $T->SINSEIDATE }}
                                                        </fieldset></td>
                                                    <td width="*" style="text-align:left;"><fieldset>
                                                            {{ $T->BIKOU }}
                                                        </fieldset></td>
                                                    <input type="hidden" name="idK[]" value="{{$T->id}}">
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-header text-center">
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <div class="col-md-10">

                                    <table class="table table-striped table-condensed table-responsive" style="margin-bottom:0px; border: solid #FF8000;">
                                        <thead>
                                        <tr>
                                            <!-- update takahashi 1997/8/3 colspan=7 → 6 -->
                                            <th class="h3" colspan="6">交通費</th>
                                        </tr>
                                        <tr>
                                            　　<th width="120" style="text-align:center; background: #EEEEEE;">路線</th>
                                            　 <th width="120" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                            　  <th width="120" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                            　  <th width="160" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                            　 <th width="120" style="text-align:center; background: #EEEEEE;">日付</th>
                                            　 <th width="*"  style="text-align:center; background: #EEEEEE;">備考</th>
                                        </tr>
                                        </thead></table>
                                    <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="border: solid #FF8000;border-top:0;"><!--スクロール-->
                                        <table id = "tableH" class="table table-striped table-bordered table-condensed table-responsive">
                                            <tbody>
                                            @foreach( $day_list as $l )
                                                <td width="120">
                                                    @if('01'== $l->ROOT){{'JR'}}
                                                    @elseif('02'== $l->ROOT){{'地下鉄'}}
                                                    @elseif('03'== $l->ROOT){{'私鉄'}}
                                                    @elseif('04'== $l->ROOT){{'タクシー'}}
                                                    @elseif('05'== $l->ROOT){{'バス'}}
                                                    @elseif('06'== $l->ROOT){{'その他'}}
                                                    @endif
                                                </td>
                                                <td width="120"><fieldset>
                                                        {{ $l->EKIFROM }}
                                                    </fieldset></td>
                                                <td width="120"><fieldset>
                                                        {{ $l->EKITO }}
                                                    </fieldset></td>
                                                <td width="160"><fieldset>
                                                        {{ $l->KOUTUUHI }}
                                                    </fieldset></td>
                                                <td width="120"><fieldset>
                                                        {{ $l->SINSEIDATE }}
                                                    </fieldset></td>
                                                <td width="*" style="text-align:left;"><fieldset>
                                                        {{ $l->BIKOU }}
                                                    </fieldset></td>
                                                <input type="hidden" name="id[]" value="{{$l->id}}">
                                                </tr>
                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>


                                    <table class="table table-striped table-bordered table-condensed table-responsive" style="border: 3px solid #FF8000;">
                                        <thead>
                                        <tr>
                                            <th width="120" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                                            <th width="120" style ="background: #EEEEEE;">-</th>
                                            <th width="120" style ="background: #EEEEEE;">-</th>
                                            <th width="160" style ="background: #EEEEEE;">{{$sum_koutuuhi}}</th><!--金額（円）-->
                                            <th width="120" style ="background: #EEEEEE;">-</th>
                                            <th width="*" style ="background: #EEEEEE;">-</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div></div>

    </form>




@endsection