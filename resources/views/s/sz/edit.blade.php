﻿<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('s/sz')}}"  method="POST">
        <div class="container">
            <input type="hidden" name="niki" value="{{$Ymd_niki}}">
            <input type="hidden" name="userid" value="{{$userid}}">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-condensed table-responsive" style="margin-bottom:0px;">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}月度週報</span><!--タイトル-->

                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:center;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="btn-group pull-right" >
                                                    <!--<button name="yes-button" value="1" class="btn btn-sm btn-warning">   <a href="/s/sz/sn{{$Ymd_Y}}{{$Ymd_M}}{{$Ymd_d}}">承認</a></button>
                                                    <button  class="btn btn-sm btn-warning"><a href="/s/sz/kk{{$Ymd_Y}}{{$Ymd_M}}{{$Ymd_d}}">却下</a></button>
													-->
                                              <!--<button name ="button-sn" id="button-sn"　 data-toggle="modal" onclick="javaScript:confirm('承認しても宜しいですか？');" value="sn" class="btn btn-sm btn-warning" >承認</button>
                                                    <button name ="button-kk" id="button-kk" 　data-toggle="modal" onclick="javaScript:confirm('却下しても宜しいですか？')"  value="kk" class="btn btn-sm btn-warning" >却下</button> -->
                                                    <button name ="button-sn" onclick="return shonin()"  data-toggle = "modal" value="sn" class="btn btn-sm btn-warning" >承認</button>
                                                    <button name ="button-kk" type = "submit" 　data-toggle="modal" onclick="return kyakka()"  value="kk" class="btn btn-sm btn-warning" >却下</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;">
                                    <thead>
                                    <th width="200">報告先メールアドレス：</th>
                                    <th width="150">TO:
                                        <select class="form-canvas" name="supervisor" disabled = "true" style="width: 110px; height:30px">
                                            <option value="01" @if('01' == $supervisor1){{'selected'}}@endif>{{$arr_supervisor[0]}}</option>
                                            <option value="02" @if('02' == $supervisor1){{'selected'}}@endif>{{$arr_supervisor[1]}}</option>
                                        </select>
                                    </th>
                                    <th>CC:
                                        <select class="form-canvas" name="assistsupervisor" disabled = "true" style="width: 110px; height:30px">
                                            <option value="01" @if('01' == $assistsupervisor1){{'selected'}}@endif>{{$arr_supervisor[0]}}</option>
                                            <option value="02" @if('02' == $assistsupervisor1){{'selected'}}@endif>{{$arr_supervisor[1]}}</option>
                                        </select>
                                    </th>
                                    </thead>
                                </table>
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;border-top:0;">
                                    <thead>
                                    <td width="80" align="center">日付</td>
                                    <td>作業内容</td>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td align="center" width="80">{{substr($date1,5)}}<br>(月)</td>
                                        <input type="hidden" name="date1" value="{{$date1}}">
                                        <td>{{$naiyo1}}</td>
                                        <input type="hidden" name="id1" value="{{$id1}}">
                                    </tr>
                                    <tr>
                                        <td align="center" width="80">{{substr($date2,5)}}<br>(火)</td>
                                        <input type="hidden" name="date2" value="{{$date2}}">
                                        <td>{{$naiyo2}}</td>
                                        <input type="hidden" name="id2" value="{{$id2}}">
                                    </tr>
                                    <tr>
                                        <td align="center" width="80">{{substr($date3,5)}}<br>(水)</td>
                                        <input type="hidden" name="date3" value="{{$date3}}">
                                        <td>{{$naiyo3}}</td>
                                        <input type="hidden" name="id3" value="{{$id3}}">
                                    </tr>
                                    <tr>
                                        <td align="center" width="80">{{substr($date4,5)}}<br>(木)</td>
                                        <input type="hidden" name="date4" value="{{$date4}}">
                                        <td>{{$naiyo4}}</td>
                                        <input type="hidden" name="id4" value="{{$id4}}">
                                    </tr>
                                    <tr>
                                        <td align="center">{{substr($date5,5)}}<br>(金)</td>
                                        <input type="hidden" name="date5" value="{{$date5}}">
                                        <td>{{$naiyo5}}</td>
                                        <input type="hidden" name="id5" value="{{$id5}}">
                                    </tr>
                                    <tr>
                                        <td align="center">{{substr($date6,5)}}<br>(土)</td>
                                        <input type="hidden" name="date6" value="{{$date6}}">
                                        <td>{{$naiyo6}}</td>
                                        <input type="hidden" name="id6" value="{{$id6}}">
                                    </tr>
                                    <tr>
                                        <td align="center">{{substr($date7,5)}}<br>(日)</td>
                                        <input type="hidden" name="date7" value="{{$date7}}">
                                        <td>{{$naiyo7}}</td>
                                        <input type="hidden" name="id7" value="{{$id7}}">
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;border-top:0;">
                                    <thead>
                                    <td>週間総括：（1000文字以内）</td>
                                    </thead>
                                    <tbody>
                                    <tr>
                                            <!-- update 2017/7/28 takahashi 140 -> 119 -->
                                        <td><textarea cols="125" disabled = "true"  style="height :200px">{{$report1}}</textarea></td>
                                        <input type="hidden" name="idR" value="{{$idR}}">
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;border-top:0;">
                                    <thead>
                                    <td>確認者のコメント：（1000文字以内）</td>
                                    </thead>
                                    <tbody>
                                    <tr>
                                       <!-- update 2017/7/28 takahashi cols 140 -> 119 -->
                                        <td><textarea cols="125" name="confirm1">{{$confirm1}}</textarea></td>
                                        <input type="hidden" name="idC" value="{{$idC}}">
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div></div> 
			<script>
			   function shonin() //承認
		   {
			   
			   rct = confirm('承認しても宜しいですか？')
			   if(rct == true)
			   {
				   
			   }
			   else
			   {
				   return false;
			   }
			
		   }
		   
		     function kyakka()　//却下
		   {
			   
			   rct = confirm('却下しても宜しいですか？')
			   if(rct == true)
			   {
				   
			   }
			   else
			   {
				   return false;
			   }
			
		   }
		   
			</script>
    </form>




@endsection