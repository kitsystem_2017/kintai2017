@extends('app')

@section('content')
    <?php
        if(session_status()==1){
            session_start();
        }
        $user = Auth::user();
        if($user){
            $userinfo = $_SESSION["userinfo"];
            $permissions = is_null($userinfo) ? null : $userinfo["pid"];
        }else{
            $permissions = null;
        }
    ?>

    <div class="text-center">
        <div class="container-fluid">
            <div class="row-fixed">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-3">
                </div>
            </div>
        </div>
    </div>
    <br/>

    <div class="text-center">
        <div class="container-fluid">
            <div class="row-fixed">
                <div class="text-center">
                    <div class="container-fluid">
                        <div class="row-fixed">

                            <div class="col-sm-1">
                                <!--<div id="datepicker"></div>-->
                            </div>

                <!-- /.col-sm-4 -->

                @if(!is_null($permissions))
                <div class="col-sm-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4 class="panel-title">勤務管理</h4>
                        </div>
                        <div class="panel-body" style="height:200px;">
                            <table width="100%">
                                <td  style="text-align:center"><a href="/k/km"><img class="home_img" alt="" src="{{ asset('img/1.png') }}"></a>
                                    <br>
                                    <font size="2">勤務表入力</font>
                                </td>
                                <td style="text-align:center">
                                    <a href="/k/kr/show"><img class="home_img" alt="" src="{{ asset('img/2.png') }}"></a>
                                    <br>
                                    <font size="2">勤務表一覧</font>
                                </td>
                                <tr>
                                    <td>
                                    <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                        <br>-->

                                    </td>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
                <!-- /.col-sm-4 -->

                @if(!is_null($permissions))
                <div class="col-sm-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="text-align:center">費用申請</h4>
                        </div>
                        <div class="panel-body" style="height:200px;">
                            <table width="100%">

                                <td style="text-align:center"><a href="/h/hk"><img class="home_img" alt=""
                                                                                             src="{{ asset('img/3.png') }}"></a>
                                    <br>
                                    <font size="2">定期券交通費申請</font>
                                </td>
                                <td style="text-align:center">
                                    <a href="/h/hg"><img class="home_img" alt="" src="{{ asset('img/4.png') }}"></a>
                                    <br>
                                    <font size="2">費用精算申請</font>
                                </td>

                                <tr>
                                    <td style="text-align:center"><a href="/h/hl"><img class="home_img" alt=""
                                                                                       src="{{ asset('img/5.png') }}"></a>
                                        <br>
                                        <font size="2">仮払い申請</font>
                                    </td>
                                    <td style="text-align:center">
                                        <a href="/h/hr/show"><img class="home_img" alt=""
                                                                  src="{{ asset('img/6.png') }}"></a>
                                        <br>
                                        <font size="2">各種精算申請一覧</font>
                                    </td>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
                <!-- /.col-sm-4 -->

                @if(!is_null($permissions))
                <div class="col-sm-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="text-align:center">週間報告</h4>
                        </div>
                        <div class="panel-body" style="height:200px;">
                            <table width="100%">
                                <td style="text-align:center"><a href="/z/zb"><img class="home_img" alt=""
                                                                                             src="{{ asset('img/7.png') }}"></a>
                                    <br>
                                    <font size="2">週間報告</font>
                                </td>
                                <td style="text-align:center">
                                    <a href="/z/zr/show"><img class="home_img" alt=""
                                                                          src="{{ asset('img/8.png') }}"></a>
                                    <br>
                                    <font size="2">週間報告一覧</font>
                                </td>
                                <tr>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
                <!-- /.col-sm-4 -->

            </div>
        </div>
    </div>
    <br/>
    <div class="text-center">
        <div class="container-fluid">
            <div class="row-fixed">

                <div class="col-sm-1">
                    <!--<div id="datepicker"></div>-->
                </div>

                @if($permissions==1||$permissions==2)
                <div class="col-sm-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="text-align:center">各種承認</h4>
                        </div>
                        <div class="panel-body" style="height:200px;">
                            <table width="100%">
                                <td  style="text-align:center"><a href="/s/sk"><img class="home_img" alt=""
                                                                                         src="{{ asset('img/9.png') }}"></a>
                                    <br>
                                    <font size="2">勤務表承認</font>
                                </td>
                                <td style="text-align:center">
                                    <a href="/s/sz"><img class="home_img" alt="" src="{{ asset('img/10.png') }}"></a>
                                    <br>
                                    <font size="2">週間報告確認</font>
                                </td>
                                <tr>
                                    <td style="text-align:center"><a href="/s/sh"><img class="home_img" alt=""
                                                                                       src="{{ asset('img/11.png') }}"></a>
                                        <br>
                                        <font size="2">費用承認</font>
                                    </td>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                            </table>
                        </div>
                    </div>
                </div>
                @elseif($permissions==3)
                    <div class="col-sm-3">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h4 class="panel-title" style="text-align:center">その他</h4>
                            </div>
                            <div class="panel-body" style="height:200px;">
                                <table>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>

                                    <tr>
                                        <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                        </td>
                                        < <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                        </td>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
                <!-- /.col-sm-4 -->

                @if($permissions==1)
                <div class="col-sm-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="text-align:center">情報設定</h4>
                        </div>
                        <div class="panel-body" style="height:200px;">
                            <table width="100%">
                                <td  style="text-align:center"><a href="/j/me"><img class="home_img" alt=""
                                                                                         src="{{ asset('img/12.png') }}"></a>
                                    <br>
                                    <font size="2">社員情報設定</font>
                                </td>
                                <td style="text-align:center">
                                    <a href="/j/bu"><img class="home_img" alt="" src="{{ asset('img/13.png') }}"></a>
                                    <br>
                                    <font size="2">部門情報設定</font>
                                </td>
                                <tr>
                                    <td style="text-align:center"><a href="/j/ka"><img class="home_img" alt="" src="{{ asset('img/13.png') }}"></a>
                                        <br>
                                        カレンダー設定
                                    </td>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- /.col-sm-4 -->

                <div class="col-sm-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="text-align:center">その他</h4>
                        </div>
                        <div class="panel-body" style="height:200px;">
                            <table>
                                <td>
                                    <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                        <br>-->
                                </td>
                                <td>
                                    <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                        <br>-->
                                </td>
                                <tr>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                                    <td>
                                        <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                            <br>-->
                                    </td>
                            </table>
                        </div>
                    </div>
                </div>
                    @elseif($permissions==2||$permissions==3)
                        <div class="col-sm-3">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title" style="text-align:center">その他</h4>
                                </div>
                                <div class="panel-body" style="height:200px;">
                                    <table>
                                        <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                        </td>
                                        <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                        </td>
                                        <tr>
                                            <td>
                                                <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                    <br>-->
                                            <td>
                                                <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                    <br>-->
                                            </td>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title" style="text-align:center">その他</h4>
                                </div>
                                <div class="panel-body" style="height:200px;">
                                    <table>
                                        <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                        </td>
                                        <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                        </td>
                                        <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                            </td>
                                        <td>
                                            <!--<td style="text-align:center"><a href="#"><img class="home_img" alt="" src=""></a>
                                                <br>-->
                                            </td>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
            </div>
        </div>
    </div>
@endsection