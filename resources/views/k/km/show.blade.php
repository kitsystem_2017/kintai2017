<!-- 勤務決定画面　申請あり　項目変更できない　disabled -->
@extends('app')

@section('content')
    <form action="{{URL('k/kr')}}" method="POST">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-condensed table-responsive" style="margin-bottom:0px;">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}月度勤務表</span><!--タイトル-->

                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:center;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="btn-group pull-right" >
                                                    <button class="btn btn-sm btn-info"> EXCELダウンロード</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;background-color:#fff;border: solid #FF8000"><!--勤務表入力項目-->
                                    <thead>
                                    <tr>
                                        <th width="49">日付</th>
                                        <th width="42">曜日</th>
                                        <th width="73.3">勤務区分</th>
                                        <th width="73.3">出勤時間</th>
                                        <th width="73.3">退勤時間</th>
                                        <th width="73.3">稼働時間</th>
                                        <th width="73.3">普通残業</th>
                                        <th width="73.3">休日残業</th>
                                        <th width="73.3">不足時間</th>
                                        <th width="264">備考</th>
                                        <th width="17"></th>
                                    </tr>
                                    </thead></table>
                                <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="border: solid #FF8000;border-top:0"><!--スクロール-->
                                    <table id="show" class="table  table-bordered table-condensed table-responsive"  >
                                        <tbody>
                                        @foreach( $day_list as $l )
                                            <tr>
                                                <td width="49" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>>{{ substr($l->NENGAPI,5) }}</td>
                                                <td width="42" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>>{{ $l->YOUBI }}</td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>>
                                                    {{$l->KINMUKBN}}
                                                </td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><fieldset>
                                                        {{ $l->STIME }}
                                                    </fieldset></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><fieldset>
                                                        {{ $l->ETIME }}
                                                    </fieldset></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->KINMUTIME}}</span></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->ZANGYOUTIMEF}}</span></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->ZANGYOUTIMEK}}</span></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->SHORTAGETIME}}</span></td>
                                                <td width="264" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><fieldset>
                                                        {{ $l->BKU }}
                                                    </fieldset></td>
                                                <input type="hidden" name="nengapi[]" value="{{$l->NENGAPI}}">
                                                <input type="hidden" name="id[]" value="{{$l->id}}">
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="background-color:#fff;border: solid #FF8000;border-top:0">
                                    <thead>
                                    <tr>
                                        <th width="49">月間合計</th><!--月間合計項目-->
                                        <th width="42">-</th>
                                        <th width="73.3">-</th>
                                        <th width="73.3">-</th>
                                        <th width="73.3">-</th>
                                        <th width="73.3">{{$sum_kinmutime}}</th><!--稼働時間-->
                                        <th width="73.3">{{$sum_zangyoutimef}}</th><!--普通残業-->
                                        <th width="73.3">{{$sum_zangyoutimek}}</th><!--休日残業-->
                                        <th width="73.3">{{$sum_shortagetime}}</th><!--不足時間-->
                                        <th width="264">代休可：{{$sum_daikyuukanobi}}日</th><!--代休日-->
                                        <th width="17"></th>
                                    </tr>
                                    </thead></table>

                            </div>

                            <div class="col-md-2">
                                <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                                <br>
                                <div>
                                    <table name='km_rule'  style="font-size:10px;" class="table table-striped table-bordered table-condensed table-responsive" ><!-- 時間参考表　-->
                                        <thead>
                                        <tr>
                                            <th>勤務<br>条件</th>
                                            <th>開始<br>時間</th>
                                            <th>終了<br>時間</th>
                                            <th>時間<br>(H)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>標準</td>
                                            <td>09:30</td>
                                            <td>18:30</td>
                                            <td>8.00</td>
                                        </tr>
                                        <tr>
                                            <td>午前</td>
                                            <td>09:30</td>
                                            <td>12:00</td>
                                            <td>2.50</td>
                                        </tr>
                                        <tr>
                                            <td>午後</td>
                                            <td>13:00</td>
                                            <td>18:30</td>
                                            <td>5.50</td>
                                        </tr>
                                        <tr>
                                            <td>深夜</td>
                                            <td>22:00</td>
                                            <td>04:00</td>
                                            <td>6.00</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>12:00</td>
                                            <td>13:00</td>
                                            <td>1.00</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>18:30</td>
                                            <td>19:00</td>
                                            <td>0.50</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>22:00</td>
                                            <td>22:30</td>
                                            <td>0.50</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>03:30</td>
                                            <td>04:00</td>
                                            <td>0.50</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div></div>
    </form>

    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker({
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
					var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();


                    window.location.pathname = '/k/km/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });
    </script>
@endsection