﻿<!-- 勤務入力画面　申請なし -->
@extends('app')
<script type="text/javascript">
function ruli_check()
{
	var stimes= document.getElementsByName("stime[]");
	var stime = stimes[val].value;
	var etimes= document.getElementsByName("etime[]");
  var etime=  etimes[val].value;
	document.write(etimes[0].value);
	alert(etimes[0].value);
	alert(etimes[1].value);
	
	alert("11111111");
	/*
		if((stime!="" && etime=="") ||((stime=="" && etime!="") )){
          alert('開始時刻もしくは終了時間が入力されていない箇所があります！');
			    exit;
		}
		*/
}

</script>




@section('content')


    <form id="km" action="{{URL('k/km')}}" method="POST" onsubmit = "ruli_check();">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-condensed table-responsive" style="margin-bottom:0px;">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}月度勤務表</span><!--タイトル-->

                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:center;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="btn-group pull-right" >
                                                    <button type="submit" class="btn btn-sm btn-warning" >保存</button>
                                                    <button id="save-button" class="btn btn-sm btn-warning" type="submit">申請</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;background-color:#fff;border: solid #FF8000"><!--勤務表入力項目-->
                                    <thead>
                                    <tr>
                                        <th width="49">日付</th>
                                        <th width="49">曜日</th>
                                        <th width="100">勤務区分</th>
                                        <th width="77">出勤時間</th>
                                        <th width="77">退勤時間</th>
                                        <th width="77">稼働時間</th>
                                        <th width="77">普通残業</th>
                                        <th width="77">休日残業</th>
                                        <th width="77">不足時間</th>
                                        <th width="264">備考</th>
                                        <th width="17"></th>
                                    </tr>
                                    </thead></table>
                                <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="border: solid #FF8000;border-top:0"><!--スクロール-->
                                    <table class="table table-bordered table-condensed table-responsive" style="background-color:#fff;">
                                        <tbody>
                                        @foreach( $day_list as $l )
                                            <tr>
                                                <td width="49" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>>{{ substr($l->NENGAPI,5) }}</td>
                                                <td width="42" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>>{{ $l->YOUBI }}</td>
                                                <input type="hidden" name="yobi[]" value="{{$l->YOUBI}}">
                                                <input type="hidden" name="kyujitsu[]" value="{{$l->KYUUJITUFLG}}">
                                                <td width="106.5" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>>
                                                    <select name="kinmukbn[]" class="form-canvas">
                                                        <option value="">--</option>
                                                        <option value="01" @if('01' == $l->KINMUKBN){{'selected'}}@endif>有休</option>
                                                        <option value="02" @if('02' == $l->KINMUKBN){{'selected'}}@endif>代休</option>
                                                        <option value="03" @if('03' == $l->KINMUKBN){{'selected'}}@endif>欠勤</option>
                                                        <option value="04" @if('04' == $l->KINMUKBN){{'selected'}}@endif>その他</option>
                                                    </select>
                                                </td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><fieldset>
                                                        <input type="text" style="width:95%;" class="stime" name="stime[]"  onchange="changestart({{substr($l->NENGAPI,8)-1}})"  size="1" value="{{ $l->STIME }}" >
                                                    </fieldset></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><fieldset>
                                                        <input type="text" style="width:95%;" class="etime"  name="etime[]" onchange="changeend({{substr($l->NENGAPI,8)-1}})"  size="1" value="{{ $l->ETIME }}">
                                                    </fieldset></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->KINMUTIME}}</span></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->ZANGYOUTIMEF}}</span></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->ZANGYOUTIMEK}}</span></td>
                                                <td width="73.3" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><span>{{$l->SHORTAGETIME}}</span></td>
                                                <td width="264" <?php if('1'==$l->KYUUJITUFLG){print "bgcolor='F6D8CE'";} ?>><fieldset>
                                                        <input type="text" name="bku[]" placeholder="" size="30" value="{{ $l->BKU }}">
                                                    </fieldset></td>
                                                <input type="hidden" name="nengapi[]" value="{{$l->NENGAPI}}">
                                                <input type="hidden" name="id[]" value="{{$l->id}}">
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="background-color:#fff;border: solid #FF8000;border-top:0">
                                    <thead>
                                    <tr>
                                        <th width="49">月間合計</th><!--月間合計項目-->
                                        <th width="42">-</th>
                                        <th width="106.5">-</th>
                                        <th width="73.3">-</th>
                                        <th width="73.3">-</th>
                                        <th width="73.3">{{$sum_kinmutime}}</th><!--稼働時間-->
                                        <th width="73.3">{{$sum_zangyoutimef}}</th><!--普通残業-->
                                        <th width="73.3">{{$sum_zangyoutimek}}</th><!--休日残業-->
                                        <th width="73.3">{{$sum_shortagetime}}</th><!--不足時間-->
                                        <th width="264">代休可：{{$sum_daikyuukanobi}}日</th><!--代休日-->
                                        <th width="17"></th>
                                    </tr>
                                    </thead></table>

                            </div>

                            <div class="col-md-2">
                                <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                                <br>
                                <div>
                                    <table name='km_rule'  style="font-size:10px;" class="table table-striped table-bordered table-condensed table-responsive" ><!-- 時間参考表　-->
                                        <thead>
                                        <tr>
                                            <th>勤務<br>条件</th>
                                            <th>開始<br>時間</th>
                                            <th>終了<br>時間</th>
                                            <th>時間<br>(H)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>標準</td>
                                            <td>09:30</td>
                                            <td>18:30</td>
                                            <td>8.00</td>
                                        </tr>
                                        <tr>
                                            <td>午前</td>
                                            <td>09:30</td>
                                            <td>12:00</td>
                                            <td>2.50</td>
                                        </tr>
                                        <tr>
                                            <td>午後</td>
                                            <td>13:00</td>
                                            <td>18:30</td>
                                            <td>5.50</td>
                                        </tr>
                                        <tr>
                                            <td>深夜</td>
                                            <td>22:00</td>
                                            <td>04:00</td>
                                            <td>6.00</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>12:00</td>
                                            <td>13:00</td>
                                            <td>1.00</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>18:30</td>
                                            <td>19:00</td>
                                            <td>0.50</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>22:00</td>
                                            <td>22:30</td>
                                            <td>0.50</td>
                                        </tr>
                                        <tr>
                                            <td>休憩</td>
                                            <td>03:30</td>
                                            <td>04:00</td>
                                            <td>0.50</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div></div>

    </form>
   <script>

 
 function changestart(val){
            var stimes= document.getElementsByName("stime[]");
            var stime=stimes[val].value;
            //
			if(stime.length < 4){
			    stime ='0'+stime;
                stime = stime.substring(stime.length-4,stime.length);
			}
		    if(!/^(20|21|22|23|[0-1]\d)[0-5]\d$/.test(stime) && !/^(20|21|22|23|[0-1]\d)(?::[0-5]\d$)/.test(stime)){
                alert("入力エラー, 'HHMM' or 'HH:MM'方式で、36時以内を入力してください。");
                stimes[val].value = null;
            }else if(/^(20|21|22|23|[0-1]\d)[0-5]\d$/.test(stime)){
				//
					stime ='0'+stime;
                    stime = stime.substring(stime.length-4,stime.length);
		        //	
                var head=stime.substring(0,2);
                var strhead=head.toString();
                var tail=stime.substring(2,4);

                if(tail=="00"){
                    tail="00";
                }else if(tail<=15){
                    tail=15;
                }else if(tail<=30){
                    tail=30;
                }else if(tail<=45){
                    tail=45;
                }else if(tail<=59){
                    tail="00";
                   var sujihead= parseInt(head);
                    sujihead+=1;
                    strhead=sujihead.toString();
                }
                strtail=tail.toString();
                stime=strhead.concat(strtail);
                stimes[val].value= stime;
                
					
            }			
            }


        function changeend(val){

            var etimes= document.getElementsByName("etime[]");
            var etime=etimes[val].value;
			//
			if(etime.length < 4){
			    etime ='0'+etime;
                etime = etime.substring(etime.length-4,etime.length);		
			}					
            //
			if(!/^(20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|[0-1]\d)[0-5]\d$/.test(etime) && !/^(20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|[0-1]\d)(?::[0-5]\d$)/.test(etime)){
                alert("入力エラー, 'HHMM' or 'HH:MM'方式で入力してください。");				  
                etimes[val].value = null;
            }
			//else if(/^(20|21|22|23|[0-1]\d)[0-5]\d$/.test(etime)){
				//深夜対応
			else if(/^(20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|[0-1]\d)[0-5]\d$/.test(etime)){
                var head=etime.substring(0,2);
                var strhead=head.toString();
                var tail=etime.substring(2,4);
                if(tail<15){
                    tail="00";
                }else if(tail<30){
                    tail=15;
                }else if(tail<45){
                    tail=30;
                }else if(tail<=59){
                    tail=45;
                }
                strtail=tail.toString();
                etime=strhead.concat(strtail);
                etimes[val].value= etime;

            }
        }



    </script>

    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker({
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
                    var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();


                    window.location.pathname = '/k/km/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });
        $(document).ready(function(){
            $('input[class = stime]').blur( function() {

                // on blur, if there is no value, set the defaultText
                if ($(this).val() != ''){
                    var str = $(this).val();
					
                    var splitfirst = str.split(':');
                    var timeline = splitfirst.join('');
					
                    var tm = timeline.substring(timeline.length - 2);
                    var th = timeline.substring(0,(timeline.length - 2));
                    var result = th+':'+tm;
                    $(this).val(result);
                }
            });

            $('input[class = etime]').blur( function() {

                // on blur, if there is no value, set the defaultText
                if ($(this).val() != ''){
                    var str = $(this).val();					
                    var splitfirst = str.split(':');
                    var timeline = splitfirst.join('');
                    var tm = timeline.substring(timeline.length - 2);
                    var th = timeline.substring(0,(timeline.length - 2));
                    var result = th+':'+tm;
                    $(this).val(result);
                }
            });

        });

        $(document).ready(function(){　　　 // 申請ボタン

           // Confirm.init('sm');
            $('#save-button').unbind();
            $('#save-button').bind('click', function(e) {
                e.preventDefault();　
				
                var nikis= document.getElementsByName("nengapi[]");
                var niki=nikis[0].value;
                            
				ret = confirm('保存していないデータは削除されます。申請して宜しいですか?')
                if(ret == true){
						window.location.pathname='/k/km/'+niki;
                       }
					
                    });
					
					 
					
                });
            
		
		
		 



    </script>
  
@endsection