﻿<!-- 勤務決定画面　申請あり　項目変更できない　disabled -->
@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-15">
                            <table class="table table-condensed table-responsive" style="margin-bottom:0px;" >
                                <thead>
                                <tr nowrap="" align="LEFT" valign="TOP">
                                    <td align="LEFT">
                                        <span>社員番号：{{$arruserlist[0]["id"]}}</span>&nbsp;&nbsp;&nbsp;
                                        <span>氏名：{{$arruserlist[0]["NAME"]}}</span>&nbsp;&nbsp;&nbsp;
                                        <span>所属：{{$arruserlist[0]["DIVISION"]}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <span style="font-size:25px; align:left;margin-right :800px">勤務表一覧</span><!--タイトル 勤務表-->
                                    </th>
                                    <th><!--年度-->
                                        <div class="btn-group pull-right" >
                                            <span style="font-size:15px">年度:</span>
                                            <select id="nendo" class="form-canvas" style="width :60px; height :30px">
                                                <option value="">--</option>
                                                <option value="{{$kotoshi}}" @if($kotoshi == $nendo){{'selected'}}@endif>{{$kotoshi}}</option>
                                                <option value="{{$kotoshi-1}}" @if($kotoshi-1 == $nendo){{'selected'}}@endif>{{$kotoshi-1}}</option>
                                                <option value="{{$kotoshi-2}}" @if($kotoshi-2 == $nendo){{'selected'}}@endif>{{$kotoshi-2}}</option>
                                                <option value="{{$kotoshi-3}}" @if($kotoshi-3 == $nendo){{'selected'}}@endif>{{$kotoshi-3}}</option>
                                                <option value="{{$kotoshi-4}}" @if($kotoshi-4 == $nendo){{'selected'}}@endif>{{$kotoshi-4}}</option>
                                            </select>
                                        </div>
                                    </th>
                                    <th>
                                        <button id="getYear" class="btn btn-xs btn-warning" style="width: 80px;height :30px">表示</button>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-15">
                            <table  class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px; border: solid #FF8000;"><!--勤務表入力項目-->
                                <thead>
                                <tr>
                                        <th width="75">年月</th>
										<th width="60">標準日数</th>
										<th width="60">稼働日数</th>
										<th width="60">標準時間</th>
										<th width="60">稼働時間</th>
										<th width="60">普通残業</th>
										<th width="60">休日残業</th>
										<th width="60">不足時間</th>
										<th width="60">有休日数</th>
										<th width="60">代休日数</th>
										<th width="60">欠勤日数</th>
										<th width="100">その他日数</th>
										<th width="100">更新日</th>
										<th width="60">承認状況</th>
										<th width="100">申請日</th>
										<th width="100">承認日</th>
										<th width="100">承認者</th>
                                </tr>
                                </thead>
                                @if(count($arrkmlist)>0)
                                    @foreach( $arrkmlist as $key=>$value )
                                        <tr>
                                            <td width="75"><span><a href="/k/km/{{substr($value["NENGETU"],0,4)."-".substr($value["NENGETU"],4,2)}}/edit">{{$value["NENGETU"]}}</a></span></td>
                                        <td width="50"><span>{{$value["STANDARDDAY"]}}</span></td>
											<td width="50"><span>{{$value["KINMUDAY"]}}</span></td>
											<td width="50"><span>{{$value["STANDARDTIME"]}}</span></td>
											<td width="50"><span>{{$value["KINMUTIME"]}}</span></td>
											<td width="50"><span>{{$value["ZANGYOUTIMEF"]}}</span></td>
											<td width="50"><span>{{$value["ZANGYOUTIMEK"]}}</span></td>
											<td width="50"><span>{{$value["SHORTAGETIME"]}}</span></td>
											<td width="50"><span>{{$value["YUUKYUUCNT"]}}</span></td>
											<td width="50"><span>{{$value["DAIKYUUCNT"]}}</span></td>
											<td width="50"><span>{{$value["KEKKINCNT"]}}</span></td>
											<td width="130"><span>{{$value["SONOTACNT"]}}</span></td>
											<td width="130"><span>{{$value["UPDATEDATE"]}}</span></td>
											<td width="50"><span ><font color="red">{{$value["SHOUNINSTATUS"]}}</font></span></td>
											<td width="130"><span>{{$value["SINSEIDATE"]}}</span></td>
											<td width="130"><span>{{$value["SHOUNINDATE"]}}</span></td>
											<td width="130"><span>{{$value["SHOUNINSHANM"]}}</span></td>             
                                        </tr>
                                    @endforeach
                                @endif

                            </table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example"><!--スクロール-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
    <script>
        $(document).ready(function(){
            $('#getYear').unbind();
            $('#getYear').bind('click', function(e) {
                e.preventDefault();
                var nendo=document.getElementById("nendo").value;
                window.location.pathname = '/k/kr/'+nendo;
            });
        });
    </script>
@endsection