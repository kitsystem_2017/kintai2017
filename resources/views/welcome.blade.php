<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <link rel="stylesheet" href="jquery-ui.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script src="jquery-ui.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
        <script src="HolidayChk1.js"></script>
        <script>
            $(function(){
                $("#datepicker").datepicker({
                    beforeShowDay: function(date) {
                        var result;
                        var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                        var hName = ktHolidayName(dd);
                        if(hName != "") {
                            result = [true, "date-holiday", hName];
                        } else {
                            switch (date.getDay()) {
                                case 0: //日曜日
                                    result = [true, "date-holiday"];
                                    break;
                                case 6: //土曜日
                                    result = [true, "date-saturday"];
                                    break;
                                default:
                                    result = [true];
                                    break;
                            }
                        }
                        return result;
                    },
                    onSelect: function(dateText, inst) {
                        alert(dateText);
                    }
                });
            });
        </script>
        <style>
            .date-holiday .ui-state-default {
                background-image:none;
                background-color:#FF9999;
            }
            .date-saturday .ui-state-default {
                background-image:none;
                background-color:#66CCFF;
            }
            body {
                margin:0;
                padding:0;
                font-family:Arial,sans-serif;
                font-size:0.8em;
            }
        </style>
    </head>
    <body>
    <p></p>
        <div id="datepicker"></div>
    </body>
</html>