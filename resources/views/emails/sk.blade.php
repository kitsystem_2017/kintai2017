<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <title>Laravelでメール送信</title>
</head>
<body>
お疲れ様です、{{$division}}の{{$name}}です。
<br>
<br>
申請が不備がありまして、却下しました。
<br>
もう一度ご確認をお願いします。
<br>
<br>

以上、よろしくお願いします。
</body>
</html>

