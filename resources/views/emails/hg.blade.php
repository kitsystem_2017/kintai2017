<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <title>Laravelでメール送信</title>
</head>
<body>
お疲れ様です、{{$division}}の{{$name}}です。
<br>
<br>
申請が不備がありまして、却下しました。
<br>
<br>
理由: {{$confirm1}}
<br>
<br>
もう一度ご確認をお願いします。
<br>
申請種別：費用精算
<br>
<br>

以上、よろしくお願いします。
</body>
</html>

