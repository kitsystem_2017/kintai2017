<!-- 勤務入力画面　申請なし -->
@extends('app')
<script type="text/javascript">
    function addRow(){
        //$("#addRow").click(function(){
            // $("table:tableHy(0)").append("<tr><td width="10%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="13.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td></tr>");
            var $table= $("#tableH");
            //updated lcb 20170725
            var vTr= "<tr><td width='20%'><input type='text' name='sinseidate[]'   style='width:80%' onfocus=this.blur() value='' ></td><td width='30%'><input type='text' name='utiwake[]' style='width:80%;' value='' ></td><td width='15%'><input type='text' onchange='check(this);' id='sonotahi' name='sonotahi[]' style='width:80%;' value='' ></td><td width='25%'><input type='text' name='bikou[]' style='width:80%;' value='' ></td><td width='10%'><fieldset><input type='button' onclick='delRow(this);' class='btn btn btn-warning' value='削除'></fieldset></td> <input type='hidden' name='id[]'></tr>";
            //var vTr= "<tr><td>aa</td><td>aa</td><td>aa</td><td>aa</td><td>aa</td></tr>";
            $table.append(vTr);

            $table.find("input[name='sinseidate[]']:last").datepicker({
                dateFormat: "yymmdd"
            });
        //})
    };

//    function delRow(){
//        var $table= $("#tableH");
//        var vTr= "<tr><td width='15%'><input type='text' name='sinseidate[]' style='width:80%;' value='' ></td><td width='25.5%'><input type='text' name='utiwake[]' style='width:80%;' value='' ></td><td width='25.5%'><input type='text' name='ikisaki[]' style='width:80%;' value='' ></td><td width='13.5%'><input type='text' name='sonotahi[]' style='width:80%;' value='' ></td><td width='25%'><input type='text' name='bikou[]' style='width:80%;' value='' ></td><td width='1.0%'><fieldset><input type='button' onclick='delRow(this)' class='btn btn btn-info' value='削除'></fieldset></td> <input type='hidden' name='id[]'></tr>";
//        //$("#delRow").click(function(){
//        // $("table:tableHy(0)").append("<tr><td width="10%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="13.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td></tr>");
////        var $table= $("#tableH");
////        var_dump($table);
////        var vTr= "<tr><td width='15%'><input type='text' style='width:80%;' value='' ></td><td width='25.5%'><input type='text' style='width:80%;' value='' ></td><td width='25.5%'><input type='text' style='width:80%;' value='' ></td><td width='13.5%'><input type='text' style='width:80%;' value='' ></td><td width='25%'><input type='text' style='width:80%;' value='' ></td></tr>";
////        //var vTr= "<tr><td>aa</td><td>aa</td><td>aa</td><td>aa</td><td>aa</td></tr>";
////        vTr.parentNode.deleteRow();
//        //})
//        $table.deleteRow(vTr.sectionRowIndex);
//
//    };
    function delRow(buttonD) {
        var $button= $(buttonD);
        var data = $button.parents("tr").find ("input[name='id[]']").val();
        var $dData = $("input[name='idD[]']");
        $dData.val(data);
        $button.parents("tr").remove();
    }

    function check(number){
        var $check1 = $(number);
        var $check2 = $check1.parents("tr").find("input[name='sonotahi[]']").val();
        var $check3 = document.getElementsByName("sonotahi[]");
        var $check5 = $check1.parents("tr").index();
        if(!/^[0-9]+$/.test($check2)){
            alert('半角正数字で入力してください');
            $check3[$check5].value = null;
        }
        //updated by lcb 20170801
        if ($check2.length>6)
        {
            alert('入力した金額の長さの制限(長さ6桁)');
            $check3[$check5].value = null;
        }
    }



</script>
@section('content')
    <form action="{{URL('h/hg')}}" method="POST" xmlns:background="http://www.w3.org/1999/xhtml">
        <input type="hidden" name="idD[]">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <div class="container">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                    <ul>
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </ul>
                </div>
            @endif


            <h2 style="text-align:center;">費用精算申請</h2>
            <br />
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-2"  align="right">
                        <h5>{{$Ymd_sinseibi}}</h5>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-button pull-right">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" name = "saveData" class="btn btn btn-warning">保存</button>&nbsp;
                 <!--           <button id="save-button" name = "applyData" class="btn btn-warning">申請</button>  -->
			     		    <button id="sinsei-button" class="btn btn-sm btn-warning" type="submit" style="width:54px; height:34px">申請</button>

                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
            <br />
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;"><!--勤務表入力項目-->
                                <thead>
                                <tr>
                                    <th width="20%"  style="text-align:center; background: #EEEEEE;">日付</th>
                                    <th width="30%" style="text-align:center; background: #EEEEEE;">費用内訳</th>
                                    <th width="15%" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                    <th width="25%" style="text-align:center; background: #EEEEEE;">備考</th>
                                    <th width="10%" style="background: #EEEEEE;"></th>
                                </tr>
                                </thead></table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:300px;border: solid #FF8000;border-top:0"><!--スクロール-->
                                <table id = "tableH" class="table table-striped table-bordered table-condensed table-responsive">
                                    <tbody>
                                    @foreach( $day_list as $l )
                                        <tr>
                                            <td width="20%"><fieldset>
                                                    <input type="text" name="sinseidate[]" style="width:80%;" onfocus=this.blur() ; value="{{ $l->SINSEIDATE }}" >
                                                </fieldset></td>
                                            <td width="30%"><fieldset>
                                                    <input type="text" name="utiwake[]" style="width:80%;" value="{{ $l->UTIWAKE }}">
                                                </fieldset></td>
                                            <td width="15%"><fieldset>
                                                    <input type="text" id="{{$l->id}}" onchange='check(this);' name="sonotahi[]" onchange='check(this)' style="width:80%;" value="{{ $l->SONOTAHI }}">
                                                </fieldset></td>
                                            <td width="25%"><fieldset>
                                                    <input type="text" name="bikou[]" style="width:80%;" value="{{ $l->BIKOU }}">
                                                </fieldset></td>
                                            <td width='10%'><fieldset>
                                                    <input type='button' onclick='delRow(this)'class='btn btn btn-warning' value='削除'>
                                                </fieldset></td>
                                            <input type="hidden" name="id[]" value="{{$l->id}}">
                                         </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-20">
                                <div class="btn-button pull-right">
                                    <br>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="button" onclick="addRow()" class="btn btn btn-warning" value="行追加">&nbsp;
                                    </br>
                                    <br>
                                </div>
                            </div>

                                <table class="table table-striped table-bordered table-condensed table-responsive" style="border: 3px solid #FF8000;">
                                    <thead>
                                        <tr>
                                            <th width="20%" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                                            <th width="30%" style ="background: #EEEEEE;">-</th>
                                            <th width="15%" style ="background: #EEEEEE;">{{$sum_sonotahi}}</th><!--金額（円）-->
                                            <th width="25%" style ="background: #EEEEEE;">-</th>
                                            <th width="10%" style ="background: #EEEEEE;"></th>
                                        </tr>
                                    </thead>
                                </table>
                        </div>

                        <div class="col-md-2">
                            <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                            <br>

                        </div>

                        </div>
                    </div>
                </div>
        </div>
    </form>

    <script>

        // $(document).ready(function(){

            // Confirm.init('sm');
            // $('#save-button').unbind();
            // $('#save-button').bind('click', function(e) {
                // e.preventDefault();
                // Confirm.show('確認', '申請して宜しいですか？', {
                    // 'Save': {
                        // 'primary': true,
                        // 'callback': function() {
                            // var nikis= document.getElementsByName("niki");
                            // var niki=nikis[0].value;
                            // window.location.pathname='/h/hg/'+niki;

                        // }
                    // }
                // });
            // });
        // });
		
		  $(document).ready(function(){  // 申請ボタン

           // Confirm.init('sm');
            $('#sinsei-button').unbind();
            $('#sinsei-button').bind('click', function(e) {
                e.preventDefault();　
				
                var nikis= document.getElementsByName("niki");
                var niki=nikis[0].value;
                            
				ret = confirm('保存していないデータは削除されます。申請して宜しいですか?')
                if(ret == true){
						window.location.pathname='/h/hg/'+niki;
                       }
					
                    });
					
					 
					
                });
		
        $(document).ready(function(){
            $("#datepicker").datepicker({
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
                    var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();
                    if(MyDate.getMonth() < 9){
                        Ymd = MyDate.getFullYear()+'-0'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();
                    }

                    window.location.pathname = '/h/hg/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });

        $(document).ready(function(){
            $("input[name='sinseidate[]']").datepicker({
                dateFormat: "yymmdd"
            });
        });

    </script>

@endsection