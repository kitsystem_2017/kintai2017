<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('h/hr')}}" method="POST" xmlns:background="http://www.w3.org/1999/xhtml">
        <div class="container">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->-->
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            
            <h2 style="text-align:center;">費用精算申請</h2>
            <br />
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-3"  align="right">
                        <h5>{{$Ymd_sinseigetsu}}</h5>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
            <br />
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;"><!--勤務表入力項目-->
                                <thead>
                                <tr>
                                    <th width="25%"  style="text-align:center; background: #EEEEEE;">日付</th>
                                    <th width="35%" style="text-align:center; background: #EEEEEE;">費用内訳</th>
                                    <th width="15%" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                    <th width="25%" style="text-align:center; background: #EEEEEE;">備考</th>
                                </tr>
                                </thead></table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:300px;border: solid #FF8000;border-top:0"><!--スクロール-->
                                <table class="table table-striped table-bordered table-condensed table-responsive">
                                    <tbody>
                                    @foreach( $day_list as $l )
                                        <tr>
                                            <td width="25%"><fieldset>
                                                    {{ $l->SINSEIDATE }}
                                                </fieldset></td>
                                            <td width="35%"><fieldset>
                                                   {{ $l->UTIWAKE }}
                                                </fieldset></td>
                                            <td width="15%"><fieldset>
                                                   {{ $l->SONOTAHI }}
                                                </fieldset></td>
                                            <td width="25%"><fieldset>
                                                   {{ $l->BIKOU }}
                                                </fieldset></td>
                                         </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                                <table class="table table-striped table-bordered table-condensed table-responsive" style="border: 3px solid #FF8000;">
                                    <thead>
                                        <tr>
                                            <th width="25%" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                                            <th width="35%" style ="background: #EEEEEE;">-</th>
                                            <th width="15%" style ="background: #EEEEEE;">{{$sum_sonotahi}}</th><!--金額（円）-->
                                            <th width="25%" style ="background: #EEEEEE;">-</th>
                                        </tr>
                                    </thead>
                                </table>
                        </div>

                        <div class="col-md-2">
                            <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                            <br>

                        </div>

                    </div>
                </div>
        </div>
    </form>

    <script>

        $(document).ready(function(){
            $("#datepicker").datepicker({
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
                    var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();


                    window.location.pathname = '/h/hg/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });


    </script>

@endsection