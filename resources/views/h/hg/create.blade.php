<!-- 精算費用申請画面 保存、申請あり-->
@extends('app')

<script>
    function insertRow(id){
        //$("#addRow").click(function(){
            //var $table= $("#tableH");
            //var vTr= "<tr><td width="10%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="13.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td></tr>";
            //var vTr= "<tr><td>aa</td><td>aa</td><td>aa</td><td>aa</td><td>aa</td></tr>";
            //$table.append(vTr);
            //$(this).hide();
        var table = document.getElementById(id);
        var row = table.insertRow(-1);

        var cell1 = row.insertCell(-1);
        var cell2 = row.insertCell(-1);
        var cell3 = row.insertCell(-1);
        var cell4 = row.insertCell(-1);

        cell1.innerHTML = "aa";
        cell2.innerHTML = "aa";
        cell3.innerHTML = "aa";
        cell4.innerHTML = "aa";

    }
</script>

<script>

    function changestart(val){
        var stimes= document.getElementsByName("stime[]");
        var stime=stimes[val].value;
        if(!/^(20|21|22|23|[0-1]\d)[0-5]\d$/.test(stime)){
            alert('时间格式不正确请输入hhmm');
        }else{

            var head=stime.substring(0,2);
            var strhead=head.toString();
            var tail=stime.substring(2,4);

            if(tail=="00"){
                tail="00";
            }else if(tail<=15){
                tail=15;
            }else if(tail<=30){
                tail=30;
            }else if(tail<=45){
                tail=45;
            }else if(tail<=59){
                tail="00";
                var sujihead= parseInt(head);
                sujihead+=1;
                strhead=sujihead.toString();
            }
            strtail=tail.toString();
            stime=strhead.concat(strtail);
            stimes[val].value= stime;

        }
    }

</script>

@section('content')
    <form action="{{URL('h/hg')}}" method="POST">
        <div class="container">
            <h2 style="text-align:center;">費用の精算申請</h2>
            <br />
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-3"  align="right">
                        <h5>{{$Ymd_sinseibi}}</h5>
                    </div>
                    <div class="col-md-3">
                        <div class="btn-button pull-right">
                            <button type="button" id = "addRow" name = "aa"  oncick = "insertRow('tableH')" class="btn btn-sm btn-info">追加</button>&nbsp;
                            <button type="button" id = "saveData" name = "bb" class="btn btn-sm btn-info">保存</button>&nbsp;
                            <button type="submit" id = "applyData" name = "cc" class="btn btn-warning">申請</button>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
            <br />
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-20">
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;"><!--勤務表入力項目-->
                                <thead>
                                <tr>
                                    <th width="10%"  style="text-align:center; background: #EEEEEE;">日付</th>
                                    <th width="25%" style="text-align:center; background: #EEEEEE;">費用内訳</th>
                                    <th width="25%" style="text-align:center; background: #EEEEEE;">プロジェクト名</th>
                                    <th width="13.5%" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                    <th width="25%" style="text-align:center; background: #EEEEEE;">備考</th>
                                    <th width="1.5%" style="background: #EEEEEE;"></th>
                                </tr>
                                </thead></table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example"><!--スクロール-->
                                <table class="table table-striped table-bordered table-condensed table-responsive" id = "tableH">
                                    <tbody>
                                        @for($i = 1; $i <= 8; $i++)
                                            <tr>
                                                <td width="10%"><fieldset>
                                                        <input  type="text"  style="width:80%;" value="" >
                                                    </fieldset></td>
                                                <td width="25.5%"><fieldset>
                                                        <input type="text" style="width:80%;" value="" >
                                                    </fieldset></td>
                                                <td width="25.5%"><fieldset>
                                                        <input type="text" style="width:80%;" value="" >
                                                    </fieldset></td>
                                                <td width="13.5%"><fieldset>
                                                        <input type="text" style="width:80%;" value="" >
                                                    </fieldset></td>
                                                <td width="25%"><fieldset>
                                                        <input type="text" style="width:80%;" value="" >
                                                    </fieldset></td>
                                             </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                                <table class="table table-striped table-bordered table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th width="10%" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                                            <th width="25%" style ="background: #EEEEEE;">-</th>
                                            <th width="25%" style ="background: #EEEEEE;">-</th>
                                            <th width="13.5%" style ="background: #EEEEEE;"></th><!--金額（円）-->
                                            <th width="25%" style ="background: #EEEEEE;">-</th>
                                            <th width="1.5%" style ="background: #EEEEEE;"></th>
                                        </tr>
                                    </thead>
                                </table>
                        </div>
                    </div>
                </div>
        </div>
    </form>





@endsection