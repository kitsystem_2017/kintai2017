<!-- 精算費用申請画面 保存、申請あり-->
@extends('app')

@section('content')
    <form action="{{URL('h/hk')}}" method="POST">
        <div class="page-header text-center">
            <h2>定期券変更届出及び交通費精算</h2>
            <br>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2" align="center">
                        <h5>{{$Ymd_sinseibi}}</h5>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-button pull-right">
                            <button type="button" id = "saveData" name = "bb" class="btn btn-sm btn-info">保存</button>&nbsp;
                            <button type="submit" id = "applyData" name = "cc" class="btn btn-warning">申請</button>
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
            </div>
            <br />

            <h3>定期券変更</h3>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-20">
                        <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;">
                            <thead>
                            <tr>
                                <th width="9%" style="text-align:center; background: #EEEEEE;">路線</th>
                                <th width="10%" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                <th width="10%" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                <th width="9%" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                <th width="13%" style="text-align:center; background: #EEEEEE;">変更日付</th>
                                <th width="14%" style="text-align:center; background: #EEEEEE;">変更事由</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="text-align:center;">
                                    <form role="form">
                                        <div class="form-group" size="12">
                                            <select class="form-canvas">
                                                <option></option>
                                                <option>JR</option>
                                                <option>地下鉄</option>
                                                <option>私鉄</option>
                                                <option>タクシー</option>
                                                <option>バス</option>
                                                <option>その他</option>
                                            </select>
                                        </div>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="10">
                                    </fieldset></td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="35">
                                    </fieldset>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="page-header text-center">
            <h3>交通費</h3>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-12">

                        <table class="table table-striped table-bordered table-condensed table-responsive" >
                            <thead>
                            <tr>
                                　　<th width="9%" style="text-align:center; background: #EEEEEE;">路線</th>
                                　 <th width="10%" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                　  <th width="10%" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                　  <th width="9%" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                　 <th width="13%" style="text-align:center; background: #EEEEEE;">日付</th>
                                　 <th width="14%" style="text-align:center; background: #EEEEEE;">備考</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="text-align:center;">
                                    <form role="form">
                                        <div class="form-group" size="12">
                                            <select class="form-canvas">
                                                <option></option>
                                                <option>JR</option>
                                                <option>地下鉄</option>
                                                <option>私鉄</option>
                                                <option>タクシー</option>
                                                <option>バス</option>
                                                <option>その他</option>
                                            </select>
                                        </div>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="10">
                                    </fieldset></td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="35">
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:center;">
                                    <form role="form">
                                        <div class="form-group" size="12">
                                            <select class="form-canvas">
                                                <option></option>
                                                <option>JR</option>
                                                <option>地下鉄</option>
                                                <option>私鉄</option>
                                                <option>タクシー</option>
                                                <option>バス</option>
                                                <option>その他</option>
                                            </select>
                                        </div>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="10">
                                    </fieldset></td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="35">
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:center;">
                                    <form role="form">
                                        <div class="form-group" size="12">
                                            <select class="form-canvas">
                                                <option></option>
                                                <option>JR</option>
                                                <option>地下鉄</option>
                                                <option>私鉄</option>
                                                <option>タクシー</option>
                                                <option>バス</option>
                                                <option>その他</option>
                                            </select>
                                        </div>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="10">
                                    </fieldset></td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="35">
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:center;">
                                    <form role="form">
                                        <div class="form-group" size="12">
                                            <select class="form-canvas">
                                                <option></option>
                                                <option>JR</option>
                                                <option>地下鉄</option>
                                                <option>私鉄</option>
                                                <option>タクシー</option>
                                                <option>バス</option>
                                                <option>その他</option>
                                            </select>
                                        </div>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="10">
                                    </fieldset></td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="35">
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:center;">
                                    <form role="form">
                                        <div class="form-group" size="12">
                                            <select class="form-canvas">
                                                <option></option>
                                                <option>JR</option>
                                                <option>地下鉄</option>
                                                <option>私鉄</option>
                                                <option>タクシー</option>
                                                <option>バス</option>
                                                <option>その他</option>
                                            </select>
                                        </div>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="10">
                                    </fieldset></td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="35">
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:center;">
                                    <form role="form">
                                        <div class="form-group" size="12">
                                            <select class="form-canvas">
                                                <option></option>
                                                <option>JR</option>
                                                <option>地下鉄</option>
                                                <option>私鉄</option>
                                                <option>タクシー</option>
                                                <option>バス</option>
                                                <option>その他</option>
                                            </select>
                                        </div>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="10">
                                    </fieldset></td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="18">
                                    </fieldset>
                                </td>
                                <td style="text-align:center;">
                                    <fieldset>
                                        <input type="text" placeholder="" size="35">
                                    </fieldset>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <table class="table table-striped table-bordered table-condensed table-responsive">
                        <thead>
                        <tr>
                            <th width="8%" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                            <th width="10%" style ="background: #EEEEEE;">-</th>
                            <th width="10%" style ="background: #EEEEEE;">-</th>
                            <th width="10%" style ="background: #EEEEEE;"></th><!--金額（円）-->
                            <th width="15%" style ="background: #EEEEEE;">-</th>
                            <th width="13%" style ="background: #EEEEEE;">-</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </form>
@endsection