﻿<!-- 勤務入力画面　申請なし -->
@extends('app')

<script type="text/javascript">
    function addRow(){
        //$("#addRow").click(function(){
        // $("table:tableHy(0)").append("<tr><td width="10%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="13.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td></tr>");
        var $table= $("#tableH");
        var vTr= "<tr><td width='120'><select name='root[]' class='form-canvas' style='height :29px' ><option>--</option><option value='01'>JR</option><option value='02'>地下鉄</option><option value='03'>私鉄</option><option value='04'>タクシー</option><option value='05'>バス</option><option value='06'>その他</option></select></td><td width='120'><input type='text' name='ekifrom[]'  style='width:160;' value='' ></td><td width='120'><input type='text' name='ekito[]' style='width:160;' value='' ></td><td width='110'><input type='text' onchange='check(this);' name='koutuuhi[]' style='width:110;' value='' ></td><td width='120'><input type='text' name='sinseidate[]' style='width:100;' onfocus=this.blur() value='' ></td><td width='*'><input type='text' name='bikou[]' style='width:80%;' value='' ></td><td width='65'><fieldset><input type='button' onclick='delRow(this);' class='btn btn btn-warning' value='削除'></fieldset></td> <input type='hidden' name='id[]'></tr>";
        //var vTr= "<tr><td>aa</td><td>aa</td><td>aa</td><td>aa</td><td>aa</td></tr>";v
        $table.append(vTr);
        //})
        $table.find("input[name='sinseidate[]']:last").datepicker({
           dateFormat: "yymmdd"
        });
    };

    function delRow(buttonD) {
        var $button= $(buttonD);
        var data = $button.parents("tr").find ("input[name='id[]']").val();
        var $dData = $("input[name='idD[]']");
        $dData.val(data);
        $button.parents("tr").remove();
    }

    function addRow1(){
        //$("#addRow").click(function(){
        // $("table:tableHy(0)").append("<tr><td width="10%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="13.5%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td><td width="25%"><fieldset><input type="text" style="width:80%;" value="" ></fieldset></td></tr>");
        var $table= $("#tableK");
        var vTr= "<tr><td width='120'><select name='rootK[]' class='form-canvas' style='height :29px'><option>--</option><option value='01'>JR</option><option value='02'>地下鉄</option><option value='03'>私鉄</option><option value='04'>タクシー</option><option value='05'>バス</option><option value='06'>その他</option></select></td><td width='120'><input type='text' name='ekifromK[]' style='width:160;' value='' ></td><td width='120'><input type='text' name='ekitoK[]' style='width:160;' value='' ></td><td width='110'><input type='text' onchange='checkK(this);' name='koutuuhiK[]' style='width:110;' value='' ></td><td width='120'><input type='text' name='sinseidateK[]' style='width:100;' onfocus=this.blur() value='' ></td><td width='*'><input type='text' name='bikouK[]' style='width:80%;' value='' ></td><td width='65'><fieldset><input type='button' onclick='delRow1(this);' class='btn btn btn-warning' value='削除'></fieldset></td> <input type='hidden' name='idK[]'></tr>";        //var vTr= "<tr><td>aa</td><td>aa</
        $table.append(vTr);
        //})
        $table.find("input[name='sinseidateK[]']:last").datepicker({
            dateFormat: "yymmdd"
        });
    };

    function delRow1(buttonD) {
        var $button= $(buttonD);
        var data = $button.parents("tr").find ("input[name='idK[]']").val();
        var $dData = $("input[name='idD1[]']");
        $dData.val(data);
        $button.parents("tr").remove();
    }

    function check(number) {
        var $check1 = $(number);
        var $check2 = $check1.parents("tr").find("input[name='koutuuhi[]']").val();
        var $check3 = document.getElementsByName("koutuuhi[]");
        var $check5 = $check1.parents("tr").index();
        if (!/^[0-9]+$/.test($check2)) {
            alert('半角正数字で入力してください');
            $check3[$check5].value = null;
        }
        if ($check2.length>6)
        {
            alert('入力した金額の長さの制限(長さ6桁)');
            $check3[$check5].value = null;
        }


    }

    function checkK(number){
        var $check1 = $(number);
        var $check2 = $check1.parents("tr").find("input[name='koutuuhiK[]']").val();
        var $check3 = document.getElementsByName("koutuuhiK[]");
        var $check5 = $check1.parents("tr").index();

        if(!/^[0-9]+$/.test($check2)){
            alert('半角正数字で入力してください');
            $check3[$check5].value = null;
        }
        //updated by lcb 20170801
        if ($check2.length>6)
        {
            alert('入力した金額の長さの制限(長さ6桁)');
            $check3[$check5].value = null;
        }

    }


</script>

@section('content')
    <form action="{{URL('h/hk')}}" method="POST" xmlns:background="http://www.w3.org/1999/xhtml">
        <input type="hidden" name="idD[]">
        <input type="hidden" name="idD1[]">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <div class="text-center">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                    <ul>
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </ul>
                </div>
            @endif


            <h2>定期券、交通費申請</h2>
            <br>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-2" align="center">
                        <h5>{{$Ymd_sinseibi}}</h5>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-button pull-right">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" name = "saveData" class="btn btn btn-warning">保存</button>&nbsp;
                        <!--    <button id="save-button" name = "applyData" class="btn btn-warning">申請</button> -->
						    <button id="sinsei-button" class="btn btn-sm btn-warning" type="submit" style="width:54px; height:34px">申請</button>

                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
            <br />

            <div  class="text-center">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-10">
                        <table class="table table-striped table-bordered table-condensed table-responsive"  style="margin-bottom:0px;border: solid #FF8000;">
                            <thead>
                            <tr>
                                <th class="h3" colspan="7">定期券変更</th>
                            </tr>
                            <tr>
                                <th width="120" style="text-align:center; background: #EEEEEE;">路線</th>
                                <th width="170" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                <th width="170" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                <th width="120" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                <th width="120" style="text-align:center; background: #EEEEEE;">変更日付</th>
                                <th width="*" style="text-align:center; background: #EEEEEE;">変更事由</th>
                                <th width="65" style="text-align:center; background: #EEEEEE;">&nbsp;</th>
                            </tr>
                            </thead>
                        </table>
                        <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:270px;border: solid #FF8000;border-top:0;"><!--スクロール-->
                                <table id = "tableK" class="table table-striped table-bordered table-condensed table-responsive">
                                <tbody>
                                @foreach( $Teki_list as $T )
                                <tr>
                                    <td width="120">
                                                <select name="rootK[]" class="form-canvas" style="height :29px">
                                                    <option>--</option>
                                                    <option value="01" @if('01' == $T->ROOT){{'selected'}}@endif>JR</option>
                                                    <option value="02" @if('02' == $T->ROOT){{'selected'}}@endif>地下鉄</option>
                                                    <option value="03" @if('03' == $T->ROOT){{'selected'}}@endif>私鉄</option>
                                                    <option value="04" @if('04' == $T->ROOT){{'selected'}}@endif>タクシー</option>
                                                    <option value="05" @if('05' == $T->ROOT){{'selected'}}@endif>バス</option>
                                                    <option value="06" @if('06' == $T->ROOT){{'selected'}}@endif>その他</option>
                                                </select>
                                    </td>
                                    <td width="120"><fieldset>
                                            <input type="text" name="ekifromK[]" style="width:160;" value="{{ $T->EKIFROM }}" >
                                        </fieldset></td>
                                    <td width="120"><fieldset>
                                            <input type="text" name="ekitoK[]" style="width:160;" value="{{ $T->EKITO }}" >
                                        </fieldset></td>
                                    <td width="120"><fieldset> 
                                            <input type="text" name="koutuuhiK[]" onchange='checkK(this);' style="width:110;" value="{{ $T->KOUTUUHI }}">
                                            
                                        </fieldset></td>
                                    <td width="120"><fieldset>
                                            <input type="text" name="sinseidateK[]" style="width:96px;" onfocus=this.blur()  value="{{ $T->SINSEIDATE }}" >
                                        </fieldset></td>
                                    <td width="*"><fieldset>
                                            <input type="text" name="bikouK[]" style="width:80%;" value="{{ $T->BIKOU }}">
                                        </fieldset></td>
                                    <td width='65'><fieldset>
                                            <input type='button' onclick='delRow1(this)'class='btn btn btn-warning' value='削除'>
                                        </fieldset></td>
                                    <input type="hidden" name="idK[]" value="{{$T->id}}">
                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                </div>

                    <div class="col-md-20">
                        <div class="btn-button pull-right" style="margin-top:5px;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="button" onclick="addRow1()" class="btn btn btn-warning" value="行追加">&nbsp;
                            </br>
                        </div>
                    </div>
                </div>
                    <div class="col-md-2">
                        <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                        <br>

                    </div>
            </div>
        </div>

        <div class="page-header text-center">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-10">

                        <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px; border: solid #FF8000;">
                            <thead>
                            <tr>
                                <th class="h3" colspan="7">交通費</th>
                            </tr>
                            <tr>
                                　　<th width="120" style="text-align:center; background: #EEEEEE;">路線</th>
                                　 <th width="170" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                　  <th width="170" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                　  <th width="120" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                　 <th width="120" style="text-align:center; background: #EEEEEE;">日付</th>
                                　 <th width="*" style="text-align:center; background: #EEEEEE;">備考</th>
                                    <th width="65" style="text-align:center; background: #EEEEEE;">&nbsp;</th>
                            </tr>
                            </thead></table>
                        <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:270px;border: solid #FF8000;border-top:0;"><!--スクロール-->
                            <table id = "tableH" class="table table-striped table-bordered table-condensed table-responsive">
                                <tbody>
                                @foreach( $day_list as $l )
                                    <td width="120">
                                        <select name="root[]" class="form-canvas" style="height :29px">
                                            <option>--</option>
                                            <option value="01" @if('01' == $l->ROOT){{'selected'}}@endif>JR</option>
                                            <option value="02" @if('02' == $l->ROOT){{'selected'}}@endif>地下鉄</option>
                                            <option value="03" @if('03' == $l->ROOT){{'selected'}}@endif>私鉄</option>
                                            <option value="04" @if('04' == $l->ROOT){{'selected'}}@endif>タクシー</option>
                                            <option value="05" @if('05' == $l->ROOT){{'selected'}}@endif>バス</option>
                                            <option value="06" @if('06' == $l->ROOT){{'selected'}}@endif>その他</option>
                                        </select>
                                    </td>
                                    <td width="120"><fieldset>
                                            <input type="text" name="ekifrom[]" style="width:160;" value="{{ $l->EKIFROM }}" >
                                        </fieldset></td>
                                    <td width="120"><fieldset>
                                            <input type="text" name="ekito[]" style="width:160;" value="{{ $l->EKITO }}" >
                                        </fieldset></td>
                                    <td width="120"><fieldset>
                                            <input type="text" name="koutuuhi[]" onchange='check(this);' style="width:110;" value="{{ $l->KOUTUUHI }}">
                                        </fieldset></td>
                                    <td width="120"><fieldset>
                                            <input type="text" name="sinseidate[]" style="width:96px;" onfocus=this.blur() value="{{ $l->SINSEIDATE }}" >
                                        </fieldset></td>
                                    <td width="*"><fieldset>
                                            <input type="text" name="bikou[]" style="width:80%;" value="{{ $l->BIKOU }}">
                                        </fieldset></td>
                                    <td width='65'><fieldset>
                                            <input type='button' onclick='delRow(this)'class='btn btn btn-warning' value='削除'>
                                        </fieldset></td>
                                    <input type="hidden" name="id[]" value="{{$l->id}}">
                                    </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                        <div class="col-md-20">
                            <div class="btn-button pull-right" style="width:100%;">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="button" onclick="addRow()" class="btn btn btn-warning" value="行追加" style="margin:5px;float:right;">&nbsp;
                                </br>
                            </div>
                        </div>

                    <table class="table table-striped table-bordered table-condensed table-responsive" style="border: 3px solid #FF8000;">
                        <thead>
                        <tr>
                            <th width="120" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                            <th width="120" style ="background: #EEEEEE;">-</th>
                            <th width="120" style ="background: #EEEEEE;">-</th>
                            <th width="160" style ="background: #EEEEEE;">{{$sum_koutuuhi}}</th><!--金額（円）-->
                            <th width="120" style ="background: #EEEEEE;">-</th>
                            <th width="*" style ="background: #EEEEEE;">-</th>
                            <th width="65" style ="background: #EEEEEE;">-</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </form>

    <script>

        // $(document).ready(function(){

            // Confirm.init('sm');
            // $('#save-button').unbind();
            // $('#save-button').bind('click', function(e) {
                // e.preventDefault();
                // Confirm.show('確認', '申請して宜しいですか？', {
                    // 'YES': {
                        // 'primary': true,
                        // 'callback': function() {
                            // var nikis= document.getElementsByName("niki");
                            // var niki=nikis[0].value;
                            // window.location.pathname='/h/hk/'+niki;

                        // }
                    // }
                // });
            // });
        // });
		
		
		  $(document).ready(function(){  // 申請ボタン

           // Confirm.init('sm');
            $('#sinsei-button').unbind();
            $('#sinsei-button').bind('click', function(e) {
                e.preventDefault();　
				
                var nikis= document.getElementsByName("niki");
                var niki=nikis[0].value;
                            
				ret = confirm('保存していないデータは削除されます。申請して宜しいですか?')
                if(ret == true){
						window.location.pathname='/h/hk/'+niki;
                       }
					
                    });
					
					 
					
                });
            
        $(document).ready(function(){
            $("#datepicker").datepicker({
                defaultDate: new Date($("input[name=niki]").val()),
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
                    var y = MyDate.getFullYear();
                    var m = MyDate.getMonth()+1;
                    var d = MyDate.getDate();
                    if(m<10) m = '0' + m;
                    if(d<10) d = '0' + d;
                    var Ymd = y + '-' + m + '-' + d;

                    window.location.pathname = '/h/hk/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });

        $(document).ready(function(){
            $("input[name='sinseidate[]'],input[name='sinseidateK[]']").datepicker({
                dateFormat: "yymmdd"
            });
        });

    </script>
@endsection