<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('h/hk')}}" method="POST" xmlns:background="http://www.w3.org/1999/xhtml">
        <input type="hidden" name="idD[]">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <div class="text-center">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <h2>定期券、交通費申請</h2>
            <br>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-2" align="center">
                        <h5>{{$Ymd_sinseigetsu}}</h5>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
            <br />

            <div  class="text-center">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-striped table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;">
                                <thead>
                                <tr>
                                    <th class="h3" colspan="6">定期券変更</th>
                                </tr>
                                <tr>
                                    <th width="120" style="text-align:center; background: #EEEEEE;">路線</th>
                                    <th width="120" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                    <th width="120" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                    <th width="160" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                    <th width="120" style="text-align:center; background: #EEEEEE;">変更日付</th>
                                    <th width="*" style="text-align:center; background: #EEEEEE;">変更事由</th>
                                </tr>
                                </thead></table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="margin-bottom:0px;border: solid #FF8000;border-top:0;"><!--スクロール-->
                                <table id = "tableK" class="table table-striped table-bordered table-condensed table-responsive">
                                    <tbody>
                                    @foreach( $Teki_list as $T )
                                        <tr>
                                            <td width="120">
                                                @if('01'== $T->ROOT){{'JR'}}
                                                @elseif('02'== $T->ROOT){{'地下鉄'}}
                                                @elseif('03'== $T->ROOT){{'私鉄'}}
                                                @elseif('04'== $T->ROOT){{'タクシー'}}
                                                @elseif('05'== $T->ROOT){{'バス'}}
                                                @elseif('06'== $T->ROOT){{'その他'}}
                                                @endif
                                            </td>
                                            <td width="120"><fieldset>
                                                    {{ $T->EKIFROM }}
                                                </fieldset></td>
                                            <td width="120"><fieldset>
                                                    {{ $T->EKITO }}
                                                </fieldset></td>
                                            <td width="160"><fieldset>
                                                    {{ $T->KOUTUUHI }}
                                                </fieldset></td>
                                            <td width="120"><fieldset>
                                                    {{ $T->SINSEIDATE }}
                                                </fieldset></td>
                                            <td width="*" style="text-align:left;"><fieldset>
                                                   {{ $T->BIKOU }}
                                                </fieldset></td>
                                            <input type="hidden" name="idK[]" value="{{$T->id}}">
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                        <br>

                    </div>
                </div>
            </div>

            <div class="page-header text-center">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">

                            <table class="table table-striped table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;">
                                <thead>
                                <tr>
                                    <th class="h3" colspan="6">交通費</th>
                                </tr>
                                <tr>
                                    　　<th width="120" style="text-align:center; background: #EEEEEE;">路線</th>
                                    　 <th width="120" style="text-align:center; background: #EEEEEE;">出発駅</th>
                                    　  <th width="120" style="text-align:center; background: #EEEEEE;">到着駅</th>
                                    　  <th width="160" style="text-align:center; background: #EEEEEE;">金額（円）</th>
                                    　 <th width="120" style="text-align:center; background: #EEEEEE;">日付</th>
                                    　 <th width="*"  style="text-align:center; background: #EEEEEE;">備考</th>
                                </tr>
                                </thead></table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="margin-bottom:0px;border: solid #FF8000;border-top:0;"><!--スクロール-->
                                <table id = "tableH" class="table table-striped table-bordered table-condensed table-responsive">
                                    <tbody>
                                    @foreach( $day_list as $l )
                                        <td width="120">
                                            @if('01'== $l->ROOT){{'JR'}}
                                            @elseif('02'== $l->ROOT){{'地下鉄'}}
                                            @elseif('03'== $l->ROOT){{'私鉄'}}
                                            @elseif('04'== $l->ROOT){{'タクシー'}}
                                            @elseif('05'== $l->ROOT){{'バス'}}
                                            @elseif('06'== $l->ROOT){{'その他'}}
                                            @endif
                                        </td>
                                        <td width="120"><fieldset>
                                                {{ $l->EKIFROM }}
                                            </fieldset></td>
                                        <td width="120"><fieldset>
                                                {{ $l->EKITO }}
                                            </fieldset></td>
                                        <td width="160"><fieldset>
                                                {{$l->KOUTUUHI}}
                                            </fieldset></td>
                                        <td width="120"><fieldset>
                                                {{ $l->SINSEIDATE }}
                                            </fieldset></td>
                                        <td width="*" style="text-align:left;"><fieldset>
                                                {{ $l->BIKOU }}
                                            </fieldset></td>
                                        <input type="hidden" name="id[]" value="{{$l->id}}">
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>


                            <table class="table table-striped table-bordered table-condensed table-responsive" style="border: 3px solid #FF8000;border-top:0;">
                                <thead>
                                <tr>
                                    <th width="120" style ="background: #EEEEEE;" >月間合計</th><!--月間合計項目-->
                                    <th width="120" style ="background: #EEEEEE;">-</th>
                                    <th width="120" style ="background: #EEEEEE;">-</th>
                                    <th width="160" style ="background: #EEEEEE;">{{$sum_koutuuhi}}</th><!--金額（円）-->
                                    <th width="120" style ="background: #EEEEEE;">-</th>
                                    <th width="*" style ="background: #EEEEEE;">-</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
    </form>

    <script>

        $(document).ready(function(){
            $("#datepicker").datepicker({
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
                    var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();


                    window.location.pathname = '/h/hk/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });


    </script>
@endsection