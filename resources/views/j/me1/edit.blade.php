<!-- 勤務入力画面　申請なし -->
@extends('app')
<script type="text/javascript">
</script>

@section('content')
    <form action="{{URL('j/me1')}}" method="POST">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                       <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <h2 style="text-align:center;">社員情報設定</h2>
                        <br />
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-5">
                               <!-- <table class="table table-striped table-bordered table-condensed table-responsive" > -->
								<table class="table table-striped table-bordered table-condensed table-responsive"  style="margin-bottom:0px;border: solid #FF8000;">
								
                                    <thead>
                                    <td width="80" align="center">社員コード</td>
                                    <td width="80" align="center">氏名(*)</td>
                                    <td width="80" align="center">所属(*)</td>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td align="center" width="80">
                                            <fieldset>
                                                <input type="text" name="id1" id="id1" value="{{$id1}}">
                                            </fieldset>
                                        </td>
                                        <td align="center" width="80">
                                            <fieldset>
                                                <input type="text" name="name1" id="name1" value="{{$name1}}">
                                            </fieldset>
                                        </td>
                                        <td align="center" width="80">
                                        <!-- <select class="form-canvas" name="division1" id="division1" value="{{$division1}}" style="width :175px; height : 28px">-->
										 <select class="form-canvas" name="division1" id="division1" value="{{$division1}}" style="width :175px; height : 28px">
                                                <option value=""></option>
                                                @foreach( $division_list as $d )
												echo $division_list;
                                                <option value={{ $d ->DIVISION }} @if($d ->DIVISION  == $division1){{'selected'}}@endif>{{ $d ->DIVISION }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>


                            </div>
                            <div class="row-fluid">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4">
                                    <div class="btn-button pull-right">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button id="search-button" name = "searchData" class="btn btn btn-warning">検索</button>&nbsp;
										<button type="submit" onclick="return func_submit();" name = "saveData" class="btn btn btn-warning">更新</button>&nbsp;
                                        <button id="delete-button" name = "deleteData" class="btn btn btn-warning">削除</button>&nbsp;
                                        <button id="return-button" name = "return" class="btn btn btn-warning">戻る</button>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                    <br>

                    <div class="container-fluid">
                        <div class="row-fluid">
                        <!--    <div style="overflow-x: auto; overflow-y: auto;" data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example">
                                <table border="1" align="center" class=" table-bordered table-condensed table-responsive" style="margin-bottom:0px;width: 100%"><!--勤務表入力項目-->
								<table class="table table-striped table-bordered table-condensed table-responsive"  style="margin-bottom:0px;border: solid #FF8000;"><!-- -->		<thead>						
								<tr><!-- -->
                                <th  colspan="8"></th> <!-- -->
                                </tr><!-- -->
                                    
                                    <tr>
                                        <th width="3%" style="text-align:center; background: #EEEEEE;"></th>
                                        <th width="10%" style="text-align:center; background: #EEEEEE;">社員番号</th>
                                        <th width="10%" style="text-align:center; background: #EEEEEE;">氏名</th>
                                        <th width="10%" style="text-align:center; background: #EEEEEE;">フリカナ</th>
                                        <th width="10%" style="text-align:center; background: #EEEEEE;">区分</th>
                                        <th width="15%" style="text-align:center; background: #EEEEEE;">所属</th>
                                        <th width="12%" style="text-align:center; background: #EEEEEE;">権限</th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">メールアドレス</th>
                                    </tr>
                                    <tr>
                                        <th width="*" style="text-align:center; background: #EEEEEE;"></th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">承認者</th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">承認者名</th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">代理承認者</th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">代理承認者名</th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">パスワード</th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">入社日</th>
                                        <th width="*" style="text-align:center; background: #EEEEEE;">備考</th>
                                    </tr>
                                    </thead>
                                          </table>
										    <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:430px;border: solid #FF8000;border-top:0;"><!--スクロール--><!-- -->
										 <table class="table table-striped table-bordered table-condensed table-responsive"> <!-- -->
                                        <tbody>
                                        @foreach( $member_list as $l )
                                            <tr>
                                                <td align="center" width="3%"><fieldset>
                                                        <input type="checkbox" name="checkbox" value="{{$l->id}}">
                                                    </fieldset></td>
                                                <td align="center" width="10%"><fieldset>
                                                        <input type="text" name="id[]" style="width:100%;" value="{{ $l->id }}">
                                                    </fieldset></td>
                                                <td align="center" width="10%"><fieldset>
                                                        <input type="text" name="name[]" style="width:100%;" value="{{ $l->NAME }}">
                                                    </fieldset></td>
                                                <td align="center" width="10%"><fieldset>
                                                        <input type="text" name="furikana[]" style="width:100%;" value="{{ $l->FURIKANA }}">
                                                    </fieldset></td>
                                                <td align="center" width="10%">
                                                <select name="position[]" class="form-canvas">
                                                    <option></option>
                                                    <option value="01" @if('01' == $l->POSITION){{'selected'}}@endif>プロパー</option>
                                                    <option value="02" @if('02' == $l->POSITION){{'selected'}}@endif>契約社員</option>
                                                    <option value="03" @if('03' == $l->POSITION){{'selected'}}@endif>出向者</option>
                                                    <option value="04" @if('04' == $l->POSITION){{'selected'}}@endif>退職者</option>
                                                    <option value="05" @if('05' == $l->POSITION){{'selected'}}@endif>その他</option>
                                                </select>
                                                </td>
                                                <td align="center" width="10%">
                                                <select name="division[]" class="form-canvas">
                                                    <option value="01"></option>
                                                    @foreach( $division_list as $d )
                                                    <option value={{ $d ->DIVISION }} @if($d ->DIVISION == $l->DIVISION){{'selected'}}@endif>{{ $d ->DIVISION }}</option>
                                                    @endforeach
                                                </select>
                                                </td>
                                                <td align="center" width="10%">
                                                <select name="permission_id[]" class="form-canvas">
                                                    <option></option>
                                                    <option value="3" @if('3' == $l->permission_id){{'selected'}}@endif>一般利用</option>
                                                    <option value="2" @if('2' == $l->permission_id){{'selected'}}@endif>勤怠承認</option>
                                                    <option value="1" @if('1' == $l->permission_id){{'selected'}}@endif>総務担当</option>
                                                    <option value="1" @if('1' == $l->permission_id){{'selected'}}@endif>システム管理</option>
                                                </select>
                                                </td>
                                                <td align="center" width="*"><fieldset>
                                                <input type="text" name="address[]" style="width:100%;" value="{{ $l->ADDRESS }}">
                                                </fieldset></td>
                                            </tr>
                                            <tr>
                                               <td align="center" width="*">
                                                    <fieldset></fieldset>
                                                </td>
                                                <?php $test1 = 'supervisorid'.$l->id; $test2 = 'assistsupervisorid'.$l->id; ?>
                                                <td align="center" width="*"><fieldset>
                                                        <input type="text" name="supervisorid[]"  id={{ $test1 }} style="width:60%;" value="{{ $l->SUPERVISORID }}">
                                                        <input type='button' onclick='index4(this)' name = "indexData4" class="btn btn-sm btn-warning" value='>>'>
                                                    </fieldset></td>
                                                <td align="center" width="*"><fieldset>
                                                        <input type="text" name="supervisor[]" disabled= "true" style="width:100%;" value="{{ $l->SUPERVISOR }}">
                                                    </fieldset></td>
                                                <td align="center" width="*"><fieldset>
                                                        <input type="text" name="assistsupervisorid[]" id={{ $test2 }} style="width:60%;" value="{{ $l->ASSISTSUPERVISORID }}">
                                                        <input type='button' onclick='index5(this)' name = "indexData5" class="btn btn-sm btn-warning" value='>>'>
                                                    </fieldset></td>
                                                <td align="center" width="*"><fieldset>
                                                        <input type="text" name="assistsupervisor[]" disabled= "true" style="width:100%;" value="{{ $l->ASSISTSUPERVISOR }}">
                                                    </fieldset></td>
                                                <td align="center" width="*"><fieldset>
                                                        <input type="text" name="password[]" style="width:100%;" value="{{ $l->PASSWORD }}">
                                                    </fieldset></td>
                                                <td align="center" width="*"><fieldset>
                                                        <input type="text" name="firstday[]" style="width:100%;" value="{{ $l->FIRSTDAY  }}">
                                                    </fieldset></td>
                                                <td align="center" width="*"><fieldset>
                                                        <input type="text" name="bikou[]" style="width:100%;" value="{{ $l->BIKOU }}">
                                                    </fieldset></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div></div>
    </form>
<script>
function func_submit() {  //更新
    var x = 0;
	//チェック
	var arr=document.getElementsByName("checkbox");
    for(var i=0;i<arr.length;i++){
        if(arr[i].checked){
            var x = 1;
        }
    }
	if (x == 0){
        //チェックエラーメッセジ
        alert('更新対象を選択してください。');
		return false;
        exit;
    }
　　　　//更新確認	
    ret = confirm('部門を更新しても宜しいですか？');
	if (ret == true){
		document.saveData.submit();
	}
	else{
		return false;
	}
}
        $(document).ready(function(){

          //$("#select").val();
            $('#search-button').unbind();
            $('#search-button').bind('click', function(e) {
            e.preventDefault();
            var nikis= document.getElementsByName("niki");
            var niki=nikis[0].value;
			//var arr=document.getElementsByName("checkbox");
            var ids= document.getElementsByName("id1");
            var id = ids[0].value;
            var names= document.getElementsByName("name1");
            var name = names[0].value;
            var divisions = document.getElementsByName("division1");
            var division = divisions[0].value;
			
			
            if(id == null){
            }else if(name == null){
                window.location.pathname='/j/me1/'+niki+'+'+id+'+'+'+'+division+'/edit';
            }else if(id == null && name == null){
                window.location.pathname='/j/me1/'+niki+'+'+'+'+'+'+division+'/edit';
            }else{
                window.location.pathname='/j/me1/'+niki+'+'+id+'+'+name+'+'+division+'/edit';
            }

           });
        });

        function index4(buttonD) {
            var nikis= document.getElementsByName("niki");
            var niki=nikis[0].value;
            var $button = $(buttonD);
            var id_idx = $button.siblings("input").attr("id");
            window.open ('/j/me4/'+niki+'+'+'+'+'+'+'00'+'+'+id_idx+'/edit','','height=1000,width=1000,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');

        }
        function index5(buttonD) {
            var nikis= document.getElementsByName("niki");
            var niki=nikis[0].value;
            var $button = $(buttonD);
              var id_idx = $button.siblings("input").attr("id");
            window.open ('/j/me5/'+niki+'+'+'+'+'+'+'00'+'+'+id_idx+'/edit','','height=1000,width=1000,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')

        }

        $(document).ready(function(){
            $("input[name='firstday[]']").datepicker({
                dateFormat: "yy/mm/dd"
            });
        });

       $(document).ready(function(){ <!-- update kamata 2017/8/18　削除

            $('#delete-button').unbind();
            $('#delete-button').bind('click', function(e) {
                e.preventDefault();
			
                            var x = 0;
                            var nikis = document.getElementsByName("niki");
                            var niki = nikis[0].value;
                            var id = 0;
                            var arr=document.getElementsByName("checkbox");
                            for(var i=0;i<arr.length;i++)    {
                                if(arr[i].checked){
                                    id = id + '+' + arr[i].value;
                                    var x = 1;
                                }
                            }
                            if (x == 0){
                                alert('削除したい社員情報を選択してください。');
								exit;
                               // window.location.reload();
                            }
	　　　　　　　　　　　　　　　　　　　　　　　　ret = confirm('社員情報を削除しても宜しいですか？');
					　　　　if (ret == true){
                            window.location.pathname = '/j/me1/' + niki + '+' + id;
                        
					}
                
            });
        });
        $(document).ready(function(){

            $('#return-button').unbind();
            $('#return-button').bind('click', function(e) {
                e.preventDefault();
                var nikis= document.getElementsByName("niki");
                var niki=nikis[0].value;

                window.location.pathname='/j/me/'+niki+'/edit';


            });
        });
		
</script>



@endsection