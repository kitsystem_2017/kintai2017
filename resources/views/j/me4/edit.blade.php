<!-- 勤務入力画面　申請なし -->
@extends('app')


@section('content')
    <form action="{{URL('j/me4')}}" method="POST">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <input type="hidden" name="id" value="{{$id}}">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <h2 style="text-align:center;">承認者検索</h2>
                        <br />
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="col-md-5">
                        </div>
                        <div class="col-md-5">
                            <div class="btn-button pull-right">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button id="search-button" name = "searchData" class="btn btn btn-warning">検索</button>&nbsp;
                                <button id="confirm-button"  name = "confirmData" class="btn btn btn-warning">確定</button>&nbsp;
                                <button id="close-button"  name = "saveData" class="btn btn btn-warning">閉じる</button>&nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-5">
                                <table class="table table-striped table-bordered table-condensed table-responsive" >
                                    <thead>
                                    <td width="80" align="center">部門</td>


                                        <td align="center" width="80">
                                     <select class="form-canvas" name="division1" id="division1" value="{{$division1}}"  style="width :175px; height : 25px">
                                                <option value="01"></option>
                                                @foreach( $division_list as $d )
                                                    <option value={{ $d ->DIVISION }} @if($d ->DIVISION  == $division1){{'selected'}}@endif>{{ $d ->DIVISION }}</option>
                                                @endforeach
                                            </select>
                                        </td>

                                    </thead>

                                    <thead>
                                    <td width="80" align="center">社員番号</td>

                                        <td align="center" width="80">
                                            <fieldset>
                                                <input type="text" name="id1" id="id1" value="{{$id1}}">
                                            </fieldset>
                                        </td>
                                    </thead>

                                    <thead>
                                    <td width="80" align="center">社員氏名</td>

                                    <td align="center" width="80">
                                        <fieldset>
                                            <input type="text" name="name1" id="name1" value="{{$name1}}">
                                        </fieldset>
                                    </td>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-20">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;"><!--勤務表入力項目-->
                                    <thead>
                                    <tr>
                                        <th width="10%" style="text-align:center; background: #EEEEEE;"></th>
                                        <th width="30%" style="text-align:center; background: #EEEEEE;">社員番号 </th>
                                        <th width="30%" style="text-align:center; background: #EEEEEE;">社員氏名</th>
                                        <th width="30%" style="text-align:center; background: #EEEEEE;">部門</th>
                                    </tr>
                                    </thead></table>
                                <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example"><!--スクロール-->
                                    <table id = "tableM" class="table table-striped table-bordered table-condensed table-responsive">
                                        <tbody>
                                        @foreach( $member_list as $key=>$value )
                                            <tr>
                                                <td width="10%"><fieldset>
                                                        <input type="radio" name="radio" value="{{ $value[id] }"}>
                                                        </fieldset></td>
                                                <td width="30%" name="id">{{ $value["id"] }}</td>
                                                <td width="30%" name="name">{{ $value["NAME"] }}</td>
                                                <td width="30%">{{ $value["DIVISION"] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div></div>

    </form>

    <script>

        $(document).ready(function(){

            $('#search-button').unbind();
            $('#search-button').bind('click', function(e) {
            e.preventDefault();
            var nikis= document.getElementsByName("niki");
            var niki=nikis[0].value;
            var id1s= document.getElementsByName("id1");
            var id1 = id1s[0].value;
            var ids= document.getElementsByName("id");
            var id = ids[0].value;
            var names= document.getElementsByName("name1");
            var name = names[0].value;
            var divisions = document.getElementsByName("division1");
            var division = divisions[0].value;

            window.location.pathname='/j/me4/'+niki+'+'+id1+'+'+name+'+'+division+'+'+id+'/edit';


           });
        });

        $(document).ready(function(){

            $('#confirm-button').unbind();
            $('#confirm-button').bind('click', function(e) {
                e.preventDefault();
                var ids= document.getElementsByName("id");
                var test1 = ids[0].value;
                 
                var	list= $('input:radio:checked').val();
                if(list==null){
                     alert("承認者を選択してください")
                }else{
                         window.opener.document.getElementById(test1).value =list;
                         window.close();
                  }
               
            
            });
        });

        $(document).ready(function(){

            $('#close-button').unbind();
            $('#close-button').bind('click', function(e) {
                e.preventDefault();

                window.close();


            });
        });




    </script>



@endsection