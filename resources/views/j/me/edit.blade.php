<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('j/me')}}"  method="POST">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <h2 style="text-align:center;">社員情報設定</h2>
                        <br />
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-20">
                                <table class="table table-striped table-bordered table-condensed table-responsive" >
                                    <thead>
                                    <td width="15%" align="center">社員コード(*)</td>
                                    <td width="15%" align="center">氏名(*)</td>
                                    <td width="15%" align="center">所属(*)</td>
                                    <td width="20%" align="center">区分(*)</td>
                                    <td width="15%" align="center">フリカナ(*)</td>
                                    <td width="*" align="center">権限(*)</td>

                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td align="center" width="15%">
                                            <fieldset>
                                                <input type="text" style="width:100%;" name="id">
                                            </fieldset>
                                        </td>
                                        <td align="center" width="15%">
                                            <fieldset>
                                                <input type="text" style="width:100%;" name="name">
                                            </fieldset>
                                        </td>
                                        <td align="center" width="15%">
                                            <select class="form-canvas" name="division"  style="height :25px">
                                                <option value=""></option>
                                                @foreach( $division_list as $d )
                                                <option value={{ $d ->DIVISION }}>{{ $d ->DIVISION }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="center" width="20%">
                                            <select class="form-canvas" name="position" style="height :25px; width :200px">
                                                <option value="00"></option>
                                                <option value="01">プロバー</option>
                                                <option value="02">契約社員</option>
                                                <option value="03">出向者</option>
                                                <option value="04">退職者</option>
                                                <option value="05">その他</option>
                                            </select>
                                        </td>
                                        <td align="center" width="15%">
                                            <fieldset>
                                                <input type="text" style="width:100%;" name="furikana">
                                            </fieldset>
                                        </td>
                                        <td align="center" width="*">
                                            <select class="form-canvas" name="permission_id" style="height :25px; width :200px">
                                                <option value="00"></option>
                                                <option value="01">一般利用</option> <!--3 -->
                                                <option value="02">勤怠承認</option><!-- 2-->
                                                <option value="03">総務担当</option><!-- 1-->
                                                <option value="04">システム管理</option><!-- 1-->
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>


                                    <thead>
                                    <td width="15%" align="center">パスワード(*)</td>
                                    <td width="15%" align="center">承認者(*)</td>
                                    <td width="15%" align="center">代理承認者(*)</td>
                                    <td width="20%" align="center">メールアドレス(*) </td>
                                    <td width="15%" align="center">入社日(*)</td>
                                    <td width="*" align="center">備考</td>

                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td align="center" width="15%">
                                            <fieldset>
                                                <input type="text" style="width:100%;" name="password">
                                            </fieldset>
                                        </td>
                                        <td align="center" width="15%">
                                            <fieldset>
                                                <input type="text" style="width:50%;" name="supervisorid" id="supervisorid">
                                                <button id="index-button2" name = "indexData2" class="btn btn-sm btn-warning">>></button>
                                            </fieldset>
                                        </td>
                                        <td align="center" width="15%">
                                            <fieldset>
                                                <input type="text" style="width:50%;" name="assistsupervisorid" id="assistsupervisorid">
                                                <button id="index-button3" name = "indexData3" class="btn btn-sm btn-warning">>></button>
                                            </fieldset>
                                        </td>
                                        <td align="center" width="20%">
                                            <fieldset>
                                                <input type="text" style="width:100%;" name="address">
                                            </fieldset>
                                        </td>
                                        <td align="center" width="15%">
                                            <fieldset>
                                                <input type="text" style="width:100%;" name="firstday" onfocus=this.blur() >
                                            </fieldset>
                                        </td>
                                        <td align="center" width="*">
                                            <fieldset>
                                                <input type="text" style="width:100%;" name="bikou">
                                            </fieldset>
                                        </td>
                                    </tr>
                                    </tbody>


                                </table>

                            </div>
                            <div class="row-fluid">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-4">
                                    <div class="btn-button pull-right">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button id="search-button" name = "searchData" class="btn btn btn-warning">検索</button>&nbsp;
                                        <button type="submit" onclick='return check(this)' name = "saveData" class="btn btn btn-warning">新規登録</button>&nbsp;
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br>
                    <br>


                </div>
            </div></div>

    </form>

    <script>

        $(document).ready(function(){
            $('#search-button').unbind();
            $('#search-button').bind('click', function(e) {
                e.preventDefault();
                var nikis= document.getElementsByName("niki");
                var niki=nikis[0].value;
                var ids= document.getElementsByName("id");
                var id = ids[0].value;
                var names= document.getElementsByName("name");
                var name = names[0].value;
                var divisions = document.getElementsByName("division");
                var division = divisions[0].value;
                if(id == null){
                    window.location.pathname='/j/me1/'+niki+'+'+'+'+name+'+'+division+'/edit';
                }else if(name == null){
                    window.location.pathname='/j/me1/'+niki+'+'+id+'+'+'+'+division+'/edit';
                }else if(id == null && name == null){
                    window.location.pathname='/j/me1/'+niki+'+'+'+'+'+'+division+'/edit';
                }else{
                    window.location.pathname='/j/me1/'+niki+'+'+id+'+'+name+'+'+division+'/edit';
                }

            });
        });


        $(document).ready(function(){
            $('#index-button2').unbind();
            $('#index-button2').bind('click', function(e) {
                e.preventDefault();
                var nikis= document.getElementsByName("niki");
                var niki=nikis[0].value;
                window.open ('/j/me2/'+niki+'+'+'+'+'+'+'00'+'/edit','','height=1000,width=1000,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')
            });
        });

        $(document).ready(function(){
            $('#index-button3').unbind();
            $('#index-button3').bind('click', function(e) {
                e.preventDefault();
                var nikis= document.getElementsByName("niki");
                var niki=nikis[0].value;
                window.open ('/j/me3/'+niki+'+'+'+'+'+'+'00'+'/edit','','height=1000,width=1000,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')
            });
        });

        function check(buttonD) {
            var nikis= document.getElementsByName("niki");
            var niki=nikis[0].value;
            var ids= document.getElementsByName("id");
            var id = ids[0].value;
            var names= document.getElementsByName("name");
            var name = names[0].value;
            var divisions = document.getElementsByName("division");
            var division = divisions[0].value;
            var addresses = document.getElementsByName("address");
            var address = addresses[0].value;
            var positions = document.getElementsByName("position");
            var position = positions[0].value;
            var furikanas = document.getElementsByName("furikana");
            var furikana = furikanas[0].value;
            var permission_ids = document.getElementsByName("permission_id");
            var permission_id = permission_ids[0].value;
            var passwords = document.getElementsByName("password");
            var password = passwords[0].value;
            var supervisorids = document.getElementsByName("supervisorid");
            var supervisorid  = supervisorids[0].value;
            var assistsupervisorids = document.getElementsByName("assistsupervisorid");
            var assistsupervisorid = assistsupervisorids[0].value;
            var firstdays = document.getElementsByName("firstday");
            var firstday = firstdays[0].value;
            //メールの書式チェック
            //var myreg = /^([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])/;
            var myreg = /^(([a-zA-Z0-9])+\.)*([a-zA-Z0-9])+\@(([a-zA-Z0-9])+\.)+([a-zA-Z0-9])+/;


            if(id == 0){
                alert("社員コードを入力してください");
                return false;
            }else if(name == 0){
                alert("社員氏名を入力してください");
                return false;
            }else if(division == 00){ //01 →　00に変更
                alert("所属を選択してください");
                return false;
            }else if(position == 00){
                alert("区分を選択してください");
                return false;
            }else if(furikana == 0){
                alert("フリカナを入力してください");
                return false;
            }else if(permission_id == 00){
                alert("権限を選択してください");
                return false;
            }else if(password == 00){
                alert("パスワードを入力してください");
                return false;
            }else if(supervisorid == 0){
                alert("承認者を選択してください");
                return false;
            }else if(assistsupervisorid == 0){
                alert("代理承認者を選択してください");
                return false;
            }else if(address == 0){
                alert("メールアドレスを入力してください");
                return false;
            }else if(!myreg.test(address)){
                alert("正しいメールアドレスを入力してください");
                return false;
            }else if(firstday == 0){
                alert("入社日を入力してください");
                return false;
            }else{
                return true;
            }
        }

        $(document).ready(function(){
            $("input[name='firstday']").datepicker({
                dateFormat: "yy/mm/dd"
            });
        });



    </script>



@endsection