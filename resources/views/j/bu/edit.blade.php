<!-- 勤務入力画面　申請なし -->
@extends('app')
<script>
</script>




@section('content')


    <form action="{{URL('j/bu')}}" method="POST" xmlns:background="http://www.w3.org/1999/xhtml">
        <input type="hidden" name="idD[]">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <div class="container">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                    <ul>
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </ul>
                </div>
            @endif

            <h2 style="text-align:center;">部門情報設定</h2>
            <br />
                <br />

                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-5">
                            <table class="table table-striped table-bordered table-condensed table-responsive"  style="border: solid #FF8000;">
                                <thead>
                                <td width="80" align="center">部門コード(*)</td>
                                <td width="80" align="center">部門名称(*)</td>

                                </thead>

                                <tbody>
                                <tr>
                                    <td align="center" width="80">
                                        <fieldset>
                                            <input type="text" style="width:100%;" name="code1">
                                        </fieldset>
                                    </td>
                                    <td align="center" width="80">
                                        <fieldset>
                                            <input type="text" style="width:100%;" name="division1">
                                        </fieldset>
                                    </td>
                                </tr>
                                </tbody>


                            </table>


                        </div>
                          <div class="row-fluid">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-3">
                                <div class="btn-button pull-right">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                             
							 <button type="submit" onclick='return check(this)' name = "saveData" class="btn btn btn-warning">登録</button>&nbsp;
                                 <button id="update-button" name = "saveData" class="btn btn btn-warning">更新</button>&nbsp;
								  <button id="delete-button" name = "deleteData" class="btn btn btn-warning">削除</button>

								
								<!-- 更新、削除が機能してない -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                </br>



            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-11">
                        <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px; border: solid #FF8000;"><!--勤務表入力項目-->
                            <thead>
                            <tr>
                                <th width="10%"  style="text-align:center; background: #EEEEEE;"></th>
                                <th width="30%" style="text-align:center; background: #EEEEEE;">部門コード</th>
                                <th width="60%" style="text-align:center; background: #EEEEEE;">部門名称</th>
                            </tr>
                            </thead></table>
                        <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example" style="height:350px;border:2px solid #FF8000;border-top:0;"><!--スクロール-->
                            <table id = "tableB" class="table table-striped table-bordered table-condensed table-responsive">
                                <tbody>
                                @foreach( $bu_list as $l )
                                    <tr>
                                        <td align="center" width="10%"><fieldset>
                                                <input type="checkbox" name="checkbox" value="{{$l->id}}">
                                            </fieldset></td>
                                        <td align="center" width="30%">
                                            {{ $l->DIVISIONCD }}
                                        </td>
                                        <td width="60%"><fieldset>
                                                <input type="text" name="division[]" style="width:100%;" value="{{ $l->DIVISION }}">
                                            </fieldset></td>
                                        <!--
                                        <td width='10%'><fieldset>
                                                <input type='button' onclick='delBu(this)'class='btn btn btn-warning' value='削除'>
                                            </fieldset></td>
                                            -->
                                        <input type="hidden" name="id[]" value="{{$l->id}}">
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <!--
                        <div class="col-md-20">
                            <div class="btn-button pull-right">
                                <br>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="button" onclick="addRow()" class="btn btn btn-warning" value="行追加">&nbsp;
                                </br>
                            </div>
                        </div>
                        -->

                    </div>
                </div>
            </div>
        </div>
        </div>
    </form>

    <script>

        // $(document).ready(function(){

            // $('#delete-button').unbind();
            // $('#delete-button').bind('click', function(e) {
                // e.preventDefault();
                // Confirm.init('sm');
                // Confirm.show('部門削除', '部門を削除しても宜しいですか？', {
                    // 'Delete': {
                        // 'primary': true,
                        // 'callback': function () {
                            // var x = 0;
                            // var nikis = document.getElementsByName("niki");
                            // var niki = nikis[0].value;
                            // var id = 0;
                            // var arr=document.getElementsByName("checkbox");
                            // for(var i=0;i<arr.length;i++)    {
                                // if(arr[i].checked){
                                    // id = id + '+' + arr[i].value;
                                    // var x = 1;
                                // }
                            // }

                            // if (x == 0){
                                // alert('削除したい部門を選択してください。');
                                // window.location.reload();
                            // }
		
                           // window.location.pathname = '/j/bu/' + niki + '+' + id;
                        // }
                    // }
                // });
            // });
        // });

		$(document).ready(function(){ <!-- update kamata 2017/8/18　削除

            $('#delete-button').unbind();
            $('#delete-button').bind('click', function(e) {
                e.preventDefault();
			
                            var x = 0;
                            var nikis = document.getElementsByName("niki");
                            var niki = nikis[0].value;
                            var id = 0;
                            var arr=document.getElementsByName("checkbox");
                            for(var i=0;i<arr.length;i++)    {
                                if(arr[i].checked){
                                    id = id + '+' + arr[i].value;
                                    var x = 1;
                                }
                            }

                            if (x == 0){
                                alert('削除したい部門を選択してください。');
								exit;
                               // window.location.reload();
                            }
	　　　　　　　　　　　　　　　　　　　　　　　　ret = confirm('部門を削除しても宜しいですか？');
					　　　　if (ret == true){
                            window.location.pathname = '/j/bu1/' + niki + '+' + id;
                        
					}
                
            });
        });
		
		
		
		
        function check(buttonD) {
            var nikis= document.getElementsByName("niki");
            var niki=nikis[0].value;
            var codes= document.getElementsByName("code1");
            var code = codes[0].value;
            var divisions= document.getElementsByName("division1");
            var division= divisions[0].value;


            if(code == 0){
                alert("部門コードを入力してください");
                return false;
				
            }else if(division == 0){
                alert("部門名前を入力してください");
                return false;
            }else{
				　ret = confirm('部門を登録しても宜しいですか？');
				if (ret == true){
                     //   alert(id);
 　　　　　　　　　　　　　　　　　　　window.location.pathname = '/j/bu1/' + niki + '+' + id + '/edit';
                　　
            }　
			else{
				return false;
				
			}
			
        }
	}


        $(document).ready(function(){   //更新

            $('#update-button').unbind();
            $('#update-button').bind('click', function(e) {
                e.preventDefault();
              //  Confirm.init('sm'); 
				var x = 0;
                            var nikis = document.getElementsByName("niki");
                            var niki = nikis[0].value;
                            var id = 0;
                            var divisions = document.getElementsByName("division[]");
                            var arr=document.getElementsByName("checkbox");
                            for(var i=0;i<arr.length;i++)    {
                                if(arr[i].checked){
                                    id = id + '+' + arr[i].value + '+' + divisions[i].value;
                                    var x = 1;
                                }
                            }
				　　　　　　　　if (x == 0){
                      //updated by lcb 20170816
                        alert('更新対象を選択してください。');
                        exit;
					//window.location.reload();
                            }
							
              　　　　　　　ret = confirm('部門を更新しても宜しいですか？');
					if (ret == true){
                     //   alert(id);
 　　　　　　　　　　　　　　　　　　　window.location.pathname = '/j/bu1/' + niki + '+' + id + '/edit';
                        
                }
                
           });
       });



    </script>


@endsection