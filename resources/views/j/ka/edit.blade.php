<!-- 勤務入力画面　申請なし -->
@extends('app')





@section('content')


    <form  action="{{URL('j/ka')}}" method="POST">
        <div class="container">
            <div class="row">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-condensed table-responsive" style="margin-bottom:0px;">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_m}}月カレンダー設定</span><!--タイトル-->

                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:center;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="btn-group pull-right" >
                                                    <button type="submit" class="btn btn-sm btn-warning" ><a href="/j/ka">保存</a></li></button>&nbsp;

                                                    <!--<button id="save-button" class="btn btn-sm btn-warning" >申請</button>-->

                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;"><!--勤務表入力項目-->
                                    <thead>
                                    <tr>
                                        <th width="49">月-日</th>
                                        <th width="42">曜日</th>
                                        <th width="73.3">平日</th>
                                        <th width="73.3">休日</th>
                                        <th width="264">備考</th>
                                    </tr>
                                    </thead></table>
                                <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example"><!--スクロール-->
                                    <table class="table table-striped table-bordered table-condensed table-responsive">
                                        <tbody>
                                        @foreach( $day_list as $l )
                                            <tr>
                                                <td width="51">{{ substr($l->NENGAPI,5) }}</td>
                                                <td width="43">{{ $l->YOUBI }}</td>
                                                <td  width="75"><input type="radio" value="0" name="kyuujituflg<?php echo $l->NENGAPI ?>" <?PHP if(0==$l->KYUUJITUFLG){print "checked='checked'";} ?>></td>
                                                <td  width="75"><input type="radio" value="1" name="kyuujituflg<?php echo $l->NENGAPI ?>" <?PHP if(1==$l->KYUUJITUFLG){print "checked='checked'";} ?>></td>
                                                <td width="261"><fieldset>
                                                        <input type="text" name="bku[]" placeholder="" size="30" value="{{ $l->BKU }}">
                                                </fieldset></td>
                                            </tr>
                                            <input type="hidden" name="nengapi[]" value="{{$l->NENGAPI}}">
                                            <input type="hidden" name="nengetu" value="{{$Ymd_Y}}{{$Ymd_m}}">
                                            <input type="hidden" name="kubun" value="edit">
                                        @endforeach
                                        <input type="hidden" name="Now_time" value="{{$Now_time}}">
                                        </tbody>
                                    </table>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>
            </div></div>

    </form>




@endsection