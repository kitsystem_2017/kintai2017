<!-- 勤務入力画面　申請なし -->
@extends('app')

@section('content')
    <form action="{{URL('j/ka')}}" method="POST"  >
        <div class="container" style="align-items: center">

            <div class="row">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <!--<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->-->
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div style="width:auto;">
                    <div class="container-fluid" >
                        <div class="row-fluid ">
                            <div class="col-md-10" style="width:84%; align-items: left;"> <!--ｌｃｂ-->
<br><br><br>
                                <table class="table table-condensed table-responsive" style=" align-items: left; margin-bottom:0px;"  >
                                    <thead>
                                    <tr>
                                        <th>
                                         <span style="font-size:25px; align:left; margin-right: 20px">カレンダー</span>
                                        </th>
                                        <th>
                                            <div class="btn-group pull-right" >
                                                <span style="font-size:15px">年度:</span>  
                                                <select id="nendo" class="form-canvas" style="width :60px; height :30px">

                                                    <option value="{{$kotoshi}}"   @if($kotoshi == $nendo){{'selected'}}@endif  >{{$kotoshi}}</option>
                                                    <option value="{{$kotoshi+1}}" @if($kotoshi+1 == $nendo){{'selected'}}@endif>{{$kotoshi+1}}</option>
                                                    <option value="{{$kotoshi+2}}" @if($kotoshi+2 == $nendo){{'selected'}}@endif>{{$kotoshi+2}}</option>
                                                    <option value="{{$kotoshi+3}}" @if($kotoshi+3 == $nendo){{'selected'}}@endif>{{$kotoshi+3}}</option>
                                                    <option value="{{$kotoshi+4}}" @if($kotoshi+4 == $nendo){{'selected'}}@endif>{{$kotoshi+4}}</option>
                                                </select>
                                            </div>
                                        </th>
                                        <th><!--保存、確認ボタン-->
                                            <div style="align:left;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                <div class="btn-group pull-right" style="alignment: right;">
                                                    <button class="btn btn-md btn-warning", onclick="getYearMonth()">設定</button>
                                                </div>

                                                <input type="hidden" name="kubun" value="show">
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                               </table>
                                <!--<div style="position:absolute;overflow:auto">-->
                                <!--<h4><div   style=" height:auto; weight:auto;overflow:hidden;" id="datepicker" ></div></h4><!--カレンダー-->

                                <h4><div   style=" align-items: left;width: auto; height:auto;overflow:hidden;" id="datepicker" ></div></h4>

                             <div class="col-md-2">
                             </div>

                         </div>
                     </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div></div>

     </form>
     <script>
         $(document).ready(
        function()
        {
             $("#datepicker").datepicker(
             {

                 defaultDate : new Date(Date.parse(document.getElementById("nendo").value+"-04-01")),
                 numberOfMonths: [2,6],
                 selectOtherYears:true,
                 beforeShowDay: function(date) {
                     var result;
                     var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate(); //20160101
                     var hName = ktHolidayName(dd); //祝日の名前
                     if(hName != "") {
                         result = [true, "date-holiday", hName];
                     }
                     else
                     {
                         switch (date.getDay())
                         {
                             case 0: //日曜日
                                 result = [true, "date-holiday"];
                                 break;
                             case 6: //土曜日
                                 //result = [true];
                                 result = [true, "date-saturday"];
                                 break;
                             default:
                                 result = [true, "date-weekday"];
                                 break;
                         }
                     }
                     return result;
                 },
                 onSelect: function(dateText, inst) {
                     var MyDate = new Date(dateText);
                     var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();
                     window.location.pathname = '/j/ka/'+ Ymd+'/edit'; // 通常の遷移

                 }
             });
         });
     </script>




     <script>
         function getYearMonth()
         {
             var nendo=document.getElementById("nendo").value;
             location.replace(nendo);
         }
     </script>
 @endsection



