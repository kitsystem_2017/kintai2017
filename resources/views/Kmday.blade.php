@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">勤務表入力</div>

                    <div class="panel-body">

                    @foreach ($kmday as $k)
                            <hr>
                            <div class="page">
                                <h4>{{ $k->SHAINCD }}</h4>
                                <div class="content">
                                    <p>
                                        {{ $k->STIME }}
                                    </p>
                                    <p>
                                        {{ $k->ETIME }}
                                    </p>
                                    <p>
                                        {{ $k->KINMUTIME }}
                                    </p>
                                </div>
                            </div>
                            <a href="{{ URL('k/create') }}" class="btn btn-success">编辑</a>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection