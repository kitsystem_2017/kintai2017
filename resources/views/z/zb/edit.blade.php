﻿<!-- 勤務決定画面　申請あり　項目変更できない　disabled -->
@extends('app')

@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
    <form action="{{URL('z/zb')}}" method="POST">
    <div class="container">
        <input type="hidden" name="niki" value="{{$Ymd_niki}}">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                    <ul>
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </ul>
                </div>
            @endif


            <div>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-condensed table-responsive" style="margin-bottom:0px;" >
                                <thead>
                                <tr>
                                    <th>
                                        <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}月週間報告</span><!--タイトル-->

                                    </th>
                                </tr>
                                <tr>
                                    <th><!--保存、確認ボタン-->
                                        <div style="align:center;">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="btn-group pull-right" >
                                                <button type="submit" class="btn btn-sm btn-warning">保存</button>&nbsp;
                                             <!--  <button id="save-button" class="btn btn-sm btn-warning" type="submit">報告</button></a> -->
											　　　<button id="sinsei-button" class="btn btn-sm btn-warning" type="submit">報告</button>
	

                                                <!--<button id="save-button" class="btn btn-sm btn-warning" >申請</button>-->

                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;">
                                <thead>
                                <th width="200">報告先メールアドレス：</th>
                                <th width="150">TO:
                                          <!-- update 2017/7/31 takahashi style="width: 110px; height:30px"を追加 -->
                                    <select class="form-canvas" name="supervisor" style="width: 110px; height:30px"> 
                                        <option value="01" @if('01' == $supervisor1){{'selected'}}@endif>{{$arr_supervisor[0]}}</option>
                                        <option value="02" @if('02' == $supervisor1){{'selected'}}@endif>{{$arr_supervisor[1]}}</option>
                                    </select>
                                </th>
                                <th>CC:
                                           <!-- update 2017/7/31 takahashi style="width: 110px; height:30px"を追加 -->
                                    <select class="form-canvas" name="assistsupervisor" style="width: 110px; height:30px">
                                        <option value="01" @if('01' == $assistsupervisor1){{'selected'}}@endif>{{$arr_supervisor[0]}}</option>
                                        <option value="02" @if('02' == $assistsupervisor1){{'selected'}}@endif>{{$arr_supervisor[1]}}</option>
                                    </select>
                                </th>
                                </thead>
                            </table>
                                         <!-- update 2017/7/28 takahashi -->
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;width: 100%;border: solid #FF8000;border-top:0;">
                                <thead>
                                <td width="80" align="center">日付</td>
                                <td>作業内容</td>
                                </thead>

                                <tbody>
                                <tr>
                                    <td align="center" width="80">{{substr($date1,5)}}<br>(月)</td>
                                    <input type="hidden" name="date1" value="{{$date1}}">
                                    <td><textarea cols="119" name="naiyo1">{{$naiyo1}}</textarea></td>
                                    <input type="hidden" name="id1" value="{{$id1}}">
                                </tr>
                                <tr>
                                    <td align="center" width="80">{{substr($date2,5)}}<br>(火)</td>
                                    <input type="hidden" name="date2" value="{{$date2}}">
                                    <td><textarea cols="119" name="naiyo2">{{$naiyo2}}</textarea></td>
                                    <input type="hidden" name="id2" value="{{$id2}}">
                                </tr>
                                <tr>
                                    <td align="center" width="80">{{substr($date3,5)}}<br>(水)</td>
                                    <input type="hidden" name="date3" value="{{$date3}}">
                                    <td><textarea cols="119" name="naiyo3">{{$naiyo3}}</textarea></td>
                                    <input type="hidden" name="id3" value="{{$id3}}">
                                </tr>
                                <tr>
                                    <td align="center" width="80">{{substr($date4,5)}}<br>(木)</td>
                                    <input type="hidden" name="date4" value="{{$date4}}">
                                    <td><textarea cols="119" name="naiyo4">{{$naiyo4}}</textarea></td>
                                    <input type="hidden" name="id4" value="{{$id4}}">
                                </tr>
                                <tr>
                                    <td align="center">{{substr($date5,5)}}<br>(金)</td>
                                    <input type="hidden" name="date5" value="{{$date5}}">
                                    <td><textarea cols="119" name="naiyo5">{{$naiyo5}}</textarea></td>
                                    <input type="hidden" name="id5" value="{{$id5}}">
                                </tr>
                                <tr>
                                    <td align="center">{{substr($date6,5)}}<br>(土)</td>
                                    <input type="hidden" name="date6" value="{{$date6}}">
                                    <td><textarea cols="119" name="naiyo6">{{$naiyo6}}</textarea></td>
                                    <input type="hidden" name="id6" value="{{$id6}}">
                                </tr>
                                <tr>
                                    <td align="center">{{substr($date7,5)}}<br>(日)</td>
                                    <input type="hidden" name="date7" value="{{$date7}}">
                                    <td><textarea cols="119" name="naiyo7">{{$naiyo7}}</textarea></td>
                                    <input type="hidden" name="id7" value="{{$id7}}">
                                </tr>
                                </tbody>
                            </table>
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;border-top:0;">
                                <thead>
                                <td>週間総括：（1000文字以内）</td>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><textarea cols="125" name="report" style="height :200px">{{$report1}}</textarea></td>
                                    <input type="hidden" name="idR" value="{{$idR}}">
                                </tr>
                                </tbody>
                            </table>

                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;border-top:0;">
                                <thead>
                                <td>確認者のコメント：（1000文字以内）</td>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><textarea cols="125" disabled = "true" name="confirm1">{{$confirm1}}</textarea></td>
                                    <input type="hidden" name="idC" value="{{$idC}}">
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="col-md-2">
                            <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                            <br>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        // $(document).ready(function(){

            // Confirm.init('sm');
            // $('#save-button').unbind();
            // $('#save-button').bind('click', function(e) {
                // e.preventDefault();
                // Confirm.show('確認', '報告して宜しいですか？', {
                    // 'YES': {
                        // 'primary': true,
                        // 'callback': function() {
                            // var nikis= document.getElementsByName("niki");
                            // var niki=nikis[0].value;
                            // window.location.pathname='/z/zb/'+niki+'+'+1;

                        // }
                    // }
                // });
            // });
        // });
		
		 $(document).ready(function(){  // 申請ボタン

           // Confirm.init('sm');
            $('#sinsei-button').unbind();
            $('#sinsei-button').bind('click', function(e) {
                e.preventDefault();　
				
                var nikis= document.getElementsByName("niki");
                var niki=nikis[0].value;
                            
				ret = confirm('保存していないデータは削除されます。申請して宜しいですか?')
                if(ret == true){
						window.location.pathname='/z/zb/'+niki+'+'+1;
                       }
					
                    });
				
                });
		
        $(document).ready(function(){
            $("#datepicker").datepicker({
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
                    var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();
                    if(MyDate.getMonth() < 9){
                        Ymd = MyDate.getFullYear()+'-0'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();
                    }

                    window.location.pathname = '/z/zb/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });


    </script>
    </form>
@endsection