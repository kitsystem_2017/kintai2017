﻿<!-- 勤務決定画面　申請あり　項目変更できない　disabled -->
@extends('app')

@section('content')
    <form action="{{URL('z/zb')}}" method="POST">
    <div class="container">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <div>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-condensed table-responsive" style="margin-bottom:0px;" >
                                <thead>
                                <tr>
                                    <th>
                                        <span style="font-size:25px; align:left;">{{$Ymd_Y}}年{{$Ymd_M}}月週間報告</span><!--タイトル-->

                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table  class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;">
                                <thead>

                                <td width="200">報告先メールアドレス：</td>
                              <!-- update 2017/7/31 takahashi width:138 ⇒　150 -->
                                <th width="150">TO:
                                    <select class="form-canvas" disabled = "true" style="width: 110px; height:30px">
                                        <option value="01" @if('01' == $supervisor1){{'selected'}}@endif>{{$arr_supervisor[0]}}</option>
                                        <option value="02" @if('02' == $supervisor1){{'selected'}}@endif>{{$arr_supervisor[1]}}</option>
                                    </select>
                                </th>
                                <th>CC:
                                    <select class="form-canvas" disabled = "true" style="width: 110px; height:30px">
                                        <option value="01" @if('01' == $assistsupervisor1){{'selected'}}@endif>{{$arr_supervisor[0]}}</option>
                                        <option value="02" @if('02' == $assistsupervisor1){{'selected'}}@endif>{{$arr_supervisor[1]}}</option>
                                    </select>
                                </th>
                                </thead>
                            </table>








                            <table  class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;width: 100%;border: solid #FF8000;border-top:0;">
                                <tr>
                                <td width="80" align="center">日付</td>
                                <td>作業内容</td>
                                </tr>

                                <tbody>
                                <tr >
                                    <td align="center" width="80">{{substr($date1,5)}}<br>(月)</td>
                                    <input type="hidden" name="date1" value="{{$date1}}">
                                    <td>{{$naiyo1}}</td>
                                    <input type="hidden" name="id1" value="{{$id1}}">
                                </tr>
                                <tr>
                                    <td align="center" width="80">{{substr($date2,5)}}<br>(火)</td>
                                    <input type="hidden" name="date2" value="{{$date2}}">
                                    <td>{{$naiyo2}}</td>
                                    <input type="hidden" name="id2" value="{{$id2}}">
                                </tr>
                                <tr>
                                    <td align="center" width="80">{{substr($date3,5)}}<br>(水)</td>
                                    <input type="hidden" name="date3" value="{{$date3}}">
                                    <td>{{$naiyo3}}</td>
                                    <input type="hidden" name="id3" value="{{$id3}}">
                                </tr>
                                <tr>
                                    <td align="center" width="80">{{substr($date4,5)}}<br>(木)</td>
                                    <input type="hidden" name="date4" value="{{$date4}}">
                                    <td>{{$naiyo4}}</td>
                                    <input type="hidden" name="id4" value="{{$id4}}">
                                </tr>
                                <tr>
                                    <td align="center">{{substr($date5,5)}}<br>(金)</td>
                                    <input type="hidden" name="date5" value="{{$date5}}">
                                    <td>{{$naiyo5}}</td>
                                    <input type="hidden" name="id5" value="{{$id5}}">
                                </tr>
                                <tr>
                                    <td align="center">{{substr($date6,5)}}<br>(土)</td>
                                    <input type="hidden" name="date6" value="{{$date6}}">
                                    <td>{{$naiyo6}}</td>
                                    <input type="hidden" name="id6" value="{{$id6}}">
                                </tr>
                                <tr>
                                    <td align="center">{{substr($date7,5)}}<br>(日)</td>
                                    <input type="hidden" name="date7" value="{{$date7}}">
                                    <td>{{$naiyo7}}</td>
                                    <input type="hidden" name="id7" value="{{$id7}}">
                                </tr>
                                </tbody>
                            </table>
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;border-top:0;">
                                <thead>
                                <td>週間総括：（1000文字以内）</td>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><textarea cols="125" disabled = "true" style="height :200px">{{$report1}}</textarea></td>
                                    <input type="hidden" name="idR" value="{{$idR}}">
                                </tr>
                                </tbody>
                            </table>

                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;border-top:0;">
                                <thead>
                                <td>確認者のコメント：（1000文字以内）</td>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><textarea cols="125" disabled = "true" name="confirm1">{{$confirm1}}</textarea></td>
                                    <input type="hidden" name="idC" value="{{$idC}}">
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="col-md-2">
                            <h4><div id="datepicker" ></div></h4><!--カレンダー-->
                            <br>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker({
                beforeShowDay: function(date) {
                    var result;
                    var dd = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                    var hName = ktHolidayName(dd);
                    if(hName != "") {
                        result = [true, "date-holiday", hName];
                    } else {
                        switch (date.getDay()) {
                            case 0: //日曜日
                                result = [true, "date-holiday"];
                                break;
                            case 6: //土曜日
                                result = [true, "date-saturday"];
                                break;
                            default:
                                result = [true];
                                break;
                        }
                    }
                    return result;
                },
                onSelect: function(dateText, inst) {
                    var MyDate = new Date(dateText);
                    var Ymd = MyDate.getFullYear()+'-'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();
                    if(MyDate.getMonth() < 9){
                        Ymd = MyDate.getFullYear()+'-0'+(MyDate.getMonth()+1)+'-'+MyDate.getDate();
                    }

                    window.location.pathname = '/z/zb/'+ Ymd+'/edit'; // 通常の遷移

                }
            });
        });


    </script>
    </form>
@endsection