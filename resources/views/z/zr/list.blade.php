<!-- 勤務決定画面　申請あり　項目変更できない　disabled -->
@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-condensed table-responsive" style="margin-bottom:0px;" >
                                <thead>
                                <tr nowrap="" align="LEFT" valign="TOP">
                                    <td align="LEFT">
                                        <span>社員番号：{{$arruserlist[0]["id"]}}</span>&nbsp;&nbsp;&nbsp;
                                        <span>氏名：{{$arruserlist[0]["NAME"]}}</span>&nbsp;&nbsp;&nbsp;
                                        <span>所属：{{$arruserlist[0]["DIVISION"]}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <span style="font-size:25px; align:left; margin-right :400px">週間報告書一覧</span><!--タイトル-->
                                    </th>
                                    <th><!--年度-->
                                     　<div class="btn-group pull-right" > <!--   <br class="btn-group pull-right" > -->
                                            <span style="font-size:15px">年度:</span>
                                            <select id="nendo" class="form-canvas" style="width :60px; height :30px">
                                                <option value="">--</option>
                                                <option value="{{$kotoshi}}" @if($kotoshi == $nendo){{'selected'}}@endif>{{$kotoshi}}</option>
                                                <option value="{{$kotoshi-1}}" @if($kotoshi-1 == $nendo){{'selected'}}@endif>{{$kotoshi-1}}</option>
                                                <option value="{{$kotoshi-2}}" @if($kotoshi-2 == $nendo){{'selected'}}@endif>{{$kotoshi-2}}</option>
                                                <option value="{{$kotoshi-3}}" @if($kotoshi-3 == $nendo){{'selected'}}@endif>{{$kotoshi-3}}</option>
                                                <option value="{{$kotoshi-4}}" @if($kotoshi-4 == $nendo){{'selected'}}@endif>{{$kotoshi-4}}</option>
                                            </select>
                                            <span style="font-size:15px">月度:</span>
                                            <select id="getudo" class="form-canvas" style="width :60px; height :30px">
                                                <option value="">--</option>
                                                <option value="01" @if('01' == $getudo){{'selected'}}@endif>1月</option>
                                                <option value="02" @if('02' == $getudo){{'selected'}}@endif>2月</option>
                                                <option value="03" @if('03' == $getudo){{'selected'}}@endif>3月</option>
                                                <option value="04" @if('04' == $getudo){{'selected'}}@endif>4月</option>
                                                <option value="05" @if('05' == $getudo){{'selected'}}@endif>5月</option>
                                                <option value="06" @if('06' == $getudo){{'selected'}}@endif>6月</option>
                                                <option value="07" @if('07' == $getudo){{'selected'}}@endif>7月</option>
                                                <option value="08" @if('08' == $getudo){{'selected'}}@endif>8月</option>
                                                <option value="09" @if('09' == $getudo){{'selected'}}@endif>9月</option>
                                                <option value="10" @if('10' == $getudo){{'selected'}}@endif>10月</option>
                                                <option value="11" @if('11' == $getudo){{'selected'}}@endif>11月</option>
                                                <option value="12" @if('12' == $getudo){{'selected'}}@endif>12月</option>
                                            </select>
                                        </div>
                                    </th>
                                    <th>
                                        <button id="getYearMonth" class="btn btn-xs btn-warning" style="width: 80px;height :30px">表示</button>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-md-10">
                            <table class="table table-striped table-bordered table-condensed table-responsive" style="margin-bottom:0px;border: solid #FF8000;"><!--勤務表入力項目-->
                                <thead>
                                <tr>
                                    <th width="65" style="text-align:center; background: #EEEEEE;">年月</th>
                                    <th width="80" style="text-align:center; background: #EEEEEE;">申請日</th>
                                    <th width="85" style="text-align:center; background: #EEEEEE;">期間</th>
                                    <th width="75" style="text-align:center; background: #EEEEEE;">報告状態</th>
                                    <th width="75" style="text-align:center; background: #EEEEEE;">承認日</th>
                                    <th width="75" style="text-align:center; background: #EEEEEE;">承認者</th>
                                </tr>
                                </thead>
                                @if(count($arrzblist)>0)
                                    @foreach( $arrzblist as $key=>$value )
                                        <tr>
                                            <td width="65"><span><a href="/z/zb/{{substr($value["SHINSEIDATE"],0,4)."-".substr($value["SHINSEIDATE"],4,2)."-".substr($value["SHINSEIDATE"],6,2)}}+0">{{$value["SHINSEIM"]}}</a></span></td>
                                            <td width="80"><span>{{$value["SHINSEIDATE"]}}</span></td>
                                            <td width="85"><span>{{$value["ZBDATE"]}}</span></td>
                                            <td width="75"><span ><font color="red">{{$value["SHOUNINSTATUS"]}}</font></span></td>
                                            <td width="75"><span>{{$value["SHOUNINDATE"]}}</span></td>
                                            <td width="75"><span>{{$value["SHOUNINSHANM"]}}</span></td>
                                        </tr>
                                    @endforeach
                                @endif

                            </table>
                            <div data-spy="scroll" data-target="#navbarExample" data-offset="50" class="scrollspy-example"><!--スクロール-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
    <script>
        $(document).ready(function(){
            $('#getYearMonth').unbind();
            $('#getYearMonth').bind('click', function(e) {
                e.preventDefault();
                var nendo=document.getElementById("nendo").value;
                var getudo=document.getElementById("getudo").value;
                window.location.pathname = '/z/zr/'+nendo+ getudo;
            });
        });
    </script>
@endsection