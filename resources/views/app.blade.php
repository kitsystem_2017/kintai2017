<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/jquery-ui.css') }}">

    <script src="{{ asset('/jquery.js') }}" km></script>
    <script src="{{ asset('/jquery.min.js') }}"></script>
    <script src="{{ asset('/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/jquery.ui.datepicker-ja.min.js')}}"></script>
    <script src="{{ asset('/HolidayChk1.js') }}"></script>

    <!-- Fonts -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css')}}">
    <!-- Jquery -->
    <script src="{{ asset('/jquery-1.11.3.min.js')}}"></script>
    <!-- Confirm Modal -->
    <script type="text/javascript" src="{{ asset('/confirm.js')}}"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>

    <script src="{{ asset('/mpdf60')}}"></script>
    <![endif]-->

    <style>
        <!--
        .date-holiday .ui-state-default {
            background-image: none;
            background-color: #fff888;
        }

        .date-saturday .ui-state-default {
            background-image: none;
            background-color: #BBCCDD;
        }

        h4 {
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
            font-size: 12px;
        }

        .home_img {
            width: 61px;
            height: 61px;
        }

        .scrollspy-example {
            height: 350px;
            overflow: auto;
            position: relative;
        }

        body {
            background-color: #aecef1;
        }

        -->

    </style>

</head>
<body>
<?php
    if(session_status()==1){
        session_start();
    }
    $permissions = null;
    $user = Auth::user();
    if($user){
        if(isset($_SESSION['userinfo'])) {
        $userinfo = $_SESSION["userinfo"];
        $permissions = is_null($userinfo) ? null : $userinfo["pid"];
            $division = is_null($userinfo) ? null : $userinfo["UD"];
            }
    }else{
        $permissions = null;
    }
?>

<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/" style="padding:0px 15px;">
            <img class="home_img" alt="" src="{{ asset('img/logo02.jpg') }}" style="width:180px;height:50px;"></a>
    </div>

    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <!--
            <li class="active"><a href="/">ホーム</a></li>
            -->
            @if(!is_null($permissions))

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        勤務管理
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/k/km">勤務表入力</a></li>
                        <li><a href="/k/kr/show"> 勤務表一覧</a></li>
                    </ul>
                </li>
            @endif
            @if(!is_null($permissions))
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    費用申請
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/h/hk">定期券、交通費申請</a></li>
                    <li><a href="/h/hg">費用精算申請</a></li>
                    <li><a href="/h/hl">仮払申請</a></li>
                    <li><a href="/h/hr/show">各種精算申請一覧</a></li>
                </ul>
            </li>
            @endif
            @if(!is_null($permissions))
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    週間報告
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                     <li><a href="/z/zb">週間報告書</a></li>
                    <li><a href="/z/zr/show">週間報告書一覧</a></li>
                </ul>
            </li>
            @endif
            @if($permissions==1||$permissions==2)
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    各種承認
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/s/sk">勤務表承認</a></li>
                    <li><a href="/s/sz">週間報告書確認</a></li>
                    <li><a href="/s/sh">費用承認</a></li>
                </ul>
            </li>
            @endif
            @if($permissions==1)
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    情報設定
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/j/me">社員情報設定</a></li>
                    <li><a href="/j/bu">部門情報設定</a></li>
                    <li><a href="/j/ka">カレンダー設定</a></li>
                </ul>
            </li>
            @endif
        </ul>
        <ul class="nav navbar-nav navbar-right" style="margin-right:60px">
            @if (Auth::guest())
                <li><a href="{{ url('/auth/login') }}">ログイン</a></li>
            <!--
                <li><a href="{{ url('/auth/register') }}">アカウントを作成</a></li>
                -->
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false"> {{$division}} ： {{ Auth::user()->name }} <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/p/reset') }}">パスワード 変更</a></li>
                        <li><a href="{{ url('/auth/logout') }}">ログアウト</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>
</body>
</html>

@yield('content')

        <!-- Scripts
	<script src="http://cdn.bootcss.com/jquery/2.1.3/jquery.min.js"></script> -->
<script src="http://cdn.bootcss.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
